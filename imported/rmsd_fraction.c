/*
  Program RMS   written by Scott M. Le Grand
  Modified by Arne Elofsson
  Copyright 1992, Scott M. Le Grand
  */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h> 
#include <sys/types.h>
#include <sys/times.h>
#include <sys/param.h>
#include <sys/time.h> 

#define	MAXATMS		5000		/* Maximum allowable atoms */
#define PI		3.14159265	/* Useful constant */

#define TRUE		1		/* Boolean definitions */
#define FALSE		0

typedef struct {
    struct {
	double x,y,z;		/* Atomic coordinates */
	double rms;		/* RMS deviation */
	char residue[8];	/* PDB info for output */
	char name[8];
	int number;
	int owner;
    } atm[MAXATMS];
    double xcen,ycen,zcen;
    int	atoms;			/* Current # of atoms */
    char	filename[800];		/* filename to read molecule from */
} molecule;

molecule	m[2];		/* Molecules to be compared */

double		superimpose_molecules();
double		rms;			/* final RMS error */
int		verbose_flag;		/* Verbose output flag */
int             output_flag;            /* Output flag */
char            output_name[80];            /* Output name */
int             output_flag_ca;            /* Output flag */
char            output_name_ca[80];            /* Output name */
int             read_molecules();
int             read_molecules2();
/*int             temp;*/

main(argc,argv)		/* Main routine */
    int argc;
    char *argv[];
{
 int	files;	/* Number of files parsed */
 int	i;	/* Counter variable */
 double	s[3][3];	/* Final transformation matrix */
 /* Initialize */
 files=0;
 verbose_flag=FALSE;
 output_flag=FALSE;
 /* Parse command line for PDB files and options */
 i=1;
/*    temp=1; */
 while (i<argc)
  {
   if (strcmp(argv[i],"-ca")==0)
    {
     strcpy(output_name_ca,argv[i+1]);
     output_flag_ca=TRUE;
     i++;
    }
   else if (strcmp(argv[i],"-o")==0)
     {
       strcpy(output_name,argv[i+1]);
       output_flag=TRUE;
       i++;
     }
   else if (strcmp(argv[i],"-v")==0)
     {
       verbose_flag=TRUE;
     }
   else
     {
       strcpy(m[files].filename,argv[i]);
       files++;
     }
   i++;
  }
 if (files==2)	/* Valid input line? */
   {
     if (read_molecules()==0)
       {
	 rms=superimpose_molecules(&m[0],&m[1],s);
	 output_results();
	 if (output_flag)
	   output_file(output_name);
       }
     //     printf("test0\n");
     if (read_molecules2()==0)
       {
	 //	 printf("test1\n");
	 rms=superimpose_molecules(&m[0],&m[1],s);
	 //	 printf("test2\n");
	 output_results2();
	 //	 printf("test3\n");
	 output_dme(&m[0],&m[1]);
	 //	 printf("test4\n");
	 if (output_flag_ca)
	   {
	     read_second_molecule(&m[0],&m[1],s);
	     output_file(output_name_ca);
	   }
       }
   }
 else
   printf("Usage: rmsd_fraction file1 file2\n");
}



int read_molecules(temp)	/* Reads in molecules to be superimposed */
    int temp;
{
  int	i,j,k,atoms;	/* Counter variables */
  char	buff[512];	/* Input string */
  char	junk[512];	/* Throwaway string */
  char	line_flag[512];	/* PDB file line mode */
  char	residue[8];	/* PDB atom info for output */
  char	name[8];
  char  tmpname[2];
  int	owner;
  int	number;
  double	x,y,z;		/* Temporary coordinates values */
  FILE *fp;
    
  for (i=0;i<2;i++)
    {
      fp=fopen(m[i].filename,"r");	/* Does file exist? */
      if (fp!=NULL)	/* If yes, read in coordinates */
	{
	  /* Initialize things */
	  m[i].xcen=m[i].ycen=m[i].zcen=0;
	  atoms=0;
	  while(fgets(buff,255,fp)!=NULL)
	    {
	      buff[26]=' ';
	      sscanf(buff,"%s %d %s %s",line_flag,&number,name,residue);
	      if (strcmp("ATOM",line_flag)==0)	/* Is it an ATOM entry? */
		{
		  tmpname[0]=name[0];
		  tmpname[1]=0;
		  if ( strcmp("H",tmpname)!=0)	   /*  Is it an H atom ? */
		    {
		      sscanf(&buff[22],"%d %lf %lf %lf",&owner,&x,&y,&z);
		      m[i].atm[atoms].x=x;
		      m[i].atm[atoms].y=y;
		      m[i].atm[atoms].z=z;
		      m[i].atm[atoms].owner=owner;
		      m[i].atm[atoms].number=number;
		      strcpy(m[i].atm[atoms].name,name);
		      strcpy(m[i].atm[atoms].residue,residue);
		      m[i].xcen+=x;
		      m[i].ycen+=y;
		      m[i].zcen+=z;
		      atoms++;
		    }
		}
	    }
	  m[i].atoms=atoms;
	  fclose(fp);
	  if (atoms!=m[0].atoms)		/* Are file sizes indentical? */
	    {
	      printf("Inconsistent number of atoms in file %s %d %d\n",m[i].filename,atoms, m[0].atoms);
	      return(1);
	    }
	  /* Now center molecule */
	  m[i].xcen/=(double)atoms;
	  m[i].ycen/=(double)atoms;
	  m[i].zcen/=(double)atoms;
	  for (j=0;j<atoms;j++)
	    {
	      m[i].atm[j].x-=m[i].xcen;
	      m[i].atm[j].y-=m[i].ycen;
	      m[i].atm[j].z-=m[i].zcen;
	    }
	}
      else
	{
	  printf("Couldn't open file %s\n",m[i].filename);
	  exit(0);
	}
    }
  return(0);
}
int read_molecules2(temp)	/* Reads in molecules to be superimposed */
    int temp;
{
  int	i,j,k,atoms;	/* Counter variables */
  char	buff[512];	/* Input string */
  char	junk[512];	/* Throwaway string */
  char	line_flag[512];	/* PDB file line mode */
  char	residue[8];	/* PDB atom info for output */
  char	name[8];
  int	owner;
  int	number;
  double	x,y,z;		/* Temporary coordinates values */
  FILE *fp;
    
  for (i=0;i<2;i++)
    {
      fp=fopen(m[i].filename,"r");	/* Does file exist? */
      if (fp!=NULL)	/* If yes, read in coordinates */
	{
	  /* Initialize things */
	  m[i].xcen=m[i].ycen=m[i].zcen=0;
	  atoms=0;
	  while(fgets(buff,255,fp)!=NULL)
	    {
	      buff[26]=' ';
	      sscanf(buff,"%s %d %s %s",line_flag,&number,name,residue);
	      if (strcmp("ATOM",line_flag)==0)	/* Is it an ATOM entry? */
		{
		  if (strcmp("CA",name)==0)	   /*  Is it an CA atom ? */
		    {
		      sscanf(&buff[22],"%d %lf %lf %lf",&owner,&x,&y,&z);
		      m[i].atm[atoms].x=x;
		      m[i].atm[atoms].y=y;
		      m[i].atm[atoms].z=z;
		      m[i].atm[atoms].owner=owner;
		      m[i].atm[atoms].number=number;
		      strcpy(m[i].atm[atoms].name,name);
		      strcpy(m[i].atm[atoms].residue,residue);
		      m[i].xcen+=x;
		      m[i].ycen+=y;
		      m[i].zcen+=z;
		      atoms++;
		    }
		}
	    }
	  m[i].atoms=atoms;
	  fclose(fp);
	  if (atoms!=m[0].atoms)		/* Are file sizes indentical? */
	    {
	      printf("Inconsistent number of atoms in file %s\n",m[i].filename);
	      return(1);
	    }
	  /* Now center molecule */
	  m[i].xcen/=(double)atoms;
	  m[i].ycen/=(double)atoms;
	  m[i].zcen/=(double)atoms;
	  for (j=0;j<atoms;j++)
	    {
	      m[i].atm[j].x-=m[i].xcen;
	      m[i].atm[j].y-=m[i].ycen;
	      m[i].atm[j].z-=m[i].zcen;
	    }
	}
      else
	{
	  printf("Couldn't open file %s\n",m[i].filename);
	  exit(0);
	}
    }
  return(0);
}
multiply_matrix(a,b,c)  /* computes C=AB */
    double a[3][3],b[3][3],c[3][3];
{
    int i,j,k;
    for (i=0;i<3;i++)
	for (j=0;j<3;j++)
	    {
		c[i][j]=0.0;
		for (k=0;k<3;k++)
		    c[i][j]+=a[i][k]*b[k][j];
	    }
}
copy_matrix(f,t) /* copy matrix f into matrix t */
    double f[3][3],t[3][3];
{
    int i,j;
    for (i=0;i<3;i++)
	for (j=0;j<3;j++)
	    t[i][j]=f[i][j];
}
transpose_matrix(m) /* Transpose a 3x3 matrix */
    double m[3][3];
{
    double dummy;
    dummy=m[0][1]; m[0][1]=m[1][0]; m[1][0]=dummy;
    dummy=m[0][2]; m[0][2]=m[2][0]; m[2][0]=dummy;
    dummy=m[1][2]; m[1][2]=m[2][1]; m[2][1]=dummy;
}
double superimpose_molecules(m1,m2,s)	/* Find RMS superimposition of m1 on m2 */
    molecule 	*m1,*m2;		/* Molecules to be superimposed */
    double 		s[3][3];	/* Final transformation matrix */
{
    int 		i,j,k;		/* Counter variables */
    double		u[3][3];	/* direct product matrix */
    double 		t[3][3];	/* Temporary storage matrix */
    double		ma[3][3];	/* x axis rotation matrix */
    double		mb[3][3];	/* y axis rotation matrix */
    double		mg[3][3];	/* z axis rotation matrix */
    double 		*d1,*d2;	/* usefule pointers */
    double 		error;		/* Final superimposition error */
    double		alpha=0.0; 	/* Angle of rotation around x axis */
    double		beta=0.0;	/* Angle of rotation around y axis */
    double		gamma=0.0;	/* Angle of rotation around z axis */
    double		x,y,z;		/* Temporary coordinate variables */
    for (i=0;i<3;i++)  /* Initialize matrices */
	for (j=0;j<3;j++)
	    s[i][j]=u[i][j]=0.0;
    s[0][0]=s[1][1]=s[2][2]=1.0;  /* Initialize S matrix to I */
    for (i=0;i<3;i++)  /* Initialize rotation matrices to I */
	for (j=0;j<3;j++)
	    ma[i][j]=mb[i][j]=mg[i][j]=s[i][j];
    for (i=0;i<m1->atoms;i++)  /* Construct U matrix */
	{
	    d1= &(m1->atm[i].x);
	    for (j=0;j<3;j++)
		{
		    d2= &(m2->atm[i].x);
		    for (k=0;k<3;k++)
			{
			    u[j][k]+=(*d1)*(*d2);
			    d2++;
			}
		    d1++;
		}
	}
    
    do
	{
	    error=0.0;
	    /* Calculate x axis rotation */
	    alpha=atan((u[2][1]-u[1][2])/(u[1][1]+u[2][2]));
	    /* Insure we are heading for a minimum, not a maximum */
	    if (cos(alpha)*(u[1][1]+u[2][2])+sin(alpha)*(u[2][1]-u[1][2])<0.0)
		alpha+=PI;
	    ma[1][1]=ma[2][2]=cos(alpha);
	    ma[2][1]=sin(alpha); ma[1][2]= -ma[2][1];
	    transpose_matrix(ma);
	    multiply_matrix(u,ma,t);
	    transpose_matrix(ma);
	    copy_matrix(t,u);
	    multiply_matrix(ma,s,t);
	    copy_matrix(t,s);
	    /* Calculate y axis rotation */
	    beta=atan((u[0][2]-u[2][0])/(u[0][0]+u[2][2]));
	    /* Insure we are heading for a minimum, not a maximum */
	    if (cos(beta)*(u[0][0]+u[2][2])+sin(beta)*(u[0][2]-u[2][0])<0.0)
		beta+=PI;
	    mb[0][0]=mb[2][2]=cos(beta);
	    mb[0][2]=sin(beta); mb[2][0]= -mb[0][2];
	    transpose_matrix(mb); 
	    multiply_matrix(u,mb,t);
	    transpose_matrix(mb);
	    copy_matrix(t,u);
	    multiply_matrix(mb,s,t);
	    copy_matrix(t,s); 
	    /* Calculate z axis rotation */
	    gamma=atan((u[1][0]-u[0][1])/(u[0][0]+u[1][1]));
	    /* Insure we are heading for a minimum, not a maximum */
	    if (cos(gamma)*(u[0][0]+u[1][1])+sin(gamma)*(u[1][0]-u[0][1])<0.0)
		gamma+=PI;
	    mg[0][0]=mg[1][1]=cos(gamma);
	    mg[1][0]=sin(gamma); mg[0][1]= -mg[1][0];
	    transpose_matrix(mg);
	    multiply_matrix(u,mg,t);
	    transpose_matrix(mg);
	    copy_matrix(t,u);
	    multiply_matrix(mg,s,t);
	    copy_matrix(t,s);
	    error=fabs(alpha)+fabs(beta)+fabs(gamma); 
	}
    while (error>0.0001);	/* Is error low enough to stop? */
    /* Now calculate final RMS superimposition */
    error=0.0;
    for (i=0;i<m1->atoms;i++)
	{

	  x=s[0][0]*m2->atm[i].x+s[0][1]*m2->atm[i].y+s[0][2]*m2->atm[i].z;
	  y=s[1][0]*m2->atm[i].x+s[1][1]*m2->atm[i].y+s[1][2]*m2->atm[i].z;
	  z=s[2][0]*m2->atm[i].x+s[2][1]*m2->atm[i].y+s[2][2]*m2->atm[i].z;
	  m2->atm[i].x=x;
	  m2->atm[i].y=y;
	  m2->atm[i].z=z;
	  x=m1->atm[i].x-x;
	  y=m1->atm[i].y-y;
	  z=m1->atm[i].z-z;
	  m1->atm[i].rms=x*x+y*y+z*z;
	  error+=m1->atm[i].rms;
	  
	}
    error/=(double)(m1->atoms);
    return(sqrt(error));
}
output_results()
{
    int	i,j;		/* Counter variable */
    double	sum;		/* Average RMS counter by residue */
    double	resatoms;	/* Number of atoms in each residue */
    char	name[8];	/* Temporary residue name variable */
    printf("Overall average RMS deviation is %lf Angstroms.\n",rms);
	    if (verbose_flag)
		{
		    /* First output RMS deviations by atom */
		    printf("\n");
		    printf("RMS deviations by atom\n");
		    printf("Atom \t Name \t  Owner \t RMS deviation\n");
		    for (i=0;i<m[0].atoms;i++)
			printf("%d \t %s \t %3s %4d \t %8.4lf\n",
			       m[0].atm[i].number,m[0].atm[i].name,
			       m[0].atm[i].residue,m[0].atm[i].owner,
			       sqrt(m[0].atm[i].rms));
		    /* Now output average RMS deviations by residue */
		    printf("\n");
		    printf("Average RMS deviations by residue\n");
		    printf("Residue \t RMS deviation\n"); 
		    j= -1;
		    for (i=0;i<=m[0].atoms;i++)
			{
			    if (m[0].atm[i].owner!=j || i==m[0].atoms)
				{
				    if (j!=-1)
					printf("%3s %4d\t %8.4lf\n",name,j,sqrt(sum/resatoms));
				    sum=0;
				    resatoms=0;
				    j=m[0].atm[i].owner;
				    strcpy(name,m[0].atm[i].residue);		
				}
			    sum+=m[0].atm[i].rms;
			    resatoms++;
			}
		}
}
output_results2()
{
    int	i,j;		/* Counter variable */
    double	sum;		/* Average RMS counter by residue */
    double	resatoms;	/* Number of atoms in each residue */
    char	name[8];	/* Temporary residue name variable */
    printf("Overall average RMS deviation for CA is %lf Angstroms.\n",rms);
    if (verbose_flag)
	{
	    /* First output RMS deviations by atom */
	    printf("\n");
	    printf("RMS deviations by CA atom\n");
	    printf("Atom \t Name \t  Owner \t RMS deviation\n");
	    for (i=0;i<m[0].atoms;i++)
		printf("%d \t %s \t %3s %4d \t %8.4lf\n",
		       m[0].atm[i].number,m[0].atm[i].name,
		       m[0].atm[i].residue,m[0].atm[i].owner,
		       sqrt(m[0].atm[i].rms));
	    
	}
}
output_dme(m1,m2)
    molecule 	*m1,*m2;	 /* Molecules to be superimposed */
{
    double	sum;		/* Average RMS counter by residue */
    double	resatoms;	/* Number of atoms in each residue */
    double      dist1,x1,y1,z1;
    double      dist2,x2,y2,z2;
    char	name[8];	/* Temporary residue name variable */
    int         i,j,count;
    
    sum=0.0;
    count=0;
    for (i=0;i<m1->atoms;i++)
	{
	    for (j=i+1;i<m1->atoms;i++)
		{
		    x1= ( (m1->atm[i].x)-(m1->atm[j].x))*( (m1->atm[i].x)-(m1->atm[j].x));
		    y1= ( (m1->atm[i].y)-(m1->atm[j].y))*( (m1->atm[i].y)-(m1->atm[j].y));
		    z1= ( (m1->atm[i].z)-(m1->atm[j].z))*( (m1->atm[i].z)-(m1->atm[j].z));
		    dist1=sqrt(x1+y1+z1);
		    
		    x2= ( (m2->atm[i].x)-(m2->atm[j].x))*( (m2->atm[i].x)-(m2->atm[j].x));
		    y2= ( (m2->atm[i].y)-(m2->atm[j].y))*( (m2->atm[i].y)-(m2->atm[j].y));
		    z2= ( (m2->atm[i].z)-(m2->atm[j].z))*( (m2->atm[i].z)-(m2->atm[j].z));
		    dist2=sqrt(x2+y2+z2);
		}
	    sum+=(dist1-dist2)*(dist1-dist2);
	    count+=1;
	}
    {
	sum/=(double)(count);
    }
    
    printf("Overall average DME deviation for CA is %lf Angstroms.\n",sqrt(sum));
}
output_file(filename)
    char filename[80];
{
    int i;
    FILE *fp;
    fp=fopen(filename,"w");
    if (fp!=NULL)
	{
	    for (i=0;i<m[1].atoms;i++)
	      {
		fprintf(fp,"ATOM   %4d  %-4s%3s  %4d     %7.3lf %7.3lf %7.3lf  1.0 %5.2lf      RMSD\n",
			m[1].atm[i].number,m[1].atm[i].name,
			m[1].atm[i].residue,m[1].atm[i].owner,
			m[1].atm[i].x,m[1].atm[i].y,m[1].atm[i].z,
			sqrt(m[0].atm[i].rms));
	      }
	    fprintf(fp,"END \n");
/*	    printf("test %d  %d \n",m[1].atoms,m[0].atoms);*/
	    for (i=0;i<m[0].atoms;i++)
	      {
		fprintf(fp,"ATOM   %4d  %-4s%3s  %4d     %7.3lf %7.3lf %7.3lf  1.0 %5.2lf      RMSD\n",
			m[0].atm[i].number,m[0].atm[i].name,
			m[0].atm[i].residue,m[0].atm[i].owner,
			m[0].atm[i].x,m[0].atm[i].y,m[0].atm[i].z,
			sqrt(m[0].atm[i].rms));
	      }
	    fprintf(fp,"END \n");
	}
    fclose(fp);
}

read_second_molecule(m1,m2,s)	/* Reads in molecules to be superimposed */
  molecule 	*m1,*m2;		/* Molecules to be superimposed */
  double	s[3][3];	/* Final transformation matrix */
{
    int	i,j,k,atoms;	/* Counter variables */
    char	buff[512];	/* Input string */
    char	junk[512];	/* Throwaway string */
    char	line_flag[512];	/* PDB file line mode */
    char	residue[8];	/* PDB atom info for output */
    char	name[8];
    double		u[3][3];	/* direct product matrix */
    double 		t[3][3];	/* Temporary storage matrix */
    double		ma[3][3];	/* x axis rotation matrix */
    double		mb[3][3];	/* y axis rotation matrix */
    double		mg[3][3];	/* z axis rotation matrix */
    double 		*d1,*d2;	/* usefule pointers */
    double 		error;		/* Final superimposition error */
    double		alpha=0.0; 	/* Angle of rotation around x axis */
    double		beta=0.0;	/* Angle of rotation around y axis */
    double		gamma=0.0;	/* Angle of rotation around z axis */
    int	owner;
    int	number;
    double	x,y,z;		/* Temporary coordinates values */
    FILE *fp;
    
    for (i=0;i<2;i++)
	{
	  fp=fopen(m[i].filename,"r");	/* Does file exist? */
	    if (fp!=NULL)	/* If yes, read in coordinates */
		{
		  /* Initialize things */
		  m[i].xcen=m[i].ycen=m[i].zcen=0;
		  atoms=0;
		  while(fgets(buff,255,fp)!=NULL)
		    {
		      sscanf(buff,"%s %d %s %s",line_flag,&number,name,residue);
		      if (strcmp("ATOM",line_flag)==0)	/* Is it an ATOM entry? */
			{
			  sscanf(&buff[22],"%d %lf %lf %lf",&owner,&x,&y,&z);
			  m[i].atm[atoms].x=x;
			  m[i].atm[atoms].y=y;
			  m[i].atm[atoms].z=z;
			  m[i].atm[atoms].owner=owner;
			  m[i].atm[atoms].number=number;
			  strcpy(m[i].atm[atoms].name,name);
			  strcpy(m[i].atm[atoms].residue,residue);
			  m[i].xcen+=x;
			  m[i].ycen+=y;
			  m[i].zcen+=z;
			  atoms++;
			}
		    }
		  m[i].atoms=atoms;
		  fclose(fp);
		  /* Now center molecule */
		  m[i].xcen/=(double)atoms;
		  m[i].ycen/=(double)atoms;
		  m[i].zcen/=(double)atoms;
		  for (j=0;j<atoms;j++)
		    {
		      m[i].atm[j].x-=m[i].xcen;
		      m[i].atm[j].y-=m[i].ycen;
		      m[i].atm[j].z-=m[i].zcen;
		    }
		}
	    else
		{
		  printf("Couldn't open file %s\n",m[i].filename);
		  exit(0);
		}
	}
    for (i=0;i<m1->atoms;i++)
	{

	    x=s[0][0]*m2->atm[i].x+s[0][1]*m2->atm[i].y+s[0][2]*m2->atm[i].z;
	    y=s[1][0]*m2->atm[i].x+s[1][1]*m2->atm[i].y+s[1][2]*m2->atm[i].z;
	    z=s[2][0]*m2->atm[i].x+s[2][1]*m2->atm[i].y+s[2][2]*m2->atm[i].z;
	    m2->atm[i].x=x;
	    m2->atm[i].y=y;
	    m2->atm[i].z=z;
	    x=m1->atm[i].x-x;
	    y=m1->atm[i].y-y;
	    z=m1->atm[i].z-z;
	    m1->atm[i].rms=x*x+y*y+z*z;
	}
    return(0);
}
