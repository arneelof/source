
	Real 		x(5), A1(5), inv(5,5), Err, Gauss, d1, a(6)
	Real		ncount, nGood, cutoff
	Character 	Filein*4, Filein2*60
	integer		Counter, nr

	counter=0

	read(5,900) Filein
	open(1,file='ais_tot.out', status='old')
	open(2,file=filein//'.ais',
     %	     status='old')
	open(3,file=filein//'.err',status='new')
	open(4,file=filein//'.ter',status='new')
	open(7,file='fort.2',status='old')

	read(7,910) nr, a(1), d1, a(2), a(3), a(4), a(5)

 	read(1,905) ((inv(i,j),j=1,5),i=1,5)
	
 10	read(2,910,err=10,end=20) nr, x(1), d1, x(2), x(3), x(4), x(5)

	x(1) = x(1) - a(1)
	x(2) = x(2) - a(2)
	x(3) = x(3) - a(3)
	x(4) = x(4) - a(4)
	x(5) = x(5) - a(5)

	  Err = 0.
	A1(1) = 0.
	A1(2) = 0.
	A1(3) = 0.
	A1(4) = 0.
	A1(5) = 0.

	do i = 1,5
	 do j = 1,5
	  A1(i) = x(j)*inv(j,i) + A1(i)
	 end do
	end do

	do i = 1,5
	 Err = A1(i)*x(i) + Err
	end do

	gauss = (1/(sqrt(3.141459)))*exp(-Err)
	if (err.lt.6.0) ngood = ngood + 1.
	ncount = ncount+1.
	
	write(3,915) nr,err,gauss
 	goto 10

 20	continue

	write(4,920) ngood/ncount, filein

 900	format(a60)
 905	format(5f10.3)
 910	format(i10,',',6(f10.3,','))
 915	format(i10,',',2(f10.4,','))
 920	format(f10.5,' ',a60)

	end

