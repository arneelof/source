(* PROTEIN DATA BANK SOURCE CODE: DSSP VERSION JUNE 1983                *)      
(* LANGUAGE: STANDARD PASCAL WITH 128 CHARACTER ASCII SET               *)      
(* AUTHORS: WOLFGANG KABSCH AND CHRISTIAN SANDER, MAX PLANCK INSTITUT           
   FUER MEDIZINISCHE FORSCHUNG, JAHNSTR. 29, 6900 HEIDELBERG, GERMANY           
   TELEPHONE: 49-6221-486275/276  TELEX: 461505 MPIMF D                         
   DO REPORT ERRORS IF YOU FIND ANY.                                            
   REFERENCE: KABSCH,W. AND SANDER,C. (1983) BIOPOLYMERS 22, 2577-2637. *)      
(*----------------------------------------------------------------------*)      
(* DEFINES SECONDARY STRUCTURE AND SOLVENT EXPOSURE OF PROTEINS FROM            
   ATOMIC COORDINATES AS GIVEN BY THE BROOKHAVEN PROTEIN DATA BANK.     *)      
(*----------------------------------------------------------------------*)      
(* PROGRAM INSTALLATION GUIDE. *)                                               
(* (1) THE PROGRAM REQUIRES THE FULL STANDARD ASCII 128 CHARACTER SET,          
       IN PARTICULAR LOWER CASE LETTERS 'abcdefg....'                           
   (2) STANDARD PASCAL MAY NOT RECOGNIZE REAL NUMBERS SUCH AS .1, +.1,          
       -.1 ON INPUT. CHANGE TO 0.1,+0.1,-0.1.                                   
   (3) THE NON-STANDARD PROCEDURE 'DATE' RETURNS THE CURRENT DAY, MONTH,        
       AND YEAR. REPLACE BY CORRESPONDING PROCEDURE FROM YOUR PASCAL            
       IMPLEMENTATION OR DELETE.                                        *)      
(*----------------------------------------------------------------------*)      
(* INPUT/OUTPUT FILES. *)                                                       
(* INPUT:   DEFAULT  INPUT UNIT, E.G. YOUR TERMINAL                             
   OUTPUT:  DEFAULT OUTPUT UNIT, E.G. YOUR TERMINAL                             
            USED FOR RUN-TIME MESSAGES. WARNINGS AND ERRORS LOOK                
            LIKE THIS: !!! TEXT !!!                                             
   TAPEIN:  FILE WITH PROTEIN DATA BANK COORDINATES, E.G. PDB3PTI.COO           
   TAPEOUT: DSSP OUTPUT OF LINE LENGTH 128, E.G. LINE PRINTER          *)       
(*----------------------------------------------------------------------*)      
(* DESCRIPTION OF OUTPUT ON FILE TAPEOUT. *)                                    
(* A SAMPLE OUTPUT FOR TRYPSIN INHIBITOR (DATA SET 3PTI) IS APPENDED            
   AS A COMMENT AT THE END OF THE PROGRAM. EACH ORIGINAL OUTPUT                 
   LINE OF 128 PRINT POSITIONS IS SPLIT INTO TWO LINES (80 AND 48 LONG)         
   TO ALLOW DISTRIBUTION AS 80 CHARACTER CARD IMAGE.                            
   TO UNDERSTAND THE OUTPUT, SEE ABOVE BIOPOLYMERS ARTICLE.                     
   IN ADDITION NOTE:                                                            
   HISTOGRAMS - E.G. 2 UNDER COLUMN '8' IN LINE 'RESIDUES PER ALPHA             
            HELIX' MEANS: THERE ARE 2 ALPHA HELICES OF LENGTH  8                
            RESIDUES IN THIS DATA SET.                                          
   #  RESIDUE AA STRUCTURE BP1 BP2 ACC ..ETC..FOR EACH RESIDUE I:               
   #  RESIDUE - TWO COLUMNS OF RESIDUE NUMBERS. FIRST COLUMN IS DSSP'S          
            SEQUENTIAL RESIDUE NUMBER, STARTING AT THE FIRST                    
            RESIDUE ACTUALLY IN THE DATA SET AND INCLUDING CHAIN BREAKS;        
            THIS NUMBER IS USED TO REFER TO RESIDUES THROUGHOUT. SECOND         
            COLUMN GIVES CRYSTALLOGRAPHERS' 'RESIDUE SEQUENCE                   
            NUMBER','INSERTION CODE' AND 'CHAIN IDENTIFIER' (SEE PROTEIN        
            DATA BANK FILE RECORD FORMAT MANUAL), GIVEN FOR REFERENCE           
            ONLY AND NOT USED FURTHER..                                         
   AA -     ONE LETTER AMINO ACID CODE, LOWER CASE FOR SS-BRIDGE CYS.           
   STRUCTURE - SEE BIOPOLYMERS                                                  
   BP1 BP2  - RESIDUE NUMBER OF FIRST AND SECOND BRIDGE PARTNER                 
            FOLLOWED BY ONE LETTER SHEET LABEL                                  
   ACC -    NUMBER OF WATER MOLECULES IN CONTACT WITH THIS RESIDUE *10.         
            OR RESIDUE WATER EXPOSED SURFACE IN ANGSTROM**2.                    
   N-H-->O ETC. -  HYDROGEN BONDS. E.G. -3,-1.4 MEANS: IF THIS RESIDUE          
            IS RESIDUE I THEN N-H OF I IS H-BONDED TO C=O OF I-3                
            WITH AN ELECTROSTATIC H-BOND ENERGY OF -1.4 KCAL/MOL.               
   TCO -    COSINE OF ANGLE BETWEEN C=O OF RESIDUE I AND C=O OF                 
            RESIDUE I-1. FOR ALPHA-HELICES, TCO IS NEAR +1, FOR                 
            BETA-SHEETS TCO IS NEAR -1. NOT USED FOR STRUCTURE                  
            DEFINITION.                                                         
   KAPPA -  VIRTUAL BOND ANGLE (BEND ANGLE) DEFINED BY THE THREE                
            C-ALPHA ATOMS OF RESIDUES I-2,I,I+2. USED TO DEFINE                 
            BEND (STRUCTURE CODE 'S').                                          
   ALPHA -  VIRTUAL TORSION ANGLE (DIHEDRAL ANGLE) DEFINED BY THE FOUR          
            C-ALPHA ATOMS OF RESIDUES I-1,I,I+1,I+2. USED TO DEFINE             
            CHIRALITY (STRUCTURE CODE '+' OR '-').                              
   PHI PSI - IUPAC PEPTIDE BACKBONE TORSION ANGLES                              
   X-CA Y-CA Z-CA -  ECHO OF C-ALPHA ATOM COORDINATES              *)           
(* END OF INTRODUCTORY COMMENTS *)                                              
(************************************************************************)      
PROGRAM DSSP(INPUT,OUTPUT,TAPEIN,TAPEOUT);                                      
(*----------------------------------------------------------------------*)      
LABEL 99;   (* PROGRAM FATAL ERROR EXIT LABEL *)                                
(*******************  MATHEMATICAL CONSTANTS  ***************************       
 YVERTEX, - ARE Y,Z-COMPONENTS OF THE FIRST ICOSAHEDRON VERTEX. THE             
 ZVERTEX    X-COMPONENT IS 0.                                                   
 EPS      - NUMERICAL TOLERANCE                                                 
  ----------------------------------------------------------------------*)      
CONST PIHALF=1.570796;PI=3.141593;TWOPI=6.283185;FOURPI=12.56637;               
      RADIAN=57.29578;YVERTEX=0.8506508;ZVERTEX=0.5257311;EPS=0.00001;          
                                                                                
(***************  ARRAY DIMENSIONING CONSTANTS  ***********************         
 NMAX     - MAXIMUM NUMBER OF AMINOACID RESIDUES IN ARRAY CHAIN                 
 MAXATOM  - MAXIMUM NUMBER OF SIDECHAIN ATOMS IN ARRAY SIDECHAIN                
 MAXBRIDGE- MAXIMUM NUMBER OF BRIDGES IN ARRAY BRIDGETABLE                      
 NFACE,   - NUMBER OF FACES OF POLYHEDRON. THE COORDINATES OF THE CENTRE        
 ORDER      OF EACH TRIANGULAR FACE ARE STORED IN ARRAY P, THE AREA             
             IS STORED IN ARRAY WP IN PROCEDURE FLAGACCESS. NFACE MUST BE       
             OF THE FORM NFACE=20*(4**ORDER), ORDER=0,1,2,...                   
             THE ACCURACY OF THE SOLVENT ACCESSIBLE SURFACE OF EACH             
             AMINOACID RESIDUE IS ONE ANGSTROEM**2 FOR ORDER=2,NFACE=320.       
 MAXPACK  - MAXIMUM NUMBER OF PROTEIN ATOMS WHICH CAN INTRUDE INTO              
             SOLVENT AROUND ANY GIVEN TEST ATOM. THE COORDINATES OF             
             THESE ATOMS ARE STORED IN ARRAY X, THEIR RADII IN ARRAY RX         
             IN PROCEDURE SURFACE.                                              
 MAXHIST  - NUMBER OF SLOTS IN ARRAYS HELIXHIST AND BETAHIST USED FOR           
             LENGTH STATISTICS OF SECONDARY STRUCTURE.                          
 MAXSS    - MAXIMUM NUMBER OF SSBOND RECORDS ON INPUT FILE. THE                 
             DISULFIDE BOND ARE SAVED IN ARRAY SSBONDS.                         
  ----------------------------------------------------------------------*)      
      NMAX=3000;MAXATOM=15000;MAXBRIDGE=255;NFACE=320;ORDER=2;MAXPACK=200;      
      MAXHIST=30;MAXSS=50;                                                      
                                                                                
(*********************  PHYSICAL CONSTANTS   ****************************       
 RN       - RADIUS OF PEPTIDE NITROGEN ATOM                                     
 RCA      - RADIUS OF PEPTIDE ALPHA-CARBON ATOM                                 
 RC       - RADIUS OF PEPTIDE C'-CARBON ATOM                                    
 RO       - RADIUS OF PEPTIDE OXYGEN ATOM                                       
 RSIDEATOM- RADIUS OF SIDECHAIN ATOM                                            
 RWATER   - RADIUS OF WATER MOLECULE                                            
 SSDIST   - MAXIMUM ALLOWED DISTANCE OF DISULFIDE BRIDGE                        
 BREAKDIST- MAXIMUM ALLOWED PEPTIDE BOND LENGTH. IF DISTANCE IS                 
             GREATER A POLYPEPTIDE CHAIN INTERRUPTION IS ASSUMED.               
 RESRAD   - MAXIMUM RADIUS OF A SPHERE AROUND C-ALPHA CONTAINING                
             ALL ATOMS OF A RESIDUE                                             
 CADIST   - MINIMUM DISTANCE BETWEEN ALPHA-CARBON ATOMS SUCH THAT NO            
             BACKBONE HYDROGEN BONDS CAN BE FORMED                              
 DIST     - SMALLEST ALLOWED DISTANCE BETWEEN ANY ATOMS                         
 Q        - COUPLING CONSTANT FOR ELECTROSTATIC ENERGY                          
                    Q=-332*0.42*0.2*1000.0                                      
 HBLOW    - LOWEST ALLOWED  ENERGY OF A HYDROGEN BOND IN CAL/MOL                
 HBHIGH   - HIGHEST ALLOWED ENERGY OF A HYDROGEN BOND IN CAL/MOL                
  ----------------------------------------------------------------------*)      
      RN=1.65;RCA=1.87;RC=1.76;RO=1.4;RSIDEATOM=1.8;RWATER=1.4;                 
      SSDIST=3.0;BREAKDIST=2.5;RESRAD=10.0;CADIST=8.0;DIST=0.5;                 
      Q=-27888.0;HBLOW=-9900;HBHIGH=-500;                                       
                                                                                
(***************** GLOBAL DATA TYPE DEFINITIONS ************************)       
TYPE  VECTOR=ARRAY[1..3] OF REAL;                                               
      CHAR6=PACKED ARRAY[1..6] OF CHAR;                                         
      BRIDGETYP=(PARALLEL,ANTIPARALLEL,NOBRIDGE);                               
      BRIDGESET=SET OF 1..MAXBRIDGE;                                            
      STRUCTURE=(SYMBOL,TURN3,TURN4,TURN5,BEND,CHIRALITY,BETA1,BETA2);          
      HYDROGENBOND=RECORD RESIDUE,ENERGY:INTEGER;
                          DIST1,ANGL1,DIST2,ANGL2:REAL END;       
      BONDS=ARRAY[1..2] OF HYDROGENBOND;                                        
      BACKBONE=RECORD                                                           
               AAIDENT:CHAR6;                                                   
               SHEETLABEL,AA:CHAR;                                              
               SS:PACKED ARRAY[SYMBOL..BETA2] OF CHAR;                          
               PARTNER:ARRAY[BETA1..BETA2] OF INTEGER;                          
               ACCESS:INTEGER;                                                  
               ALPHA,KAPPA:REAL;                                                
               ACCEPTOR,DONOR:BONDS;                                            
               H,N,CA,C,O:VECTOR;                                               
               ATOMPOINTER,NSIDEATOMS:INTEGER;                                  
               END;                                                             
      BRIDGE = RECORD                                                           
               SHEETNAME,LADDERNAME:CHAR;BTYP:BRIDGETYP;                        
               LINKSET:BRIDGESET;                                               
               IB,IE:INTEGER;JB,JE:INTEGER;FROM,TOWARDS:INTEGER;                
               END;                                                             
VAR   NSS,NSSINTRA,NSSINTER,LCHAIN,NBRIDGE:INTEGER;                             
      SSBONDS:ARRAY[1..MAXSS,1..2] OF CHAR6;                                    
      CHAIN:ARRAY[0..NMAX] OF BACKBONE;TAPEIN,TAPEOUT:TEXT;                     
      SIDECHAIN:ARRAY[1..MAXATOM] OF VECTOR;                                    
      BRIDGETABLE:ARRAY[1..MAXBRIDGE] OF BRIDGE;                                
                                                                                
(*----------------------------------------------------------------------*)      
(*PROCEDURE DATE(VAR YEAR,MONTH,DAY:INTEGER);EXTERN;                          *)
                                                                                
FUNCTION ACOS(X:REAL):REAL;                                                     
BEGIN ACOS:=PIHALF-ARCTAN(X/SQRT(1.0-X*X)) END;  (* ACOS *)                     
                                                                                
FUNCTION ATAN2(Y,X:REAL):REAL;                                                  
VAR   Z:REAL;                                                                   
BEGIN IF X<>0.0 THEN Z:=ARCTAN(Y/X) ELSE IF Y>0.0 THEN Z:=PIHALF ELSE           
      IF Y<0.0 THEN Z:=-PIHALF ELSE Z:=TWOPI;                                   
      IF X<0.0 THEN IF Y>0.0 THEN Z:=Z+PI ELSE Z:=Z-PI;ATAN2:=Z;                
END;  (* ATAN2 *)                                                               
                                                                                
PROCEDURE DIFF(X,Y:VECTOR;VAR Z:VECTOR);                                        
BEGIN Z[1]:=X[1]-Y[1];Z[2]:=X[2]-Y[2];Z[3]:=X[3]-Y[3] END;(* DIFF *)            
                                                                                
FUNCTION DOT(X,Y:VECTOR):REAL;                                                  
BEGIN DOT:=X[1]*Y[1]+X[2]*Y[2]+X[3]*Y[3] END;(* DOT *)                          
                                                                                
PROCEDURE CROSS(X,Y:VECTOR;VAR Z:VECTOR);                                       
BEGIN Z[1]:=X[2]*Y[3]-Y[2]*X[3];                                                
      Z[2]:=X[3]*Y[1]-Y[3]*X[1];                                                
      Z[3]:=X[1]*Y[2]-Y[1]*X[2];                                                
END;(* CROSS *)                                                                 
                                                                                
PROCEDURE NORM(VAR X:VECTOR;VAR XNORM:REAL);                                    
(* RETURNS INPUT VECTOR X NORMALIZED TO UNIT LENGTH.                            
   XNORM IS THE ORIGINAL LENGTH OF X.                         *)                
BEGIN XNORM:=SQR(X[1])+SQR(X[2])+SQR(X[3]);IF XNORM>0.0 THEN                    
      BEGIN XNORM:=SQRT(XNORM);X[1]:=X[1]/XNORM;                                
             X[2]:=X[2]/XNORM;X[3]:=X[3]/XNORM                                  
END;  END;  (* NORM *)                                                          
                                                                                
FUNCTION DIHEDRALANGLE(V1,V2,V3,V4:VECTOR):REAL;                                
(*CALCULATES TORSION ANGLE OF A SET OF 4 ATOMS V1-V2-V3-V4.                     
  DIHEDRALANGLE IS THE ANGLE BETWEEN THE PROJECTION OF                          
  V1-V2 AND THE PROJECTION OF V4-V3 ONTO A PLANE NORMAL TO                      
  BOND V2-V3.                                                                   
*)                                                                              
VAR I:INTEGER;U,V:REAL;V12,V43,X,Y,Z,P:VECTOR;                                  
BEGIN DIFF(V1,V2,V12);DIFF(V4,V3,V43);DIFF(V2,V3,Z);                            
      CROSS(Z,V12,P);CROSS(Z,V43,X);CROSS(Z,X,Y);U:=DOT(X,X);V:=DOT(Y,Y);       
      DIHEDRALANGLE:=360.0;IF (U>0.0) AND (V>0.0) THEN                          
      BEGIN U:=DOT(P,X)/SQRT(U);V:=DOT(P,Y)/SQRT(V);                            
             IF (U<>0.0)OR(V<>0.0) THEN DIHEDRALANGLE:=ATAN2(V,U)*RADIAN        
END;  END;(* DIHEDRALANGLE *)                                                   
                                                                                
FUNCTION COSANGLE(V1,V2,V3,V4:VECTOR):REAL;                                     
VAR  U,V:VECTOR;X:REAL;                                                         
BEGIN DIFF(V1,V2,U);DIFF(V3,V4,V);X:=DOT(U,U)*DOT(V,V);                         
      IF X>0.0 THEN COSANGLE:=DOT(U,V)/SQRT(X) ELSE COSANGLE:=0.0;              
END;(* COSANGLE *)                                                              
                                                                                
FUNCTION DISTANCE(U,V:VECTOR):REAL;                                             
BEGIN DISTANCE:=SQRT(SQR(U[1]-V[1])+SQR(U[2]-V[2])+SQR(U[3]-V[3])) END;         
                                                                                
(*----------------------------------------------------------------------*)      
FUNCTION NOCHAINBREAK(I,J:INTEGER):BOOLEAN;                                     
VAR   K:INTEGER;TEST:BOOLEAN;                                                   
BEGIN TEST:=NOT((I<1)OR(J>NMAX)OR(I>J));K:=I;                                   
      WHILE TEST AND (K<=J) DO                                                  
      IF CHAIN[K].AA='!' THEN TEST:=FALSE ELSE K:=K+1;                          
      NOCHAINBREAK:=TEST;                                                       
END;(* NOCHAINBREAK *)                                                          
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
(* SEE BROOKHAVEN PROTEIN DATA BANK ATOMIC COORDINATE ENTRY FORMAT              
                                    OF DEC. 1981.                               
   ---------------------------------------------------------------------*)      
PROCEDURE INPUTCOORDINATES(VAR LCHAIN:INTEGER);                                 
TYPE  CARDTYPE=(HEADERCARD,COMPNDCARD,SOURCECARD,AUTHORCARD,                    
                SSBONDCARD,ATOMCARD,TERCARD,ENDCARD,OTHERCARD);                 
                                                                                
      CARDCONTENTS=RECORD CASE ART:CARDTYPE OF                                  
      HEADERCARD,                                                               
      COMPNDCARD,                                                               
      SOURCECARD,                                                               
      AUTHORCARD:  (Z:PACKED ARRAY[1..128] OF CHAR);                            
      SSBONDCARD:  (R:ARRAY[1..2] OF CHAR6);                                    
      ATOMCARD  :  (ATOMNAME:PACKED ARRAY[1..4] OF CHAR;                        
                    ALTLOC,RESIDUENAME:CHAR;                                    
                    RESEQNUM:CHAR6;                                             
                    COORDINATES:VECTOR);                                        
      TERCARD,                                                                  
      ENDCARD,                                                                  
      OTHERCARD :  ();                                                          
      END;         (* CARDCONTENTS TYPE DEFINITION *)                           
                                                                                
VAR   YEAR,MONTH,DAY,I,J,LATOM:INTEGER;                                         
      CORELIMIT,FINISH:BOOLEAN;OLDRESEQNUM:CHAR6;                               
      HPOSITION:VECTOR;DCO:REAL;                                                
      HB0:HYDROGENBOND;S:STRUCTURE;                                             
      CTYPE:CARDTYPE;CARDINFO:CARDCONTENTS;                                     
      CARDHIST:ARRAY[HEADERCARD..OTHERCARD] OF INTEGER;                         
                                                                                
FUNCTION ONELETTERCODE(C1,C2,C3:CHAR):CHAR;                                     
VAR   AASYMBOL:PACKED ARRAY[1..50] OF CHAR;                                     
      AMINOACID:PACKED ARRAY[1..150] OF CHAR;                                   
      STRING:ARRAY[1..5] OF PACKED ARRAY[1..30] OF CHAR;                        
      I,L,K:INTEGER;A:CHAR;                                                     
BEGIN AASYMBOL:='ARNDCEQGHILKMFPSTWYVBZXXXXXXXXXXXXXXXX--CCCCIPPPW-';           
      STRING[1]:='ALAARGASNASPCYSGLUGLNGLYHISILE';                              
      STRING[2]:='LEULYSMETPHEPROSERTHRTRPTYRVAL';                              
      STRING[3]:='ASXGLXACDALBALIABUAROBASBETHSE';                              
      STRING[4]:='HYPHYLORNPCASARTAUTHYUNKACEFOR';                              
      STRING[5]:='CYHCSHCSSCYXILUPRZPR0CPRTRYHOH';                              
      L:=0;FOR K:=1 TO 5 DO FOR I:=1 TO 30 DO                                   
      BEGIN L:=L+1;AMINOACID[L]:=STRING[K,I] END;                               
      A:='-';I:=1;K:=1;WHILE (K<51) AND (A='-') DO                              
      BEGIN IF AMINOACID[I]=C1 THEN                                             
             IF AMINOACID[I+1]=C2 THEN                                          
             IF AMINOACID[I+2]=C3 THEN A:=AASYMBOL[K];                          
             I:=I+3;K:=K+1;                                                     
      END;ONELETTERCODE:=A;                                                     
END;  (* ONELETTERCODE *)                                                       
                                                                                
PROCEDURE READCARD(VAR CARDINFO:CARDCONTENTS);                                  
VAR   C,C1,C2,C3:CHAR;K,L,M:INTEGER;KEY:CHAR6;                                  
BEGIN WITH CARDINFO DO                                                          
      BEGIN ART:=ENDCARD;IF NOT EOF(TAPEIN) THEN                                
             BEGIN KEY:='END   ';ART:=OTHERCARD;                                
                  FOR L:=1 TO 6 DO                                              
                  IF NOT EOLN(TAPEIN) THEN READ(TAPEIN,KEY[L]);                 
                  IF KEY='HEADER' THEN ART:=HEADERCARD;                         
                  IF KEY='COMPND' THEN ART:=COMPNDCARD;                         
                  IF KEY='SOURCE' THEN ART:=SOURCECARD;                         
                  IF KEY='AUTHOR' THEN ART:=AUTHORCARD;                         
                  IF KEY='SSBOND' THEN ART:=SSBONDCARD;                         
                  IF KEY='ATOM  ' THEN ART:=ATOMCARD;                           
                  IF KEY='TER   ' THEN ART:=TERCARD;                            
                  IF KEY='END   ' THEN ART:=ENDCARD;                            
                  CASE ART OF                                                   
HEADERCARD,                                                                     
COMPNDCARD,                                                                     
SOURCECARD,                                                                     
AUTHORCARD:       BEGIN FOR L:=1 TO 6 DO Z[L]:=KEY[L];                          
                        FOR L:=7 TO 127 DO Z[L]:=' ';Z[128]:='.';               
                        IF ART=HEADERCARD THEN M:=66 ELSE M:=70;                
                        FOR L:=7 TO M DO                                        
                        IF NOT EOLN(TAPEIN) THEN READ(TAPEIN,Z[L]);             
                  END;                                                          
SSBONDCARD:       BEGIN FOR L:=7 TO 8 DO READ(TAPEIN,C);                        
                        FOR K:=1 TO 2 DO                                        
                        BEGIN FOR L:=1 TO 7 DO READ(TAPEIN,C);                  
                              READ(TAPEIN,R[K][6],C);                           
                              FOR L:=1 TO 5 DO READ(TAPEIN,R[K][L]);            
                  END;  END;                                                    
ATOMCARD:         BEGIN FOR L:=7 TO 12 DO READ(TAPEIN,C);                       
                        FOR L:=13 TO 16 DO READ(TAPEIN,ATOMNAME[L-12]);         
                        READ(TAPEIN,ALTLOC,C1,C2,C3);                           
                        RESIDUENAME:=ONELETTERCODE(C1,C2,C3);                   
                        READ(TAPEIN,C,RESEQNUM[6]);                             
                        FOR L:=1 TO 5 DO READ(TAPEIN,RESEQNUM[L]);              
                        FOR L:=1 TO 3 DO READ(TAPEIN,COORDINATES[L]);           
                  END;                                                          
TERCARD,                                                                        
ENDCARD,                                                                        
OTHERCARD :       ;(* NULL CASE *)                                              
                  END;  READLN(TAPEIN);                                         
END;  END;  END;  (* READCARD *)                                                
                                                                                
BEGIN HB0.RESIDUE:=0;HB0.ENERGY:=0;                                             
      FOR I:=0 TO NMAX DO WITH CHAIN[I] DO                                      
      BEGIN FOR J:=1 TO 6 DO AAIDENT[J]:=' ';AA:='!';ACCESS:=0;                 
             FOR S:=SYMBOL TO BETA2 DO SS[S]:=' ';SHEETLABEL:=' ';              
             PARTNER[BETA1]:=0;PARTNER[BETA2]:=0;ALPHA:=360.0;                  
             FOR J:=1 TO 2 DO BEGIN ACCEPTOR[J]:=HB0;DONOR[J]:=HB0 END;         
             ATOMPOINTER:=0;NSIDEATOMS:=0;FOR J:=1 TO 3 DO                      
             BEGIN H[J]:=0.0;N[J]:=0.0;CA[J]:=0.0;C[J]:=0.0;O[J]:=0.0 END;      
      END;NSS:=0;LATOM:=0;YEAR:=2001;MONTH:=12;DAY:=24;                         
(*      DATE(YEAR,MONTH,DAY);                                                 *)
      WRITE(TAPEOUT,'**** SECONDARY STRUCTURE DEFINITION ');                    
      WRITE(TAPEOUT,'BY THE PROGRAM DSSP, VERSION JUNE 1983 ****');             
      WRITE(TAPEOUT,' MONTH=',MONTH:2,' DAY=',DAY:2,' YEAR=',YEAR:4);           
      FOR I:=106 TO 127 DO WRITE(TAPEOUT,' ');WRITELN(TAPEOUT,'.');             
      WRITE(TAPEOUT,'REFERENCE W. KABSCH AND C.SANDER, BIOPOLYMERS ');          
      WRITE(TAPEOUT,'22 (1983) 2577-2637');                                     
      FOR I:=66 TO 127 DO WRITE(TAPEOUT,' '); WRITELN(TAPEOUT,'.');             
      FOR CTYPE:=HEADERCARD TO OTHERCARD DO CARDHIST[CTYPE]:=0;                 
      CORELIMIT:=FALSE;FINISH:=FALSE;OLDRESEQNUM:='      ';                     
      REPEAT READCARD(CARDINFO);WITH CARDINFO DO                                
             BEGIN CARDHIST[ART]:=CARDHIST[ART]+1;                              
                   CASE ART OF                                                  
HEADERCARD,                                                                     
COMPNDCARD,                                                                     
SOURCECARD,                                                                     
AUTHORCARD:        IF CARDHIST[ART]=1 THEN                                      
                   BEGIN FOR I:=1 TO 128 DO WRITE(TAPEOUT,Z[I]);                
                         WRITELN(TAPEOUT);                                      
                   END;                                                         
SSBONDCARD:        BEGIN NSS:=NSS+1;                                            
                         FOR I:=1 TO 2 DO SSBONDS[NSS,I]:=R[I];                 
                   END;                                                         
ATOMCARD  :        IF(RESIDUENAME<>'-')AND(ALTLOC IN [' ','A'])THEN             
                   BEGIN IF(ATOMNAME=' N  ')AND(OLDRESEQNUM<>RESEQNUM)THEN      
                         BEGIN IF NOCHAINBREAK(LCHAIN,LCHAIN) THEN              
                               IF(DISTANCE(CHAIN[LCHAIN].C,COORDINATES)>        
                                  BREAKDIST)                                    
                               OR(RESEQNUM[6]<>OLDRESEQNUM[6])THEN              
                               LCHAIN:=LCHAIN+1;                                
                               IF NOT NOCHAINBREAK(LCHAIN,LCHAIN) OR            
                                  (RESIDUENAME='P') THEN                        
                               HPOSITION:=COORDINATES                           
                               ELSE WITH CHAIN[LCHAIN] DO                       
                               BEGIN DCO:=DISTANCE(C,O);                        
                                     FOR I:=1 TO 3 DO                           
                                     HPOSITION[I]:=COORDINATES[I]+              
                                                  (C[I]-O[I])/DCO;              
                               END;IF (LCHAIN+2)>NMAX THEN CORELIMIT:=TRUE      
                               ELSE                                             
                               BEGIN LCHAIN:=LCHAIN+1;WITH CHAIN[LCHAIN] DO     
                                     BEGIN ATOMPOINTER:=LATOM;                  
                                           AAIDENT:=RESEQNUM;                   
                                           H:=HPOSITION;                        
                                           AA:=RESIDUENAME;                     
                                     END;  OLDRESEQNUM:=RESEQNUM;               
                         END   END;                                             
                         IF(OLDRESEQNUM=RESEQNUM)THEN WITH CHAIN[LCHAIN] DO     
                         IF ATOMNAME=' N  ' THEN  N:=COORDINATES ELSE           
                         IF ATOMNAME=' CA ' THEN CA:=COORDINATES ELSE           
                         IF ATOMNAME=' C  ' THEN  C:=COORDINATES ELSE           
                         IF ATOMNAME=' O  ' THEN  O:=COORDINATES ELSE           
                         BEGIN LATOM:=LATOM+1;                                  
                               IF LATOM>MAXATOM THEN CORELIMIT:=TRUE ELSE       
                               BEGIN NSIDEATOMS:=NSIDEATOMS+1;                  
                                     SIDECHAIN[LATOM]:=COORDINATES;             
                   END;  END;  END;                                             
TERCARD   :        IF NOCHAINBREAK(LCHAIN,LCHAIN) THEN                          
                   LCHAIN:=LCHAIN+1;                                            
ENDCARD   :        FINISH:=TRUE;                                                
OTHERCARD :        ;(* NULL CASE *)                                             
             END;  END;                                                         
      UNTIL  CORELIMIT OR FINISH;                                               
      IF CORELIMIT THEN                                                         
      WRITELN(' !!! NUMBER OF ATOMS EXCEEDS STORAGE CAPACITY !!!');             
      IF NOT NOCHAINBREAK(LCHAIN,LCHAIN) THEN LCHAIN:=LCHAIN-1;                 
      FOR CTYPE:=HEADERCARD TO AUTHORCARD DO                                    
      IF CARDHIST[CTYPE]<1 THEN                                                 
      WRITELN(' !!! HEADER,COMPND,SOURCE OR AUTHOR CARD MISSING !!!');          
      IF LCHAIN<1 THEN                                                          
      BEGIN WRITELN(' !!! BACKBONE COORDINATE SET INCOMPLETE !!!');             
             GOTO 99;                                                           
      END;                                                                      
      IF LATOM=0 THEN                                                           
      WRITELN(' !!! ALL SIDECHAIN COORDINATES MISSING !!!');                    
END;(* INPUTCOORDINATES *)                                                      
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
FUNCTION TESTBOND(I,J:INTEGER):BOOLEAN;                                         
(* TESTBOND IS TRUE IF I IS DONOR[=NH] TO J, OTHERWISE FALSE *)                 
BEGIN WITH CHAIN[I] DO                                                          
      TESTBOND:=((ACCEPTOR[1].RESIDUE=J)AND(ACCEPTOR[1].ENERGY<HBHIGH))         
              OR((ACCEPTOR[2].RESIDUE=J)AND(ACCEPTOR[2].ENERGY<HBHIGH))         
END;(* TESTBOND *)                                                              
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
PROCEDURE FLAGSSBONDS;                                                          
VAR SSBOND:BOOLEAN;CC:CHAR;I,J,II,JJ:INTEGER;D:REAL;                            
                                                                                
FUNCTION TESTSSBOND(I,J:INTEGER):BOOLEAN;                                       
VAR   SSBOND:BOOLEAN;K:INTEGER;                                                 
BEGIN SSBOND:=FALSE;K:=1;IF NOCHAINBREAK(I,I) AND NOCHAINBREAK(J,J) THEN        
      WHILE NOT (SSBOND OR (K>NSS)) DO                                          
      BEGIN SSBOND:=((CHAIN[I].AAIDENT=SSBONDS[K,1])AND                         
                     (CHAIN[J].AAIDENT=SSBONDS[K,2]))OR                         
                    ((CHAIN[I].AAIDENT=SSBONDS[K,2])AND                         
                     (CHAIN[J].AAIDENT=SSBONDS[K,1]));K:=K+1;                   
      END;  TESTSSBOND:=SSBOND;                                                 
END;  (* TESTSSBONDS *)                                                         
                                                                                
BEGIN NSSINTRA:=0;NSSINTER:=0;CC:=PRED('a');FOR I:=1 TO LCHAIN-2 DO             
      IF (CHAIN[I].AA='C')AND(CHAIN[I].NSIDEATOMS>1) THEN                       
      BEGIN II:=CHAIN[I].ATOMPOINTER+2;J:=I+1;                                  
             REPEAT J:=J+1;SSBOND:=FALSE;                                       
                   IF (CHAIN[J].NSIDEATOMS>1)AND(CHAIN[J].AA='C') THEN          
                   JJ:=CHAIN[J].ATOMPOINTER+2 ELSE JJ:=0;                       
                   IF JJ>0 THEN                                                 
                   SSBOND:=DISTANCE(SIDECHAIN[II],SIDECHAIN[JJ])<SSDIST;        
             UNTIL  SSBOND OR (J=LCHAIN);                                       
             IF (SSBOND) AND NOT TESTSSBOND(I,J) THEN                           
             WRITELN(' !!! ADDITIONAL SSBOND FOUND BETWEEN RESIDUES'            
                           ,I:4, ' AND',J:4,' !!!');                            
      END;  IF NSS>0 THEN FOR I:=1 TO LCHAIN-2 DO WITH CHAIN[I] DO              
      IF AA='C' THEN FOR J:=I+2 TO LCHAIN DO IF CHAIN[J].AA='C' THEN            
      IF TESTSSBOND(I,J) THEN                                                   
      BEGIN IF CC='z' THEN                                                      
             BEGIN WRITELN(' !!! SS-BRIDGE LABEL RESTART AT a !!!');            
                  CC:=PRED('a');                                                
             END;  CC:=SUCC(CC);AA:=CC;CHAIN[J].AA:=CC;                         
             IF NOCHAINBREAK(I,J) THEN NSSINTRA:=NSSINTRA+1                     
                                 ELSE NSSINTER:=NSSINTER+1;                     
             IF NSIDEATOMS>1 THEN IF CHAIN[J].NSIDEATOMS>1 THEN                 
             BEGIN JJ:=CHAIN[J].ATOMPOINTER+2;II:=ATOMPOINTER+2;                
                  D:=DISTANCE(SIDECHAIN[II],SIDECHAIN[JJ]);                     
                  IF D>SSDIST THEN WRITELN(' !!! SSBOND DISTANCE IS',           
                  D:5:1,' BETWEEN RESIDUES',I:5,' AND',J:5,' !!!');             
      END;  END;  IF NSS<>(NSSINTRA+NSSINTER) THEN                              
      WRITELN(' !!! ERROR IN SSBOND DATA RECORDS !!!');                         
END;  (* FLAGSSBONDS *)                                                         
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
PROCEDURE FLAGCHIRALITY;                                                        
VAR   I:INTEGER;CKAP,SKAP:REAL;                                                 
BEGIN FOR I:=2 TO LCHAIN-2 DO WITH CHAIN[I] DO                                  
      IF NOCHAINBREAK(I-1,I+2) THEN                                             
      BEGIN                                                                     
      ALPHA:=DIHEDRALANGLE(CHAIN[I-1].CA,CA,CHAIN[I+1].CA,CHAIN[I+2].CA);       
      IF ALPHA<0.0 THEN SS[CHIRALITY]:='-' ELSE SS[CHIRALITY]:='+';             
      END;                                                                      
                                                                                
      FOR I:=3 TO LCHAIN-2 DO WITH CHAIN[I] DO                                  
      IF NOCHAINBREAK(I-2,I+2) THEN BEGIN                                       
      CKAP:=COSANGLE(CHAIN[I].CA,CHAIN[I-2].CA,CHAIN[I+2].CA,CHAIN[I].CA);      
      SKAP:=SQRT(1-CKAP*CKAP);KAPPA:=RADIAN*ATAN2(SKAP,CKAP);                   
      END;                                                                      
                                                                                
 END;(* CHIRALITY *)                                                            
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
PROCEDURE FLAGHYDROGENBONDS;                                                    
VAR  I,J:INTEGER;                                                               
                                                                                
PROCEDURE BONDENERGY(I,J:INTEGER;VAR HBE:INTEGER;VAR DNO,THETA,DHO,PSI:REAL);   
(*RESIDUE I IS DONOR[=NH],J IS ACCEPTOR[=CO] OF THE PROTON IN THE               
   HYDROGEN BOND. THE BONDENERGY IS IN CAL/MOL *)                               
VAR   ACOSARG,DHC,DNC:REAL;                                                     
BEGIN HBE:=0;DNO:=0.0;THETA:=0.0;DHO:=0.0;PSI:=0.0;
      WITH CHAIN[I] DO IF AA<>'P' THEN                                   
      BEGIN DHO:=DISTANCE(H,CHAIN[J].O);DHC:=DISTANCE(H,CHAIN[J].C);            
            DNC:=DISTANCE(N,CHAIN[J].C);DNO:=DISTANCE(N,CHAIN[J].O);           
            IF (DHO<DIST)OR(DHC<DIST)OR(DNC<DIST)OR(DNO<DIST) THEN             
            HBE:=HBLOW ELSE HBE:=ROUND(Q/DHO-Q/DHC+Q/DNC-Q/DNO);               
            IF HBE<=HBLOW THEN                                                 
            BEGIN WRITELN(' !!! CONTACT BETWEEN RESIDUES',I:5,' AND',J:5,      
                            '  TOO CLOSE !!!');HBE:=HBLOW;                     
            END;
            ACOSARG := (1.0+DNO*DNO-DHO*DHO)/(2*DNO);
            IF (ACOSARG >= 1.0) THEN THETA := 0.0
            ELSE IF (ACOSARG <= -1.0) THEN THETA := 180.0
            ELSE THETA:=ACOS((1.0+DNO*DNO-DHO*DHO)/(2*DNO))*RADIAN;
            ACOSARG := (1.0+DHO*DHO-DNO*DNO)/(2*DHO);
            IF (ACOSARG >= 1.0) THEN PSI := 0.0
            ELSE IF (ACOSARG <= -1.0) THEN PSI := 180.0
            ELSE PSI:=ACOS((1.0+DHO*DHO-DNO*DNO)/(2*DHO))*RADIAN;                 
END;  END;  (* BONDENERGY *)                                     
                                                                                
PROCEDURE UPDATEBONDS(VAR B:BONDS;HB:HYDROGENBOND);                             
BEGIN IF HB.ENERGY<B[1].ENERGY THEN                                             
      BEGIN B[2]:=B[1];B[1]:=HB END                                             
      ELSE IF HB.ENERGY<B[2].ENERGY THEN B[2]:=HB                               
END;(* UPDATEBONDS *)                                                           
                                                                                
PROCEDURE SETBONDS(I,J:INTEGER); (*I IS NH, J IS CO*)                           
VAR   HB:HYDROGENBOND;HBE:INTEGER;DNO,THETA,DHO,PSI:REAL;                  
BEGIN BONDENERGY(I,J,HBE,DNO,THETA,DHO,PSI);                             
      HB.ENERGY:=HBE;HB.DIST1:=DNO;HB.ANGL1:=THETA;HB.RESIDUE:=J;
                     HB.DIST2:=DHO;HB.ANGL2:=PSI;            
      (* CO(J) IS ACCEPTOR OF NH(I) *)                                          
      UPDATEBONDS(CHAIN[I].ACCEPTOR,HB);                                        
      HB.RESIDUE:=I;UPDATEBONDS(CHAIN[J].DONOR,HB)                              
END;(* SETBOND *)                                                               
                                                                                
BEGIN FOR I:=1 TO LCHAIN DO IF NOCHAINBREAK(I,I) THEN WITH CHAIN[I] DO          
      FOR J:=I+1 TO LCHAIN DO IF NOCHAINBREAK(J,J) THEN                         
      IF DISTANCE(CA,CHAIN[J].CA)<CADIST THEN                                   
      BEGIN SETBONDS(I,J);IF J<>(I+1) THEN SETBONDS(J,I) END;                   
END;  (* FLAGHYDROGENBONDS *)                                                   
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
PROCEDURE FLAGBRIDGE;                                                           
VAR   I:INTEGER;                                                                
                                                                                
PROCEDURE TESTBRIDGE(I:INTEGER);                                                
VAR   J1,J2,J:INTEGER;B:BRIDGETYP;                                              
                                                                                
PROCEDURE LADDER(I,J:INTEGER;B:BRIDGETYP);                                      
VAR   K:INTEGER;FOUND:BOOLEAN;                                                  
BEGIN FOUND:=FALSE;K:=1;IF (B<>NOBRIDGE)AND(I<J) THEN                           
      REPEAT WITH BRIDGETABLE[K] DO                                             
             IF IB=0 THEN                                                       
             BEGIN IB:=I;IE:=I;JB:=J;JE:=J;FROM:=0;TOWARDS:=0;                  
                   BTYP:=B;NBRIDGE:=NBRIDGE+1;FOUND:=TRUE;                      
             END   ELSE                                                         
             BEGIN FOUND:=(BTYP=B)AND(I=(IE+1))AND NOCHAINBREAK(IE,I)AND        
                   (((J=(JE+1))AND(B=PARALLEL)AND NOCHAINBREAK(JE,J))OR         
                 ((J=(JB-1))AND(B=ANTIPARALLEL)AND NOCHAINBREAK(J,JB)));        
                   IF FOUND THEN                                                
                   BEGIN IE:=IE+1;                                              
                         IF B=PARALLEL THEN JE:=JE+1 ELSE JB:=JB-1              
                   END  ELSE                                                    
                   BEGIN K:=K+1;IF K>MAXBRIDGE THEN                             
                         BEGIN WRITELN(' !!! BRIDGETABLE OVERFLOW !!!');        
                               GOTO 99;                                         
             END;  END;  END;                                                   
      UNTIL  FOUND                                                              
END;  (* LADDER *)                                                              
                                                                                
BEGIN J1:=0;J2:=0;J:=I+3;IF NOCHAINBREAK(I-1,I+1) THEN                          
      WHILE (J2=0)AND(J<LCHAIN) DO                                              
      BEGIN IF NOCHAINBREAK(J-1,J+1) THEN                                       
             BEGIN IF (TESTBOND(I+1,J) AND TESTBOND(J  ,I-1))OR                 
                     (TESTBOND(J+1,I)  AND TESTBOND(I  ,J-1))                   
                  THEN B:=PARALLEL ELSE                                         
                  IF (TESTBOND(I+1,J-1)AND TESTBOND(J+1,I-1))OR                 
                     (TESTBOND(J  ,I  )AND TESTBOND(I  ,J  ))                   
                  THEN B:=ANTIPARALLEL ELSE B:=NOBRIDGE;                        
                  IF B<>NOBRIDGE THEN                                           
                  BEGIN IF J1=0 THEN BEGIN J1:=J;LADDER(I,J,B) END              
                                ELSE IF J<>J1 THEN                              
                                     BEGIN J2:=J;LADDER(I,J,B) END;             
             END;  END;  J:=J+1;                                                
END;  END;  (* TESTBRIDGE *)                                                    
                                                                                
PROCEDURE EXTENDLADDER;                                                         
VAR   I,J,IB1,JB1,JE1:INTEGER;BULGE:BOOLEAN;                                    
BEGIN FOR I:=1 TO NBRIDGE DO WITH BRIDGETABLE[I] DO                             
      BEGIN J:=I+1;WHILE (J<=NBRIDGE)AND(TOWARDS=0) DO                          
             BEGIN IB1:=BRIDGETABLE[J].IB;                                      
                  JB1:=BRIDGETABLE[J].JB;JE1:=BRIDGETABLE[J].JE;                
                  BULGE:=NOCHAINBREAK(IE,IB1)AND((IB1-IE)<6)AND                 
                  (BRIDGETABLE[J].BTYP=BTYP)AND(BRIDGETABLE[J].FROM=0);         
                  IF BULGE THEN CASE BTYP OF                                    
PARALLEL:         BULGE:=((((JB1-JE)<6)AND((IB1-IE)<3))OR                       
                          ((JB1-JE)<3))AND NOCHAINBREAK(JE,JB1);                
ANTIPARALLEL:     BULGE:=((((JB-JE1)<6)AND((IB1-IE)<3))OR                       
                         ((JB-JE1)<3))AND NOCHAINBREAK(JE1,JB);                 
                  END;IF BULGE THEN                                             
                  BEGIN TOWARDS:=J;BRIDGETABLE[J].FROM:=I END;J:=J+1;           
      END;  END;                                                                
      FOR I:=1 TO NBRIDGE DO WITH BRIDGETABLE[I] DO IF FROM=0 THEN              
      BEGIN LINKSET:=[];J:=I;                                                   
             REPEAT LINKSET:=LINKSET+[J];J:=BRIDGETABLE[J].TOWARDS              
             UNTIL  J=0;J:=TOWARDS;WHILE J<>0 DO                                
             BEGIN BRIDGETABLE[J].LINKSET:=LINKSET;                             
                  J:=BRIDGETABLE[J].TOWARDS                                     
END;  END;  END;  (* EXTENDLADDER *)                                            
                                                                                
PROCEDURE SHEET;                                                                
VAR   ASCI,I,J:INTEGER;CCS:CHAR;LADDERSET,SHEETSET:BRIDGESET;                   
                                                                                
PROCEDURE FINDSHEET;                                                            
VAR   L1,L2:INTEGER;FINISH:BOOLEAN;                                             
                                                                                
FUNCTION LINK(L1,L2:INTEGER):BOOLEAN;                                           
(* LINK IS TRUE IF THERE IS A COMMON RESIDUE IN LADDERS L1 AND L2 *)            
VAR   IB1,IE1,JB1,JE1,IB2,IE2,JB2,JE2:INTEGER;                                  
BEGIN IB1:=BRIDGETABLE[L1].IB;IE1:=BRIDGETABLE[L1].IE;                          
      JB1:=BRIDGETABLE[L1].JB;JE1:=BRIDGETABLE[L1].JE;                          
      IB2:=BRIDGETABLE[L2].IB;IE2:=BRIDGETABLE[L2].IE;                          
      JB2:=BRIDGETABLE[L2].JB;JE2:=BRIDGETABLE[L2].JE;                          
      LINK:=((IE1>=IB2)AND(IB1<=IE2))OR((IE1>=JB2)AND(IB1<=JE2))                
           OR((JE1>=IB2)AND(JB1<=IE2))OR((JE1>=JB2)AND(JB1<=JE2));              
END;  (* LINK *)                                                                
                                                                                
BEGIN SHEETSET:=[];L1:=0;IF LADDERSET<>[] THEN                                  
      REPEAT L1:=L1+1 UNTIL L1 IN LADDERSET;IF L1>0 THEN                        
      SHEETSET:=BRIDGETABLE[L1].LINKSET;IF L1>0 THEN                            
      REPEAT FINISH:=TRUE;FOR L1:=1 TO NBRIDGE DO IF L1 IN SHEETSET THEN        
             FOR L2:=1 TO NBRIDGE DO IF L2 IN LADDERSET THEN                    
             IF LINK(L1,L2) THEN                                                
             BEGIN SHEETSET:=SHEETSET+BRIDGETABLE[L2].LINKSET;                  
                   LADDERSET:=LADDERSET-BRIDGETABLE[L2].LINKSET;                
                   FINISH:=FALSE;                                               
             END;                                                               
      UNTIL  FINISH;                                                            
END;  (* FINDSHEET *)                                                           
                                                                                
BEGIN LADDERSET:=[];FOR I:=1 TO NBRIDGE DO LADDERSET:=LADDERSET+[I];            
      CCS:=PRED('A');ASCI:=64;WHILE LADDERSET<>[] DO                            
      BEGIN CCS:=SUCC(CCS);IF CCS>'z' THEN                                      
             BEGIN WRITELN(' !!! SHEET LABEL RESTART AT A !!!');                
                  CCS:='A';                                                     
             END;  FINDSHEET;FOR I:=1 TO NBRIDGE DO                             
             WITH BRIDGETABLE[I] DO IF (I IN SHEETSET)AND(FROM=0) THEN          
             BEGIN IF ASCI=90 THEN                                              
                  BEGIN WRITELN(' !!! STRAND LABEL RESTART AT A !!!');          
                        ASCI:=64;                                               
                  END;  ASCI:=ASCI+1;IF BTYP=PARALLEL THEN                      
                  LADDERNAME:=CHR(ASCI+32) ELSE LADDERNAME:=CHR(ASCI);          
                  SHEETNAME:=CCS;LINKSET:=SHEETSET;J:=TOWARDS;                  
                  WHILE J<>0 DO                                                 
                  BEGIN BRIDGETABLE[J].LADDERNAME:=LADDERNAME;                  
                        BRIDGETABLE[J].SHEETNAME :=SHEETNAME ;                  
                        BRIDGETABLE[J].LINKSET:=SHEETSET;                       
                        J:=BRIDGETABLE[J].TOWARDS                               
END;  END;  END;  END;  (* SHEET *)                                             
                                                                                
PROCEDURE MARKSTRANDS;                                                          
VAR   I,J,L,IB0,IE0,JB0,JE0:INTEGER;BETA,BETAI,BETAJ:STRUCTURE;                 
      ISET,JSET:ARRAY[BETA1..BETA2] OF SET OF CHAR;CC:CHAR;                     
BEGIN FOR I:=1 TO NBRIDGE DO IF BRIDGETABLE[I].FROM=0 THEN                      
      BEGIN J:=I;FOR BETA:=BETA1 TO BETA2 DO                                    
             BEGIN ISET[BETA]:=[];JSET[BETA]:=[] END;                           
             IB0:=LCHAIN;IE0:=0;JB0:=LCHAIN;JE0:=0;                             
             REPEAT WITH BRIDGETABLE[J] DO                                      
                   BEGIN FOR L:=IB TO IE DO WITH CHAIN[L] DO                    
                         FOR BETA:=BETA1 TO BETA2 DO                            
                         ISET[BETA]:=ISET[BETA]+[SS[BETA]];                     
                         FOR L:=JB TO JE DO WITH CHAIN[L] DO                    
                         FOR BETA:=BETA1 TO BETA2 DO                            
                         JSET[BETA]:=JSET[BETA]+[SS[BETA]];                     
                         IF IB<IB0 THEN IB0:=IB;IF IE>IE0 THEN IE0:=IE;         
                         IF JB<JB0 THEN JB0:=JB;IF JE>JE0 THEN JE0:=JE;         
                         J:=TOWARDS;                                            
                   END                                                          
             UNTIL  J=0;J:=I;                                                   
             IF ISET[BETA1]=[' '] THEN BETAI:=BETA1 ELSE BETAI:=BETA2;          
             IF JSET[BETA1]=[' '] THEN BETAJ:=BETA1 ELSE BETAJ:=BETA2;          
             IF (ISET[BETAI]<>[' '])OR(JSET[BETAJ]<>[' ']) THEN                 
             WRITELN(' !!! STRAND COLUMN OVERWRITTEN !!!');                     
             REPEAT WITH BRIDGETABLE[J] DO                                      
                   BEGIN FOR L:=IB TO IE DO WITH CHAIN[L] DO                    
                         BEGIN SS[BETAI]:=LADDERNAME;IF BTYP=PARALLEL THEN      
                               PARTNER[BETAI]:=JB+L-IB ELSE                     
                               PARTNER[BETAI]:=JE-L+IB;                         
                         END;  FOR L:=JB TO JE DO WITH CHAIN[L] DO              
                         BEGIN SS[BETAJ]:=LADDERNAME;IF BTYP=PARALLEL THEN      
                               PARTNER[BETAJ]:=IB+L-JB  ELSE                    
                               PARTNER[BETAJ]:=IE-L+JB;                         
                         END;  J:=TOWARDS;                                      
                   END                                                          
             UNTIL  J=0;  IF IB0=IE0 THEN CC:='B' ELSE CC:='E';                 
             FOR J:=IB0 TO IE0 DO WITH CHAIN[J] DO                              
             IF SS[SYMBOL]<>'E' THEN SS[SYMBOL]:=CC;                            
             FOR J:=JB0 TO JE0 DO WITH CHAIN[J] DO                              
             IF SS[SYMBOL]<>'E' THEN SS[SYMBOL]:=CC;                            
      END;  FOR J:=1 TO NBRIDGE DO WITH BRIDGETABLE[J] DO                       
      BEGIN FOR L:=IB TO IE DO CHAIN[L].SHEETLABEL:=SHEETNAME;                  
             FOR L:=JB TO JE DO CHAIN[L].SHEETLABEL:=SHEETNAME;                 
END;  END;  (* MARKSTRANDS *)                                                   
                                                                                
BEGIN FOR I:=1 TO MAXBRIDGE DO WITH BRIDGETABLE[I] DO                           
      BEGIN IB:=0;IE:=0;JB:=0;JE:=0;BTYP:=NOBRIDGE END;                         
      NBRIDGE:=0;FOR I:=2 TO LCHAIN-1 DO TESTBRIDGE(I);                         
      IF NBRIDGE>0 THEN BEGIN EXTENDLADDER;SHEET;MARKSTRANDS END;               
END;  (* FLAGBRIDGE *)                                                          
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
PROCEDURE FLAGTURN;                                                             
VAR   I,J,K:INTEGER;TURN:STRUCTURE;CC:CHAR;                                     
                                                                                
PROCEDURE FLAGSYMBOL;                                                           
(* FLAGS ALPHA HELICES AND TURNS IN SYMBOL COLUMN *)                            
VAR   I,J,K:INTEGER;CC:CHAR;NHSET:SET OF CHAR;TURN:STRUCTURE;                   
      EMPTY:BOOLEAN;                                                            
BEGIN NHSET:=['>','X'];FOR I:=2 TO LCHAIN-4 DO                                  
      IF (CHAIN[I-1].SS[TURN4] IN NHSET) AND                                    
          (CHAIN[I].SS[TURN4] IN NHSET) THEN                                    
      FOR J:=I TO I+3 DO CHAIN[J].SS[SYMBOL]:='H';                              
      FOR I:=2 TO LCHAIN-3 DO IF (CHAIN[I-1].SS[TURN3] IN NHSET)AND             
                                 (CHAIN[I  ].SS[TURN3] IN NHSET)THEN            
      BEGIN EMPTY:=TRUE;FOR J:=I TO I+2 DO WITH CHAIN[J] DO                     
             IF NOT (SS[SYMBOL] IN [' ','G']) THEN EMPTY:=FALSE;                
             IF EMPTY THEN FOR J:=I TO I+2 DO CHAIN[J].SS[SYMBOL]:='G'          
      END;                                                                      
      FOR I:=2 TO LCHAIN-5 DO IF (CHAIN[I-1].SS[TURN5] IN NHSET)AND             
                                 (CHAIN[I  ].SS[TURN5] IN NHSET)THEN            
      BEGIN EMPTY:=TRUE;FOR J:=I TO I+4 DO WITH CHAIN[J] DO                     
             IF NOT (SS[SYMBOL] IN [' ','I']) THEN EMPTY:=FALSE;                
             IF EMPTY THEN FOR J:=I TO I+4 DO CHAIN[J].SS[SYMBOL]:='I'          
      END;                                                                      
      FOR I:=2 TO LCHAIN-1 DO WITH CHAIN[I] DO IF SS[SYMBOL]=' ' THEN           
      BEGIN CC:=' ';J:=1;FOR TURN:=TURN3 TO TURN5 DO                            
             BEGIN J:=J+1;FOR K:=1 TO J DO IF I>K THEN                          
                  IF CHAIN[I-K].SS[TURN] IN NHSET THEN CC:='T';                 
             END;IF (CC=' ') THEN CC:=SS[BEND];SS[SYMBOL]:=CC;                  
END;  END; (* FLAGSYMBOL *)                                                     
                                                                                
BEGIN K:=2;CC:='2';FOR TURN:=TURN3 TO TURN5 DO                                  
      BEGIN K:=K+1;CC:=SUCC(CC);FOR I:=1 TO LCHAIN-K DO                         
             IF NOCHAINBREAK(I,I+K) THEN IF TESTBOND(I+K,I) THEN                
             BEGIN CHAIN[I+K].SS[TURN]:='<';FOR J:=1 TO K-1 DO                  
                  WITH CHAIN[I+J] DO IF SS[TURN]=' ' THEN SS[TURN]:=CC;         
                  WITH CHAIN[I] DO IF SS[TURN]='<' THEN                         
                  SS[TURN]:='X' ELSE SS[TURN]:='>';                             
      END;  END;                                                                
      FOR I:=1 TO LCHAIN DO WITH CHAIN[I] DO                                    
      IF (KAPPA<>360.0) AND (KAPPA>70.0) THEN SS[BEND]:='S';                    
      FLAGSYMBOL;                                                               
END;(* FLAGTURN *)                                                              
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
PROCEDURE FLAGACCESS;                                                           
VAR   I,K,NP:INTEGER;F:REAL;P:ARRAY[1..NFACE] OF VECTOR;                        
      WP:ARRAY[1..NFACE] OF REAL;                                               
                                                                                
PROCEDURE POLYEDER;                                                             
VAR   V:ARRAY[1..12] OF VECTOR;A,B:REAL;I,J,K,LEVEL:INTEGER;                    
                                                                                
PROCEDURE TRIANGLE(X1,X2,X3:VECTOR;LEVEL:INTEGER);                              
VAR   K,LEVEL1:INTEGER;XNORM:REAL;X4,X5,X6:VECTOR;                              
BEGIN IF LEVEL>0 THEN                                                           
      BEGIN LEVEL1:=LEVEL-1;FOR K:=1 TO 3 DO                                    
             BEGIN X4[K]:=X1[K]+X2[K];X5[K]:=X2[K]+X3[K];X6[K]:=X1[K]+X3[K]     
             END;NORM(X4,XNORM);NORM(X5,XNORM);NORM(X6,XNORM);                  
             TRIANGLE(X1,X4,X6,LEVEL1);TRIANGLE(X4,X2,X5,LEVEL1);               
             TRIANGLE(X4,X5,X6,LEVEL1);TRIANGLE(X5,X3,X6,LEVEL1);               
      END   ELSE                                                                
      BEGIN FOR K:=1 TO 3 DO X6[K]:=X1[K]+X2[K]+X3[K];NORM(X6,XNORM);           
             NP:=NP+1;P[NP]:=X6;DIFF(X3,X1,X5);DIFF(X2,X1,X4);                  
             CROSS(X5,X4,X6);NORM(X6,XNORM);WP[NP]:=XNORM/2.0                   
END;  END;  (* TRIANGLE *)                                                      
                                                                                
BEGIN (* GENERATES ALL 12 VERTICES OF ICOSAHEDRON *)                            
      K:=0;A:=YVERTEX;B:=ZVERTEX;FOR I:=1 TO 2 DO                               
      BEGIN A:=-A;FOR J:=1 TO 2 DO                                              
             BEGIN B:=-B;                                                       
                  K:=K+1;V[K][1]:=0.0;V[K][2]:=A  ;V[K][3]:=B  ;                
                  K:=K+1;V[K][1]:=B  ;V[K][2]:=0.0;V[K][3]:=A  ;                
                  K:=K+1;V[K][1]:=A  ;V[K][2]:=B  ;V[K][3]:=0.0;                
      END;  END;  NP:=0;LEVEL:=ORDER;                                           
      (* GET ALL 20 FACES OF ICOSAHEDRON *)                                     
      FOR I:=1 TO 10 DO FOR J:=I+1 TO 11 DO                                     
      IF DISTANCE(V[I],V[J])<1.1 THEN FOR K:=J+1 TO 12 DO                       
      IF (DISTANCE(V[I],V[K])<1.1)AND(DISTANCE(V[J],V[K])<1.1) THEN             
      TRIANGLE(V[I],V[J],V[K],LEVEL);(* FIND INTEGRATION POINTS *)              
      A:=0.0;FOR I:=1 TO NP DO A:=A+WP[I];A:=FOURPI/A;                          
      FOR I:=1 TO NP DO WP[I]:=WP[I]*A;                                         
END;  (* POLYEDER  *)                                                           
                                                                                
FUNCTION SURFACE(XATOM:VECTOR;RATOM:REAL):REAL;                                 
VAR   NX,I,J:INTEGER;F,RADIUS:REAL;XX:VECTOR;                                   
      X:ARRAY[1..MAXPACK] OF VECTOR;RX:ARRAY[1..MAXPACK] OF REAL;               
                                                                                
FUNCTION STEP(XX:VECTOR):BOOLEAN;                                               
VAR   K:INTEGER;ONE:BOOLEAN;                                                    
BEGIN ONE:=TRUE;K:=1;WHILE (K<=NX) AND ONE DO                                   
      IF DISTANCE(XX,X[K])<(RX[K]+RWATER) THEN ONE:=FALSE ELSE K:=K+1;          
      STEP:=ONE;                                                                
END;  (* STEP *)                                                                
                                                                                
PROCEDURE LISTE(XX:VECTOR;RXX:REAL);                                            
VAR   Y,Z:VECTOR;I,K:INTEGER;D:REAL;                                            
                                                                                
PROCEDURE LISTENTRY(XX,YY:VECTOR;D,R:REAL);                                     
VAR   ZZ:VECTOR;DELTA:REAL;                                                     
BEGIN DELTA:=DISTANCE(XX,YY);IF DELTA<(D+R) THEN IF DELTA>EPS THEN              
      BEGIN NX:=NX+1;IF NX>MAXPACK THEN                                         
             BEGIN WRITELN(' !!! TABLE OVERFLOW IN FLAGACCESS !!!');GOTO 99     
             END   ELSE                                                         
             BEGIN DIFF(YY,XX,ZZ);X[NX]:=ZZ;RX[NX]:=R END;                      
END;  END;  (* LISTENTRY *)                                                     
                                                                                
BEGIN NX:=0;D:=RXX+RWATER+RWATER;FOR I:=1 TO LCHAIN DO                          
      IF NOCHAINBREAK(I,I) THEN WITH CHAIN[I] DO                                
      IF DISTANCE(XX,CA)<(D+RESRAD) THEN                                        
      BEGIN LISTENTRY(XX,N,D,RN);LISTENTRY(XX,CA,D,RCA);                        
             LISTENTRY(XX,C,D,RC);LISTENTRY(XX,O,D,RO);                         
             IF NSIDEATOMS>0 THEN FOR K:=1 TO NSIDEATOMS DO                     
             LISTENTRY(XX,SIDECHAIN[ATOMPOINTER+K],D,RSIDEATOM);                
END;  END;  (* LISTE *)                                                         
                                                                                
BEGIN LISTE(XATOM,RATOM);RADIUS:=RATOM+RWATER;F:=0;FOR I:=1 TO NP DO            
      BEGIN FOR J:=1 TO 3 DO XX[J]:=P[I][J]*RADIUS;                             
             IF STEP(XX) THEN F:=F+WP[I];                                       
      END;  SURFACE:=RADIUS*RADIUS*F                                            
END;  (* SURFACE *)                                                             
                                                                                
BEGIN POLYEDER;                                                                 
      FOR I:=1 TO LCHAIN DO IF NOCHAINBREAK(I,I) THEN WITH CHAIN[I] DO          
      BEGIN F:=SURFACE(N,RN)+SURFACE(CA,RCA)+SURFACE(C,RC)+SURFACE(O,RO);       
             IF NSIDEATOMS>0 THEN FOR K:=1 TO NSIDEATOMS DO                     
             F:=F+SURFACE(SIDECHAIN[ATOMPOINTER+K],RSIDEATOM);                  
             ACCESS:=ROUND(F);                                                  
END;  END;  (* FLAGACCESS *)                                                    
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
PROCEDURE PRINTOUT;                                                             
VAR   I,J:INTEGER;S:STRUCTURE;PHI,PSI,TCO:REAL;                                 
                                                                                
PROCEDURE STATISTICS;                                                           
VAR   I,J,K,NCHAIN,NRES,NHBOND,LHELIX:INTEGER;B:BRIDGETYP;CC:CHAR;              
      SURFACE:REAL;NHBTURN:ARRAY[-5..5] OF INTEGER;LADDERSET:BRIDGESET;         
      HBRIDGE:ARRAY[PARALLEL..ANTIPARALLEL] OF INTEGER;                         
      HELIXHIST,SHEETHIST:ARRAY[1..MAXHIST] OF INTEGER;                         
      BETAHIST:ARRAY[PARALLEL..ANTIPARALLEL,1..MAXHIST] OF INTEGER;             
BEGIN LHELIX:=0;NHBOND:=0;NCHAIN:=0;NRES:=0;FOR I:=1 TO MAXHIST DO              
      BEGIN FOR B:=PARALLEL TO ANTIPARALLEL DO BETAHIST[B][I]:=0;               
             HELIXHIST[I]:=0;SHEETHIST[I]:=0                                    
      END;  SURFACE:=0.0;FOR K:=-5 TO 5 DO NHBTURN[K]:=0;                       
      FOR B:=PARALLEL TO ANTIPARALLEL DO HBRIDGE[B]:=0;                         
      FOR I:=0 TO LCHAIN DO WITH CHAIN[I] DO                                    
      BEGIN IF NOCHAINBREAK(I,I) THEN                                           
             BEGIN NRES:=NRES+1;SURFACE:=SURFACE+ACCESS;                        
                  FOR J:=1 TO 2 DO IF DONOR[J].ENERGY<HBHIGH THEN               
                  BEGIN NHBOND:=NHBOND+1;K:=DONOR[J].RESIDUE-I;                 
                        IF ABS(K)<6 THEN                                        
                        NHBTURN[K]:=NHBTURN[K]+1;                               
             END   END   ELSE NCHAIN:=NCHAIN+1;                                 
             IF SS[SYMBOL]='H' THEN LHELIX:=LHELIX+1 ELSE IF LHELIX>0 THEN      
             BEGIN IF LHELIX>MAXHIST THEN LHELIX:=MAXHIST;                      
                  HELIXHIST[LHELIX]:=HELIXHIST[LHELIX]+1;LHELIX:=0;             
      END;  END;                                                                
      IF NBRIDGE>0 THEN FOR I:=1 TO NBRIDGE DO WITH BRIDGETABLE[I] DO           
      BEGIN HBRIDGE[BTYP]:=HBRIDGE[BTYP]+(IE-IB)+2; IF FROM=0 THEN              
             BEGIN J:=I;K:=0;                                                   
                  REPEAT K:=K+BRIDGETABLE[J].IE-BRIDGETABLE[J].IB+1;            
                         J:=BRIDGETABLE[J].TOWARDS                              
                  UNTIL  J=0;IF K>MAXHIST THEN K:=MAXHIST;                      
                  BETAHIST[BTYP][K]:=BETAHIST[BTYP][K]+1;                       
      END;  END;  IF NBRIDGE>0 THEN                                             
      BEGIN LADDERSET:=[];FOR I:=1 TO NBRIDGE DO LADDERSET:=LADDERSET+[I];      
             FOR I:=1 TO NBRIDGE DO WITH BRIDGETABLE[I] DO                      
             IF (FROM=0)AND(I IN LADDERSET) THEN                                
             BEGIN IF ([I]<>LINKSET)OR(IE>IB) THEN                              
                  BEGIN K:=0;FOR J:=1 TO NBRIDGE DO                             
                        IF (BRIDGETABLE[J].FROM=0)AND(J IN LINKSET) THEN        
                        K:=K+1;SHEETHIST[K]:=SHEETHIST[K]+1;                    
                  END;  LADDERSET:=LADDERSET-LINKSET;                           
      END;  END;                                                                
      WRITELN(TAPEOUT,NRES:5,NCHAIN:3,(NSSINTER+NSSINTRA):3,NSSINTRA:3,         
      NSSINTER:3,' TOTAL NUMBER OF RESIDUES, NUMBER OF CHAINS, ',               
      'NUMBER OF SS-BRIDGES(TOTAL,INTRACHAIN,INTERCHAIN)',                      
      '                .');                                                     
      WRITELN(TAPEOUT,SURFACE:8:1,'   ACCESSIBLE SURFACE OF',                   
      ' PROTEIN (ANGSTROM**2)                                 ',                
      '                                        .');                             
      WRITELN(TAPEOUT,NHBOND:5,(100.0*NHBOND/NRES):5:1,                         
      '   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(J)  , ',            
      'SAME NUMBER PER 100 RESIDUES                              .');           
      I:=HBRIDGE[PARALLEL];J:=HBRIDGE[ANTIPARALLEL];                            
      WRITELN(TAPEOUT,I:5,(100.0*I/NRES):5:1,                                   
      '   TOTAL NUMBER OF HYDROGEN BONDS IN     PARALLEL BRIDGES, ',            
      'SAME NUMBER PER 100 RESIDUES                              .');           
      WRITELN(TAPEOUT,J:5,(100.0*J/NRES):5:1,                                   
      '   TOTAL NUMBER OF HYDROGEN BONDS IN ANTIPARALLEL BRIDGES, ',            
      'SAME NUMBER PER 100 RESIDUES                              .');           
      FOR I:=-5 TO 5 DO                                                         
      BEGIN IF I<0 THEN CC:='-' ELSE CC:='+';K:=ABS(I);                         
             WRITELN(TAPEOUT,NHBTURN[I]:5,(100.0*NHBTURN[I]/NRES):5:1,          
             '   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I',          
             CC,K:1,'), ',                                                      
      'SAME NUMBER PER 100 RESIDUES                              .');           
      END;  FOR I:=1 TO MAXHIST DO WRITE(TAPEOUT,I:3);                          
      WRITELN(TAPEOUT,'     *** HISTOGRAMS OF ***           .');                
      FOR I:=1 TO MAXHIST DO WRITE(TAPEOUT,HELIXHIST[I]:3);                     
      WRITELN(TAPEOUT,'    RESIDUES PER ALPHA HELIX         .');                
      FOR I:=1 TO MAXHIST DO WRITE(TAPEOUT,BETAHIST[PARALLEL][I]:3);            
      WRITELN(TAPEOUT,'    PARALLEL BRIDGES PER LADDER      .');                
      FOR I:=1 TO MAXHIST DO WRITE(TAPEOUT,BETAHIST[ANTIPARALLEL][I]:3);        
      WRITELN(TAPEOUT,'    ANTIPARALLEL BRIDGES PER LADDER  .');                
      FOR I:=1 TO MAXHIST DO WRITE(TAPEOUT,SHEETHIST[I]:3);                     
      WRITELN(TAPEOUT,'    LADDERS PER SHEET                .');                
END;  (* STATISTICS *)                                                          
                                                                                
PROCEDURE WRITEHB(I:INTEGER;HB:HYDROGENBOND);                                   
VAR   E:REAL;                                                                   
BEGIN WITH HB DO                                                                
      BEGIN IF RESIDUE<>0 THEN RESIDUE:=RESIDUE-I;
            E:=ENERGY/1000.0;             
            IF ENERGY=0 THEN BEGIN DIST1:=0.0; ANGL1:=0.0 END;
            WRITE(TAPEOUT,RESIDUE:4,',',E:4:1,',',DIST1:5:2,',',ANGL1:6:1);  
END;  END;(* WRITEHB *)                                                         
                                                                                
BEGIN STATISTICS;WRITELN(TAPEOUT,                                               
      '  #  RESIDUE AA STRUCTURE BP1 BP2  ACC',                                 
      '     TCO  KAPPA ALPHA  PHI   PSI    ',                                   
             'X-CA   Y-CA   Z-CA ');                                            
      FOR I:=1 TO LCHAIN DO WITH CHAIN[I] DO                                    
      BEGIN WRITE(TAPEOUT,I:5,' ');FOR J:=1 TO 6 DO                             
               WRITE(TAPEOUT,AAIDENT[J]);                                    
            WRITE(TAPEOUT,' ',AA,'  ',SS[SYMBOL],' ');                         
            FOR S:=TURN3 TO BETA2 DO WRITE(TAPEOUT,SS[S]);                     
            FOR S:=BETA1 TO BETA2 DO WRITE(TAPEOUT,PARTNER[S]:4);              
            WRITE(TAPEOUT,SHEETLABEL,ACCESS:4,' ');                            
            PHI:=360.0;PSI:=360.0;TCO:=0.0;                                    
            IF NOCHAINBREAK(I-1,I) THEN                                        
            BEGIN PHI:=DIHEDRALANGLE(CHAIN[I-1].C,N,CA,C);                     
                  TCO:=COSANGLE(C,O,CHAIN[I-1].C,CHAIN[I-1].O);                 
            END;  IF NOCHAINBREAK(I,I+1) THEN                                  
            PSI:=DIHEDRALANGLE(N,CA,C,CHAIN[I+1].N);                           
            WRITELN(TAPEOUT,TCO:8:3,KAPPA:6:1,ALPHA:6:1,PHI:6:1,PSI:6:1,       
                            CA[1]:7:1,CA[2]:7:1,CA[3]:7:1);                     
      END;                                                                      
            WRITELN(TAPEOUT);                                                  
            WRITELN(TAPEOUT,                                                   
            '  #  RESIDUE AA STRUCTURE  N-H-->O               O-->H-N ',    
            '              N-H-->O               O-->H-N ');           
            WRITELN(TAPEOUT);
            WRITELN(TAPEOUT,
            '                            #   E   D NO  <)ONH   #   E  ',
            ' D NO  <)ONH   #   E   D NO  <)ONH   #   E   D NO  <)ONH ');
            WRITELN(TAPEOUT,
            '                                    D HO  <)NHO          ',
            ' D HO  <)NHO           D HO  <)NHO           D HO  <)NHO ');
            WRITELN(TAPEOUT);
            WRITELN(TAPEOUT); 
      FOR I:=1 TO LCHAIN DO WITH CHAIN[I] DO                                    
      BEGIN WRITE(TAPEOUT,I:5,' ');FOR J:=1 TO 6 DO                             
            WRITE(TAPEOUT,AAIDENT[J]);                                         
            WRITE(TAPEOUT,' ',AA,'  ',SS[SYMBOL],' ');                         
            FOR S:=TURN3 TO BETA2 DO WRITE(TAPEOUT,SS[S]);                     
            FOR J:=1 TO 2 DO                                                   
               BEGIN WRITEHB(I,ACCEPTOR[J]);WRITEHB(I,DONOR[J]) END;           
            WRITELN(TAPEOUT);
            WITH ACCEPTOR[1] DO BEGIN
               IF ENERGY=0 THEN BEGIN DIST2:=0.0; ANGL2:=0.0 END;
               WRITE(TAPEOUT,DIST2:40:2,',',ANGL2:6:1);  
            END;
            WITH DONOR[1] DO BEGIN
               IF ENERGY=0 THEN BEGIN DIST2:=0.0; ANGL2:=0.0 END;
               WRITE(TAPEOUT,DIST2:15:2,',',ANGL2:6:1);
            END;
            WITH ACCEPTOR[2] DO BEGIN
               IF ENERGY=0 THEN BEGIN DIST2:=0.0; ANGL2:=0.0 END;
               WRITE(TAPEOUT,DIST2:15:2,',',ANGL2:6:1);
            END;
            WITH DONOR[2] DO BEGIN
               IF ENERGY=0 THEN BEGIN DIST2:=0.0; ANGL2:=0.0 END;
               WRITELN(TAPEOUT,DIST2:15:2,',',ANGL2:6:1);
            END;
END;  END; (* PRINTOUT *)                                                       
                                                                                
(*----------------------------------------------------------------------*)      
(*----------------------------------------------------------------------*)      
BEGIN RESET(TAPEIN);REWRITE(TAPEOUT);WRITELN('ENTER DSSP');                     
      LCHAIN:=0;INPUTCOORDINATES(LCHAIN);                                       
      IF NOT NOCHAINBREAK(1,LCHAIN) THEN                                        
      WRITELN(' !!! POLYPEPTIDE CHAIN INTERRUPTED !!!');                        
      WRITELN('INPUTCOORDINATES DONE',LCHAIN);                                  
      FLAGSSBONDS;       WRITELN('FLAGSSBONDS DONE');                           
      FLAGCHIRALITY;     WRITELN('FLAGCHIRALITY DONE');                         
      FLAGHYDROGENBONDS; WRITELN('FLAGHYDROGENBONDS DONE');                     
      FLAGBRIDGE;        WRITELN('FLAGBRIDGE DONE');                            
      FLAGTURN;          WRITELN('FLAGTURN DONE');                              
      FLAGACCESS;        WRITELN('FLAGACCESS DONE');                            
      PRINTOUT;          WRITELN('PRINTOUT DONE');                              
99:; (* ERROR EXIT *)                                                           
END. (* END OF PROGRAM DSSP *)                                                  
(************************************************************************)      
(* SAMPLE OUTPUT FOR TRYPSIN INHIBITOR - PROTEIN DATA BANK DATA SET 3PTI.       
   TO RECREATE CORRECT ORIGINAL OUTPUT, MERGE EACH TWO LINES INTO ONE.          
**** SECONDARY STRUCTURE DEFINITION BY THE PROGRAM DSSP, VERSION JUNE 1983 **** 
MONTH=11 DAY=29 YEAR=1983                      .                                
REFERENCE W. KABSCH AND C.SANDER, BIOPOLYMERS 22 (1983) 2577-2637               
                                               .                                
HEADER    PROTEINASE INHIBITOR (TRYPSIN)          01-NOV-76   3PTI              
                                               .                                
COMPND    TRYPSIN INHIBITOR                                                     
                                               .                                
SOURCE    BOVINE (BOS TAURUS) PANCREAS                                          
                                               .                                
AUTHOR    R.HUBER,D.KUKLA,A.RUEHLMANN,O.EPP,H.FORMANEK,                         
                                               .                                
   58  1  3  3  0 TOTAL NUMBER OF RESIDUES, NUMBER OF CHAINS, NUMBER OF SS-BRIDG
ES(TOTAL,INTRACHAIN,INTERCHAIN)                .                                
  4121.0   ACCESSIBLE SURFACE OF PROTEIN (ANGSTROM**2)                          
                                               .                                
   28 48.3   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(J)  , SAME NUMBER
 PER 100 RESIDUES                              .                                
    0  0.0   TOTAL NUMBER OF HYDROGEN BONDS IN     PARALLEL BRIDGES, SAME NUMBER
 PER 100 RESIDUES                              .                                
   10 17.2   TOTAL NUMBER OF HYDROGEN BONDS IN ANTIPARALLEL BRIDGES, SAME NUMBER
 PER 100 RESIDUES                              .                                
    1  1.7   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I-5), SAME NUMBER
 PER 100 RESIDUES                              .                                
    0  0.0   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I-4), SAME NUMBER
 PER 100 RESIDUES                              .                                
    0  0.0   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I-3), SAME NUMBER
 PER 100 RESIDUES                              .                                
    0  0.0   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I-2), SAME NUMBER
 PER 100 RESIDUES                              .                                
    0  0.0   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I-1), SAME NUMBER
 PER 100 RESIDUES                              .                                
    0  0.0   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I+0), SAME NUMBER
 PER 100 RESIDUES                              .                                
    0  0.0   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I+1), SAME NUMBER
 PER 100 RESIDUES                              .                                
    6 10.3   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I+2), SAME NUMBER
 PER 100 RESIDUES                              .                                
    3  5.2   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I+3), SAME NUMBER
 PER 100 RESIDUES                              .                                
    7 12.1   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I+4), SAME NUMBER
 PER 100 RESIDUES                              .                                
    1  1.7   TOTAL NUMBER OF HYDROGEN BONDS OF TYPE O(I)-->H-N(I+5), SAME NUMBER
 PER 100 RESIDUES                              .                                
  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 2
7 28 29 30     *** HISTOGRAMS OF ***           .                                
  0  0  0  0  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  
0  0  0  0    RESIDUES PER ALPHA HELIX         .                                
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  
0  0  0  0    PARALLEL BRIDGES PER LADDER      .                                
  1  0  0  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  
0  0  0  0    ANTIPARALLEL BRIDGES PER LADDER  .                                
  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  
0  0  0  0    LADDERS PER SHEET                .                                
  #  RESIDUE AA STRUCTURE BP1 BP2  ACC   N-H-->O  O-->H-N  N-H-->O  O-->H-N    T
CO  KAPPA ALPHA  PHI   PSI    X-CA   Y-CA   Z-CA                                
    1    1   R              0   0  143    0, 0.0  54,-0.2   0, 0.0  53,-0.1   0.
000   0.0 360.0 360.0 143.6   25.6   26.9   -1.7                                
    2    2   P    >   -     0   0   50    0, 0.0   3,-1.3   0, 0.0  52, 0.0  -0.
167   0.0-118.4 -58.2 152.0   26.2   25.8    1.9                                
    3    3   D  G >  S+     0   0  145    1,-0.2   3,-1.7   2,-0.2   4,-0.2   0.
780 109.7  66.8 -60.5 -30.0   27.3   22.2    2.6                                
    4    4   F  G >  S+     0   0   37    1,-0.3   3,-0.9   2,-0.2  -1,-0.2   0.
613  83.0  76.7 -73.7 -12.2   24.2   21.5    4.7                                
    5    5   a  G <  S+     0   0    0   -3,-1.3  20,-0.3   1,-0.2  -1,-0.3   0.
644  88.9  58.8 -65.3 -17.5   22.2   21.8    1.5                                
    6    6   L  G <  S+     0   0  106   -3,-1.7  -1,-0.2  18,-0.1  -2,-0.2   0.
490  82.3 102.8 -91.7  -5.4   23.4   18.4    0.5                                
    7    7   E  S <  S-     0   0   54   -3,-0.9  -3, 0.0  -4,-0.2   0, 0.0  -0.
498  78.6-107.2 -74.9 146.8   22.0   16.5    3.5                                
    8    8   P        -     0   0   96    0, 0.0  35,-0.2   0, 0.0  -1,-0.1  -0.
309  47.2 -84.0 -67.1 157.2   18.9   14.5    3.1                                
    9    9   P        -     0   0   51    0, 0.0   2,-0.6   0, 0.0  35,-0.1  -0.
347  41.5-147.5 -64.1 145.4   15.7   15.8    4.7                                
   10   10   Y        -     0   0   86   33,-0.3  31,-0.3   1,-0.2   0, 0.0  -0.
937  17.1-176.1-125.7 106.0   15.3   14.9    8.3                                
   11   11   T        -     0   0   70   -2,-0.6  25,-2.3  24,-0.1  26,-0.3   0.
895  34.2-154.4 -70.0 -41.2   11.8   14.3    9.6                                
   12   12   G        -     0   0   21    1,-0.2  27,-0.1  23,-0.1  -1,-0.1  -0.
229  30.3 -65.4  93.5 178.5   13.0   13.9   13.2                                
   13   13   P  S    S+     0   0   89    0, 0.0  -1,-0.2   0, 0.0   2,-0.2   0.
507 100.0  90.1 -85.9  -6.6   11.5   12.0   16.0                                
   14   14   b        -     0   0   53   24,-0.4  23,-0.1  -3,-0.3  24,-0.1  -0.
469  66.2-140.1 -89.1 164.8    8.3   13.9   16.6                                
   15   15   K        +     0   0  190   -2,-0.2  22,-0.1  22,-0.1   2,-0.1  -0.
075  55.4 119.0-126.7  31.9    5.0   13.2   14.9                                
   16   16   A        -     0   0   45   20,-2.2   2,-0.4   2, 0.0  21,-0.1  -0.
310  63.6-122.2 -77.3 178.5    3.4   16.5   14.1                                
   17   17   R        +     0   0  216   -2,-0.1   2,-0.6  19,-0.1  19,-0.2  -0.
795  42.3 159.0-134.0  87.5    2.7   17.3   10.4                                
   18   18   I  E     -A   35   0A  63   17,-2.0  17,-3.5  -2,-0.4   2,-0.6  -0.
957  35.1-136.5-125.1 117.8    4.5   20.5    9.4                                
   19   19   I  E     +A   34   0A 101   -2,-0.6   2,-0.3  15,-0.2  15,-0.2  -0.
673  36.4 163.2 -83.7 118.1    5.1   20.9    5.7                                
   20   20   R  E     -A   33   0A  51   13,-2.4  13,-2.1  -2,-0.6   2,-0.4  -0.
779  32.3-116.9-125.9 172.1    8.6   22.2    5.0                                
   21   21   Y  E     -AB  32  45A  68   24,-3.1  24,-2.9  -2,-0.3   2,-0.3  -0.
875  23.1-175.3-110.1 145.8   11.0   22.5    2.1                                
   22   22   F  E     -A   31   0A  22    9,-2.5   9,-2.4  -2,-0.4   2,-0.4  -0.
968  30.6-112.1-129.4 151.2   14.4   20.8    1.7                                
   23   23   Y  E     -A   30   0A  10   -2,-0.3   2,-0.8   7,-0.2   7,-0.2  -0.
727  22.0-154.5 -79.6 129.5   16.9   21.2   -1.1                                
   24   24   N  E >>> -A   29   0A  44    5,-2.3   4,-1.9  -2,-0.4   5,-1.0  -0.
902   4.2-163.8-109.9 103.8   17.3   18.1   -3.3                                
   25   25   A  T 345S+     0   0   48   -2,-0.8  -1,-0.2 -20,-0.3 -19,-0.1   0.
759  86.1  55.3 -65.2 -28.4   20.8   18.4   -4.7                                
   26   26   K  T 345S+     0   0  202    1,-0.2  -1,-0.3   2, 0.0  -2, 0.0   0.
821 115.1  38.4 -66.9 -33.9   20.2   15.8   -7.3                                
   27   27   A  T <45S-     0   0   50   -3,-0.7  -2,-0.2   2,-0.2  -1,-0.2   0.
671 102.2-129.8 -92.9 -20.5   17.2   17.6   -8.7                                
   28   28   G  T  <5S+     0   0   36   -4,-1.9   2,-0.3   1,-0.2  -3,-0.2   0.
616  76.0  65.2  81.0  14.6   18.6   21.1   -8.3                                
   29   29   L  E   <S-A   24   0A  81   -5,-1.0  -5,-2.3   0, 0.0   2,-0.3  -0.
928  85.2 -83.3-159.3 174.9   15.5   22.5   -6.5                                
   30   30   c  E     -A   23   0A  19   -2,-0.3   2,-0.3  -7,-0.2  -7,-0.2  -0.
650  35.7-177.0 -98.5 145.7   13.5   22.3   -3.3                                
   31   31   Q  E     -A   22   0A  79   -9,-2.4  -9,-2.5  -2,-0.3   2,-0.2  -0.
880  28.2-102.4-132.9 160.7   10.7   19.8   -2.6                                
   32   32   T  E     +A   21   0A  80   -2,-0.3   2,-0.3 -11,-0.2 -11,-0.2  -0.
570  35.5 171.5 -83.5 150.3    8.4   19.4    0.3                                
   33   33   F  E     -A   20   0A  24  -13,-2.1 -13,-2.4  -2,-0.2   2,-0.6  -0.
939  39.7-102.8-145.7 166.3    8.6   16.8    3.1                                
   34   34   V  E     -A   19   0A  72   -2,-0.3   2,-0.5 -15,-0.2 -15,-0.2  -0.
843  33.5-157.8 -95.7 120.0    6.7   16.4    6.4                                
   35   35   Y  E     -A   18   0A  17  -17,-3.5 -17,-2.0  -2,-0.6 -24,-0.1  -0.
827  13.0-140.4-100.1 133.7    8.8   17.6    9.3                                
   36   36   G        -     0   0    1  -25,-2.3 -20,-2.2  -2,-0.5 -24,-0.1   0.
567  41.9-107.1 -72.7 -11.6    7.8   16.1   12.6                                
   37   37   G  S    S+     0   0   36  -26,-0.3   2,-0.3   1,-0.2 -25,-0.1   0.
409  85.1  74.3 104.7   0.0    8.3   19.3   14.6                                
   38   38   b  S    S+     0   0   24  -24,-0.1 -24,-0.4   1,-0.1  -2,-0.2  -1.
000  85.4  11.7-145.9 150.7   11.4   18.7   16.5                                
   39   39   R  S    S-     0   0  195   -2,-0.3  -1,-0.1 -27,-0.1 -27,-0.1   0.
852  73.8-168.7  61.0  40.1   15.2   18.4   16.3                                
   40   40   A        -     0   0   50   -3,-0.1   2,-0.2 -28,-0.1  -1,-0.1  -0.
149  14.9-142.1 -60.9 152.4   15.5   19.9   12.8                                
   41   41   K        -     0   0  108  -31,-0.3 -31,-0.1   2,-0.1  -1,-0.1  -0.
481  36.6 -97.9-102.7-179.9   18.7   19.8   10.7                                
   42   42   R  S    S+     0   0  181   -2,-0.2   2,-1.8   1,-0.2 -37,-0.1   0.
664 107.3  80.7 -85.2 -19.7   19.4   22.8    8.6                                
   43   43   N  S    S+     0   0    3  -39,-0.2 -33,-0.3 -35,-0.2   2,-0.3  -0.
574  73.7 103.7 -77.2  73.8   18.2   21.7    5.2                                
   44   44   N        +     0   0   20   -2,-1.8   2,-0.3 -22,-0.1 -22,-0.2  -0.
845  46.4 174.8-166.1 107.1   14.7   22.6    6.3                                
   45   45   F  B     -B   21   0A  21  -24,-2.9 -24,-3.1  -2,-0.3   3,-0.1  -0.
868  36.5-132.6-128.7 157.9   12.9   25.8    5.2                                
   46   46   K  S    S+     0   0  160   -2,-0.3   2,-0.3 -26,-0.2  -1,-0.1   0.
527  91.2  18.7 -88.4  -7.5    9.5   27.3    5.5                                
   47   47   S  S  > S-     0   0   31  -26,-0.1   4,-1.9   1,-0.1 -26,-0.1  -0.
982  72.9-118.3-154.2 161.6    9.1   28.1    1.8                                
   48   48   A  H  > S+     0   0   30   -2,-0.3   4,-2.9   2,-0.2 -18,-0.1   0.
845 114.6  56.4 -66.9 -34.1   10.6   27.1   -1.5                                
   49   49   E  H  > S+     0   0  131    2,-0.2   4,-1.9   1,-0.2  -1,-0.2   0.
896 108.5  44.6 -69.5 -42.1   11.8   30.7   -2.0                                
   50   50   D  H  > S+     0   0   77    2,-0.2   4,-1.8   1,-0.2  -1,-0.2   0.
884 114.2  52.1 -65.0 -40.8   13.7   30.8    1.2                                
   51   51   c  H  X S+     0   0    0   -4,-1.9   4,-3.2   2,-0.2  -2,-0.2   0.
930 112.9  42.1 -59.4 -49.4   15.1   27.3    0.4                                
   52   52   M  H  X S+     0   0   72   -4,-2.9   4,-2.3   2,-0.2   5,-0.2   0.
818 110.0  58.1 -70.8 -32.2   16.3   28.3   -3.1                                
   53   53   R  H  < S+     0   0  175   -4,-1.9  -1,-0.2   2,-0.2  -2,-0.2   0.
895 117.4  33.7 -62.9 -41.7   17.7   31.6   -1.9                                
   54   54   T  H  < S+     0   0   53   -4,-1.8  -2,-0.2   1,-0.1   3,-0.2   0.
906 136.1  21.2 -76.4 -46.3   19.9   29.7    0.5                                
   55   55   a  H  < S+     0   0    0   -4,-3.2  -3,-0.2 -54,-0.2  -2,-0.2   0.
488  93.5  89.6-105.9  -4.2   20.6   26.6   -1.5                                
   56   56   G  S  < S-     0   0   21   -4,-2.3  -1,-0.1  -5,-0.2  -3,-0.1   0.
627  76.6-143.8 -77.8 -14.5   20.0   27.3   -5.2                                
   57   57   G              0   0   35   -5,-0.2  -1,-0.1  -3,-0.2  -2,-0.1   0.
046   0.0 360.0  72.5 168.9   23.6   28.4   -5.9                                
   58   58   A              0   0  178    0, 0.0  -1, 0.0   0, 0.0   0, 0.0  -0.
962   0.0 360.0 164.7 360.0   24.0   31.2   -8.4        *)                      
(* END OF DSSP SAMPLE OUTPUT *)                                                 
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
