* This program uses a 50 * 50 grid of information values generated from 
* the gaussian window to produce a profile.



	implicit none

	integer maxPos,maxRes,maxSS,maxX,maxY
	real maxAb, inc

	parameter (maxAb = 234.0, inc=0.02000)
	parameter (maxPos=2000,maxRes=21,maxSS=3,maxX=50,maxY=50)

	character line*80, TableName*80, PrfName*80, EnvName*80
	integer pos1,pos2,pos3,temp1,temp2,temp3
	integer TableUnit/1/, PrfUnit/2/, EnvUnit/3/
	real CoilPen
	logical ReadEnvFile, ReadScoringTable
	integer Res(maxPos),SS(maxPos)
	real Ab(maxPos), Fp(maxPos)
	integer infos(MaxRes,MaxSS,MaxY,MaxX)
	integer profile(MaxPos,MaxRes)
	integer index1,index2,index3,index4
	integer Length, Bcol, Xcol
	integer getresint, getssint
	character threetoone*1
	character InttoRes, IntToSS


	print*, 'Enter name of file containing information values'
	read(5,'(A)') TableName
	open(TableUnit,file=TableName,form='formatted',status='old',readonly)

	print*, 'Enter name of profile'
	read(5,'(A)') PrfName
	open(PrfUnit,file=PrfName,form='formatted',status='unknown')

	print*, 'Enter name of .env file'
	read(5,'(A)') EnvName
	open(EnvUnit,file=EnvName,form='formatted',status='old',readonly)

	print*, 'Enter name of gap penalty for coil regions'
	read(5,*) CoilPen


* Read AB, FP, SS, and RES from .env file

	if ( .not. ReadEnvFile(Res,SS,Ab,Fp,EnvUnit,Length) ) then
	 print*, ' Problems Reading Environment File...'
	 goto 999
	end if

* Read in information values

	if ( .not. ReadScoringTable(Infos,TableUnit) ) then
	 print*, ' Problems Reading Environment File...'
	 goto 999
	end if

* Now get values for the profile

	do pos1 = 1,Length

	 index2 = SS(pos1)
	

	 index3 = NINT( (Ab(pos1) / MaxAb) / inc )
	 index4 = NINT(  Fp(pos1) / inc  )


	 if (index3 .eq. 0) index3 = 1
	 if (index4 .eq. 0) index4 = 1


	 if (Ab(pos1) .ge. 0 .and. Fp(pos1) .ge. 0) then
	  do index1 = 1,MaxRes
	   profile(pos1,index1) = infos(index1,index2,index3,index4)
	   if (profile(pos1,index1) .lt. -999) profile(pos1,index1) = -999
	   if (profile(pos1,index1) .gt.  999) profile(pos1,index1) =  999
	  end do
	 else
	  do index1 = 1,MaxRes
	   profile(pos1,index1) = 0
	  end do
	 end if
	end do
	 

* write out information values to the profile

	index1 = INDEX(PrfName,' ') - 1

	write(PrfUnit,'(A,T54,A,I6)')  '(Peptide) PROFILE OF:  '//
     &			PrfName(MAX(1,index1-27):index1),'Length:',Length

	write(PrfUnit,'(A)') ' '
	write(PrfUnit,'(A,F6.1)') 'Gap Penalties for Coil Regions =',CoilPen
	write(PrfUnit,'(A)') ' '
	write(PrfUnit,'(A)') 'Cons A    B    C    D    E    F    G'//
     &			 '    H    I    K    L    M    N    P    Q'//
     &			 '    R    S    T    V    W    Y    Z'//
     &			 '   Gap  Len  ..'


	do pos1 = 1,Length
	 Bcol = NINT( (Real(profile(pos1,getresint('D'))) + 
     &		       Real(profile(pos1,getresint('N'))) )
     &			/ 2)
	 Xcol = NINT( (Real(profile(pos1,getresint('E'))) + 
     &		       Real(profile(pos1,getresint('Q'))) )
     &			/ 2)

	 if (MOD((pos1-1),10) .eq. 0 .and. pos1 .gt. 1) then
	  write(PrfUnit,'(A,I5)') '!',pos1
	 end if

	 if (SS(pos1) .eq. 1) then
	  write(PrfUnit,'(A,24I5)') IntToRes(Res(pos1))//IntToSS(SS(pos1)),
     &		profile(pos1,1),Bcol,(profile(pos1,pos2),pos2=2,20),
     &		Xcol,NINT(CoilPen),NINT(CoilPen)
	 else
	  write(PrfUnit,'(A,24I5)') IntToRes(Res(pos1))//IntToSS(SS(pos1)),
     &		profile(pos1,1),Bcol,(profile(pos1,pos2),pos2=2,20),
     &		Xcol,100,100
	 end if

	end do

	 write(PrfUnit,'(A)') ' *    0    0    0    0    0    0    '//
     &		'0    0    0    0    0    0    0    0    0    0    '//
     &		'0    0    0    0    0    0'

 999	continue

	stop
	end

* Reads the Scoring table

	logical function ReadScoringTable(Infos,TableUnit)

	implicit none

	integer maxRes,maxSS,maxX,maxY
	parameter (maxRes=21,maxSS=3,maxX=50,maxY=50)

	integer TableUnit
	character line*80
	integer Infos(MaxRes,MaxSS,MaxY,MaxX)
	integer index1,index2,index3,index4
	integer pos1/0/,pos2,pos3
	character threetoone*1
	integer getresint, getssint

	ReadScoringTable = .false.

	do while(INDEX(line,'..') .eq. 0)
	 read(TableUnit,'(A)') line
	end do

	do while(.not. ReadScoringTable)
	 read(TableUnit,'(A)',end=10) line
	 index1 = GetResInt(ThreeToOne(line(1:3)))
	 index2 = GetSSInt(line(5:5))
	 do pos1 = 1,MaxY
	  read(TableUnit,'(26I5)') 
     &		(Infos(index1,index2,pos1,pos2),pos2=1,MaxX/2+1)
	  read(TableUnit,'(24I5)') 
     &		(Infos(index1,index2,pos1,pos2),pos2=MaxX/2+2,MaxX)
	 end do
	end do

	Return	
 10	continue

	ReadScoringTable = .true.

	return

	end ! ReadScoringTable

********************************************************************
********************************************************************
********************************************************************
********************************************************************
********************************************************************
* Reads a .env file

	logical function ReadEnvFile(Res,SS,Ab,Fp,EnvUnit,Length)

	implicit none

	character line*80
	integer Res(*),SS(*),EnvUnit,pos1
	real Ab(*),Fp(*)
	integer getresint,getssint
	character threetoone, tempC*1
	integer Length

	ReadEnvFile = .false.

	do while(INDEX(line,'..') .eq. 0)
	 read(EnvUnit,'(A)') line
	end do

	pos1 = 0

	do while (.not.ReadEnvFile)
	 read(EnvUnit,'(A)',end=10) line
	 pos1 = pos1 + 1	

	 TempC = ThreeToOne(line(11:13))		 

c	write(6,*) tempc

	 Res(pos1) = GetResInt(TempC)		 


	 SS(pos1) = GetSSInt(line(30:30))
	 read(line(14:20),'(F7.1)') Ab(pos1)
	 read(line(21:27),'(F7.2)') Fp(pos1)

c	write(6,*) pos1

	end do
		
	return

 10	continue

	Length = pos1

	ReadEnvFile = .true.

	return
	end
************************************************************************
************************************************************************
************************************************************************
************************************************************************

	character function threetoone(threelet)

	implicit none

	character threelet*3
	integer B
	character threeres(21)*3

	data threeres / 'ALA','CYS','ASP','GLU','PHE','GLY','HIS', 
     &		        'ILE','LYS','LEU','MET','ASN','PRO','GLN',
     &     	        'ARG','SER','THR','VAL','TRP','TYR','GAP' /

	character ResType(21)*1

	data ResType /'A','C','D','E','F','G','H','I','K','L','M',
     &		      'N','P','Q','R','S','T','V','W','Y','$'/




	do B = 1,21
	 if (threelet .eq. threeres(B)) then
	  threetoone = ResType(B)
	  Return
	 end if
	end do
	
	threetoone = '?'

	return
	end

***********************************************************************
***********************************************************************
***********************************************************************
***********************************************************************
	Integer function GetResInt(Res)

	implicit none

	integer NumResTypes
	parameter (NumResTypes=21)
	integer B


c	line = 'ALA CYS ASP GLU PHE GLY HIS ILE LYS LEU MET ASN PRO GLN '//
c    &     	       'ARG SER THR VAL TRP TYR GAP '

	character Res*1, ResType(NumResTypes)*1
	data ResType /'A','C','D','E','F','G','H','I','K','L','M',
     &		      'N','P','Q','R','S','T','V','W','Y','$'/

	do B = 1,NumResTypes
	 if (Res .eq. ResType(B)) then
	  GetResInt = B
	  Return
	 end if
	end do
	
	GetResInt = 0

	Return

	End
***********************************************************************
***********************************************************************
***********************************************************************

***********************************************************************
***********************************************************************
***********************************************************************
***********************************************************************
	character function IntToRes(Int)

	implicit none

	integer NumResTypes
	parameter (NumResTypes=21)
	integer B, int


c	line = 'ALA CYS ASP GLU PHE GLY HIS ILE LYS LEU MET ASN PRO GLN '//
c    &     	       'ARG SER THR VAL TRP TYR GAP '

	character Res*1, ResType(NumResTypes)*1
	data ResType /'A','C','D','E','F','G','H','I','K','L','M',
     &		      'N','P','Q','R','S','T','V','W','Y','$'/


	if (Int .lt. 1 .or. Int .gt. NumResTypes) then
	 IntToRes = '?'
	 Return
	else
	 IntToRes = ResType(Int)
	end if	

	Return

	End
***********************************************************************
***********************************************************************
***********************************************************************
***********************************************************************
***********************************************************************
***********************************************************************
***********************************************************************
	character function IntToSS(Int)

	implicit none

	integer NumSSTypes
	parameter (NumSSTypes=3)
	integer B, int


	character SSType(NumSSTypes)*1
	data SSType /'C','H','S'/


	if (Int .lt. 1 .or. Int .gt. NumSSTypes) then
	 IntToSS = '?'
	 Return
	else
	 IntToSS = SSType(Int)
	end if	

	Return

	End
***********************************************************************
***********************************************************************
***********************************************************************

***********************************************************************
***********************************************************************
***********************************************************************
	Integer function GetSSInt(SS)

	implicit none

	integer NumSSTypes
	parameter (NumSSTypes=3)
	integer B

	character SS*1, SSType(NumSSTypes)*1
	data SSType /'C','H','S'/

	do B = 1,NumSSTypes
	 if (SS .eq. SSType(B)) then
	  GetSSInt = B
	  Return
	 end if
	end do

	GetSSInt = 1

	Return
	End	

***********************************************************************
***********************************************************************
***********************************************************************
***********************************************************************
