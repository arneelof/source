C THIS PROGRAM WILL READ IN A LIST OF .PAIR FILES FROM PAIR.LIS
C IT WILL READ IN THE PDB POSITION NUMBER, AMINO ACID, THE AREA BURIED, 
C FRACTION POLAR, THE SS STRUCTURE AND DISCRETE ENVIRONMET FROM THE .ENV FILES
C IT WILL WRITE OUT A NEW .PAIRENV FILE THAT INCLUDES FOR EACH CHAIN
C THE RESIDUE TYPE, SS, ENV, AB, AND FP.




	program getenvforpairs


	implicit none

	integer maxpairs,maxres
	Parameter (maxpairs=500,maxres=2000)

	character line*80,envfiles(2,maxpairs)*80,file1*80,file2*80
	character pdbfiles(2,maxpairs)*80, pairs(maxpairs)*80
	character pdb1*50, pdb2*50
	character pdbseq(2,maxres),prfseqenv(2,maxres)*4
	character*80 pdb(2000), pdbnew(2000), pdbsubs(2000)
	integer B,C,D,E,F,G,H,I
	integer numpdb, numpdbnew, numpdbsubs
	integer numpairs, pdbnum(2,maxres), pdbcount(2), seqcount(2)
	real Ab(2,maxres), Fp(2,maxres)
	integer pdbPos(2,maxres)
	integer inx1, inx2, inx3, inx4, inx5, inx6


C	READ IN LIST OF .PAIR FILES

	print*, 'Enter name of file with pairs listed.'
	read(5,'(A)') file1 
	open(1,file=file1,status='old',form='formatted')
	numpairs = 0
	do while(.true.)
	 read(1,'(A)',end=10) line
	 B = INDEX(line,'.pair')
	 if (B .ne. 0) then
	  numpairs = numpairs + 1
	  pairs(numpairs) = line(1:B)//'pair'
	 end if
	end do
 10	continue
	close(1)

c	call readdirectorylist(pdb,pdbnew,pdbsubs,numpdb,numpdbnew,
c     &			numpdbsubs)

	do B = 1,numpairs

	  inx1 = index(pairs(B),"/") + 1
	  inx2 = index(pairs(B),"_") - 1
	  inx3 = inx2 + 2
	  inx4 = index(pairs(B),".") - 1

	  inx5 = inx2 - inx1 + 1
	  inx6 = inx4 - inx3 + 1

	  pdb1 = pairs(B)(inx1:inx2)
	  pdb2 = pairs(B)(inx3:inx4)

	  if (inx5 .eq. 4) then
	   pdbfiles(1,B) = '/friedel/pdb/pdb'//pdb1(1:inx5)//'.ent'
	  else
	   pdbfiles(1,B) = '/usr2/users/dwrice/pdb_subs/'//pdb1(1:inx5)//'.pdb'
	  end if

	  if (inx6 .eq. 4) then
	   pdbfiles(2,B) = '/friedel/pdb/pdb'//pdb2(1:inx6)//'.ent'
	  else
	   pdbfiles(2,B) = '/usr2/users/dwrice/pdb_subs/'//pdb2(1:inx6)//'.pdb'
	  end if

	  envfiles(1,B) =
     &       '/joule2/users/dwrice/shared/homenv/'//pdb1(1:inx5)//'.env'
	  envfiles(2,B) =
     &       '/joule2/users/dwrice/shared/homenv/'//pdb2(1:inx6)//'.env'


c	  print*, envfiles(1,B)
c	  print*, envfiles(2,B)

	end do 

c	  call getdir(pairs,numpairs,maxpairs,pdbfiles,pdb,pdbnew,
c     &		envfiles,pdbsubs,numpdb,numpdbnew,numpdbsubs,B,C)


	do B = 1,numpairs
c	 call getpdbsequences(pdbseq,pdbnum,
c     &		pdbfiles,maxpairs,maxres,B,pdbcount)
	 call getenvseqpos(prfseqenv,envfiles,maxres,maxpairs,
     &		seqcount,B,Ab,Fp,pdbPos)
c	 do C = 1,2
c	  call alignseq(pdbseq,pdbnum,
c     &		pdbcount(C), prfseqenv,seqcount(C),C)
c	 end do

	 call writenewpairfile(prfseqenv,pdbnum,pdbcount,seqcount,
     &		pairs(B),maxres,Ab,Fp,pdbPos)
	end do
	stop
	end !!main
**********************************************************************
**********************************************************************
**********************************************************************
	subroutine writenewpairfile(prfseqenv,pdbnum,pdbcount,
     &		seqcount,pairs,maxres,Ab,Fp,pdbPos)

	implicit none

	integer maxres
	character pairs*80
	character*132 line
	character*4 prfseqenv(2,maxres),tempenv(2)
	integer pdbnum(2,maxres), pdbcount(2),seqcount(2)
	integer endline,B,C,D,E,F,G,H,pos1,pos2
	real Ab(2,maxres), Fp(2,maxres)
	integer pdbPos(2,maxres)
	real tempAb(2), tempFp(2)

	B = INDEX(pairs,'.') - 1
	open(1,file=pairs,form='formatted',status='old',
     &		readonly)
	open(2,file=pairs(1:B)//'.pairenv',
     &		form='formatted',status='unknown')

	do while (INDEX(line,'Distance') .eq. 0)
	 read(1,'(A)') line
	 B = endline(line,132)
	 write(2,'(A)') line(1:B)
	end do
	read(1,'(A)')
	write(2,'(A)')
c		       '0        1         2         3
	write(2,'(A)') '       Ab   Fp         Ab   Fp'
	write(2,'(A)') '------------------------------'

	line(1:80) = 'initilize                              first'
	do while(INDEX(line(1:80),'                               '//
     &		'                                             ') .eq. 0)
	 G = 0
	 H = 0
	 read(1,'(A)') line
	 if (line(1:5) .ne. '     ') Then
	  G = 1
	  read(line(1:5),'(BNI)') pos1
	 end if
	 if (line(12:15) .ne. '     ') then
	  H = 1
	  read (line(12:15),'(BNI)') pos2
	 end if

	 if (G .eq. 1 .or. H .eq. 1) then

	  if (G .eq. 1) then
	   do B = 1,SeqCount(1)
	    if (pdbPos(1,B) .eq. pos1) then
	     tempenv(1) = prfseqenv(1,B)
	     tempAb(1) = Ab(1,B)	
	     tempFp(1) = Fp(1,B)
	     if (line(6:6) .eq. 'O') pdbPos(1,B) = -999
	     goto 10
	    end if
	   end do
	   write(6,'(A)') '!!! ERROR !!! file = '//'pdbpairs:'//pairs
	   write(6,'(A,I)') '!!! ERROR !!! L. pdb position not found. ',pos1
	   tempenv(1) = '    '
	   tempAb(1) = -1.0	
	   tempFp(1) = -1.0	
	  end if
 10	  continue
	  if (G .eq. 0) then
	   tempenv(1) = '   '
	   tempAb(1) = -1.0	
	   tempFp(1) = -1.0	
	  end if
	  if (H .eq. 1) then
	   do C = 1,seqcount(2)
	    if (pdbPos(2,C) .eq. pos2) then
	     tempenv(2) = prfseqenv(2,C)
	     tempAb(2) = Ab(2,C)	
	     tempFp(2) = Fp(2,C)	
	     if (line(16:16) .eq. 'O') pdbPos(2,C) = -999
	     goto 20
	    end if
	   end do
	   write(6,'(A)') '!!! ERROR !!! file = '//'pdbpairs:'//pairs
	   write(6,'(A,I)') '!!! ERROR !!! R. pdb position not found. ',pos2
	   tempenv(2) = '    '
	   tempAb(2) = -1.0	
	   tempFp(2) = -1.0	
	  end if
 20	  continue
	  if (H .eq. 0) then
	   tempenv(2) = '   '
	   tempAb(2) = -1.0
	   tempFp(2) = -1.0	
	  end if

	  D = endline(line,132)
	  
	  if (H .eq. 1 .and. G .eq. 1) then
	   write(2,'(2(A,F6.1,F5.2,1X),A)') 
     &		tempenv(1),tempAb(1),tempFp(1), 
     &		tempenv(2),tempAb(2),tempFp(2),line(1:D)
	  else if (G .eq. 1) then
	   write(2,'(A,F6.1,F5.2,1X,16X,A)') 
     &		tempenv(1),tempAb(1),tempFp(1), 
     &		line(1:D)
	  else
	   write(2,'(16X,A,F6.1,F5.2,1X,A)')  
     &		tempenv(2),tempAb(2),tempFp(2),line(1:D)
	  end if
	 end if
	end do
	write(2,'(A)')
	do B = 1,2
	 read(1,'(A)') line
	 D = endline(line,132)
	 write(2,'(A)') line(1:D)
	end do
	close(1)
	close(2)
	return
	end !!!!!!!!!!!!!!!!writenewpairfile
***********************************************************************
***********************************************************************
***********************************************************************
	Integer function endline(line,maxlength)

	implicit none

	integer maxlength
	integer B,C
	character*132 line

	do B = 1,maxlength
	 C = (maxlength + 1) - B
	 if (line(C:C) .ne. ' ') then
	  endline = C
 	  return
	 end if
	end do
	endline = 0
	return
	end
***********************************************************************
***********************************************************************
***********************************************************************
C	Needleman and Wunch

	subroutine alignseq(pdbseq,seqnum,seq1num,
     &		prfseqenv,seq2num,oftwo)

	CHARACTER*1 seq1(2000),seq2(2000),pdbseq(2,2000)
	Character*4 prfseqenv(2,2000),tempenv(2000)
	Character*80 seqline
	Character*1000 seq1al,seq2al
	Integer typ1(1000),typ2(1000),bigcol,bigrow,tempnum(2000)
	Integer mdm(22,22),prerow(1000,1000),precol(1000,1000)
	Real  mtrx(1000,1000)
	Real R1,Rold,Rnew
        INTEGER I,J,K,L,M,N,seqnum(2,2000),B,oftwo
	Integer Ia,Ib,alncol(1000),alnrow(1000)
	Integer alncol2(1000),alnrow2(1000)
	Integer Seq1num,Seq2num,alnum,pos1,pos2

C read in Dahoff matrix
	open(1,file='m251b:mdm.250',status='old',form='formatted',readonly)
	do I=1,22
	  read(1,10) mdm(I,1),mdm(I,2),mdm(I,3),mdm(I,4),mdm(I,5),
     &		     mdm(I,6),mdm(I,7),mdm(I,8),mdm(I,9),mdm(I,10),
     &		     mdm(I,11),mdm(I,12),mdm(I,13),mdm(I,14),mdm(I,15),
     &		     mdm(I,16),mdm(I,17),mdm(I,18),mdm(I,19),mdm(I,20),
     &		     mdm(I,21),mdm(I,22)
 10	  format(2x,22I3)
	end do
	close(unit=1)

c read in sequences 

	do I = 1,seq2num
	 seq2(I) = prfseqenv(oftwo,I)(1:1)
	end do

	do I = 1,seq1num
	 seq1(I) = pdbseq(oftwo,I)
	end do

c give each amino acid an integer for accessing scoring table
	do I = 1,seq1num
	  if (seq1(I) .eq. 'A' .or. seq1(I) .eq. 'a') then
	    typ1(I) = 1
	  else if (seq1(I) .eq. 'R' .or. seq1(I) .eq. 'r') then
	    typ1(I) = 2
	  else if (seq1(I) .eq. 'N' .or. seq1(I) .eq. 'n') then
	    typ1(I) = 3
	  else if (seq1(I) .eq. 'D' .or. seq1(I) .eq. 'd') then
	    typ1(I) = 4
	  else if (seq1(I) .eq. 'C' .or. seq1(I) .eq. 'c') then
	    typ1(I) = 5
	  else if (seq1(I) .eq. 'Q' .or. seq1(I) .eq. 'q') then
	    typ1(I) = 6
	  else if (seq1(I) .eq. 'E' .or. seq1(I) .eq. 'e') then
	    typ1(I) = 7
	  else if (seq1(I) .eq. 'G' .or. seq1(I) .eq. 'g') then
	    typ1(I) = 8
	  else if (seq1(I) .eq. 'H' .or. seq1(I) .eq. 'h') then
	    typ1(I) = 9
	  else if (seq1(I) .eq. 'I' .or. seq1(I) .eq. 'i') then
	    typ1(I) = 10
	  else if (seq1(I) .eq. 'L' .or. seq1(I) .eq. 'l') then
	    typ1(I) = 11
	  else if (seq1(I) .eq. 'K' .or. seq1(I) .eq. 'k') then
	    typ1(I) = 12
	  else if (seq1(I) .eq. 'M' .or. seq1(I) .eq. 'm') then
	    typ1(I) = 13
	  else if (seq1(I) .eq. 'F' .or. seq1(I) .eq. 'f') then
	    typ1(I) = 14
	  else if (seq1(I) .eq. 'P' .or. seq1(I) .eq. 'p') then
	    typ1(I) = 15
	  else if (seq1(I) .eq. 'S' .or. seq1(I) .eq. 's') then
	    typ1(I) = 16
	  else if (seq1(I) .eq. 'T' .or. seq1(I) .eq. 't') then
	    typ1(I) = 17
	  else if (seq1(I) .eq. 'W' .or. seq1(I) .eq. 'w') then
	    typ1(I) = 18
	  else if (seq1(I) .eq. 'Y' .or. seq1(I) .eq. 'y') then
	    typ1(I) = 19
	  else if (seq1(I) .eq. 'V' .or. seq1(I) .eq. 'v') then
	    typ1(I) = 20
	  else if (seq1(I) .eq. 'X' .or. seq1(I) .eq. 'x') then
	    typ1(I) = 21
	  else if (seq1(I) .eq. '?' .or. seq1(I) .eq. '?') then
	    typ1(I) = 22
	  else
	    print*, '!!! ERROR typing amino acid !!!'
	  end if
	end do

	do I = 1,seq2num
	  if (seq2(I) .eq. 'A' .or. seq2(I) .eq. 'a') then
	    typ2(I) = 1
	  else if (seq2(I) .eq. 'R' .or. seq2(I) .eq. 'r') then
	    typ2(I) = 2
	  else if (seq2(I) .eq. 'N' .or. seq2(I) .eq. 'n') then
	    typ2(I) = 3
	  else if (seq2(I) .eq. 'D' .or. seq2(I) .eq. 'd') then
	    typ2(I) = 4
	  else if (seq2(I) .eq. 'C' .or. seq2(I) .eq. 'c') then
	    typ2(I) = 5
	  else if (seq2(I) .eq. 'Q' .or. seq2(I) .eq. 'q') then
	    typ2(I) = 6
	  else if (seq2(I) .eq. 'E' .or. seq2(I) .eq. 'e') then
	    typ2(I) = 7
	  else if (seq2(I) .eq. 'G' .or. seq2(I) .eq. 'g') then
	    typ2(I) = 8
	  else if (seq2(I) .eq. 'H' .or. seq2(I) .eq. 'h') then
	    typ2(I) = 9
	  else if (seq2(I) .eq. 'I' .or. seq2(I) .eq. 'i') then
	    typ2(I) = 10
	  else if (seq2(I) .eq. 'L' .or. seq2(I) .eq. 'l') then
	    typ2(I) = 11
	  else if (seq2(I) .eq. 'K' .or. seq2(I) .eq. 'k') then
	    typ2(I) = 12
	  else if (seq2(I) .eq. 'M' .or. seq2(I) .eq. 'm') then
	    typ2(I) = 13
	  else if (seq2(I) .eq. 'F' .or. seq2(I) .eq. 'f') then
	    typ2(I) = 14
	  else if (seq2(I) .eq. 'P' .or. seq2(I) .eq. 'p') then
	    typ2(I) = 15
	  else if (seq2(I) .eq. 'S' .or. seq2(I) .eq. 's') then
	    typ2(I) = 16
	  else if (seq2(I) .eq. 'T' .or. seq2(I) .eq. 't') then
	    typ2(I) = 17
	  else if (seq2(I) .eq. 'W' .or. seq2(I) .eq. 'w') then
	    typ2(I) = 18
	  else if (seq2(I) .eq. 'Y' .or. seq2(I) .eq. 'y') then
	    typ2(I) = 19
	  else if (seq2(I) .eq. 'V' .or. seq2(I) .eq. 'v') then
	    typ2(I) = 20
	  else if (seq2(I) .eq. 'X' .or. seq2(I) .eq. 'x') then
	    typ2(I) = 21
	  else if (seq2(I) .eq. '?' .or. seq2(I) .eq. '?') then
	    typ2(I) = 22
	  else
	    print*, 'seq2(I) has no amino acid !!! ERROR typing amino acid'
	  end if
	end do

c first column of scoring matrix
	do I = 1,seq1num
	  mtrx(I,1) = Real(mdm(typ1(I),typ2(1)))
	end do
c first row of scoring matrix
	do I = 1,seq2num
	  mtrx(1,I) = Real(mdm(typ1(1),typ2(I)))
	end do
c rest of scoring matrix
	do I = 2,seq2num
	 do J = 2,seq1num
	  R1 = Real(mdm( typ2(I),typ1(J) ))
	  Rold = -1000.0
	  do K = 1,I-1
	   Rnew = R1 + mtrx(J-1,K)
	   if (K .eq. (I-2)) then
	    Rnew = Rnew - 5.0
	   else if (K .lt. (I-2)) then
	    Rnew = Rnew - 5.0 - 0.2*(I-K-2)
	   end if
	   if (Rnew .gt. Rold) then
	    Rold = Rnew
	    precol(J,I) = K
	    prerow(J,I) = J-1
	   end if
	  end do

	  do K = 1,J-1
	   Rnew = R1 + mtrx(K,I-1)
	   if (K .eq. (J-2)) then
	    Rnew = Rnew - 5.0
	   else if (K .lt. (J-2)) then
	    Rnew = Rnew - 5.0 - 0.2*(J-K-2)
	   end if
	   if (Rnew .gt. Rold) then
	    Rold = Rnew
	    precol(J,I) = I-1
	    prerow(J,I) = K
	   end if
	  end do

	  mtrx(J,I) = Rold

	 end do
	end do
c find largest value in last row or column
	Rold = -1000.0
	do I = 1,seq1num
	Rnew =  mtrx(I,seq2num)
	 if (Rnew .gt. Rold) then
	  Rold = Rnew
	  bigrow = I
	  bigcol = seq2num
	 end if
	end do

	do I = 1,seq2num
	 Rnew = mtrx(seq1num,I)
	 if (Rnew .gt. Rold) then
	  Rold = Rnew
	  bigrow = seq1num
	  bigcol = I
	 end if
	end do
	print*, 'Top score is:',rnew

c but best path rows and columns into arrays

	do I = 1,1000
	 alncol(I) = 0
	 alnrow(I) = 0
	end do
	alncol(1) = bigcol
	alnrow(1) = bigrow
	alnum = 1
	do I = 2,1000
	 alnrow(I) = prerow(bigrow,bigcol)
	 alncol(I) = precol(bigrow,bigcol)
	 bigrow = alnrow(I)
	 bigcol = alncol(I)
	 alnum = alnum + 1
	 if (bigrow .eq. 1 .or. bigcol .eq. 1) then
	  goto 100
	 end if
	end do
 100	continue

c invert array

	do I = 1,alnum
	 alnrow2(I) = alnrow(alnum + 1 - I)
	 alncol2(I) = alncol(alnum + 1 - I)
	 print*, alnrow2(I),alncol2(I)
	end do

	pos1 = 0
	if (alncol2(1) .gt. alnrow2(1)) then
	 pos1 = alncol2(1) - alnrow2(1)
	 do I = 1,pos1
	  seq1al(I:I) = '_'
	  seq2al(I:I) = seq2(I)
	 end do
	 pos1 = pos1 + 1
	 seq1al(pos1:pos1) = seq1(1)
	 seq2al(pos1:pos1) = seq2(I)
	else if (alnrow2(1) .gt. alncol2(1)) then
	 pos1 = alnrow2(1) - alncol2(1)
	 do I = 1,pos1
	  seq2al(I:I) = '_'
	  seq1al(I:I) = seq1(I)
	 end do
	 pos1 = pos1 + 1
	 seq1al(pos1:pos1) = seq1(I)
	 seq2al(pos1:pos1) = seq2(1)
	else if (alnrow2(1) .eq. alncol2(1)) then
	 pos1 = 1
	 seq1al(1:1) = seq1(1)
	 seq2al(1:1) = seq2(1)
	end if	 

	do I = 1,alnum-1
	 K = 0
	 J = (alncol2(I+1)-alncol2(I)) - (alnrow2(I+1)-alnrow2(I))
	 if (J.gt.0) then
	  do K=1,J
	   pos1 = pos1 + 1
	   seq1al(pos1:pos1) = '_'
	   seq2al(pos1:pos1) = seq2(alncol2(I) + K)
	  end do
	  pos1 = pos1+1
	  seq1al(pos1:pos1) = seq1(alnrow2(I+1))
	  seq2al(pos1:pos1) = seq2(alncol2(I+1))
	 else if (J.lt.0) then
	  do K=1,ABS(J)
	   pos1 = pos1 + 1
	   seq2al(pos1:pos1) = '_'
	   seq1al(pos1:pos1) = seq1(alnrow2(I) + K)
	  end do
	  pos1 = pos1+1
	  seq1al(pos1:pos1) = seq1(alnrow2(I+1))
	  seq2al(pos1:pos1) = seq2(alncol2(I+1))
	 else if (J .eq. 0) then
	  pos1 = pos1+1
	  seq1al(pos1:pos1) = seq1(alnrow2(I+1))
	  seq2al(pos1:pos1) = seq2(alncol2(I+1))
	 end if
	end do
	pos2 = pos1
	if (seq1num.gt.alnrow2(alnum)) then
	 J = seq1num - alnrow2(alnum)
	 do I = 1,J
	  pos1 = pos1 + 1
	  seq1al(pos1:pos1) = seq1(alnrow2(alnum) + I)
	 end do
	end if
	if (seq2num.gt.alncol2(alnum)) then
	 J = seq2num - alncol2(alnum)
	 do I = 1,J
	  pos2 = pos2 + 1
	  seq2al(pos2:pos2) = seq2(alncol2(alnum) + I)
	 end do
	end if

C	SEQ1 AND SEQ2 ARE NOW ALIGNED IN SEQ1AL AND SEQ2AL
C	ADJUST ARRAYS SEQ1,PRFSEQENV, AND SEQNUM TO CORRESPOND TO ALIGHMENT
C	NOW POSITION X IN PRFSEQENV CORRESPONDS WITH PDB RES NUMBER AT
C	POS X OF SEQNUM

	B = 0
	do I = 1,pos1
	 PDBSEQ(OFTWO,I) = SEQ1AL(I:I)
	 if (seq1al(I:I) .ne. '_') then
	  B = B + 1
	  tempnum(I) = seqnum(oftwo,B)
	 else
	  tempnum(I) = 0
	 end if
	end do
	do I = 1,pos1
	 seqnum(oftwo,I) = tempnum(I)
	end do

	B = 0
	do I = 1,pos2
	 if (seq2al(I:I) .ne. '_') then
	  B = B + 1
	  tempenv(I) = prfseqenv(oftwo,B)
	 else
	  tempenv(I) = '!!!!'
	 end if
	end do
	do I = 1,pos2
	 prfseqenv(oftwo,I) = tempenv(I)
	end do

	seq1num = pos1
	seq2num = pos2

	return
	end
**********************************************************************
**********************************************************************
**********************************************************************
	subroutine getenvseqpos(prfseqenv,envfiles,maxres,maxpairs,
     &			seqcount,B,Ab,Fp,pdbPos)

	implicit none

	integer maxres,maxpairs
	character prfseqenv(2,maxres)*4,res
	character line*132, envfiles(2,maxpairs)*80
	integer B,C,D,E,F,G,seqcount(2)
	integer pdbPos(2,maxres)
	real Ab(2,maxres), Fp(2,maxres)
	character oldchain, newchain
	do C =  1,2
	 seqcount(C) = 0
	 open(1,file=envfiles(C,B),form='formatted',status='old',readonly)

	 do while(INDEX(line,'..') .eq. 0)
	  read(1,'(A)') line
	 end do

	 read(1,'(A)',end=10) line
	 oldchain = line(2:2)
	 backspace(1)

	 do while(.true.)
	  read(1,'(A)',end=10) line
	  newchain = line(2:2)
	  if (newchain .eq. oldchain) then
	   seqcount(C) = seqcount(C) + 1
	   call threetoone(res,line(11:13))
	   prfseqenv(C,seqcount(C)) = res//line(30:30)//line(33:34)
	   read(line(14:21),*) Ab(C,seqcount(C))
	   read(line(22:28),*) Fp(C,seqcount(C))
	   read(line(3:8),*) pdbPos(C,seqcount(C))
	  else
	   write(6,'(A)') ' Multiple chain detected: '//envfiles(c,b)
	   goto 10
	  end if
	 end do
 10	 continue
	 close(1)
	end do
	return
	end
************************************************************************
************************************************************************
************************************************************************
	subroutine getpdbsequences(pdbseq,pdbnum,pdbfiles,maxpairs,
     &		maxres,B,pdbcount)

	implicit none

	integer maxpairs,maxres
	character line*80, pdbfiles(2,maxpairs)*80
	character pdbseq(2,maxres),chain,newchain
	integer B,C,D,E,F
	integer pdbnum(2,maxres),resnum,pdbcount(2)

	do C = 1,2
	 pdbnum(C,1) = 0
	 open(1,file=pdbfiles(C,B),form='formatted',status='old',readonly)
	 pdbcount(C) = 0
	 do while (.true.)
	  read(1,'(A)',end=10) line
	  if (line(1:4) .eq. 'ATOM') then
	   read(line(23:26),'(BNI)') resnum
	   read(line(22:22),'(A)') newchain
	   if (pdbcount(C) .eq. 0) then
	    pdbcount(C) = pdbcount(C) + 1
	    chain = newchain
	    call threetoone(pdbseq(C,pdbcount(C)),line(18:20))
	    pdbnum(C,pdbcount(C)) = resnum
	   else if (newchain .eq. chain .and. resnum .ne.
     &		pdbnum(C,pdbcount(C))) then
	    pdbcount(C) = pdbcount(C) + 1
	    call threetoone(pdbseq(C,pdbcount(C)),line(18:20))
	    pdbnum(C,pdbcount(C)) = resnum
	   end if
	  end if
	 end do
 10	 continue
	 close(1)
	end do
	return
	end !!!!!!!!!!!!!!!! getpdbsequences
******************************************************************
******************************************************************
******************************************************************
******************************************************************
	subroutine readdirectorylist(pdb,pdbnew,pdbsubs,numpdb,numpdbnew,
     &			numpdbsubs)

	implicit none

	character line*80
	character*80 pdb(2000),pdbnew(2000),pdbsubs(2000)
	integer B,C,D,E,F,G,H,I
	integer numpdb,numpdbnew, numpdbsubs


	open(1,file='pdb.lis',form='formatted',status='old')
	numpdb = 0
	do while(.true.)
	 numpdb = numpdb + 1
	 read(1,'(A)',end=10) pdb(numpdb)
	 do B = 1,INDEX(pdb(numpdb),';') - 1
	  call getlowercase(pdb(numpdb)(B:B))
	 end do
	end do
 10	continue
	close(unit=1)

	open(1,file='pdbnew.lis',form='formatted',status='old')
	numpdbnew = 0
	do while(.true.)
	 numpdbnew = numpdbnew + 1
	 read(1,'(A)',end=20) pdbnew(numpdbnew)
	 do B = 1,INDEX(pdbnew(numpdbnew),';') - 1
	  call getlowercase(pdbnew(numpdbnew)(B:B))
	 end do
	end do
 20	continue
	close(unit=1)

	open(1,file='pdbsubs.lis',form='formatted',status='old')
	numpdbsubs = 0
	do while(.true.)
	 numpdbsubs = numpdbsubs + 1
	 read(1,'(A)',end=30) pdbsubs(numpdbsubs)
	 do B = 1,INDEX(pdb(numpdbsubs),';') - 1
	  call getlowercase(pdbsubs(numpdbsubs)(B:B))
	 end do
	end do
 30	continue
	close(unit=1)
	return
	end !!!!!!!!!!! readdirectorylist
********************************************************************
********************************************************************
********************************************************************
********************************************************************
	subroutine getdir(pairs,numpairs,maxpairs,pdbfiles,pdb,pdbnew,
     &		envfiles,pdbsubs,numpdb,numpdbnew,numpdbsubs,B,G)

	implicit none

	integer maxpairs
	character line*80, envfiles(2,maxpairs)*50,pdbfiles(2,maxpairs)*50
	character*80 pdb(2000), pdbnew(2000), pdbsubs(2000)
	character pairs(maxpairs)*50
	integer B,C,D,E,F,G,H,I, numpairs
	integer numpdb,numpdbnew,numpdbsubs

C	FIND CORRECT DIRECTORY

	if (G .eq. 1) then
	 C = INDEX(pairs(B),'_') - 1
	 D = INDEX(pairs(B),'.') - 1
	 do E = 1,D
	  call getlowercase(pairs(B)(E:E))
	 end do
	 pdbfiles(2,B) = pairs(B)(C+2:D)
	 pdbfiles(1,B) = pairs(B)(1:C)
	else
	 C = INDEX(pdbfiles(2,B),' ') - 1
	end if

	do E = 1,numpdb
	 if (INDEX(pdb(E),pdbfiles(G,B)(1:C)) .ne. 0) then
	  envfiles(G,B) = 'env:'//pdbfiles(G,B)(1:C)//'.env'
	  pdbfiles(G,B) = 'pdb:pdb'//pdbfiles(G,B)(1:C)//'.ent'
	  return
	 end if
	end do 

	do E = 1,numpdbnew
	 if (INDEX(pdbnew(E),pdbfiles(G,B)(1:C)) .ne. 0) then
	  envfiles(G,B) = 'env:'//pdbfiles(G,B)(1:C)//'.env'
	  pdbfiles(G,B) = 'pdbnew:pdb'//pdbfiles(G,B)(1:C)//'.ent'
	  return
	 end if
	end do 

	do E = 1,numpdbsubs
	 if (INDEX(pdbsubs(E),pdbfiles(G,B)(1:C)) .ne. 0) then
	  envfiles(G,B) = 'subprof:'//pdbfiles(G,B)(1:C)//'.env'
	  pdbfiles(G,B) = 'pdbsubs:'//pdbfiles(G,B)(1:C)//'.pdb'
	  return
	 end if
	end do 

	return
	end ! getdir
*********************************************************************
*********************************************************************
*********************************************************************
*********************************************************************
	subroutine threetoone(onelet,threelet)

	implicit none

	character threelet*3
	character onelet*1
	integer B,C,D

	do B = 1,3
	 call getlowercase(threelet(B:B))
	end do

	if (threelet .eq. 'ala') then
	 onelet = 'A'
	else if (threelet .eq. 'cys') then
	 onelet = 'C'
	else if (threelet .eq. 'asp') then
	 onelet = 'D'
	else if (threelet .eq. 'glu') then
	 onelet = 'E'
	else if (threelet .eq. 'phe') then
	 onelet = 'F'
	else if (threelet .eq. 'gly') then
	 onelet = 'G'
	else if (threelet .eq. 'his') then
	 onelet = 'H'
	else if (threelet .eq. 'ile') then
	 onelet = 'I'
	else if (threelet .eq. 'lys') then
	 onelet = 'K'
	else if (threelet .eq. 'leu') then
	 onelet = 'L'
	else if (threelet .eq. 'met') then
	 onelet = 'M'
	else if (threelet .eq. 'asn') then
	 onelet = 'N'
	else if (threelet .eq. 'pro') then
	 onelet = 'P'
	else if (threelet .eq. 'gln') then
	 onelet = 'Q'
	else if (threelet .eq. 'arg') then
	 onelet = 'R'
	else if (threelet .eq. 'ser') then
	 onelet = 'S'
	else if (threelet .eq. 'thr') then
	 onelet = 'T'
	else if (threelet .eq. 'val') then
	 onelet = 'V'
	else if (threelet .eq. 'trp') then
	 onelet = 'W'
	else if (threelet .eq. 'tyr') then
	 onelet = 'Y'
	else
	 onelet = 'X'
	end if

	return
	end !!!!!!!!!!!1 threetoone
************************************************************************
************************************************************************
************************************************************************
************************************************************************
	subroutine getlowercase(chr)

	implicit none

	character chr

	if (Ichar(chr) .ge. 65 .and.
     &	    Ichar(chr) .le. 90) then
	 chr = Char(Ichar(chr) + 32)
	end if
	return
	end
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
