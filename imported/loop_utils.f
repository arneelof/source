c
	subroutine loop(iseed,ires,nloop,icount,ifail)
c
c	This program constructs the backbone coords step by step
c	i.e., peptide plane by peptide plane.(N,CA,C') from residue ires
c	to residue ires+nloop, with correct (but idealized) stereo chemistry.
c	It leaves the rest of the molecule invariant (hopefully).
c	Since there are four equations to be solved, 2*nloop-4 angles can be chosen
c	arbitrarily...
c	The equations are solved using Newton-Raphson method (see Knapp, 1993).
c	Because there is a linear approximation in the algorithm, the algorithm
c	can fail; in this case, it will be reported it and you just have to rerun it.
c
c	This assumes that omega angle is 180. (no cis peptide).
c
c...	nloop_max in the present version is 8.
c
	include 'marc_1.def'
	parameter (nang=16)
c
	integer	i,j,k,kk,nat,ndi,nref,nseq,ndihed,ires,icount,ifail,ierr
	integer bond(nangtot,4),natom,listatom(nrestot),iseed,nloop
        integer idihed(nresdef),iside(nresdef),list1,list2,ip
	integer	listdihed(nrestot),i1(12),itype(nrestot)
        integer nrotamer(nresdef),nanglerot(nresdef)
c
        real*8  rotamer(nresdef,nrotmax,nangrot),chi(nangside)
	real*8	coord(ncortot),dihed(nangtot)
	real*8	pside(51),R00(3)
	real*8	a1(3),b1(3),c1(3),a(3),b(3),c(3),n(3),n1(3),u1(3),v1(3)
	real*8	pold6(3),p1(3),p2(3),p3(3),p6(3),pnew1(3),pnew3(3),ortho(3)
	real*8	pold1(3),pold3(3),p7(3),p5(3),pnew6(3),target(3),target1(3)
	real*8	kappa1,kappa2,psi1,phi1,psi2,phi2,ang1,ang2,dist
	real*8	pi,pif,const1,const2,const3,const4,ang,dist0,dist1,dist2,score
	real*8	errx,errf,tolf,tolx,eps,val1,val2,val,test,test_orth,dlud
	real*8	rot1(nang),rot(nang)
	real*8  norm1,norm2,norm3
	real*8 	alpha(4,4),beta(4),indx(4),beta_old(4),alpha_old(4,4)
c
c...	this dimensioning depends on the length of the loop (5 in most cases).
c
	real*8	R0(3),R(3,8),R1(3,8),R2(3,8)
	real*8	nvect(3,8),cvect(3,8),avect(3,8),bvect(3,8)
	real*8	nvect0(3,8),cvect0(3,8),avect0(3,8),bvect0(3,8)
c
	real*4	ran2
c
	character 	seq(nrestot)*4
        character       nameat(nresdef,30)*4
        character       backbone(7)*4,backpro(7)*4,backgly(7)*4
c
	common /angle/   dihed,ndihed,listdihed
        common /protein/ nseq,itype,seq
        common /xyz/     coord,natom,listatom
	common /defbond/ nref,bond
        common /default/ nameat,backbone,backpro,backgly,iside,idihed
	common /constants/dist0,dist1,dist2,const1,const2,const3,const4
c
c...	define some data
c
	ifail=1
	ntrial=25
	tolx=0.0001
	tolf=0.0001
c
	pi=acos(-1.)
	pif=pi/180.
c
c...	position pointer at residue ires in coord and dihed lists
c
	ndi=0
	nat = 0
	do 50 i = 1,ires-1
		ndi=ndi+listdihed(i)
		nat=nat+listatom(i)
50	continue
c
c...	get some initial coords (start at residue ires) and target Calpha atom
c
	list1=listatom(ires)
	do 100 k=1,3
		pold6(k)=coord(3*(nat+5-listatom(ires-1))+k)
		p1(k)=coord(3*(nat)+k)
		p3(k)=coord(3*(nat+2)+k)
		p6(k)=coord(3*(nat+5)+k)
100	continue
c
	call diffvect(pold6,p1,a1)
	call normvect(a1,norm1)
	call diffvect(p3,p6,c1)
	call normvect(c1,norm2)
	call diffvect(p3,p1,n1)
	call normvect(n1,norm3)
	if(norm1*norm2*norm3.eq.0) then
		ifail=0
		goto 1700
	endif
	do k=1,3
		avect(k,1)=a1(k)/norm1
		cvect(k,1)=c1(k)/norm2
		nvect(k,1)=n1(k)/norm3
	enddo
	call crossvect(avect(1,1),nvect(1,1),ortho)
	call crossvect(avect(1,1),ortho,b1)
	call normvect(b1,norm1)
	if(norm1.eq.0) then
	    ifail=0
   	    goto 1700
	else
	    do k=1,3
		bvect(k,1)=b1(k)/norm1
	    enddo
	endif
c
	do 120 k=1,3
		avect0(k,1)=avect(k,1)
		bvect0(k,1)=bvect(k,1)
		nvect0(k,1)=nvect(k,1)
		cvect0(k,1)=cvect(k,1)
120	continue
c
c...	define all a(i) and b(i) vectors in the loop, with standard geometry.
c
c...	paste new peptide plane with correct valence angle at Calpha (110.)
c...	deduce c and nprim sequentially.
c
	do 200 i=1,nloop+1
c
		do 150 k=1,3
			avect(k,i+1)=const3*avect(k,i)-const4*bvect(k,i)
			avect0(k,i+1)=avect(k,i+1)
		    	bvect(k,i+1)=const4*avect(k,i)+const3*bvect(k,i)
			bvect0(k,i+1)=bvect(k,i+1)
c
			cvect(k,i)=const1*avect(k,i+1)+const2*bvect(k,i+1)
			cvect0(k,i)=cvect(k,i)
			nvect(k,i+1)=-const1*avect(k,i+1)-const2*bvect(k,i+1)
			nvect0(k,i+1)=nvect(k,i+1)
150		continue		
c
200	continue
c
c...	define R(3,j), j.lt.nloop, target(3) vector and end-to-end vector R0(3)
c
	listn=0
	do i=0,nloop-1
		listn=listn+listatom(ires+i)
	enddo
c
	do k=1,3
		pnew6(k)=coord(3*(nat+listn+5)+k)
		pnew3(k)=coord(3*(nat+listn+2)+k)
	enddo
c
	do k=1,3
	   target(k)=pnew6(k)-pnew3(k)
	   R0(k)=pnew3(k)-p3(k)
	   do j=1,nloop
		R(k,j)=(cvect(k,j)-nvect(k,j+1)+avect(k,j+1))*dist0
	   enddo
	enddo
c
	call normvect(target,norm1)
	if(norm1.eq.0) then
		ifail=0
		goto 1700
	else
		do k=1,3
			target(k)=target(k)/norm1
		enddo
	endif
c
c...	initialize dihedral angles to wild type values.
c
	ndi1=ndi
	do 300 i=1,nloop
		rot(2*(i-1)+1) = pi + dihed(ndi1+1)
		rot1(2*(i-1)+1)= pi + dihed(ndi1+1)
		rot(2*i) = - dihed(ndi1+listdihed(ires+i-1)-1)
		rot1(2*i)= - dihed(ndi1+listdihed(ires+i-1)-1)
		ndi1=ndi1+listdihed(ires+i-1)
300	continue
c
	icount=0
320	continue
c
c...	Find which angles are going to be randomly mutated
c...	(between 1 and 2*nloop) and their new values.
c
	do i=1,2*nloop - 4
c
330		i1(i)=int(2*nloop*ran2(iseed)) + 1
		if(i1(i).lt.1.or.i1(i).gt.2*nloop) goto 330
		do j=1,i-1
			if(i1(i).eq.i1(j)) goto 330
		enddo
c
		rot(i1(i))=ran2(iseed)*2*pi
c		write(6,*)' i1,rot1,rot=',i1(i),rot1(i1(i))/pif,rot(i1(i))/pif
c
	enddo
c
c...	Special case if the mutation concerns a proline and its phi angle.
c
c	do i=1,2*nloop-4
c		jj=ires+int((i1(i)-1)/2)
c		ll=mod(i1(i),2)
c		if(itype(jj).eq.7.and.ll.eq.1) rot(i1(i))=-60.*pif
c	enddo
c
c...	Apply these rotations to nvect, cvect, avect and R, 
c...	starting from the extended form (nvect0, cvect0, avect0).
c
	call newvect(nvect0,cvect0,avect0,nvect,cvect,avect,R,R0,rot,nloop)
c
c...	This is the Newton-Raphson part of the algorithm.
c
	kcount=0
350	continue
c
	do 800 kkk=1,ntrial
		icount=icount+1
		kcount=kcount+1
		call usrfun(R,R0,alpha,beta,nvect,cvect,i1,target,nloop)
		errf=0
		do 400 i=1,4
			errf=errf+abs(beta(i))
400		continue
		if(errf.le.tolf) goto 1000
c
		do i=1,4
		   beta_old(i)=beta(i)
		   do j=1,4
			alpha_old(i,j)=alpha(i,j)
		   enddo
		enddo
c
		call ludcmp(alpha,indx,dlud)
c		call lubksb(alpha,indx,beta)
      		call mprove(alpha_old,alpha,indx,beta_old,beta)
c
c...		check what is going on with these small rotations just found
c		call check_small(R,beta,nvect,cvect,i1,nloop)
c
		errx=0.
		do i=1,4
			errx=errx+abs(beta(i))
		enddo
c
		if(errx.gt.2.) then
			factor=errx
		else
			factor=2.
		endif
c
		i=0
		do 600 ii=1,2*nloop
		   	do j=1,2*nloop-4
		   		if(ii.eq.i1(j)) goto 600
			enddo
			i=i+1
			rot1(ii)=rot1(ii) - 2.*beta(i)/factor
			rot(ii) =rot(ii)  - 2.*beta(i)/factor
			rot(ii)=mod(rot(ii),2*pi)
			if(rot(ii).gt.pi) rot(ii)=rot(ii) - 2*pi
			if(rot(ii).lt.pi) rot(ii)=rot(ii) + 2*pi
600		continue
c
		if(errx.le.tolx) goto 1000
c
c...		update nvect, cvect avect and R using new rotations angles.
c
		call newvect(nvect0,cvect0,avect0,nvect,cvect,avect,R,R0,rot,
     1			nloop)
c
c...		check parallelism of nvect and cvect after applying rotations
c
		test_orth=0.
		do j=1,nloop-1
			do k=1,3
				u1(k)=nvect(k,j+1)
				v1(k)=cvect(k,j)
			enddo
			call dotvect(u1,v1,test)
			test_orth=test_orth+test
		enddo
		if(abs(nloop+test_orth-1).gt.0.001) then
				write(6,*)' error on orth:',test_orth
				ifail=0
				goto 1700
		endif
c
c...		check result (lack of closure) at each cycle of Newton Raphson.
c
c		do i=1,3
c		  R00(i)=0.
c                 do kk=1,nloop
c			R00(i)=R00(i)+R(i,kk)
c		   enddo
c		enddo
c
c		write(6,'(a,3f8.3)') ' R0 achieved=',(R00(kk),kk=1,3)	
c		write(6,'(a,3f8.3)') ' R0 target  =',(R0(kk),kk=1,3)	
c		call dotvect(target,nvect(1,4),score)
c		write(6,'(a,3f8.3)')' target c4(k):',(target(kk),kk=1,3)
c		write(6,'(a,4f8.3)') ' score, R0=',score+1./3.,(R0(kk),kk=1,3)	
c
800	continue
c
	if(icount.lt.200) then
		goto 320
	else
		ifail=0
		goto 1590
	endif
c
c...	end of loop on refining remaining angles
c
1000	continue
c
c...	End of Newton Raphson refinement...
c
c	write(6,*)' refinement successful !!!',icount
c
c...	now update dihed list from rot list (radian units)
c
	ndi1=ndi 
	do 1200 k=1,nloop
c
		dihed(ndi1+1) = pi + rot(2*(k-1)+1)
		dihed(ndi1+listdihed(ires+k-1)-1) = - rot(2*k)
		ndi1=ndi1+listdihed(ires+k-1)
c
1200	continue
c
c...	and build new coordinates in coord table (p6, p1, p3).
c
	ilast=0
	do 1500 i=ires,ires+nloop-1
	   list1=listatom(i)
	   do 1400 k=1,3
		coord(3*(nat+5)+k)=p3(k) + dist0*cvect(k,i-ires+1)
		p6(k)=coord(3*(nat+5)+k)
		pold1(k)=p1(k)
		coord(3*(nat+list1)+k)=p6(k) + dist0*avect(k,i-ires+2)
		p1(k)=coord(3*(nat+list1)+k)
		pold3(k)=p3(k)
		coord(3*(nat+list1+2)+k)=p1(k) - dist0*nvect(k,i-ires+2)
		p3(k)=coord(3*(nat+list1+2)+k)
1400	   continue
c
c..	now build CB, HN and O (carbonyl oxygen) as well for this residue
c
	   ang=120.*pif
	   call sp2(pold3,p6,p1,p7,ang,dist1)
	   call sp2(p6,p1,p3,p2,ang,dist2)
	   ang1=110.*pif
	   ang2=110.*pif
	   ip=2
	   call nsp3(pold1,pold3,p6,p5,ang1,ang2,ip,dist0)
c
	   do 1420 k=1,3
		coord(3*(nat+list1+1)+k)=p2(k)
		coord(3*(nat+4)+k)=p5(k)
		coord(3*(nat+6)+k)=p7(k)
1420	   continue
c
	   goto 1490
c
c	   if(itype(i).eq.1) goto 1490
c
c	   Build sidechain (or replace CB by HA2 for a glycine)
c
	   nchi = idihed(itype(i))
c
c	   do 1480 irot=1,nrotamer(itype(i))
c		do 1460 k = 1,nanglerot(itype(i))
c			chi(k) = rotamer(itype(i),irot,k)*pif
c1460		continue
c
c		do 1470 k = nanglerot(itype(i))+1,nchi
c			chi(k) = pi
c1470		continue
c
c...	   Just copy dihedral side chain angles from original conformation.
c
	        do 1470 j = 1,nchi
		     chi(j) = dihed(ndi+1+j) 
1470	        continue
c
	        call sidechain(itype(i),p1,p3,p5,p2,chi,nchi,pside,nsideb)
c
c1480	   continue
c
c	   Now store sidechain :
c
	   do 1485 j = 1,3*nsideb
		coord(3*(nat+7)+j) = pside(j)
1485	   continue
c
1490	   nat=nat+listatom(i)
	   ndi=ndi+listdihed(i)
c
c...	end of loop on building new coordinates for this loop.
c
1500	continue
c
c...	Take care of last peptide bond:
c...	get phi psi of last bond using torsion subr. 
c	(they have been modified too)
c
	ilast=1
	do 1550 k=1,3
		pnew1(k)=coord(3*(nat+listatom(ires+nloop))+k)
1550	continue
c
	call torsion(p6,p1,p3,pnew6,val1)
	call torsion(p1,p3,pnew6,pnew1,val2)
	dihed(ndi+1)=val1
	dihed(ndi+listdihed(ires+nloop)-1)=val2
c
c...	and check CB atom and H-N atom (it may be that it is the wrong one)
c
	ang=120.*pif
	call sp2(p6,p1,p3,p2,ang,dist2)
	ang1=110.*pif
	ang2=110.*pif
	ip=2
	call nsp3(p1,p3,pnew6,p5,ang1,ang2,ip,dist0)
c
	do 1580 k=1,3
		coord(3*(nat+1)+k)=p2(k)
		coord(3*(nat+4)+k)=p5(k)
1580	continue
c
1590	continue
c
c...	End of subr.
c
1700	return
	end
c
	subroutine usrfun(R,R0,alpha,beta,nvect,cvect,i1,target,nloop)
c
	real*8	alpha(4,4),beta(4),kappa1,phi1,psi1,pi,pif
	real*8	cvect(3,8),nvect(3,8),R(3,8),R0(3),target(3)
	real*8	u(3),u1(3),v1(3),fact1,fact2,fact3
c
	integer	i1(12),j1,i,ii,j,k,kk,l,nloop,nloop1
c
	pi=acos(-1.)
	pif=pi/180.
c
c...	define alpha matrix and beta vector to be solved by Newton Raphson algo.
c
	do 100 i=1,4
	   do 50 k=1,4
		alpha(k,i)=0.
50	   continue
	   beta(i)=0.
100	continue
c
	nloop1=nloop+1
	call crossvect(target,nvect(1,nloop1),u)
c
c	write(6,'(a,i4,3f8.3)'),' nloop,u(3)=',nloop,(u(k),k=1,3)
c	write(6,'(a,3f8.3)'),' target(k)',(target(k),k=1,3)
c	write(6,'(a,3f8.3)'),' nvect(k,4)=',(nvect(k,nloop+1),k=1,3)
c	write(6,'(a,3f8.3)'),' cvect(k,3)=',(cvect(k,nloop),k=1,3)
c
	ii=0
c
	do 700 i=1,2*nloop
	   l=mod(i,2)
	   j=int((i-1)/2)+1
	   do kk=1,2*nloop-4
	   	if(i.eq.i1(kk)) goto 700
	   enddo
	   ii=ii+1
c	   write(6,'(a,6i4)')' i,ii,i1,j1,j,l=',i,ii,i1,j1,j,l
	   if(l.eq.1) then
		call dotvect(u,nvect(1,j),fact1)
	   	alpha(1,ii)=fact1 
	   elseif(l.eq.0) then
		call dotvect(u,cvect(1,j),fact2)
	   	alpha(1,ii)=fact2 
	   endif
c
700	continue
c
	do 1000 j=1,nloop
	   ii=0
	   do 900 k=1,j
	   	do kk=1,2*nloop-4
	      		if((2*(k-1)+1).eq.i1(kk)) goto 840
	   	enddo
		ii=ii+1
		call crossvect(R(1,j),nvect(1,k),u1)
		do 820 kk=1,3
		   alpha(kk+1,ii)=alpha(kk+1,ii) + u1(kk)
820		continue
840		continue
		do kk=1,2*nloop -4
			if((2*k).eq.i1(kk)) goto 900
		enddo
		ii=ii+1
		call crossvect(R(1,j),cvect(1,k),v1)
		do 860 kk=1,3
		   alpha(kk+1,ii)=alpha(kk+1,ii) + v1(kk)
860		continue
900	   continue
c
1000	continue
c
	call dotvect(target,nvect(1,nloop1),fact3)
	beta(1) = 1./3. + fact3
	do 1200 ii=2,4
	   do 1100 j=1,nloop
c
c...		There appears to be a (typo?) error in Knapp (1993)?
c		beta(ii) = beta(ii) - (R(ii-1,j)-(R0(ii-1)/real(nloop)))
c
		beta(ii) = beta(ii) + (R(ii-1,j)-(R0(ii-1)/real(nloop)))
1100	   continue
1200	continue
c
	return
	end
c
	subroutine newvect(nvect0,cvect0,avect0,nvect,cvect,avect,R,R0,rot,
     1		nloop)
c
	parameter (nang=16)
c
	real*8	nvect(3,8),cvect(3,8),nvect0(3,8),cvect0(3,8)
	real*8	rot(nang)
	real*8	kappa1,kappa2,psi1,psi2,phi1,phi2,pi,pif,dist
	real*8	R(3,8),R0(3),avect(3,8),avect0(3,8)
c
	integer	nloop,i,j,k
c
c...	Rotate, using phi psi angles defined in the rot table (radian units),
c...	all nvect and cvect in the loop.
c
	pi=acos(-1.)
	pif=pi/180.
	dist=1.4363
c
c...	initialize to extended from
c
	do j=1,nloop+1
	   do k=1,3
		nvect(k,j)=nvect0(k,j)
		cvect(k,j)=cvect0(k,j)
		avect(k,j)=avect0(k,j)
	   enddo
	enddo
c
	do i=1,nloop
c
	kappa1 = (rot(2*(i-1)+1)/pif)
	kappa2 = rot(2*i)/pif
c	
	if(abs(nvect(3,i)).gt.1.) then
		in=nint(nvect(3,i))
		nvect(3,i)=in
	endif
	psi1=acosd(nvect(3,i))
	if(abs(sind(psi1)).gt.0.0001) then 
     		phi1=atan2d(nvect(2,i)/sind(psi1),nvect(1,i)/sind(psi1))
	else
		phi1=180.
	endif
c	write(6,*)' kappa1,psi1,phi1=',kappa1,psi1,phi1
	do j=i,nloop+1
	   call apply_rot_vect(kappa1,psi1,phi1,cvect(1,j))
	   call apply_rot_vect(kappa1,psi1,phi1,avect(1,j+1))
	   call apply_rot_vect(kappa1,psi1,phi1,nvect(1,j+1))
	enddo
c
	if(abs(cvect(3,i)).gt.1.) then
		in=nint(cvect(3,i))
		cvect(3,i)=in
	endif
	psi2=acosd(cvect(3,i))
	if(abs(sind(psi2)).gt.0.0001) then 
     		phi2=atan2d(cvect(2,i)/sind(psi2),cvect(1,i)/sind(psi2))
	else
		phi2=190.
	endif
c	write(6,*)' kappa2,psi2,phi2=',kappa2,psi2,phi2
c
	do j=i,nloop
	   call apply_rot_vect(kappa2,psi2,phi2,avect(1,j+1))
	   call apply_rot_vect(kappa2,psi2,phi2,nvect(1,j+1))
	   call apply_rot_vect(kappa2,psi2,phi2,cvect(1,j+1))
	enddo
c
	enddo
c
	do i=1,3
	   do j=1,nloop
		R(i,j)=dist*(avect(i,j+1)-nvect(i,j+1)+cvect(i,j))
	   enddo
	enddo
c
	return
	end
c
	subroutine ludcmp(alpha,indx,d)
c
c...	taken directly from Numerical Recipes (the book).
c
	real*8 alpha(4,4),indx(4),vv(100),aamax,sum,dum,d,tiny
c
	integer	n,i,j,k,imax
c
	n=4
	d=1.
	tiny=0.0000001
c
	do 12 i=1,n
		aamax=0.
		do 11 j=1,n
		   if(abs(alpha(i,j)).gt.aamax) aamax=abs(alpha(i,j))
11		continue
		if(aamax.lt.0.00000001) pause ' singular matrix'
		vv(i)=1./aamax
12	continue
c
	do 19 j=1,n
		do 14 i=1,j-1
		   sum=alpha(i,j)
		   do 13 k=1,i-1
			sum=sum-alpha(i,k)*alpha(k,j)
13		   continue
		   alpha(i,j)=sum
14		continue
		aamax=0.
		do 16 i=j,n
		   sum=alpha(i,j)
		   do 15 k=1,j-1
			sum=sum-alpha(i,k)*alpha(k,j)
15		   continue
		   alpha(i,j)=sum
		   dum=vv(i)*abs(sum)
		   if(dum.ge.aamax) then
			imax=i
			aamax=dum
		   endif
16		continue
c
	if(j.ne.imax) then
		do 17 k=1,n
			dum=alpha(imax,k)
			alpha(imax,k)=alpha(j,k)
			alpha(j,k)=dum
17		continue
		d=-d
		vv(imax)=vv(j)
	endif
c
	indx(j)=imax
	if(abs(alpha(j,j)).lt.0.0000001) alpha(j,j)=tiny
	if(j.ne.n) then
		dum=1./alpha(j,j)
		do 18 i=j+1,n
			alpha(i,j)=alpha(i,j)*dum
18		continue
	endif
19	continue
c
c	write(6,*)' new alpha:'
c	do ii=1,4
c		write(6,'(4f10.3)')(alpha(ii,kk),kk=1,4)
c	enddo
c	write(6,'(a,4f8.3)')' indx=',(indx(kk),kk=1,4)
c
	return
	end
c
	subroutine lubksb(alpha,indx,beta)
c
c...	taken directly from Numerical Recipes (the book).
c
	real*8 alpha(4,4),indx(4),beta(4),sum
c
	integer	i,n,k,ll,j
c
c	write(6,*)' alpha matrix after entering lubksb program...'
c	do i=1,4
c	   write(6,'(4f8.3)')(alpha(i,k),k=1,4)
c	enddo
c
	n=4
c
	ii=0
	do 12 i=1,n
		ll=indx(i)
		sum=beta(ll)
		beta(ll)=beta(i)
		if(ii.ne.0) then
		  do 11 j=ii,i-1
			sum=sum-alpha(i,j)*beta(j)
11		  continue
		elseif (sum.ne.0) then
		  ii=i
		endif
		beta(i)=sum
12	continue
c
	do 14 i=n,1,-1
		sum=beta(i)
		if(i.lt.n) then
		   do 13 j=i+1,n
			sum=sum-alpha(i,j)*beta(j)
13		   continue
		endif
		beta(i)=sum/alpha(i,i)
14	continue
c
	return	
	end
c
	subroutine mprove(a,alud,indx,b,x)
c
c...	improves a solution vector x of linear equation A.x=b
c...	A, b and x are input, as is dimension n.
c...	also input is alud the LU decomposition of matrix A (LUDCMP) and indx.
c...	On output, X is modified and improved.
c...	Highly recommended by the book: Numerical Recipes.
c
c	parameter (nmax=100)
	real*8	r(100)
	real*8 a(4,4),alud(4,4),b(4),x(4),indx(4)	
	real*8	sdp
c
	n=4
	do 12 i=1,n
		sdp=-b(i)
		do 11 j=1,n
			sdp=sdp+dble(a(i,j))*dble(x(j))
11		continue
		r(i)=sdp
12	continue
	call lubksb(alud,indx,r)
	do 13 i=1,n
		x(i)=x(i)-r(i)
13	continue
c
	return
	end
c
