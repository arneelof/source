#!/usr/bin/perl

$MAXSUB="/afs/pdc.kth.se/home/a/arnee/source/rmsdtest/i686/maxsub";
# my $CUTOFF=30;

%aaa_a = (
  'ALA','A',
  'ARG','R',
  'ASN','N',
  'ASP','D',
  'CYS','C',
  'GLN','Q',
  'GLU','E',
  'GLY','G',
  'HIS','H',
  'ILE','I',
  'LEU','L',
  'LYS','K',
  'MET','M',
  'PHE','F',
  'PRO','P',
  'SER','S',
  'THR','T',
  'TRP','W',
  'TYR','Y',
  'VAL','V');

if(@ARGV<2){
  die"usage: $0 <target> <models> [<tmp>]\n";}
if(@ARGV>2){
  open(TMP,">$ARGV[2]");}

if(!-s $ARGV[0]){
  die"Could not open $ARGV[0]";}

open(MODELS,"$ARGV[1]")||die"Could not open $ARGV[1]";
$late='';
$mn=0;
open(MODEL,">/tmp/maxsub.$$");
while(<MODELS>){
  if($_ =~s/^PARENT *//){
    chop($_);
    $parent=$_;
    $late='';
    $mn=0;}
  elsif($_=~/^REMARK PARENT .* ERROR:/){
    print "\tERROR\n";}
  elsif($_=~/^REMARK LATE ERROR:/){
    $late="\tLATE";}
  elsif($_=~/^ATOM........ CA /){
    print MODEL $_;
    $mn++;}
  elsif($_=~/^TER/){ # && $mn){
    close(MODEL);
    &maxsub();
    $late='';
    $mn=0;
    open(MODEL,">/tmp/maxsub.$$");}}
close(MODELS);
if($mn){
  close(MODEL);
  &maxsub();
  $late='';
  $mn=0;}
unlink("/tmp/maxsub.$$");

sub maxsub
{ print TMP "MODEL: $parent\n";
  my $mx;
  my $ca;
  my $rms;
  if($mn){
    open(FILE,"$MAXSUB /tmp/maxsub.$$ $ARGV[0] 3.5|")
      ||die"$MAXSUB /tmp/maxsub.$$ $ARGV[0] 3.5";
    my @maxsub=<FILE>;
    close(FILE);
    print TMP join("",@maxsub);
    my @res=grep(/^REMARK HAS .*MAXSUB:/,@maxsub);
    $mx=$res[0]; chop($mx);
    $mx=~s/.*MAXSUB: ([0-9\.]*) \(.*/$1/;
    $ca=$res[0]; chop($ca);
    $ca=~s/^REMARK HAS ([0-9]*) .*/$1/;
    $rms=$res[0]; chop($rms);
    $rms=~s/^REMARK HAS [0-9]* *AT RMS ([0-9\.]*)\. /$1/;}
  my $short='';
  if($mn<25){
    $short="\tSHORT";}
  print sprintf("%.3f\t%d\t%.3f\t%s$short$late\n",$mx,$ca,$rms,$parent);
}
