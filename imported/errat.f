C       
C	ERRAT - Analyzes statistics of interactions between different
C	atom types to identify regions of protein structures that may be
C	in error or require adjustment.
C       
C	version 1.0
C       
C	copyright 1993
C	Chris Colovos and Todd Yeates
C	UCLA Dept. of Chem. & Biochem. and Molecular Biology Institute
C	yeates@uclaue.mbi.ucla.edu
C       
C	For more details, or when referring to this program, please
C	cite:
C	Colovos, C. and Yeates, T. O. (1993) Protein Science, 2, 1511-1519.
C       
C	This program may not be redistributed in whole, in part or in
C	modified form, freely or for sale, except with permission of
C	the authors.
C       
C       
C	Atoms are classified as either C, N, or O (also S). Interactions
C	are classified as:
C       
C       Carbon - Nitrogen	(CN)
C       Carbon - Oxygen	(CO)
C       Nitrogen - Nitrogen	(NN)
C       Nitrogen - Oxygen	(NO)
C       Oxygen - Oxygen	(OO)
C       
C	Interactions between covalently bonded atoms and those within the
C	same residue are excluded. A sliding window is used to examine
C	local quality; this version uses a 9-residue window. The input
C	file is in PDB format. The output file contains as error value for
C	each window. The 95% confidence limit for the error functions is
C	about 6. The 99% limit is about 9 (ie. the probability of observing a
C	pattern with an error value >9. in a database of reliable structures
C	is 1%). This version of the program is specific for a 3.5A definition
C	for an atomic interaction (database parameters for other distances
C	are not included here).
C       
C	USAGE:
C	line 1: input coordinate filename (PDB format)
C	line 2: output error filename
C       
C	KNOWN BUGS:
C	1) Residue numbers are incremented by 1000 for multiple protein chains,
C	so a fatal error will occur in the unlikely event of multiple subunits
C	with greater than 1000 residues in a single chain.
C	2) Windowing is not handled very cleanly at the ends of multiple chains.
C       

	program ERRAT

	integer*4  atmnam(200 000), atmnum 
	integer*4  resnum(200 000), nres, rold, SR, StrRes
	integer*4  IPEN(3,3), respen(3,3,50000)
	integer*4  i, j, k, m, n
	integer*4  CCpen, CNpen, COpen, NNpen, NOpen, OOpen
	real*4  xo(3), error(10000)
	real*4  ResInt(10000), ISUM, Itemp
	real*4  radius, count3, rlim, err
	character  atnam(200 000)*1, atnamtemp*3
	character  bnam(200 000)*1, dummy*4
	character  filein*60, fileout*60
	character  sub*2, subold*2, flag*6
	real*4 score(3,3)
	data  score /
	1     0.166, -0.472,  0.060,
	2    -0.472,  0.045,  0.406,
	3     0.060,  0.406, -0.736/

	integer*4  nbx(3)
	integer*4  ibox1(0:15, 400 000), ibox2(100 000)
	real*4  boxsize, x(3, 200 000)
	integer*4  ind, ix, iy, iz
	real*4  zmin(3), zmax(3), orig(3)
	character*8 itime

	radius=3.5
	boxsize=4.
	overlap=0.01

C	call time(itime)
C	write (6,*) itime

	rlim = 2320 -1596.67*(radius)+320.0*(radius*radius)-13.33*
	1    (radius*radius*radius)
	rsq = radius * radius

	write(6,*) ' Input pdb filename '
	read(5,900) filein
	open(1,file=filein,status='old')

	write(6,*) ' Output error filename '
	read(5,900) fileout
	open(2,file=fileout,status='new')

C       C *** POSTSCRIPT PLOT NOT IMPLEMENTED YET ***
C       C	write(6,*) ' Output postscript filename '
C       C	read(5,900) fileout
C       C	open(3,file=fileout,status='new',carriagecontrol='list')

C       
C       
C	Read in the data from PDB files
C       
C       dummy = dummy variable used to check if the line
C       from the PDB file begins with ATOM
C       atnamtemp = full PDB atom name ( i.e. CA, CB,... ) (temp)
C       atnam(i) = truncated atom name (C, N, O, S)
C       nres = residue number (temp)
C       resnum(i) = residue number of atom i
C       xo(j) = (x, y, z) coordinates (temp)
C       x(j,i) = (x, y, z) coordinates of atom i
C       atmnum = last atom number
C       
C       i = atom number
C       j = 1 to 3 => x,y,z
C       
C       

	nadd = 0
	i=0

 10	read(1,901,end=20,err=10) dummy, atnamtemp, sub, nres, xo
	if (dummy .ne. 'ATOM') goto 10
	if (nres .lt. 1) goto 10
	i = i + 1
	atnam(i) = atnamtemp(1:1)
	if (i.ge.2.and.sub.ne.subold) nadd = nadd+1
	resnum(i) = nres + nadd*1000

C       
C       
C	Checks to see if there is a gap in sequence
C       
C       rold = previous residue number
C       
C       1st if statement checks to see if there is a new reside
C       in the sequence
C       
C       2nd if statement checks to see if there is a gap between
C       resnum(i) and rold
C       
C       
	if (i .ge. 2) then
	   if (resnum(i) .lt. resnum(i-1)) then
	      write (6,998) i,resnum(i-1), resnum(i)
C	      write(*,*)'TEST1>',dummy,atnamtemp,sub,nres,xo
C	      write(*,*)'TEST1b>',sub,subold
C	      write(*,*)'TEST2>',i,resnum(i),nres,nadd
	       nadd = nadd+1
	       resnum(i) = nres + nadd*1000
C	      stop
	   end if
	   if (sub .eq. subold) then
	      if (resnum(i) .ne. resnum(i-1)) then
	         if (resnum(i) .ne. rold+1) then
		    write(6,*)'MISSING RESIDUES:',rold+1,'----->',resnum(i)-1
	         end if
	         rold = resnum(i)
	      end if
	   end if
	end if
	subold = sub

	do j = 1,3
	   x(j,i) = xo(j)
	end do

C       
C       
C	Assigns atom Backbone Designation
C       
C       bnam(i) = B if atom is on the backbone
C       = 0 if atom is on the sidechain
C       
C       
	bnam(i)='0'
	if ((atnamtemp.eq.'N  ') .or. (atnamtemp.eq.'O  ') .or.
     %	     (atnamtemp.eq.'C  ') .or. (atnamtemp.eq.'CA ')) then
	   bnam(i) = 'B'
	end if

C       
C       
C	Assigns the atom a value from 1 to 3 based on atom type
C       
C       1 = Carbon
C       2 = Nitrogen
C       3 = Oxygen, Sulfur
C       
C       
	atmnam(i)=0
	if (atnam(i).eq.'C') then
	   atmnam(i) = 1
	end if
	if (atnam(i).eq.'N') then
	   atmnam(i) = 2
	end if
	if (atnam(i).eq.'O') then
	   atmnam(i) = 3
	end if
	if (atnam(i).eq.'S') then
	   atmnam(i) = 3
	end if
	if (atmnam(i) .eq. 0) then
	   i=i-1
	   goto 10
	end if

	atmnum = i

	goto 10

 20	continue
	write (6,*) ' Number of atoms accepted = ', atmnum

C       
C       
C	Determines number and type of atomic interactions within a sphere
C	around each atom.
C       
C	NOT including C-N backbone-backbone interaction of adjacent residues
C       or any interactions between atoms of the same residue
C       
C       m = counter loop 1
C       n = counter loop 2
C       count3 = number of interaction pairs
C       
C       respen(b(m),b(n),a(m)) = number of interactions
C       a(m) = residue number
C       b(m) = atom type 1
C       b(n) = atom type 2
C       
C       resint(m) = total number of interactions
C       
C       
C	Assign the atoms to boxes

	do i=1,3
	   zmin(i)=999.
	   zmax(i)=-999.
	end do

	do i=1,atmnum
	   do j=1,3
	      if (x(j,i) .lt. zmin(j)) zmin(j)=x(j,i)
	      if (x(j,i) .gt. zmax(j)) zmax(j)=x(j,i)
	   end do
	end do
CCC     
	write (6,*) ' Coordinate limits : '
	do i=1,3
	   write (6,*) zmin(i), zmax(i)
	   orig(i)=zmin(i)-0.00001
	end do

	do i=1,3
	   nbx(i)=int((zmax(i)-orig(i)+0.00001)/boxsize)+1
	end do
	write (6,*) ' # of boxes in each direction : ',(nbx(i),i=1,3)
	if (nbx(1)*nbx(2)*nbx(3) .gt. 400 000) stop ' TOO MANY BOXES '
	do i=1,nbx(1)*nbx(2)*nbx(3)
	   ibox1(0,i)=0
	end do

	do i=1,atmnum
	   ix=int((x(1,i)-orig(1))/boxsize)
	   iy=int((x(2,i)-orig(2))/boxsize)
	   iz=int((x(3,i)-orig(3))/boxsize)
	   ind = 1 + ix + iy*nbx(1) + iz*nbx(1)*nbx(2)
	   ibox1(0,ind)=ibox1(0,ind)+1
	   ibox1(ibox1(0,ind),ind)=i
	   ibox2(i)=ind
	end do

	do i=1,nbx(1)*nbx(2)*nbx(3)
CCC     write (6,*) i, (ibox1(j,i), j=0,5)
	   if (ibox1(0,i) .gt. 15) then
	      write (6,*) ' TOO MANY ATOMS IN BOX ', ibox1(0,i)
	      do j=1,ibox1(0,i)
		 write (6,*) '  ', ibox1(j,i)
	      end do
	      stop
	   end if
	   if (ibox1(0,i) .gt. most) most=ibox1(0,i)
	end do
CCC	write (6,*) ' Most # of atoms in a box = ', most



	count3=0.
	ndelta=1+int(radius/boxsize)

	do m=1,atmnum

	   jbz=int((x(3,m)-orig(3))/boxsize)
	   jby=int((x(2,m)-orig(2))/boxsize)
	   jbx=int((x(1,m)-orig(1))/boxsize)
	   ibz1=jbz-ndelta
	   if (ibz1 .lt. 0) ibz1=0
	   ibz2=jbz+ndelta
	   if (ibz2 .gt. nbx(3)-1) ibz2=nbx(3)-1
	   iby1=jby-ndelta
	   if (iby1 .lt. 0) iby1=0
	   iby2=jby+ndelta
	   if (iby2 .gt. nbx(2)-1) iby2=nbx(2)-1
	   ibx1=jbx-ndelta
	   if (ibx1 .lt. 0) ibx1=0
	   ibx2=jbx+ndelta
	   if (ibx2 .gt. nbx(1)-1) ibx2=nbx(1)-1
	   do ibz=ibz1,ibz2
	      do iby=iby1,iby2
		 do ibx=ibx1,ibx2
		    ind=1+ibx+iby*nbx(1)+ibz*nbx(1)*nbx(2)
		    
		    do i=1,ibox1(0,ind)
		       n=ibox1(i,ind)

		       dsq=0.
		       do j=1,3
			  dsq=dsq+(x(j,n)-x(j,m))**2
		       end do
		       if (dsq .le. rsq) then

			  if (resnum(m).lt.resnum(n)) then
			     
			     if (bnam(m) .eq. 'B' .and. bnam(n) .eq. 'B' .and.
	1			  resnum(n) .eq. (resnum(m) + 1) .and.
	2			  atnam(m).eq.'C  ' .and. atnam(n).eq.'N  ' ) then
			     else

C	Store on a per-residue basis

				respen(atmnam(m),atmnam(n),resnum(m))=
	1			     respen(atmnam(m),atmnam(n),resnum(m))+1
				ResInt(ResNum(m)) = ResInt(ResNum(m)) + 1.
				count3=count3+1.
C       
C	Include symmetric interactions
C       
				respen(atmnam(n),atmnam(m),resnum(n))=
	1			     respen(atmnam(n),atmnam(m),resnum(n))+1
				ResInt(ResNum(n)) = ResInt(ResNum(n)) + 1.
				count3=count3+1.
				
			     end if
			  end if
		       end if
		    end do
		 end do
	      end do
	   end do
	end do


C       
C       
C	Begin the window calculations
C       
C       k =  window radius
C       StrRes =  starting residue
C       IPEN(i,j) =  total number of (i,j)-type interactions
C       ISUM =  total number of interactions
C       
C       
C	write (2,999) filein(1:40), radius

	StrRes = resnum(1)+4
	isum=0.

	do i = 1,3
	   do j = 1,3
	      IPEN(i,j)= 0
	   end do
	end do
	

	do k = -4,4
	   do i = 1,3
	      do j = 1,3
		 IPEN(i,j) = respen(i,j,(strres+k)) + IPEN(i,j)
	      end do
	   end do
	   ISUM = ResInt(strres+k) + ISUM
	end do
	
	CCpen = IPEN(1,1)
	CNpen = IPEN(1,2)+IPEN(2,1)
	COpen = IPEN(1,3)+IPEN(3,1)
	NNpen = IPEN(2,2)
	NOpen = IPEN(2,3)+IPEN(3,2)
	OOpen = IPEN(3,3)
	Itemp = ISUM

	ireg = 0

	if (ISUM.eq.0.0) then
	   Itemp = 1.0
	   write(6,*) '**** WARNING: Zero interacions in window. RES:',StrRes
	   ireg = ireg + 1
	   goto 50
	end if

	if (ISUM.lt.rlim) then
	   Itemp = 1.0
	   write(6,*) '**** WARNING: Few interacions in window. RES:',StrRes
	   ireg = ireg + 1
	   goto 50
	end if

	error(strres)=0.
	if (resint(strres) .gt. 0.) then
	   call normal(ccpen,copen,nnpen,nopen,oopen,itemp,err)
CC	write (6,*) 'test 1', strres, ccpen,cnpen,copen,nnpen,nopen,oopen,itemp,err
	   flag='      '
C	   if (err .gt. 6.) flag='     ?'
C	   if (err .gt. 9.) flag='  ????'
	   error(strres)=err
	   write(2,902) strres,err,flag
	end if
C       
C       
C       
C	Iterate the window through the entire structure
C       
 50	   do SR = resnum(1)+5,resnum(atmnum)-4

	   do i = 1,3
	      do j = 1,3
		 IPEN(i,j)= respen(i,j,(SR+4))+IPEN(i,j)-respen(i,j,(SR-5))
	      end do
	   end do

	   ISUM = ResInt((SR+4)) + ISUM - ResInt((SR-5))
	   CCpen = IPEN(1,1)
	   CNpen = IPEN(1,2)+IPEN(2,1)
	   COpen = IPEN(1,3)+IPEN(3,1)
	   NNpen = IPEN(2,2)
	   NOpen = IPEN(2,3)+IPEN(3,2)
	   OOpen = IPEN(3,3)
	   Itemp = ISUM

	   error(SR)=0.
	   if (resint(SR) .gt. 0.) then

	      if (ISUM.eq.0.0) then
		 Itemp = 1.0
		 write(6,*) '**** WARNING: Zero interacions in window. RES:',SR
		 ireg = ireg + 1
		 goto 60
	      end if

	      if (ISUM.lt.rlim) then
		 Itemp = 1.0
		 write(6,*) '**** WARNING: Few interacions in window. RES:',SR
		 ireg = ireg + 1
		 goto 60
	      end if

	      call normal(ccpen,copen,nnpen,nopen,oopen,itemp,err)
CCC	write (6,*) 'test 2',SR, ccpen,cnpen,copen,nnpen,nopen,oopen,itemp,err
	      flag='      '
C	      if (err .gt. 6.) flag='     ?'
C	      if (err .gt. 9.) flag='  ????'
	      error(SR)=err
	      write(2,902) SR,err,flag

 60	      continue
	   end if
	end do

	itemp=0

	do i = 1,3
	   do j = 1,3
	      ipen(i,j)=0.
	   end do
	end do
	itemp=0
	do sr = resnum(1),resnum(atmnum)
	   do i = 1,3
	      do j = 1,3
		 ipen(i,j)=ipen(i,j)+respen(i,j,(SR))*score(i,j)
		 itemp=itemp+respen(i,j,(SR))*score(i,j)
C		 write(6,*)'SCORE>',i,j,respen(i,j,(SR))*score(i,j)
	      end do
	   end do
	end do
	write(2,*)'Total-Score:',itemp
	itemp=0

	do i = 1,3
	   do j = 1,3
	      ipen(i,j)=0.
	   end do
	end do
	itemp=0
	do sr = resnum(1),resnum(atmnum)
	   do i = 1,3
	      do j = 1,3
		 ipen(i,j)=ipen(i,j)+respen(i,j,(SR))
		 itemp=itemp+respen(i,j,(SR))
	      end do
	   end do
	end do

	CCpen = IPEN(1,1)
	CNpen = IPEN(1,2)+IPEN(2,1)
	COpen = IPEN(1,3)+IPEN(3,1)
	NNpen = IPEN(2,2)
	NOpen = IPEN(2,3)+IPEN(3,2)
	OOpen = IPEN(3,3)

	call normal(ccpen,copen,nnpen,nopen,oopen,itemp,err)

	write(2,*)'Total-Errat:',err

C	call time(itime)
C	write (6,*) itime

C       
C       
C	Format statements
C       
C       

 900	format(a60)
 901	format(a4,9x,a3,4x,a2,i4,4x,5f8.3)
 902	format(i10, f25.1, a6)
 903	format(7(i9,','),f9.0,',')
 908	format(f8.3)
 998	format(' FATAL ERROR: NON-MONOTONIC RESIDUE NUMBERS', 3i6)
 999	format (' File = ', a40, ' Radius =', f6.2,/,
	1    ' Residue #:   Error: ')
	
	stop
	end



	subroutine  normal(a,b,c,d,e,tot,err)

	real*4  x(5), A1(5), inv(5,5), av(5)
	integer*4  a, b, c, d, e
	real*4  tot, err

	data  av / 0.225, 0.281, 0.071, 0.237, 0.044 /
	data  inv /
	1    1154.343,  600.213, 1051.018, 1132.885,  960.738,
	2    600.213, 1286.818, 1282.042,  957.156,  612.789,
	3    1051.018, 1282.042, 3519.471,  991.974, 1226.491,
	4    1132.885,  957.156,  991.974, 1798.672,  820.355,
	5    960.738,  612.789, 1226.491,  820.355, 2428.966 /

	x(1) = float(a)/tot - av(1)
	x(2) = float(b)/tot - av(2)
	x(3) = float(c)/tot - av(3)
	x(4) = float(d)/tot - av(4)
	x(5) = float(e)/tot - av(5)

	A1(1) = 0.
	A1(2) = 0.
	A1(3) = 0.
	A1(4) = 0.
	A1(5) = 0.

	do i = 1,5
	   do j = 1,5
	      A1(i) = x(j)*inv(j,i) + A1(i)
	   end do
	end do

	err = 0.
	do i = 1,5
	   err = A1(i)*x(i) + err
	end do

	return
	end
