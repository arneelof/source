
	Real 		x(5), A1(5), inv(5,5), Gauss, d1, a(6)
	Real		ncount, nGood, cutoff, err(10000)
	Character 	Filein*4, Filein2*60
	integer		Counter, nr

	counter=0

	read(5,98) Filein
C	open(1,file='ais_tot.out', status='old')
C	open(2,file=filein//'.ais',
C     %	     status='old')
	open(3,file=filein//'.err',status='old')
C	open(4,file=filein//'.ter',status='new')
	open(8,file=filein//'.ps',status='new')
C	open(7,file='fort.2',status='old')

	write(8,999) '%!'
	write(8,*) '/Times-Bold findfont 6 scalefont setfont'
	write(8,*) '90 rotate'
	write(8,*) '20 -450 translate'
	write(8,*) '2 2 scale'
C	read(7,910) nr, a(1), d1, a(2), a(3), a(4), a(5)
C
C 	read(1,905) ((inv(i,j),j=1,5),i=1,5)
C	
C 10C	read(2,910,err=10,end=20) nr, x(1), d1, x(2), x(3), x(4), x(5)
C
C	x(1) = x(1) - a(1)
C	x(2) = x(2) - a(2)
C	x(3) = x(3) - a(3)
C	x(4) = x(4) - a(4)
C	x(5) = x(5) - a(5)
C
C	err(nr) = 0.
C	A1(1) = 0.
C	A1(2) = 0.
C	A1(3) = 0.
C	A1(4) = 0.
C	A1(5) = 0.
C
C	do i = 1,5
C	 do j = 1,5
C	  A1(i) = x(j)*inv(j,i) + A1(i)
C	 end do
C	end do
C
C	do i = 1,5
C	 err(nr) = A1(i)*x(i) + err(nr)
C	end do
C
C	if (err(nr).lt.6.0) ngood = ngood + 1.
CC	ncount = ncount+1.
C	
C	write(3,915) nr,err(nr)
C 	goto 10
	nmin=99999
	nmax=0
	do while (.true.)
	  read (3,*,end=99) nr, err(nr)
	  write (6,*) nr, err(nr)
	  if (nr .lt. nmin) nmin=nr
	  if (nr .gt. nmax) nmax=nr
	end do
 99	continue

 20	continue

C	write(4,920) ngood/ncount, filein

	write(8,*) 'newpath'
	write(8,*) '0 0 moveto'
	istart=10*int(float(nmin)/10.)
	iend=10*(int(float(nmax)/10.)+1)
	do i = istart, iend ,10
	write(8,*) i,0,' lineto'
	write(8,*) i,-2,' lineto'
	write(8,*) i,0,' lineto'
C	imax = i
	end do
	write(8,*) 'stroke'	
	write(8,*) 'newpath'
	write(8,*) '0 0 moveto'
	do i = 0,25,5
	write(8,*) 0,3*i,' lineto'
	write(8,*) -2,3*i,' lineto'
	write(8,*) 0,3*i,' lineto'
	end do
	write(8,*) 'stroke'	
	it = 0
	do i = nmin, nmax
	  ii=i-istart
	  if (err(i).ge.6.0) then
	    if (it.eq.0) then
	      write(8,*) 'newpath'
	      write(8,*) ii,3*6,' moveto'
	      it = 1
	    end if
	    write(8,*) ii,3*err(i),' lineto'
	  end if
	  if (err(i).lt.6.0.and.it.eq.1) then
	    write(8,*) ii,6*3,' lineto'
	  write(8,*) 'closepath'
	  write(8,*) '.5 setgray'
	  write(8,*) 'fill'
	  it = 0
	  end if
	end do
	      write(8,*) 'newpath'
	write(8,*) '0 setgray'
	write(8,*) '0 0 moveto'
	do i = nmin, nmax
	 ii=i-istart
	 write(8,*) ii,3*err(i), ' lineto'
	end do
	write(8,*) 'stroke'
	do i = istart, iend, 20
	  ii=i-istart
	  write(8,*) ii-3,' -10', ' moveto'
	  if (i.ge.0.and.i.lt.10) then
	    write(8,882) '(',i,')','show'
	  end if
	  if (i.ge.10.and.i.lt.100) then
	    write(8,887) '(',i,')','show'
	  end if
	  if (i.ge.100.and.i.lt.1000) then
	    write(8,998) '(',i,')','show'
	  end if
	  if (i.ge.1000.and.i.lt.10000) then
	    write(8,885) '(',i,')','show'
	  end if
	end do
	imax=iend
	write(8,*) 'newpath'
	write(8,*) '.2 setlinewidth'
	write(8,*) 0,6*3,' moveto'
	write(8,*) imax,6*3,' lineto'
	write(8,*) 'stroke'
	write(8,*) '/Times-Bold findfont 10 scalefont setfont'
	write(8,*) imax/2-10,80,' moveto'
	write(8,*) '(',filein,')',' show'
	write(8,*) 'showpage'

 900	format(a60)
  98	format(a4)
 905	format(5f10.3)
 910	format(i10,',',6(f10.3,','))
C 915	format(i10,',',2(f10.4,','))
 915	format(i8,f10.4)
 920	format(f10.5,' ',a60)
 887	format(a1,i2,a1,a4)
 882	format(a1,i1,a1,a4)
 885	format(a1,i4,a1,a4)
 998	format(a1,i3,a1,a4)
 999	format(a2)

	end

