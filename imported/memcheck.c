#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#define BS 1048576

int main (int argc, char *argv[]) {
  int ms, iter, i, j;
  char **buf;

  if (argc == 3) {
    sscanf(argv[1], "%d", &ms);
    sscanf(argv[2], "%d", &iter);
  }
  else {
    printf("Usage : memtest <test size in MB> <number of iterations>\n");
    return 0;
  }
  printf("Scanning %d MB for memory errors in %d iterations.\n", ms, iter);
  assert((buf = (char **)malloc(ms * sizeof(char *))) != NULL);
  /* Compare 0x00 against buf[0] and 0xFF against buf[1] */
  assert((buf[0] = (char *)malloc(BS * sizeof (char))) != NULL);
  assert((buf[1] = (char *)malloc(BS * sizeof (char))) != NULL);
  assert(memset(buf[0], 0x00, BS) != NULL);
  assert(memset(buf[1], 0xFF, BS) != NULL);
  /* Make sure they are ok */
  for (i = 0; i < BS; i++) {
    if (buf[0][i] != 0x00) {
      printf("Failed 0x00 compare! Found 0x%x for i %d\n", buf[0][i], i);
      return 0;
    }
    if (buf[1][i] != (char)0xFF) {
      printf("Failed 0xFF compare! Found 0x%x for i %d\n", buf[1][i], i);
      return 0;
    }
  }
  for (j = 0; j < iter; j++) {
    for (i = 2; i < ms; i++) {
      if ( j == 0 )
	assert((buf[i] = (char *)malloc(BS * sizeof (char))) != NULL);
      assert(memset(buf[i], 0xFF, BS) != NULL);
      if (memcmp(buf[i], buf[1], BS) != 0) {
	printf("Failed 0xFF compare after %u bytes and %d iterations\n", BS*i, j);
	return 0;
      }
      assert(memset(buf[i], 0x00, BS) != NULL);
      if (memcmp(buf[i], buf[0], BS) != 0) {
	printf("Failed 0x00 compare after %u bytes and %d iterations!\n", BS*i, j);
	return 0;
      }
    }
  }
  printf("No errors found. Exiting.\n");

  return 1;
}
