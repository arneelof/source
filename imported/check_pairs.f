C This program will check aligned structures to see how the
C secondary structure is conserved.  If % identical secondary structure 
C is below MinSSid then the user will be warned.  If the rmsd is 
C above MAXRMSD then the user will be warned.  If the percent overlap is 
C below MINOVERLAP then the user will be warned.  
C Reads .pairenv files.  The output of getenvforpairs.for.


	program checkPairs


	implicit none

	integer maxpairs, maxres
	Parameter (maxpairs=500,maxres=2000)

	character line*132, file1*80
	character pairs(maxpairs)*50
	integer B,C,D,E,F,G,H,I
	integer NumL, NumR, NumAligned, NumSSid
	integer numpairs
	real RMSD
	real MinSSid/70.0/, MaxRMSD/4.0/, MinOverLap/60.0/
	real PerSSid, PerOverLapL, PerOverLapR, PerOverLap
	logical marker, Left, Right
	integer EndLine
	integer InOlapL, InOlapR, TempL, TempR
	integer NumResId
	real    PerResIdL, PerResIdR

	open(2,file='badpairs.dat',form='formatted',status='new')
	open(3,file='pairsstats.dat',form='formatted',status='new')
	open(4,file='goodpairs.dat',form='formatted',status='new')

	write(2,'(A)') '  File                   RMSD  %SSid %Ol_L %Ol_R'//
     &		' #aln NumL NumR %id_L %id_R'
	write(3,'(A)') '  File                   RMSD  %SSid %Ol_L %Ol_R'//
     &		' #aln NumL NumR %id_L %id_R'
	write(4,'(A)') '  File                   RMSD  %SSid %Ol_L %Ol_R'//
     &		' #aln NumL NumR %id_L %id_R'

C	READ IN LIST OF .PAIRENV FILES

	print*, 'Enter name of file with list of pairs.'
	read(5,'(A)') file1
	open(1,file=file1,status='old',form='formatted')
	numpairs = 0
	do while(.true.)
	 read(1,'(A)',end=20) line
	 B = INDEX(line,' ')
	 if (B .ne. 0) then
	  numpairs = numpairs + 1
	  pairs(numpairs) = line(1:B)
	 end if
	end do
 20	continue
	close(1)

	do B = 1,NumPairs

	 NumL = 0
	 NumR = 0
	 NumAligned = 0
	 NumSSid = 0
	 NumResid = 0
	 InOlapL = 0
	 InOlapR = 0
	 TempL = 0
	 TempR = 0

	 open(1,file=pairs(B),form='formatted',status='old')
	
	 marker = .false.
	 do while (line(1:10) .ne. '----------')
	  read(1,'(A)',end=10) line(1:10)
	 end do
	 marker = .true.
	
C Read till end of file

	 do while (.true.)
	  read(1,'(A)',end=10) line
	  Left = .false.
	  Right = .false.
	  if (INDEX(line,'CA').ne. 0) then
	   if (INDEX(line(32:40),'CA').ne. 0) then
	    NumL = NumL + 1
	    TempL = TempL + 1
	    Left = .true.
	   end if
	   if (INDEX(line(44:51),'CA').ne. 0) then
	    NumR = NumR + 1
	    TempR = TempR + 1
	    Right = .true.
	   end if
	   if (Left .and. Right) then
	    if (INDEX(line(40:44),'=').ne. 0) then
	     if (NumAligned .eq. 0) then
	      InOlapL = 1
	      InOlapR = 1
	      TempL = 0
	      TempR = 0
	     else
	      InOlapL = InOlapL + TempL
	      InOlapR = InOlapR + TempR
	      TempL = 0
	      TempR = 0
	     end if

	     Numaligned = Numaligned + 1

	     if (line(2:2) .eq. line(18:18)) then
	      if (line(2:2) .eq. 'C' .or. line(2:2) .eq. 'H' .or.
     &		  line(2:2) .eq. 'S') then
	       NumSSid = NumSSid + 1
	      end if ! if standard SS
	     end if ! if SS1 = SS2
	     if (line(1:1) .eq. line(17:17)) then
	      if (line(1:1) .ne. '?') then
	       NumResid = NumResid + 1
	      end if ! if not bad residue
	     end if ! if res1 = res2

	    end if ! if aligned with '='
	   end if ! if left and right has 'CA'
	  else if (INDEX(line,'rms deviation =') .ne. 0) then
	   C = INDEX(line,'tion =') + 6
	   read(line(C:C+20),*) RMSD
	  end if ! 'CA' INDEXed
	 end do ! till end of file

 10	 continue

	 if (.not.marker) then
	  write(2,'(A)') '!!! ERROR !!! reading format '//pairs(B)
	  write(3,'(A)') '!!! ERROR !!! reading format '//pairs(B)
	  write(4,'(A)') '!!! ERROR !!! reading format '//pairs(B)
	 else
	  C = INDEX(pairs(B),' ') - 1
	  if (NumAligned .eq. 0) then
	   NumAligned = 1
	   InOlapL = 1
	   InOlapR = 1
	  end if
	  PerSSid = Real(NumSSid) / Real(NumAligned) * 100.0	  
	  PerOverLapL = Real(NumAligned) / Real(NumL) * 100.0
	  PerOverLapR = Real(NumAligned) / Real(NumR) * 100.0
	  PerResIdL = Real(NumResId) / Real(InOlapL) * 100.0
	  PerResIdR = Real(NumResId) / Real(InOlapR) * 100.0
	  Write(3,'(A,T25,F5.2,1X,3F6.1,3I5,2F6.1)') Pairs(B)(1:C),
     &		RMSD, PerSSid, PerOverLapL, PerOverLapR, NumAligned,
     &		NumL, NumR, PerResIdL, PerResIdR
	  PerOverLap = Max(PerOverLapL,PerOverLapR)
	  if (PerSSid .lt. MinSSid .or. RMSD .gt. MaxRMSD .or.
     &		PerOverLap .lt. MinOverLap) then
	   Write(2,'(A,T25,F5.2,1X,3F6.1,3I5,2F6.1)') Pairs(B)(1:C),
     &		RMSD, PerSSid, PerOverLapL, PerOverLapR, NumAligned,
     &		NumL, NumR, PerResIdL, PerResIdR
	  else
	   Write(4,'(A,T25,F5.2,1X,3F6.1,3I5,2F6.1)') Pairs(B)(1:C),
     &		RMSD, PerSSid, PerOverLapL, PerOverLapR, NumAligned,
     &		NumL, NumR, PerResIdL, PerResIdR
	  end if ! if bad
	 end if ! .not. marker
	 close(1)
	end do

	end !!main
***********************************************************************
***********************************************************************
***********************************************************************
	Integer function endline(line,maxlength)

	implicit none

	integer maxlength
	integer B,C
	character*132 line

	do B = 1,maxlength
	 C = (maxlength + 1) - B
	 if (line(C:C) .ne. ' ') then
	  endline = C
 	  return
	 end if
	end do
	endline = 0
	return
	end
***********************************************************************
***********************************************************************
***********************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
	subroutine getlowercase(chr)

	implicit none

	character chr

	if (Ichar(chr) .ge. 65 .and.
     &	    Ichar(chr) .le. 90) then
	 chr = Char(Ichar(chr) + 32)
	end if
	return
	end
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************

