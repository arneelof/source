#include <stdio.h>

char *newname(char *oldname)
{
static char outname[256];
char *t, *cpo;
cpo = outname;
for (t = oldname; t != NULL && *t != ';'; ++t)
	*cpo++ = tolower(*t);
if (*(cpo-1) == '.') *(cpo-1) = '\0';
*cpo = '\0';
return(outname);
}

main(int argc, char **argv)
{
int i;
for (i = 1; i < argc; ++i) {
	if (rename(argv[i],newname(argv[i])))
		printf("**** Error **** ");
	printf("%s -> %s\n",argv[i],newname(argv[i]));
	}
}
