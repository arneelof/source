
FFLAG2=     -Q -g -qcheck  -qINTSIZE=4 -qREALSIZE=8 -qcharlen=5000 \
	-qst_size=75000 -qaux_size=50000 -qtkq_size=50000 -qpd_size=100

CPPFLAG= -I./  -DBIGREAL  -DSMALLINTEGER  -DRS6000 \
	-DSMALLADDR -DFORTRAN_SECNDS -DHAVE_MALLOC  -DHAVE_POINTERS -DAIX

#CPPFLAG= -I./  -WFBIGREAL,SMALLINTEGER,RS6000,SMALLADDR,FORTRAN_SECNDS,\
#XS	HAVE_MALLOC,HAVE_POINTERS,AIX

FFLAG=  -u  $(FFLAG2)
LFLAG=  -lxlf90
EXTRA= cstuff.o 
LINK=	xlf $(FFLAG)
F77=	xlf 
CPP=  /usr/ccs/lib/cpp
CC=	cc 
CCFLAG= -g -O $(CPPFLAG)
CD=	cd

MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino
EXTENSION=src
OBJDIR= .
BINDIR= .

# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make


#  xlf should recognize CPP and long lines I guess in .F files
#
#  -WF,<cpp option>,<cpp option>,...
#
#

backbone.o: backbone.f backbone.src
	$(CPP) -P $(CPPFLAG) $(SIZE) backbone.src > backbone.cpp
	awk -f split.awk backbone.cpp > backbone.f
	$(F77) -c -qi4 -qcheck -I./ backbone.f
	rm -f backbone.cpp
	rm -f backbone.f


#.src.o:	
#	awk -f split.awk $*.src > $*.F
#	$(F77) -c $(FFLAG) $(CPPFLAG) -I./ $*.F
#	rm -f $*.F

.src.o:	
	$(CPP) -P $(CPPFLAG) $(SIZE) $*.src > $*.cpp
	awk -f split.awk $*.cpp > $*.f
	$(F77) -c $(FFLAG) -I./ $*.f
	rm -f $*.cpp
	rm -f $*.f


.c.o:	
	$(CC) -c $(CCFLAG)  $*.c

# Special dependencies for .src files 


.SUFFIXES: .src .f .h .o .fcm .FCM

