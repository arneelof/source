C     The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      Program Palign_list
C     
C     A heuristic sequence/profile search program. Made for membrane profiles.
C     
      implicit none
#include <dynprog.h>
#include <seqio.h>

      real seqprofile(maxproflen,maxaa+2)
      real profile(maxproflen,maxaa+2),gapopen,gapext,score
      real dynscore_msa
      real dynscore,x(maxproflen),y(maxproflen),z(maxproflen)
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      character*320 proffile,seqfile,filename1,filename2
      character*800 oldline,line,dbfile,tempname,firstname
      character*10 proftype,seqtype
      character*1 char,char1,char2
      integer stlen,templen
      character*4 resnum(maxseqlen),profresnum(maxseqlen)
      integer wdlen,linelen,i,j,k,proflen,alitype,type,num
      integer profseq(maxseqlen),profss(maxseqlen),numss,numaa,cfiles,aa
      integer seq(maxseqlen),seqss(maxseqlen),seqlen,traceback(maxseqlen)
      integer seqtoseq,seqtoss,infile,dbsize
      integer seqfilelen,proffilelen,numtm,seqs,nexti
      integer maxhits,maxdbsize,method
      logical proffound,inverse,seqfound,renorm,convert,printali,printpdb
      logical normprof
      external nexti
      real cutoff,gapcost,signcutoff,dynheuristic
      real profsum,profsum2,profaver
      real profsd
      integer profnum,ktup,hash(maxktup,maxhash),position(maxktup,maxhash)
      external dynheuristic
      real length,minout
      real qfreq(maxaa,maxaa),loopgap(3),ssmatch(0:3,0:3)
      real evalue,zs_to_e,lamb,beta
      external dynscore,seqtoseq,seqtoss,zs_to_e,dynscore_msa
      
      parameter (maxhits=25000)
      parameter (maxdbsize=125000)
      parameter (infile=91)
      parameter (numss=3)
      parameter (numaa=20)
      real heurscore(maxdbsize),tempscore
      real sumscore,sum2score,fscore(maxdbsize),averscore,sd,zscore,oldaverscore
      integer dseqlen(maxdbsize),dproflen(maxdbsize)
      integer fseqlen(maxhits),fproflen(maxhits),indx(maxhits),numout
      character*800 name(maxhits)
      real lambda,shift

      
      
C     call foo(bar)
C     default values
      proftype='unknown'
      seqtype='unknown'
      lambda=0.5
      shift=0.45
      proflen=0
      seqlen=0
      inverse=.false.
      aa=numaa
      printali=.false.
      printpdb=.false.
      normprof=.false.
#ifdef HEUR
      ktup=2
      cutoff=10.
      gapcost=30.
#endif
      minout=10
      renorm=.true.
C     numtop=20
      alitype=0
#ifdef PRFPRF
      gapopen=-2
      gapext=-.2
      method=0
#else
      gapopen=-11
      gapext=-1
#endif
      convert=.true.
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
      ssmatch(0,0)=0
#ifdef PMEMBR
      ssmatch(1,1)=0
#else
      ssmatch(1,1)=1
#endif
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
#ifdef PMEMBR
      ssmatch(3,3)=0
#else
      ssmatch(3,3)=1
#endif
      call init
#ifdef HEUR
      call init_int(position,maxktup*maxhash,0)
      call init_int(hash,maxktup*maxhash,0)
#endif
      call init_int(seq,maxseqlen,0)
      call init_int(seqss,maxseqlen,-1)
      call init_int(profss,maxseqlen,-1)
      call init_int(profseq,maxseqlen,0)
      call init_int(traceback,maxseqlen,0)
      
      call init_real(profile,maxproflen*(maxaa+2),0.)
      profile(1,1)=-9999
#ifdef PRFPRF
      call init_real(seqprofile,maxproflen*(maxaa+2),0.)
      seqprofile(1,1)=-9999
#endif
      call init_real(x,maxseqlen,0.)
      call init_real(y,maxseqlen,0.)
      call init_real(z,maxseqlen,0.)
      call init_real(table,maxaa*maxaa,0.)
      do i=1,maxseqlen
        call encodi(i,resnum(i),4,stlen)
        call encodi(i,profresnum(i),4,stlen)
      end do

      
      
      filename1='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contDistHL'
      filename2='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contEneHL'
C     filename1='/home/gvh/arne/source/palign/parkLevitt/contDistHL'
C     filename2='/home/gvh/arne/source/palign/parkLevitt/contEneHL'
      
      call defaulttable_blosum62(table,maxaa)
      
      cfiles=1 
      call getarg(cfiles,line)
      linelen=len(line)
      call trima(line,linelen)
      linelen=len(oldline)
      call trima(oldline,linelen)
      type=1

#ifdef IFC      
      do while  (line .ne. oldline ) 
        oldline=line
#else
      do while  (line .ne. '' ) 
#endif
        if (line(1:5).eq.'-help' .or. line(1:2).eq.'-h' .or. line(1:2).eq.'-?' ) then 
         write(*,('(a)')) "Usage:"
          write(*,('(a)')) "-table file   : Use a sequence table file in blast format"
          write(*,('(a)')) "-fasta file   : Use a sequence table file in fasta format (default pam250)"
          write(*,('(a)')) "-local        : Use local alignments (default)"
          write(*,('(a)')) "-global       : Use global alignment"
          write(*,('(a)')) "-gloloc       : Use global-local alignment"
          write(*,('(a)')) "-nonorm       : Do not divide score with length"
          write(*,('(a)')) "-inire  file1 file2    : Energy files for threading energy"
          write(*,('(a)')) "-loopgap A B C : cost for gaps in differenst SS regions (default 1,1,1)"
          write(*,('(a)')) "-ssmatch a b c d e f g h i : Scores for secondary structure regions"
          write(*,('(a)')) "-ali        : alignment is printed to stdout for all files"
          write(*,('(a)')) "-pdb        : PDB coordinates are printed to stdout for all files"
          write(*,('(a)')) "                            default 100 010 001 for secstr and"
          write(*,('(a)')) "                            000 010 00 for TM and"
          write(*,('(a)')) "-go X          : Gap opening cost"
          write(*,('(a)')) "-ge X          : Gap  extension cost"
#ifdef PRFPRF
          write(*,('(a)')) "-readmsa          : Do not use seqtable when creating profile"
          write(*,('(a)')) "-readnn file      : read a probscore neural network from a file"
          write(*,('(a)')) "-readnnraw file   : read a neural network from a file"
          write(*,('(a)')) "-shift X          : Factor shift See Yona and Levitt"
          write(*,('(a)')) "-lambda Y         : Factor Lambda See Yona and Levitt"
          write(*,('(a)')) "-profsim          : use profsim scoring"
          write(*,('(a)')) "-ffas             : use ffas scoring"
          write(*,('(a)')) "-picasso          : use picasso3 scoring"
          write(*,('(a)')) "-logaver          : use logaver scoring (default)"
          write(*,('(a)')) "-normprof          : normalize profiles to length 1"
#endif
C     write(*,('(a)')) "-numtop (20)   Number of diagonals to use in the heuristic search"
#ifdef HEUR
          write(*,('(a)')) "-cutoff (12)   Cutoff for hash"
          write(*,('(a)')) "-signcutoff (4*cutoff)  cutoff for diagonals to be used "
          write(*,('(a)')) "-gapcost (30)  Cost for gaps in the heuristic search"
#endif
          write(*,('(a)')) "-minout (10) maximum E-value for beeing printed"
          write(*,('(a)')) "-seq file1 file2 ... - : files to read for sequence info"
          write(*,('(a)')) "file1 file2     : sequence and profile-list file"
          write(*,('(a)')) "Note somehow atleast one sequence or profile-list file has to be named"
          
          stop
        else if (line(1:6).eq.'-table') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readseqtable(line,table,maxaa)
C     call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-fasta') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readfastaseqtable(line,table,maxaa)
C     call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-local') then
          alitype=0
        else if (line(1:7).eq.'-global') then
          alitype=10
        else if (line(1:7).eq.'-gloloc') then
          alitype=1
        else if (line(1:7).eq.'-gloloc') then
          alitype=1
        else if (line(1:7).eq.'-locglo') then
          alitype=1
        else if (line(1:9).eq.'-endsfree') then
          alitype=1
#ifdef PRFPRF
        else if (line(1:8).eq.'-readmsa') then
          convert=.false.
        else if (line(1:10).eq.'-readnnraw') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readnn(line)
          method=6
        else if (line(1:7).eq.'-readnn') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readnn(line)
          method=5
        else if (line(1:6).eq.'-shift') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) shift
        else if (line(1:7).eq.'-lambda') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) lambda
        else if (line(1:8).eq.'-profsim') then
          method=1
        else if (line(1:5).eq.'-ffas') then
          method=2
        else if (line(1:8).eq.'-logaver') then
          method=3
        else if (line(1:8).eq.'-picasso') then
          method=4
       else if (line(1:9).eq.'-normprof') then
          normprof=.true.
#endif
        else if (line(1:7).eq.'-nonorm') then
          renorm=.false.
        else if (line(1:8).eq.'-loopgap') then
          do j=1,3
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) loopgap(j)
            write(*,*)'loopgap',loopgap
          end do
        else if (line(1:8).eq.'-ssmatch') then
          do j=1,3
            do k=1,3
              cfiles=cfiles+1
              call getarg(cfiles,line)
              linelen=len(line)
              call trima(line,linelen)
              read (line,*) ssmatch(j,k)
            end do
          end do
        else if (line(1:3).eq.'-go') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapopen
        else if (line(1:3).eq.'-ge') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapext
        else if (line(1:4).eq.'-ali') then
          printali=.true.
        else if (line(1:4).eq.'-pdb') then
          printpdb=.true.
#ifdef HEUR
        else if (line(1:5).eq.'-ktup') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) ktup
        else if (line(1:7).eq.'-cutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) cutoff
        else if (line(1:11).eq.'-signcutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) signcutoff
        else if (line(1:8).eq.'-gapcost') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapcost
#endif
        else if (line(1:7).eq.'-minout') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) minout
C     else if (line(1:7).eq.'-numtop') then
C     cfiles=cfiles+1
C     call getarg(cfiles,line)
C     linelen=len(line)
C     call trima(line,linelen)
C     read (line,*) numtop
C     if (numtop.ge.maxtop)  call stopprocess('ERROR> numtop too large !')
#ifndef HEUR
#ifndef PMEMBR
#ifndef PRFPRF
        else if (line(1:4).eq.'-seq') then
          templen=0
          cfiles=cfiles+1
          call getarg(cfiles,line)
          do while (line(1:1).ne.'-')
            seqfile=line
C            call trima(seqfile,linelen)
            call readanyseq(line,seq,seqlen,seqfound,profile,seqss,seqssfreq,x,y,z,seqtype,resnum)
            if (seqlen .gt. maxseqlen) call stopprocess('ERROR> Sequence too long !')
            if (templen .ne. 0 .and. proflen .ne. templen) then
              write(*,*) 'Lengths ',templen,proflen 
              call stopprocess('ERROR> sequences of different length !')
            end if
            templen=proflen
            write(*,('(a,1x,a,1x,a,i7)'))'INFO: READING sequence type: ',seqtype,' len: ',seqlen
            cfiles=cfiles+1
            call getarg(cfiles,line)

#ifndef PRFPRF
            if (seqss(1).ne.-1) then
              write(*,*)'Using secondary structure predictions (4):'
              call addsstoseq(seq,seqss,seqlen,numaa)
C     call writeprofile(profile,profseq,proflen,60)
            end if
#endif
          end do
#endif
#endif
#endif

        else if (line(1:5).eq.'-prof') then
           templen=0
           cfiles=cfiles+1
          call getarg(cfiles,line)
          profile(1,1)=-9999
          seqprofile(1,1)=-9999
          do while (line(1:1).ne.'-')
            proffile=line
            proffilelen=len(proffile)
            call trima(proffile,proffilelen)
#ifdef PRFPRF
            call readanyseq(line,profseq,proflen,proffound,profile,profss,profssfreq,x,y,z,proftype,profresnum)
            if ( (.not.convert ).and. (proftype .eq. 'psiblast')) then
              call readpsiblastfreq(line,profile,profseq,proflen,proffound)
              proftype='psiblastfreq'
            end if
            if (normprof) then
               write(*,*) 'INFO> normalizing profiles'
               call profnorm(profile,proflen,aa)
            end if
#else 
            call readanyseq(line,profseq,proflen,proffound,profile,profss,profssfreq,x,y,z,proftype,profresnum)
#endif
            if (proflen .gt. maxproflen) call stopprocess('ERROR> Profile too long !')
            if (templen .ne. 0 .and. proflen .ne. templen) then
              write(*,*) 'Lengths ',templen,proflen 
              call stopprocess('ERROR> profiles of different length !')
            end if
            templen=proflen
            write(*,('(a,1x,a,1x,a,i7)'))'INFO READING1 profile type: ',proftype,' len: ',proflen
C            write(*,*)'INFO READING profile type: ',proftype,' len: ',proflen
            cfiles=cfiles+1
            call getarg(cfiles,line)
          end do
          if (profile(1,1) .eq. -9999) then            
            write(*,('(a)'))'Making a profile:'
            call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
          else
            if (gapopen .gt. 0) gapopen=-gapopen
            if (gapext .gt. 0) gapext=-gapext
            do i=1,proflen
              profile(i,penopen)=gapopen+gapext
              profile(i,penext)=gapext
            end do
          end if
#ifndef PRFPRF
          if (profss(1).ne.-1 ) then
            write(*,*)'Making a secondary structureprofile:',profss(1),profss(2)
            call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
          end if
#else
          if (profss(1).ne.-1 ) then
            write(*,*)'Adding loopgap'
            call makeloopgap(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
          end if
#endif
          type=type+1
          inverse=.true.
        else 
C     Now the last argument has to be a database file 
          linelen=len(line)
          call trima(line,linelen)
          if (linelen .gt. 0)  dbfile=line
        end if
        cfiles=cfiles+1
        call getarg(cfiles,line)
        linelen=len(line)
        call trima(line,linelen)
      end do
      
      
C     for profiles
      lamb = 0.3176
      beta = 10
      call init_freq(aafreq,qfreq,table,lamb,maxaa,numaa)
      
#ifdef HEUR
      call makehash(profile,proflen,hash,position,ktup,cutoff,maxaa)
      if (signcutoff .eq. 0) then
        signcutoff=4*cutoff
      end if
      profsum=0
      profsum2=0
      profnum=0
      do i=1,proflen
        do j=1,aa
          profsum =profsum+profile(i,j)
          profnum =profnum+1
          profsum2=profsum2+profile(i,j)**2
        end do
      end do
      profaver=profsum/profnum
      profsd=sqrt((profsum2-profsum*profsum/profnum)/(profnum-1))
#endif
      
C     We have to find the average and sd values of the profile !!
    
      
      j=0
      Open(unit=infile,file=dbfile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      sumscore=0
      sum2score=0
      numout=0
      num=0

      if (.not. inverse) then 
#ifdef PMEMBR
        call stopprocess('You have to specify a profile')
#elif HEUR
        call stopprocess('You have to specify a profile')
#elif PRFPRF
        call stopprocess('You have to specify a profile')
#else
        firstname=seqfile
        write(*,'(a)')'INFO> starting sequence-profile alignments '
        do while (.true.)
          read(infile,'(a)',err=777,end=666) line
C          write(*,('(a)'))line
          linelen=len(line)
          proffilelen=len(proffile)
          call nextwd(line,linelen,proffile,proffilelen,wdlen)
          call trima(proffile,proffilelen)
C     write(*,('(a)'))'TESTING3',seqfile
          profss(1)=-1
          x(1)=-9999.0
          profile(1,1)=-9999
          do while (proffilelen .gt. 0)
            call readanyseq(proffile,profseq,proflen,proffound,profile,profss,profssfreq,x,y,z,proftype,resnum)
            if (proflen .gt. maxseqlen) call stopprocess('ERROR> profile too long !')
            write(*,('(a,1x,a,1x,a,i7)'))'INFO READING2 profile type: ',seqtype,' len: ',seqlen
            write(*,('(a,1x,a)')) 'PARENT1 ',proffile(1:proffilelen)
            linelen=len(line)
            proffilelen=len(proffile)
            call nextwd(line,linelen,proffile,proffilelen,wdlen)
            call trima(proffile,proffilelen)
          end do
          if (profile(1,1) .eq. -9999) then
            write(*,('(a)'))'Making a profile:'
            call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
          else
            if (gapopen .gt. 0) gapopen=-gapopen
            if (gapext .gt. 0) gapext=-gapext
            do i=1,proflen
              profile(i,penopen)=gapopen+gapext
              profile(i,penext)=gapext
            end do
          end if
#ifndef PRFPRF
          if (profss(1).ne.-1) then
            write(*,*)'Making a secondary structureprofile:',profss(1),profss(2)
            call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
          end if
#else
          if (profss(1).ne.-1) then
            write(*,*)'Adding loopgap info (2)'
            call makeloopgap(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
          end if
#endif
          length=seqlen*proflen
          dseqlen(num)=seqlen
          dproflen(num)=proflen
          if (printali) then
            call dyntrace(profile,proflen,seq,seqlen,alitype,score,traceback)
            do i=1,seqlen
              call numtochar(seqtoseq(seq(i),numaa),char1)
              if (seq(i) .eq. 0) char1='X'
              if (traceback(i).gt.0) then
                call numtochar(seqtoseq(profseq(traceback(i)),numaa),char2)
                if (profseq(traceback(i)) .eq. 0) char2='X'
              else
                char2='.'
              end if
              write(*,('(3x,a,4x,a4,6x,a,1x,i7)'))char1,resnum(i),char2,traceback(i)
            end do
            write(*,('(a)')) 'TER'
          else
            score=dynscore(profile,proflen,seq,seqlen,alitype)
          endif
          num=num+1
C     Should I really divide by length for global alignments ??
          if (renorm)  score=score/log(length)
          heurscore(num)=score
          sumscore=sumscore+score
          sum2score=sum2score+score**2
C      write(*,*)'TEST-SCORE: ',score*log(length),score
          seqss(1)=-1

        end do
#endif
      else
#ifdef PRFPRF
      if (convert) then
        write(*,*)'INFO> Converting profile '
        call profconvert(profile,proflen,aa)
C        write(*,*)'TEST> ',profile(1,1)
      end if
      if (normprof) then
         write(*,*) 'INFO> normalizing profiles'
        call profnorm(profile,proflen,aa)
      end if
        write(*,'(a)')'INFO> starting profile-profile alignments '
#else
        write(*,'(a)')'INFO> starting profile-sequence alignments '
#endif
        firstname=proffile
        do while (.true.)
#ifdef PMEMBR
          linelen=len(line)
          read(infile,*,err=777,end=666) line,seqlen,numtm
C          write(*,*) 'TEST ',line, seqlen, numtm
          call trima(line,linelen)
          if (seqlen .gt. maxseqlen) then
            write(*,('(a,1x,i9)'))'Sequence longer than maxsequence, not using the complete sequence',seqlen
            seqlen=maxseqlen
          end if
          read(infile,*,err=777,end=666) (seq(i),i=1,seqlen)
#elif PRFPRF
          if(.not.convert) then
             read(infile,'(a)',err=777,end=666) line
             linelen=len(line)
             seqfilelen=len(seqfile)
             call nextwd(line,linelen,seqfile,seqfilelen,wdlen)
             call trima(seqfile,seqfilelen)
             call readpsiblastfreq(seqfile,seqprofile,seq,seqlen,seqfound)
             if (.not.seqfound) call stopprocess('Could not read profile info')
             seqtype='psiblastfreq'
             linelen=len(line)
             seqfilelen=len(seqfile)
             write(*,('(a,1x,a)')) 'PARENT2 ',seqfile(1:seqfilelen)
          else
#else
C             write(*,*)'testing66'
             read(infile,'(a)',err=777,end=666) line
             linelen=len(line)
             seqfilelen=len(seqfile)
             call nextwd(line,linelen,seqfile,seqfilelen,wdlen)
             call trima(seqfile,seqfilelen)
C     write(*,*)'TESTING3',seqfile
             seqss(1)=-1
             x(1)=-9999.0
             do while (seqfilelen .gt. 0)
                call readanyseq(seqfile,seq,seqlen,seqfound,seqprofile,seqss,seqssfreq,x,y,z,seqtype,resnum)
                if (seqlen .gt. maxseqlen) call stopprocess('ERROR> Sequence too long !')
                write(*,('(a,1x,a,1x,a,i7)'))'INFO: READING3 sequence type: ',seqtype,' len: ',seqlen
                linelen=len(line)
                seqfilelen=len(seqfile)
                write(*,('(a,1x,a)')) 'PARENT3 ',seqfile(1:seqfilelen)
                call nextwd(line,linelen,seqfile,seqfilelen,wdlen)
                call trima(seqfile,seqfilelen)
             end do
#endif
#ifdef PRFPRF
          end if
         if (convert) then
            if (seqprofile(1,1) .eq. -9999) then            
               write(*,('(a)'))'INFO> Making a profile'
               call makeseqprof(seqprofile,seq,seqlen,table,gapopen,gapext)
            end if
            write(*,('(a)'))'INFO> Converting sequence profile'
            call profconvert(seqprofile,seqlen,numaa)
         end if
         if (normprof) then
            write(*,*) 'INFO> normalizing profile'
            call profnorm(seqprofile,seqlen,numaa)
         end if

C         write(*,*)'CONVtest ',(seqprofile(1,j),j=1,20)
#else 
         if (seqss(1).ne.-1) then
            write(*,*)'Using secondary structure predictions (5):',seqlen
            call addsstoseq(seq,seqss,seqlen,numaa)
         end if
#endif

          length=seqlen*proflen
#ifdef HEUR
          score=dynheuristic(profile,proflen,seq,seqlen,alitype,
     $         hash,position,ktup,gapcost,signcutoff)   
C          write(*,*)'TESTH ',score
#else
          if (printali) then
#ifdef PRFPRF
             write(*,*) 'trace (1)'
#else 
             call dyntrace(profile,proflen,seq,seqlen,alitype,score,traceback)
#endif
            do i=1,seqlen
              call numtochar(seqtoseq(seq(i),numaa),char1)
              if (seq(i) .eq. 0) char1='X'
              if (traceback(i).gt.0) then
                call numtochar(seqtoseq(profseq(traceback(i)),numaa),char2)
                if (profseq(traceback(i)) .eq. 0) char2='X'
              else
                char2='.'
              end if
            write(*,('(3x,a,4x,a4,6x,a,1x,i7)'))char1,resnum(i),char2,traceback(i)
         end do
          write(*,('(a)')) 'TER'
          else

#ifdef PRFPRF
C             write(*,*) 'scoring (1)'
C             call dyntrace_msa(profile,proflen,seqprofile,seqlen,alitype,score,traceback,seqss,profss,seqssfreq,profssfreq,aa,shift,lambda,method,ssmatch)
C             write(*,*) 'test_trace ',score
          score=dynscore_msa(profile,proflen,seqprofile,seqlen,alitype,
     $            seqss,profss,seqssfreq,profssfreq,aa,shift,lambda,method,
     $            ssmatch)
C             write(*,*) 'test_score ',score

#else 
          score=dynscore(profile,proflen,seq,seqlen,alitype)
C          write(*,*)'test ',score
#endif
          end if
#endif
          num=num+1
          dseqlen(num)=seqlen
          dproflen(num)=proflen
C     Should I really divide by length for global alignments ??
          if (renorm)  score=score/log(length)
          heurscore(num)=score
          sumscore=sumscore+score
          sum2score=sum2score+score**2
C         write(*,*)'TEST-SCORE2: ',score*log(length),score
          seqss(1)=-1
        end do
      end if
 666  continue
      write(*,'(a)')'INFO> Alignments are done'
      close (unit=infile)
C
C Now we need to calculate E-values
C
      
      averscore=sumscore/num
      sd=sqrt(abs((sum2score-sumscore*sumscore/num)/(num-1)))
      j=1
      oldaverscore=averscore+1
C      write(*,*)'test1',j,oldaverscore,averscore,sd,dbsize
      do while (j .lt. 5 .and. averscore .ne. oldaverscore)
        oldaverscore=averscore
        j=j+1
        dbsize=0
        sumscore=0
        sum2score=0
        do i=1,num
          zscore=(heurscore(i)-averscore)/sd
#ifdef HEUR
          if (zscore .lt. 7) then
#else
          if (zscore .lt. 7 .and. zscore .gt. -3) then
#endif
            dbsize=dbsize+1
            sumscore=sumscore+heurscore(i)
            sum2score=sum2score+heurscore(i)**2
          end if
        end do
        if (sumscore.gt.0 .and. dbsize.gt.5) then
          averscore=sumscore/dbsize
          sd=sqrt((sum2score-sumscore*sumscore/dbsize)/(dbsize-1))
        end if
C      write(*,*)'test2',j,oldaverscore,averscore,sd,dbsize
      end do

C
C This is to sort the score
C
C      write(*,*)'test0',dbfile
      Open(unit=infile,file=dbfile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
C      write(*,*)'test1'
      do j=1,num 
#ifdef PMEMBR
        read(infile,*,err=777,end=667) line,seqlen,numtm
        call trima(line,linelen)
        if (seqlen .gt. maxseqlen) then
C          write(*,*)'WARNING: Sequence longer than maxsequence, not using the complete sequence'
          seqlen=maxseqlen
        end if
        read(infile,*,err=777,end=667) (seq(i),i=1,seqlen)
#elif PRFPRF
           read(infile,'(a)',err=777,end=666) line
           linelen=len(line)
           seqfilelen=len(seqfile)
           call nextwd(line,linelen,seqfile,seqfilelen,wdlen)
           tempname=seqfile
           call trima(seqfile,seqfilelen)
#else
C      write(*,*)'test2'
        read(infile,'(a)',err=777,end=666) line
C        write(*,*)'TESTING3',line
        linelen=len(line)
        seqfilelen=len(seqfile)
        call nextwd(line,linelen,seqfile,seqfilelen,wdlen)
        tempname=seqfile
        call trima(seqfile,seqfilelen)
C        write(*,*)'TESTING4',tempname
#endif
        score=heurscore(j)
        zscore=(heurscore(j)-averscore)/sd
        evalue=zs_to_e(zscore,dbsize)
#ifdef HEUR
C For heuristic scoring we need to recalculate the score for the ones over the cutoff
        if (evalue .le. minout*4. ) then
          score=dynscore(profile,proflen,seq,seqlen,alitype)
          length=seqlen*proflen
          if (renorm) score=score/log(length)
          zscore=(score-averscore)/sd
          evalue=zs_to_e(zscore,dbsize)
        end if  
#endif       
C        write(*,*) 'test1',i,score,zscore,evalue,numout

        if (evalue .le. minout  ) then
          numout=numout+1
          if (numout .gt. maxdbsize) then
            write(*,('(a)'))'Numout larger than maxdbsize, sequence ignored'
          else
C            write(*,*)'test3: ',numout,score,averscore,sd,dbsize,seqlen,proflen,j,dseqlen(j),dproflen(j)
            fseqlen(numout)=dseqlen(j)
            fproflen(numout)=dproflen(j)
            fscore(numout)=score
#ifdef PMEMBR
            name(numout)=line
#else
            name(numout)=tempname
#endif
          end if
        end if
      end do
        
 667  continue
      write(*,'(1(a,1x),i8,3x,2(e11.3,1x))') 'INFO> From E-valuecalc.',dbsize,
     $     averscore,sd
      if (numout .eq. 0) call stopprocess('no hits found')
      proffilelen=len(firstname)
      call trima(firstname,proffilelen)        
      proffilelen=min(proffilelen,300);
      call indexx(numout,fscore,indx)
      i=min(numout,maxdbsize)
      
      zscore=(fscore(indx(i))-averscore)/sd
      evalue=zs_to_e(zscore,dbsize)
C      write(*,*)'test2: ',zscore,minout,evalue,i,numout
      do while(evalue.le.minout .and. i.ge.1)
        length=fseqlen(indx(i))*fproflen(indx(i))
C        write(*,*)'test ',i,indx(i),fseqlen(indx(i)),fproflen(indx(i))
        linelen=len(name(indx(i)))
        call trima(name(indx(i)),linelen)
        linelen=min(linelen,300);
        evalue=zs_to_e(zscore,dbsize)
        linelen=len(name(indx(i)))
        call trima(name(indx(i)),linelen)
        if (renorm) then
          tempscore=fscore(indx(i))*log(length)
        else
          tempscore=fscore(indx(i))
        end if
        write(*,'(1(a,1x),2(a,1x),f11.5,3i8,3x,3(e11.3,1x))') 'SCORE> ',
     $       firstname(1:proffilelen),name(indx(i))(1:linelen),
     $       tempscore,numout-i+1,
     $       fproflen(indx(i)),fseqlen(indx(i)),fscore(indx(i)),zscore,evalue
        i=i-1
        if(i.ge.1) then
          zscore=(fscore(indx(i))-averscore)/sd
        else
          zscore=-9999
        end if
        evalue=zs_to_e(zscore,dbsize)
      end do
      
      
      close(unit=infile)
      stop
 777  write(*,*) line
      call stopprocess('cannot read list')
      
      end
      
***********************************************************************
      subroutine stopprocess(string)
      character*(*) string
      write(*,('(a)')) string
      write(*,('(a)'))'STOP> Job terminates'
      stop
      end
      
