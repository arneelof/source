C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
************************************************************************
      subroutine makeseqprof(profile,seq,seqlen,table,gapopen,gapext)
C
#include <dynprog.h>      
      integer i,j
      integer seq(*),seqlen
      real profile(maxseqlen,maxaa+2),gapopen,gapext
      real table(maxaa,maxaa)


      if (gapopen .gt. 0) gapopen=-gapopen
      if (gapext .gt. 0) gapext=-gapext
      do i=1,seqlen
        do j=1,20
C          write(*,*)'seqprof',i,j,seq(i),table(j,seq(i))
          profile(i,j)=table(j,seq(i))
        end do
        profile(i,penopen)=gapopen+gapext
        profile(i,penext)=gapext
      end do

C      do i=1,profseqlen
C        write(lout,'(a,i3,a4,i3,20(3x,f5.2,1x,f5.2))')'TEST-seqtable ',i,
C     $       profseq(i),profseqnum(i),
C     $       (profilescores(i,j),seqtable(j,profseqnum(i)) ,j=1,20)
C        end do
      end

************************************************************************
      subroutine makessprof(profile,ss,seqlen,gap,match,aa,sstypes)
C
      implicit none
#include <dynprog.h>      
      integer i,j,k
      integer ss(*),seqlen,sstypes,aa
      real profile(maxseqlen,maxaa+2),temp
      real gap(sstypes),match(0:sstypes,0:sstypes)

      do i=1,seqlen
        do j=1,aa
          temp=profile(i,j)
          do k=1,sstypes
C            write(*,*)'ssprof',i,j,k,profile(i,j),match(ss(i),k),gap(ss(i)),ss(i
            profile(i,j+(k-1)*aa)=temp+match(ss(i),k)
          end do
        end do
        profile(i,penopen)=profile(i,penopen)*gap(ss(i))
        profile(i,penext)=profile(i,penext)*gap(ss(i))
      end do

C      do i=1,profseqlen
C        write(lout,'(a,i3,a4,i3,20(3x,f5.2,1x,f5.2))')'TEST-seqtable ',i,
C     $       profseq(i),profseqnum(i),
C     $       (profilescores(i,j),seqtable(j,profseqnum(i)) ,j=1,20)
C        end do
      end


************************************************************************
      subroutine makeloopgap(profile,ss,seqlen,gap,match,aa,sstypes)
C
      implicit none
#include <dynprog.h>      
      integer i,j,k
      integer ss(*),seqlen,sstypes,aa
      real profile(maxseqlen,maxaa+2),temp
      real gap(sstypes),match(0:sstypes,0:sstypes)

      do i=1,seqlen
        profile(i,penopen)=profile(i,penopen)*gap(ss(i))
        profile(i,penext)=profile(i,penext)*gap(ss(i))
      end do
      end

************************************************************************
      subroutine makessfreqprof(profile,ss,seqlen,gap,match,aa,sstypes)
C
      implicit none
#include <dynprog.h>      
      integer i,j,k
      integer ss(*),seqlen,sstypes,aa
      real profile(maxseqlen,maxaa+2),temp
      real gap(sstypes),match(0:sstypes,0:sstypes)

C This is for frequency profiles
C First we normalize frequence contributions
      do i=1,sstypes
        temp=0
        do j=1,sstypes
          temp=temp+match(i,j)
        end do
        do j=1,sstypes
C          write(*,*)'ssmatchtest1 ',i,j,match(i,j)
          match(i,j)=match(i,j)/temp
C          write(*,*)'ssmatchtest2 ',i,j,match(i,j)
        end do
      end do

      do i=1,seqlen
        do j=1,aa
          temp=profile(i,j)
          do k=1,sstypes
C            write(*,*)'ssprof',i,j,k,profile(i,j),match(ss(i),k),gap(ss(i)),ss(i
            profile(i,j+(k-1)*aa)=temp*match(ss(i),k)
          end do
        end do
        profile(i,penopen)=profile(i,penopen)*gap(ss(i))
        profile(i,penext)=profile(i,penext)*gap(ss(i))
      end do

C      do i=1,profseqlen
C        write(lout,'(a,i3,a4,i3,20(3x,f5.2,1x,f5.2))')'TEST-seqtable ',i,
C     $       profseq(i),profseqnum(i),
C     $       (profilescores(i,j),seqtable(j,profseqnum(i)) ,j=1,20)
C        end do
      end

************************************************************************
      subroutine addsstoseq(seq,ss,seqlen,aa)
      implicit none
      integer i ,seqlen,aa
      integer ss(*),seq(*)
      do i=1,seqlen
        seq(i)=seq(i)+max(0,(ss(i)-1)*aa)
C        write(*,*)'tst ',i,ss(i),aa
C        seq(i)=seq(i)+(ss(i)-1)*aa
      end do
      end
************************************************************************
C
C     Pairpotential energy
C
      subroutine makepairprofile(seq,seqlen,x,y,z,profile,pairpotential,cutoff,numaa)
      implicit none

#include <dynprog.h>      
      integer seqlen,seq(*),pairpotential(maxaa,maxaa),i,j,k,numaa
      real profile(maxseqlen,maxaa+2),x(*),y(*),z(*),cutoff,dist

      do i=1,seqlen
        do k=1,numaa
          profile(i,k)=0
        end do
        do j=i+5,seqlen
          dist=(x(i)-x(j))**2+(y(i)-y(j))**2+(z(i)-z(j))**2
          if (dist .lt. cutoff) then
            do k=1,numaa
              profile(i,k)=profile(i,k)+pairpotential(k,seq(j))
            end do
          end if
        end do
      end do
      return
      end
C************************************************************************
      subroutine profconvert(profile,len,numaa)
C Converts a log-profile to a probability profile

      implicit none
#include <dynprog.h>
#include <seqio.h>

      real profile(maxproflen,maxaa+2),sum,lamb
      integer i,j,numaa,len
C This paramter is for psiblast profiles !!
      parameter(lamb = 0.3176)

C We have problems with different average values of profiles..
C We can normalize to average of -1
C      sum=0
C      do i=1,len
C        do j=1,numaa
C          sum=sum+profile(i,j)
C          write(*,*)'testprf ',i,j,profile(i,j),sum
C        end do
C      end do
C      shift=2+sum/(len*numaa)
C      write(*,*) 'Shifting profile ',shift,numaa
C      do i=1,len
C        do j=1,numaa
C          profile(i,j)=profile(i,j)-shift
C        end do
C      end do
      
      do i=1,len
        sum=0
        do j=1,numaa
C          write(*,*)'proftest1 ',i,j,profile(i,j)
C          profile(i,j)=exp(profile(i,j)*lamb)
          profile(i,j)=aafreq(j)*exp(profile(i,j)*lamb)
          sum=sum+profile(i,j)
        end do        
        do j=1,numaa
          profile(i,j)=profile(i,j)/sum
C          write(*,*)'proftest3 ',i,j,profile(i,j),sum
        end do        
      end do
      return
      end
C************************************************************************
      subroutine readscalepsiblast(SCprofile,seqlen)
C Converts a log-profile to a logistically scaled profile, as used by ProfNet
      implicit none
#include <dynprog.h>      
      integer seqlen,i,j,namtonumold
      real SCprofile(maxproflen,maxaa+2),foo(maxaa+2)
      character*3 res
      external namtonumold,numtonam

      do i=1,seqlen
         do j=1,20
            call numtonam(j,res)
            foo(j)=1/(1+exp(-SCprofile(i,j)))
         end do        
         do j=1,20
            call numtonam(j,res)
            SCprofile(i,namtonumold(res))=foo(j)
         end do
      end do
      return
      end

C************************************************************************
      subroutine profnorm(profile,len,numaa)
C Converts a log-profile to a probability profile

      implicit none
#include <dynprog.h>
#include <seqio.h>

      real profile(maxproflen,maxaa+2),sum
      integer i,j,numaa,len
      
      do i=1,len
        sum=0
        do j=1,numaa
          sum=sum+profile(i,j)**2
        end do        
        sum=sqrt(sum)
        do j=1,numaa
          profile(i,j)=profile(i,j)/sum
        end do        
      end do
      return
      end
