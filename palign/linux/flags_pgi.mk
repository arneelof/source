FFLAG2=    -O3 -static    -funroll-loops -Wall  \
	-ffixed-line-length-none -finline-functions -fomit-frame-pointer -Wimplicit -DGNU

#
#FFLAG2=   -g -ffixed-line-length-none  -fomit-frame-pointer -Wimplicit  -DGNU 


#CPPFLAG= -I./ \
#	 -DBIGREAL  -DSMALLINTEGER  -DGNU \
#	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS


FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 

LINK=g77
F77=g77
CPP=  cpp
#CC=	/modules/gnu/gcc/linux/bin/gcc 
CC=gcc
CCFLAG= -O $(CPPFLAG) -Dgnu  -I./
LFLAG= $(FFLAG2)
CD=	cd

EXTENSION= F

OBJDIR= .
BINDIR= .

MAKE= make

.SUFFIXES: .F .h .o .c .src .f
.src.F:	
	$(F77) -c $(FFLAG) -I./ $*.F

.F.o:	
	$(F77) -c $(FFLAG) -I./ $*.F

.c.o:	
	$(CC) -c $(CCFLAG)  $*.c
