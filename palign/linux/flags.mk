OPTFLAG=  -O3 -static    -funroll-loops -finline-functions
FFLAG2=   $(OPTFLAG) -Wall  -ffixed-line-length-none  -fomit-frame-pointer  -DGNU 


#FFLAG2=   -g -ffixed-line-length-none  -fomit-frame-pointer -Wimplicit  -DGNU 


#CPPFLAG= -I./ \
#	 -DBIGREAL  -DSMALLINTEGER  -DGNU \
#	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS


FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 

LINK=gfortran
F77=gfortran
CPP=  cpp
#CC=	/modules/gnu/gcc/linux/bin/gcc 
CC=gcc
CCFLAG=  $(CPPFLAG) $(OPTFLAG) -DGNU  -I./
LFLAG= $(FFLAG2)
CD=	cd

EXTENSION= F

OBJDIR= .
BINDIR= .

MAKE= make


.SUFFIXES: .F .h .o .c .src .f
.src.F:	
	$(F77) -c $(FFLAG) -I./ $*.F

.F.o:	
	$(F77) -c $(FFLAG) -I./ $*.F

.c.o:	
	$(CC) -c $(CCFLAG)  $*.c


