FFLAG2=     -O9 -static -s -mv8  -ffast-math     -funroll-loops  \
	-ffixed-line-length-none -finline-functions -fomit-frame-pointer

# FFLAG2= -g

#        options = -s -static -O9 -finline-functions -funroll-loops \
#        -funroll-all-loops -fomit-frame-pointer

Note:   The static option makes big diff 
CPPFLAG= -I./  -DBIGREAL  -DSMALLINTEGER  -DGNU \
	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS
FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 
LINK=	/modules/gnu/gcc/sun4/bin/g77 
F77=	/modules/gnu/gcc/sun4/bin/g77
CPP=  cpp
CC=	gcc 
CCFLAG= -g -O $(CPPFLAG) -Dgnu 
CD=	cd

MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino

OBJDIR= .
BINDIR= $(HOME)/source/source5/sun4.g77


# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make

.SUFFIXES: .F .h .o .c .src .f
.src.F:	
#	sed "s/\\\T/\	/g" $*.src  | awk -f split.awk > $*.F
#	cp $*.src $*.F
	$(F77) -c $(FFLAG) -I./ $*.F
#	rm -f $*.F

.F.o:	
#	sed "s/\\\T/\	/g" $*.src  | awk -f split.awk > $*.F
#	awk -f split.awk $*.src | sed "s/\\\T/\	/g" > $*.F
#	cp $*.src $*.F
	$(F77) -c $(FFLAG) -I./ $*.F
#	rm -f $*.F


.c.o:	
	$(CC) -c $(CCFLAG)  $*.c
