C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
C
C  THIS GROUP OF STRING COMMANDS DEALS WITH PICKING OFF THE FIRST WORD
C
      SUBROUTINE NEXTWD(ST,STLEN,WD,WDMAX,WDLEN)
C
C     THIS ROUTINE WILL REMOVE A WORD FROM ST AND PLACE IT
C     IN WD. A WORD IS DEFINED AS A SEQUENCE OF NON-BLANK CHARACTERS.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN,WDMAX,WDLEN
      CHARACTER*(*) ST,WD
#include <stringch.h>
      integer IND,IPT,IPT2
      CHARACTER*1 BLANK,TAB
      DATA BLANK/' '/
      TAB=char(9)
C
C     FIRST LOOK PAST ANY LEADING BLANKS.
C
      IND=0 
    1 IND=IND+1
      IF(IND.GT.STLEN) then
         WDLEN=0
         STLEN=0
         ST=BLANK
         WD=BLANK
         RETURN
      end if
C      write(*,*) "SEARCH:",IND,":",ST(IND:IND),":"
      IF(ST(IND:IND).EQ.BLANK .OR. ST(IND:IND).EQ.TAB) GOTO 1
C      write(*,*) "Word found",IND
C
C     NOW GET THE WORD
C
      IPT=IND
    2 IPT=IPT+1
      IF(IPT.GT.STLEN) then
         STLEN=0
         WDLEN=IPT-IND
         IF(WDLEN.GT.WDMAX) then
            WDLEN=WDMAX
            IPT=WDMAX+IND
         end if
         WD=ST(IND:IPT-1)
         ST=BLANK
         RETURN
      end if
C      write(*,*) "tests:",ichar(st(ipt:ipt)),":",ichar(TAB),":",ichar(BLANK)
C      IF(ST(IPT:IPT).EQ.TAB) WRITE(*,*) "TAB"
      IF(ST(IPT:IPT).NE.BLANK .AND. ST(IPT:IPT).NE.TAB) GOTO 2
C
C     NOW WE HAVE THE WORD, DELETE IT FROM ST
C
C      write(*,*) "testing1 ",st
      IPT2=IPT
      WDLEN=IPT-IND
      IF(WDLEN.GT.WDMAX) then
         WDLEN=WDMAX
         IPT=WDMAX+IND
      end if
      WD=ST(IND:IPT-1)
      SCRTCH=ST(IPT2:STLEN)
      ST=SCRTCH
      STLEN=STLEN-IPT2+1 
C      write(*,*) "testing2 ",st
      RETURN
      END
C     

      SUBROUTINE NEXTOP(ST,START,STLEN,WD,WDMAX,WDLEN)
C
C     This routine will remove an opword from ST and place it
C     in WD. An opword is defined as a sequence of non-blank
C     characters or one of the following symbols ':','(',')'.
C     The search is started at character index START.
C
C
C     Axel Brunger, 25-MAR-83
C     overhauled 2-JUL-84 - BRB
C
#include <impnon.h>
      character*(*) ST,WD
      INTEGER   START, STLEN, WDMAX, WDLEN 
C
#include <stringch.h>
      integer   IND, IPT
      LOGICAL   CONDIT
      CHARACTER*1 STEMP
C
      WD=' '
      WDLEN=0
      IF (STLEN.LT.START) RETURN
C
      IND=START
      do while (.not. (ST(IND:IND).NE.' '.OR.IND.GE.STLEN))
         IND=IND+1
      end do
      STEMP=ST(IND:IND)
      IF (STEMP.EQ.' ') then
         STLEN=START-1 
         RETURN
      end if
C
      IPT=IND
      CONDIT=(STEMP.EQ.')'.OR.STEMP.EQ.'('.OR.STEMP.EQ.':'
     1     .OR.IND.GE.STLEN)
      do while (.not.CONDIT)
         CONDIT=(STEMP.EQ.')'.OR.STEMP.EQ.'('.OR.STEMP.EQ.':'
     1        .OR.IND.GE.STLEN)
         IND=IND+1
         IF (.NOT.CONDIT) then
            STEMP=ST(IND:IND)
            CONDIT=(STEMP.EQ.' '.OR.STEMP.EQ.')'.OR.STEMP.EQ.'('
     1        .OR.STEMP.EQ.':')
         end if
      end do
      WD=ST(IPT:IND-1)
      WDLEN=MIN(IND-IPT,WDMAX)
C
      if (START.GT.1) then
         if (STLEN.GE.IND) then
            SCRTCH=ST(1:START-1)//ST(IND:STLEN)
         ELSE 
            SCRTCH=ST(1:START-1)
         end if
      ELSE
         if (STLEN.GE.IND)then
            SCRTCH=ST(IND:STLEN)
         ELSE 
            SCRTCH=' '
         end if
      end if
C
      ST=SCRTCH
      STLEN=STLEN-IND+START
      RETURN
      END
C     

      REAL FUNCTION NEXTF(ST,STLEN)
C
C     Reads the next word off of ST using NEXTWD, converts it to a
C     floating point number, and returns it.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN
      CHARACTER*(*) ST
#include <stringch.h>
      real DECODF
C 
      CALL NEXTWD(ST,STLEN,SWDTCH,SWDMAX,SWDLEN)
      NEXTF = DECODF(SWDTCH,SWDLEN) 
      RETURN
      END
C     

      REAL FUNCTION NEXTF2(ST,STLEN,value)
C
C     Reads the next word off of ST using NEXTWD, converts it to a
C     floating point number, and returns it.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN
      CHARACTER*(*) ST
#include <stringch.h>
      real DECODF,value
C 
      CALL NEXTWD(ST,STLEN,SWDTCH,SWDMAX,SWDLEN)
      if (swdlen .le. 0) then
        nextf2=value
        return
      end if
      NEXTF2 = DECODF(SWDTCH,SWDLEN) 
      RETURN
      END
C     

      INTEGER FUNCTION NEXTI(ST,STLEN)
C
C     Reads the next word off of ST using NEXTWD, converts it to an
C     integer, and returns it.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN
      CHARACTER*(*) ST
#include <stringch.h>
      integer DECODI
C
      CALL NEXTWD(ST,STLEN,SWDTCH,SWDMAX,SWDLEN)
      NEXTI = DECODI(SWDTCH,SWDLEN)
      RETURN
      END
C     

      INTEGER FUNCTION NEXTI2(ST,STLEN,value)
C
C     Reads the next word off of ST using NEXTWD, converts it to an
C     integer, and returns it.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN
      CHARACTER*(*) ST
#include <stringch.h>
      integer DECODI,value
C
      CALL NEXTWD(ST,STLEN,SWDTCH,SWDMAX,SWDLEN)
      NEXTI2 = DECODI(SWDTCH,SWDLEN)
      if (swdlen .le. 0) then
        nexti2=value
        return
      end if
      RETURN
      END
C     

      CHARACTER*(*) FUNCTION NEXTA4(ST,STLEN)
C
C     Gets the next word off of ST into a four byte word. The word will
C     be blank filled. Declare this function as needed so that Fortran
C     does not do any type conversion on assignment.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN
      CHARACTER*(*) ST
#include <stringch.h>
      integer L
C
      L=LEN(NEXTA4)
      CALL NEXTWD(ST,STLEN,SWDTCH,L,SWDLEN)
      if (SWDLEN.GT.0) then
         NEXTA4=SWDTCH(1:SWDLEN)
      ELSE 
         NEXTA4='            '
      end if
      RETURN
      END
C     

      SUBROUTINE NXTWDA(ST,STLEN,WD,WDMAX,WDLEN,START)
C
C     This routine will remove a word from ST and place it
C     in WD. A word is defined as a sequence of non-blank
C     characters. The search is started at character index START.
C
C
C     WDLEN seems to return the length of the word.
C

C
C     Axel Brunger, 25-MAR-83
C     overhauled 2-JUL-84 - BRB
C
#include <impnon.h>
      character*(*) ST,WD
      INTEGER   START, STLEN, WDMAX, WDLEN
C
#include <stringch.h>
      integer   IND, IPT 
      LOGICAL   CONDIT
      CHARACTER*1 STEMP
C
      WD='                  ' 
      WDLEN=0
      IF (STLEN.LT.START) RETURN
C
      IND=START
      do while (.not. (ST(IND:IND).NE.' '.OR.IND.GE.STLEN))
         IND=IND+1
      end do
      STEMP=ST(IND:IND)
      IF (STEMP.EQ.' ') then
         STLEN=START-1
         RETURN
      end if
C
      IPT=IND
      CONDIT=(IND.GE.STLEN)
      IND=IND+1
      IF (.NOT.CONDIT) then
         STEMP=ST(IND:IND)
         CONDIT=(STEMP.EQ.' ')
      end if
      do while (.not.CONDIT)
         CONDIT=(IND.GE.STLEN)
         IND=IND+1
         IF (.NOT.CONDIT) then
            STEMP=ST(IND:IND)
            CONDIT=(STEMP.EQ.' ')
         end if
      end do
      WD=ST(IPT:IND-1)
      WDLEN=MIN(IND-IPT,WDMAX)
C
      if (START.GT.1) then
         if (STLEN.GE.IND) then
            SCRTCH=ST(1:START-1)//ST(IND:STLEN)
         ELSE 
            SCRTCH=ST(1:START-1)
         end if
      ELSE
         if (STLEN.GE.IND) then
            SCRTCH=ST(IND:STLEN)
         ELSE
            SCRTCH=' '
         end if
      end if
C
      ST=SCRTCH
      STLEN=STLEN-IND+START
      RETURN
      END
C     

C
C  THIS GROUP OF STRING COMMANDS DEALS WITH PICKING OFF AN INTERNAL WORD
C
      SUBROUTINE GTRMWD(ST,STLEN,KEY,KEYLEN,WD,WDMAX,WDLEN)
C
C     THIS ROUTINE PICKS UP THE WORD AFTER THE KEY WORD KEY IN THE
C     STRING ST AND PLACES IT INTO WD. THE KEY AND SUCCEEDING WORD ARE
C     THEN DELETED FROM ST. THE KEY MUST BE FOLLOWED BY A BLANK SO THAT
C     JOINED WORDS WILL NOT WORK.
C     WDLEN seems to return the length of the word.
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN,KEYLEN,WDMAX,WDLEN
      CHARACTER*(*) ST,KEY,WD
      EXTERNAL INDXRA
      INTEGER INDXRA,IND
C
      WDLEN=0
      IND=INDXRA(ST,STLEN,KEY,KEYLEN,.FALSE.)
      IF(IND.GT.0)  CALL NXTWDA(ST,STLEN,WD,WDMAX,WDLEN,IND)
      RETURN
      END
C     

      SUBROUTINE GTRMWA(ST,STLEN,KEY,KEYLEN,WD,WDMAX,WDLEN)
C
C      GeT and ReMove Word Abreviated
C     Gets and removes a word from ST prefixed by the keyword KEY. This
C     routine is similar to GTRMWD except it allows KEY to be an
C     abbreviation.
C     WDLEN seems to return the length of the word.
C
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN,KEYLEN,WDMAX,WDLEN
      CHARACTER*(*) ST,KEY,WD
      EXTERNAL INDXRA
      INTEGER  INDXRA,IND
C
      WDLEN=0
      IND=INDXRA(ST,STLEN,KEY,KEYLEN,.TRUE.)
      IF(IND.GT.0) CALL NXTWDA(ST,STLEN,WD,WDMAX,WDLEN,IND)
      RETURN 
      END
C     

      INTEGER FUNCTION INDXA(ST,STLEN,KEYWRD)
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      CHARACTER*(*) ST,KEYWRD
      INTEGER STLEN,wdlen
      EXTERNAL INDXRA
      INTEGER  INDXRA
C
      wdlen=len(keywrd)
#ifndef   TRIMA_PROBLEM
      call trima(keywrd,wdlen)
#endif
      INDXA=INDXRA(ST,STLEN,KEYWRD,wdlen,.TRUE.)
      RETURN
      END
C     

      INTEGER FUNCTION GTRMI(ST,STLEN,KEYWRD,DEFNUM)
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      CHARACTER*(*) ST
      INTEGER STLEN,wdlen
      CHARACTER*(*) KEYWRD
      INTEGER DEFNUM,NUM,DECODI
C
#include <stringch.h>
c
C 
      NUM=DEFNUM
      wdlen=LEN(KEYWRD)
#ifndef  TRIMA_PROBLEM
      call trima(keywrd,wdlen)
#endif
      IF(STLEN.GT.0) then
         CALL GTRMWA(ST,STLEN,KEYWRD,wdlen,SWDTCH,SWDMAX,SWDLEN)
         IF (SWDLEN.NE.0) NUM=DECODI(SWDTCH,SWDLEN)
      end if
      GTRMI=NUM
      RETURN
      END
C     
      REAL FUNCTION GTRMF(ST,STLEN,KEYWRD,DEFNUM)
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      CHARACTER*(*) ST
      INTEGER STLEN,wdlen
      CHARACTER*(*) KEYWRD
      REAL DEFNUM,NUM,DECODF
C
#include <stringch.h>
c
C
      NUM=DEFNUM
      wdlen=LEN(KEYWRD)
#ifndef  TRIMA_PROBLEM
      call trima(keywrd,wdlen)
#endif
      IF(STLEN.GT.0) then
         CALL GTRMWA(ST,STLEN,KEYWRD,wdlen,SWDTCH,SWDMAX,SWDLEN)
         IF (SWDLEN.NE.0) NUM=DECODF(SWDTCH,SWDLEN)
      end if
      GTRMF=NUM
      RETURN
      END
C     

      CHARACTER*(*) FUNCTION GTRMA(ST,STLEN,KEYWRD)
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      CHARACTER*(*) ST
      INTEGER STLEN,wdlen2
      CHARACTER*(*) KEYWRD
      INTEGER WDLEN
C
#include <stringch.h>
c
      GTRMA='                           '
      wdlen2=LEN(KEYWRD)
#ifndef  TRIMA_PROBLEM
      call trima(keywrd,wdlen2)
#endif
      IF(STLEN.GT.0)  then
         WDLEN=LEN(GTRMA)
         CALL GTRMWA(ST,STLEN,KEYWRD,wdlen2,SWDTCH,WDLEN,SWDLEN)
         IF (SWDLEN.NE.0) GTRMA=SWDTCH(1:SWDLEN)
      end if
C
      RETURN
      END
C     

      INTEGER FUNCTION INDXRA(ST,STLEN,KEY,KEYLEN,LABREV)
C
C     Performs the INDX function except it removes the word in which
C     KEY was found. The KEY must be preceded by a blank or the
C     start of the string if LABREV is true.
C     It must be followed by a blank if LABREV is false.
C     This function can search for and handle abbreviations. In writing
C     this routine, one must realize that one must check every occurence
C     of KEY to see if it has the requisite blanks. 
C
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN,KEYLEN
      CHARACTER*(*) ST,KEY
      LOGICAL LABREV,start
      EXTERNAL INDXAF 
      INTEGER  INDXAF,IND,III
      CHARACTER*1 BLANK
      DATA BLANK/' '/
C 
      IND = 0
      iii = 0
      start=.true.
      do while (start .or. (.not. (IND .EQ. 0 .OR. III .GT. 0)))
         start=.false. 
         III = 0
         IND = INDXAF(ST,STLEN,KEY,KEYLEN,IND)
         if ((IND .NE. 0)) then
            if (IND.EQ.1 .or. ST(IND-1:IND-1).EQ.BLANK)  III = IND
            if (.not.(LABREV.or.(IND+KEYLEN-1.EQ.STLEN).or.
     $           (ST(IND+KEYLEN:IND+KEYLEN).EQ.BLANK))) III=0
         end if
      end do
      IF (III.GT.0) CALL ZAPWD(ST,STLEN,III)
      INDXRA=III
      RETURN
      END
C     

      SUBROUTINE ZAPWD(ST,STLEN,IND)
C
C     Replaces the word starting at IND in ST to blanks. There had
C     better be a non-blank character at ST(IND), otherwise ZAPWD will
C     die.
C
C  This routine is to remain internal to STRING.FLX  - BRB
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN,IND
      CHARACTER*(*) ST
#include <stringch.h>
c
      CALL NXTWDA(ST,STLEN,SCRTCH,SCRMAX,SCRLEN,IND)
      RETURN
      END
C     

      INTEGER FUNCTION INDXAF(ST,STLEN,KEY,KEYLEN,IND)
C
C     Searches the string, ST, for KEY, starting at ST(IND+1). If the
C     search fails, IND is set to zero.
C
C  This routine is to remain internal to STRING.FLX  - BRB
C
C     Author: Robert Bruccoleri
C 
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN,KEYLEN,IND
      CHARACTER*(*) ST,KEY
      EXTERNAL INDX
      INTEGER  INDX
      INTEGER J,K,L
C
C      IF(IND.LT.0) CALL DIEWRN(-2)
      INDXAF=0
      IF(KEYLEN.LE.0) RETURN
      J=IND+1
      IF(J+KEYLEN-1.GT.STLEN) RETURN
      L=STLEN-J+1
      K=INDX(ST(J:STLEN),L,KEY,KEYLEN)
      IF(K.EQ.0) RETURN
      INDXAF=K+J-1
      RETURN
      END
C     

C
C  THIS GROUP OF STRING COMMANDS DEALS WITH STRING COMPARISONS
C
      LOGICAL FUNCTION EQST(ST1,LEN1,ST2,LEN2)
C
C     COMPARES TWO STRINGS AND RETURNS TRUE IF THEY ARE EQUAL.
C
#include <impnon.h>
      character*(*) ST1,ST2
      INTEGER LEN1,LEN2
C
      IF (LEN1.LE.0 .AND. LEN2.LE.0) then
         EQST=.TRUE.
         RETURN
      end if
      IF(LEN1.NE.LEN2) then
         EQST=.FALSE.
         RETURN
      end if
      EQST=ST1(1:LEN1).EQ.ST2(1:LEN2)
      RETURN
      END
C     

      LOGICAL FUNCTION EQSTA(ST,STLEN,KEYWRD)
C
#include <impnon.h>
      character*(*) ST,KEYWRD
      INTEGER STLEN,wdlen
      EXTERNAL EQST
      LOGICAL EQST
C
      wdlen=LEN(KEYWRD)
#ifndef  TRIMA_PROBLEM
      call trima(keywrd,wdlen)
#endif
      EQSTA=EQST(ST,STLEN,KEYWRD,wdlen)
      RETURN
      END
C     

      LOGICAL FUNCTION LTSTEQ(ST1,ST1LEN,ST2,ST2LEN,LEQFG)
C
C     Determines whether string st1 is lexicographically less or equal
C     than string st2, i.e. st1 <= st2. If one string is equal to the
C     beginning of the other string it is considered to be less than it.
C     Axel Brunger, FEB-83
C
#include <impnon.h>
      character*(*) ST1,ST2
      INTEGER   ST1LEN,ST2LEN
      LOGICAL LEQFG
C
      LOGICAL DONE
      INTEGER I
C
      I=0
      done=.false.
      do while (.not.DONE)
         DONE=.TRUE.
         I=I+1
         if    (I.GT.ST2LEN.AND.ST2LEN.EQ.ST1LEN.AND.LEQFG)then
            LTSTEQ=.TRUE. 
         elseif(I.GT.ST2LEN) then
            LTSTEQ=.FALSE.
         elseif (I.GT.ST1LEN) then
            LTSTEQ=.TRUE.
         elseif (ST1(I:I).LT.ST2(I:I)) then
            LTSTEQ=.TRUE.
         elseif(ST1(I:I).EQ.ST2(I:I)) then
            DONE=.FALSE.
         else
            LTSTEQ=.FALSE.
         end if
      end do
      RETURN
      END

C     
************************************************************************
      SUBROUTINE TRIMA(ST,STLEN)
C
C     THIS ROUTINE DECREMENTS STLEN UNTIL IT POINTS TO THE FIRST
C     NON BLANK CHARACTER. IT HAS THE EFFECT OF REMOVING THE
C     THE TRAILING SPACES. IT ALSO REMOVES FRONT END BLANKS
C     B. R. BROOKS 3/83
C
#include <impnon.h>
      character*(*) ST
      INTEGER STLEN
C
C      write (*,*)'TEST-1'
#ifdef TRIMA_PROBLEM
      if (stlen.le.1)  then
        stlen=0
        return
      end if
#endif
      CALL TRIMC(ST,STLEN)
C      write (*,*)'TEST-2'
      CALL TRIMB(ST,STLEN)
C      write (*,*)'TEST-3'
      if (stlen .eq.1 .and. ST(1:1).eq." ") stlen=0
      RETURN
      END
C         
************************************************************
C
C  THIS GROUP OF STRING COMMANDS DEALS WITH STRING MANIPULATION
C
      SUBROUTINE TRIMC(ST,STLEN)
C
C     THIS ROUTINE DECREMENTS STLEN UNTIL IT POINTS TO THE FIRST
C     NON BLANK CHARACTER. IT HAS THE EFFECT OF REMOVING THE
C     THE TRAILING SPACES.
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN
      CHARACTER*(*) ST
      CHARACTER*1 BLANK
      DATA BLANK/' '/
C 
 100  CONTINUE
C      write(*,*)'TEST-trim',st,stlen,ichar(st(stlen:stlen))
#ifdef STLEN_PROBLEMS 
      if (stlen.eq.1 .and. ichar(st(stlen:stlen)).eq.32) return
#endif
      IF(STLEN.EQ.0) then
         ST=BLANK
         RETURN
      end if
      IF(.not.(ST(STLEN:STLEN).eq.BLANK.or.ichar(ST(STLEN:STLEN)).eq.0)) then
         IF(LEN(ST).GT.STLEN+1) ST(STLEN+1:)=' '
         RETURN
      end if
      STLEN=STLEN-1
      GOTO 100
      END
C     

************************************************************
      SUBROUTINE TRIMB(ST,STLEN)
C
C     THIS ROUTINE REMOVES FRONT END BLANKS
C     B. R. BROOKS 3/83
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z) 
      INTEGER STLEN
      CHARACTER*(*) ST

C
#include <stringch.h>
      integer N
      CHARACTER*1 BLANK
      DATA BLANK/' '/
C
C      write(*,*)'TEST-trimb',st,stlen,ichar(st(stlen:stlen))
#ifdef STLEN_PROBLEMS 
      if (stlen.eq.1 .and. ichar(st(stlen:stlen)).eq.32) return
#endif

      IF(STLEN.gt.0) then
         N=1
         
         do while (.not.(ST(N:N).NE.BLANK.OR.N.EQ.STLEN))
            N=N+1
         end do
         IF (ST(N:N).EQ.BLANK.AND.N.EQ.STLEN) then
            STLEN=0
            ST=BLANK
            RETURN
         end if
         if (n.gt.1) then
           SCRTCH=ST(N:STLEN)
           ST=scrtch
           STLEN=STLEN-N+1
         end if
      end if
      RETURN
      END
C     
************************************************************************
      SUBROUTINE CMPRST(ST,STLEN)
C
C      This routine compresses a string by removing all excess
C     blanks. Any group of blanks is replaced by a single blank.
C     Blanks within double quotes will not be compressed.
C
C     Bernard R. Brooks    9/84
C
#include <impnon.h>
      character*(*) ST
      INTEGER STLEN
C
      INTEGER I,IPT
      CHARACTER*1 BLANK,SDBLQ
      LOGICAL LBLANK,LDBLQ
      DATA BLANK/' '/,SDBLQ/'"'/
C
      IPT=0
      LBLANK=.FALSE.
      LDBLQ=.FALSE.
      DO I=1,STLEN
         IF(ST(I:I).EQ.SDBLQ) LDBLQ=.NOT.LDBLQ
         if (LBLANK) then
            IF(ST(I:I).NE.BLANK .OR. LDBLQ) then
               LBLANK=.FALSE.
               IPT=IPT+1
            end if
         ELSE
            LBLANK=(ST(I:I).EQ.BLANK)
            IPT=IPT+1
         end if
         IF(IPT.NE.I) ST(IPT:IPT)=ST(I:I)
      end do
      STLEN=IPT
      RETURN
      END
C     

      SUBROUTINE CNVTUC(ST,STLEN)
C
C     This routine converts a string to upper case and removes
C     nonacceptable control characters. This routine is ASCII code
C     dependant.
C
C       BRB and AB
C
#include <impnon.h>
      integer STLEN
      CHARACTER*(*) ST
      CHARACTER*1 LITA,LITZ,BIGA,BLANK
C  ,TILDE
      INTEGER ITAB
      PARAMETER (LITA='a',LITZ='z',BIGA='A',ITAB=9,BLANK=' ')
C  ,TILDE='~')
C      INTEGER IBLANK,ITILDE
      INTEGER ILITA,ILITZ,IBIGA,ICHR,I
C
      ILITA=ICHAR(LITA)
      ILITZ=ICHAR(LITZ)
      IBIGA=ICHAR(BIGA)-ILITA
C      IBLANK=ICHAR(BLANK)
C      ITILDE=ICHAR(TILDE)
C
      DO I=1,STLEN
         ICHR=ICHAR(ST(I:I))
         IF(ICHR.GE.ILITA.AND.ICHR.LE.ILITZ) ST(I:I)=CHAR(ICHR+IBIGA)
         IF(ICHR .EQ. ITAB) ST(I:I)=BLANK 
C      IF(ICHR.LT.IBLANK.OR.ICHR.GT.ITILDE) ST(I:I)=BLANK
      end do
      RETURN 
      END
C     

      SUBROUTINE SPLITI(STRING,NUM,Alpha,STLEN)
C
C     Axel Brunger, 26-JAN-83
C
C     For the string 'repeat{digit} repeat{Alpha}'
C     the routine splits the numerical and the Alphanumerical part
C     and stores the integer in NUM ,the latter part in Alpha.
C     Leading blanks are removed. On output the length of Alpha is
C     stored in STLEN. However, Alpha is filled up with blanks to
C     STLEN on input.
C     NUM is 0 if no integers are present.
C
#include <impnon.h>
      integer STLEN
      CHARACTER*(*) STRING, Alpha
      INTEGER NUM
C
      INTEGER IND, J, LEN, DECODI
      CHARACTER*1 NUMBER(10)
      DATA NUMBER/'0','1','2','3','4','5','6','7','8','9'/
C
C
      LEN=STLEN
      CALL TRIMC(STRING,LEN)
      IF(LEN.EQ.0) then
         NUM=0
         Alpha=' '
         RETURN
      end if
C
      IND=1
      do while (.not. (IND.EQ.LEN.OR.NUMBER(J).NE.STRING(IND:IND)))
         IND=IND+1
         J=1
         do while (.not. (NUMBER(J).EQ.STRING(IND:IND).OR.J.EQ.10))
            J=J+1
         end do
      end do

      IF (IND.EQ.LEN.AND.NUMBER(J).EQ.STRING(IND:IND)) IND=IND+1
      DO J=1,IND-1
         Alpha(J:J)=STRING(J:J)
      end do
      if (IND.GT.1) then
         NUM=DECODI(Alpha,IND-1)
      ELSE
         NUM=0
      end if
      Alpha=' '
      DO J=1,LEN-IND+1
         Alpha(J:J)=STRING(IND+J-1:IND+J-1)
      end do
      STLEN=LEN-IND+1
      RETURN
      END
C     

C
C  THIS GROUP OF STRING COMMANDS DEALS WITH DATA CONVERSION
C
      SUBROUTINE ENCODI(I,ST,MXSTLN,STLEN)
C
C     ENCODE THE INTEGER I INTO THE STRING ST 
C
C     BRB -  3-JUL-1984
C
#include <impnon.h>
      integer I,MXSTLN,STLEN
      CHARACTER*(*) ST
#include <stringch.h>
c
      WRITE(SCRTCH,25) I
  25  FORMAT(I20)
      SCRLEN=20
      CALL TRIMA(SCRTCH,SCRLEN)
C
      IF(SCRLEN.GT.MXSTLN) then
         write(*,*) ' *** <ENCODI>:  STRING TOO SMALL'
         ST=' '
         STLEN=0
         RETURN
      end if
C
      ST=SCRTCH(1:SCRLEN)
      STLEN=SCRLEN
      RETURN
      END
C     
      SUBROUTINE ENCODF(R,ST,MXSTLN,STLEN)
C     
C     THIS WILL ENCODE THE REAL NUMBER R INTO THE STRING ST AND WILL
C     ATTEMPT TO SHORTEN THE ENCODING AS MUCH AS POSSIBLE. THE NUMBER IS
C     CONVERTED USING A 1PG14.6 FORMAT. THE TECHNIQUES USED FOR
C     SHORTENING ARE REMOVAL OF LEADING BLANKS, REMOVAL OF TRAILING
C     BLANKS, REMOVAL OF A SUPERFLUOUS DECIMAL POINT AND REMOVAL OF
C     LEADING ZEROES IN THE EXPONENT.
C     
C     Author: Robert Bruccoleri
C     
#include <impnon.h>
c     IMPLICIT INTEGER(A-Z)
      REAL R
#if GNU|RS6000
      real*4 tempreal
#endif
      INTEGER MXSTLN,STLEN
      CHARACTER*(*) ST
C     
#include <stringch.h>
 50   character*20 WD
      EXTERNAL INDX
      INTEGER  INDX
      INTEGER WDLEN,IPT,I
C     
      FMTLEN=0
      if(FMTLEN.GT.0) then
C     
C     process stored format for coding
C     
         if (INDX(FMTWD(1:FMTLEN),FMTLEN,'I',1).GT.0) then
C     process with an integer format
#if GNU|RS6000
           tempreal=R
           I=int(R+SIGN(0.5,tempreal))
#else
           I=R+SIGN(0.5,R)
#endif
            WRITE(WD,FMTWD(1:FMTLEN)) I
         ELSE
            WRITE(WD,FMTWD(1:FMTLEN)) R
         end if
         WDLEN=20
         CALL TRIMC(WD,WDLEN)
         ST=WD
         STLEN=WDLEN
      ELSE
C     
C     process standard format coding
C     
         WRITE(WD,200) R
 200     FORMAT (1PG14.6)
         WDLEN=20
         CALL TRIMA(WD,WDLEN)
C     
C     trim off zeroes following the decimal
C     
         IPT=INDX(WD,WDLEN,'E',1)
C     
         if (IPT.GT.0) then
            READ(WD(IPT+1:WDLEN),'(I3)') I
            IF(I.EQ.0) WDLEN=IPT-1
         ELSE
            IPT=WDLEN+1
         end if
         I=IPT
         ipt=ipt-1
         do while (.not. (IPT.EQ.1 .OR. WD(IPT:IPT).NE.'0'))
            IPT=IPT-1
         end do
         IF(WD(IPT:IPT).EQ.'.') IPT=IPT-1
C     
         if (I.LE.WDLEN) then
            ST=WD(1:IPT)//WD(I:WDLEN)
            STLEN=WDLEN-I+IPT+1
         ELSE
            ST=WD(1:IPT)
            STLEN=IPT
         end if
C     
      end if
C     
      IF(STLEN.GT.MXSTLN) then
         WRITE(*,*) ' *** <ENCODF>: STRING TOO SMALL'
         STLEN=0
         return
      end if
C    
      RETURN 
      END
C     

      INTEGER FUNCTION DECODI(ST,STLEN)
C
C     Converts the string into an integer. A Fortran DECODE statement is
C     used.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c     IMPLICIT INTEGER(A-Z) 
      INTEGER STLEN
      CHARACTER*(*) ST
      CHARACTER*6 FMT
      INTEGER L
C
      DECODI=0
      L=STLEN
      CALL TRIMC(ST,L)
C      IF(L.EQ.0) then
C         WRITE (*,190)
C 190     FORMAT(' WARNING from DECODI -- Zero length string being ', 
C     2        'converted to 0')
C         RETURN
C      end if
      WRITE(FMT,200) L
  200 FORMAT('(I',I3,')')
      READ(ST,FMT,ERR=9) L
      DECODI=L
      RETURN 
C
    9 WRITE(*,201) ' WARNING FROM DECODI - COULD NOT CONVERT STRING'
 201  FORMAT(A)
      WRITE(*,201) ST(1:MIN(STLEN,80))
      WRITE(*,201) 'ZERO WILL BE RETURNED.'
      DECODI=0
      RETURN
      END
C     

      REAL FUNCTION DECODF(ST,STLEN)
C
C     DECODES THE STRING INTO A REAL NUMBER AND RETURNS ITS VALUE. IT 
C     USES A F<STLEN>.0 FORMAT SO THAT THE ABSENCE OF A DECIMAL POINT
C     WILL NOT MESS THINGS UP.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
c      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN
      CHARACTER*(*) ST
      CHARACTER*7 FMT
      EXTERNAL EQSTA
      LOGICAL  EQSTA
      INTEGER IOFF,L 
      DOUBLE PRECISION
     &       VAL
C
      DECODF=0.0
      L=STLEN
      IOFF=1
      CALL TRIMC(ST,L)
C      IF (L.EQ.0) then
C         WRITE (*,190)
C 190     FORMAT(' WARNING from DECODF -- Zero length string being ',
C     2        'converted to 0.')
C         RETURN
C      end if
      IF (L.GT.99) then
         WRITE(*,*) ' *** <DECODF>: STRING HAS TOO MANY DIGITS. ZERO USED'
         RETURN
      end if
C
      IF(L.GT.2 .AND. EQSTA(ST,2,'--')) then
         L=L-2
         IOFF=3
      end if
      WRITE(FMT,195) '(F',L,'.0)'
  195 FORMAT(A,I2,A)
      IF(STLEN.LT.IOFF) RETURN
      READ(ST(IOFF:STLEN),FMT,ERR=9) VAL
      DECODF=VAL
      RETURN
C
    9 WRITE(*,201)' WARNING FROM DECODF - COULD NOT CONVERT STRING'
 201  FORMAT(A)
      WRITE(*,201)   ST(1:MIN(6,STLEN))
      WRITE(*,201) 'ZERO WILL BE RETURNED.'
      DECODF=0.0
      RETURN
      END
C     
C
      INTEGER FUNCTION INDX(ST,STLEN,TARGET,TARLEN)
C
C     THIS ROUTINE PERFORMS THE INDEX FUNCTION ON STRINGS WHICH ARE
C     STORED IN CHARACTER ARRAYS.
C
C     Author: Robert Bruccoleri
C
#include <impnon.h>
C      IMPLICIT INTEGER(A-Z)
      INTEGER STLEN,TARLEN
      CHARACTER*(*) ST,TARGET
      INTEGER LIM,I
C
      INDX=0
      IF (STLEN.LE.0) RETURN
      IF(TARLEN.GT.STLEN) RETURN
      IF (TARLEN.LE.0) then
         INDX=1
         RETURN
      endif
      LIM=STLEN-TARLEN+1
      DO I=1,LIM
         IF(ST(I:I+TARLEN-1).EQ.TARGET(1:TARLEN)) then
            INDX=I
            RETURN
         end if
      end do
      RETURN
      END
********************
      SUBROUTINE CONCAT(ST,STLEN,ADDST)
      integer start,stlen
      CHARACTER*(*) ST,ADDST
      START=STLEN+1
      STLEN=STLEN+LEN(ADDST)
      IF((STLEN.GT.LEN(ST)))  then
        write(*,200)st,addst,stlen,len(st),len(addst)
 200    FORMAT(' Error in CONCAT -- Overflow.',a,a,3i20)
        CALL stopprocess(' Error in CONCAT -- Overflow.')
      end if
      ST(START:STLEN)=ADDST
      st(stlen+1:len(st))=' '
      RETURN
      end
********************
      SUBROUTINE CONCAT_trima(ST,STLEN,ADDST)
      integer start,stlen,addlen
      CHARACTER*(*) ST,ADDST
      addlen=len(addst)
      call trima(addst,addlen)
      START=STLEN+1
      STLEN=STLEN+addlen
      IF(.NOT.(STLEN.GT.LEN(ST))) GO TO 99999
      write(*,200)st,addst
 200  FORMAT('WARNING> Overflow in concat trima string cut.: ',a,a)
C      CALL stopprocess(' Error in CONCAT -- Overflow.')
      addlen=len(st)-start+1
99999 ST(START:STLEN)=ADDST(1:addlen)
      st(stlen+1:len(st))='  '
      RETURN
      end
