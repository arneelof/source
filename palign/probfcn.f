C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
C FILE: PROBFCN.FOR - Miscellaneous probability functions
C
      function x2prob(idgf,ssq)
C
C Returns the probability that a reduced chi-square value as poor as SSQ
C would be achieved for random data with IDGF degrees of freedom. 
C Should be larger than say 0.1 to be very good; values less than 0.001
C indicate bad fit. THIS ALL ASSUMES PROPER (NORMAL) WEIGHTING HAS BEEN
C USED !!!
C 
C (This is just a simplified interface to routine gammq)
C
       integer idgf 
       real ssq,x2prob,gammq
       external gammq
C
       real a,x2    
C
       a=idgf/2.0
       x2=(idgf*ssq)/2.0
       x2prob=gammq(a,x2)
       return
       end
C
       real function gammq(a,x)
C
C The incomplete gamma function Q(A,X)=1-P(A,X)
C Use: to estimate probability that random data should give a chi-square
C      w/ idf degrees of freedom larger than chi2: prob=gammq(0.5*idf,0.5*chi2)
C
C  NO ERROR  CHECKS! User has to make sure that A>0, X>0!!!!
C From Numerical Recipes (see page 162 & 506)
C
      real a,x                                              
C
      real gamser,gammcf,gln
C                           
      if(x .lt. a+1.0)then
        call gser(gamser,a,x,gln)
        gammq=1.0-gamser
      else
        call gcf(gammcf,a,x,gln) 
        gammq=gammcf
      endif
      return
      end
C
      subroutine gser(gamser,a,x,gln)
C
C Returns inclomplete gamma function by series expansion
C
      real gamser,a,x,gln
C
      real ap,sum,del,EPS,gammln
      integer n,ITMAX
      parameter (ITMAX=200,EPS=3.0E-7)
C
      gln=gammln(a)
      if(x .le. 0)then
        gamser=0.0
        return
      endif
      ap=a
      sum=1./a
      del=sum
      do 10 n=1,ITMAX
        ap=ap+1.0
        del=del*x/ap
        sum=sum+del
        if(abs(del) .lt. abs(sum)*EPS) goto 20
10      continue        
20    gamser=sum*exp(-x+a*log(X)-gln)
      return
      end
C
      subroutine gcf(gammcf,a,x,gln)
C
C Calculates incomplete gamma function 1-p(a,x) by continued fraction
C
      real gammcf,a,x,gln
C
      real gammln,gold,a0,a1,b0,b1,fac,an,ana,anf,g,EPS
      integer ITMAX,n
      parameter (ITMAX=200,EPS=3.0E-7)
C                                     
      gln=gammln(a)
      gold=0.0
      a0=1.0
      a1=x
      b0=0.0
      b1=1.0
      fac=1.0
      do 10 n=1,ITMAX
        an=n
        ana=an-a
        a0=(a1+a0*ana)*fac
        b0=(b1+b0*ana)*fac
        anf=an*fac
        a1=x*a0+anf*a1
        b1=x*b0+anf*b1
        if(a1 .ne. 0.0)then
          fac=1.0/a1
          g=b1*fac
          if(abs((g-gold)/g) .lt. EPS) goto 20
          gold=g
        endif
10    continue
      write(*,*) '%WARNING: GCF gamma calculation not converged!'
20    gammcf=exp(-x+a*log(x)-gln)*g
      return
      end
C
      real function gammln(xx)
C
C     calculates the value ln(gamma(xx)) for xx>0.
C
      real xx
C
      real cof(6),stp,half,one,fpf,x,tmp,ser
      integer j
      data cof,stp /76.18009173d0,-86.50532033d0,24.01409822d0,
     &     -1.231739516d0,0.120858003d-2,-0.536382d-5,2.50662827465d0/
      data half,one,fpf /0.5d0,1.0d0,5.5d0/
C
      x=xx-one
      tmp=x+fpf
      tmp=(x+half)*log(tmp)-tmp
      ser=one
      do 10 j=1,6
        x=x+one
10      ser=ser+cof(j)/x
      gammln=tmp+log(stp*ser)
      return
      end
C
      FUNCTION TTEST95(IDF) 
C
C calculates ttest coefficient to give 95% confidence interval
C (the coefficient will almost always be close to 2.0)
      integer idf
      real TA(30),ttest95
      DATA TA/12.706,4.303,3.182,2.776,2.571,2.447,2.365,2.306,2.262,
     1     2.228,2.201,2.179,2.160,2.145,2.131,2.120,2.110,2.101,2.093,2.086,
     2     2.080,2.074,2.069,2.064,2.060,2.056,2.052,2.048,2.045,2.042/
      IF(IDF-30)10,10,11
 10   TTEST95=TA(IDF)
      RETURN
 11   IF(IDF-120)12,12,13
 13   TTEST95=1.96
      RETURN
 12   IF(IDF-40)14,14,15
 14   TTEST95=2.042-0.021*FLOAT(IDF-30)/10.0
      RETURN
 15   IF(IDF-60)16,16,17
 16   TTEST95=2.021-0.021*FLOAT(IDF-40)/20.0
      RETURN
 17   TTEST95=2.000-0.002*FLOAT(IDF-60)/60.0
      RETURN                       
      END
