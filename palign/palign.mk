PALIGN=	palign.o 

PMEMBR=	pmembr.o 

PALIGN2= palign_list_new.o 

PALIGNP= palign_prfprf.o 

PALIGNPT= palign_prfprf_test.o 

PALIGNT= palign_test.o 

PALIGNP2= palign_list_prfprf.o


PMEMBR2= pmembr_list.o 

PMEMBRH= pmembr_hash.o dynheuristic.o

PALIGNH= palign_hash.o dynheuristic.o


PALIGNJ= palign_jeanette.o

TEST=	test.o 

LIBR= 	stringa.o stringch.o numrec.o anal_main.o probfcn.o vector.o\
	vdw.o alignanal.o io.o profiles.o

TRACE= dyntrace.o dynprog.o 

LIBMSA=dynprog_msa.o  

LIBMSAC=dynprog_msa_c.o profprof_FFAS.o profprof_profsim.o profprof_logAver.o profprof_picasso3.o




include flags.mk

OBJDIR= .
BINDIR= .




#
#  The real stuff .....
#

$(BINDIR)/palign: $(INCL) $(PALIGN)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palign $(PALIGN) $(LIBR) $(TRACE) 

$(BINDIR)/pmembr: $(INCL) $(PMEMBR)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pmembr $(PMEMBR) $(LIBR) $(TRACE) 

$(BINDIR)/palign2: $(INCL) $(PALIGN2)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palign2 $(PALIGN2) $(LIBR) $(TRACE) 

$(BINDIR)/pmembr2: $(INCL) $(PMEMBR2)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pmembr2 $(PMEMBR2) $(LIBR) $(TRACE) 

$(BINDIR)/pmembrh: $(INCL) $(PMEMBRH)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pmembrh $(PMEMBRH) $(LIBR) $(TRACE) 

$(BINDIR)/palignh: $(INCL) $(PALIGNH)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palignh $(PALIGNH) $(LIBR) $(TRACE) 


$(BINDIR)/palignp: $(INCL) $(PALIGNP)   $(LIBMSA) $(LIBR) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palignp $(PALIGNP) $(LIBMSA) $(LIBR) 

$(BINDIR)/palignpt: $(INCL) $(PALIGNPT)   $(LIBMSA) $(LIBR) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palignpt $(PALIGNPT) $(LIBMSA) $(LIBR) 

$(BINDIR)/palignt: $(INCL) $(PALIGNT)   $(LIBMSA) $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palignt $(PALIGNT) $(LIBMSA) $(LIBR) 

$(BINDIR)/palignp2: $(INCL) $(PALIGNP2)   $(LIBMSA) $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palignp2 $(PALIGNP2) $(LIBMSA) $(LIBR)




# Here are special objective files for -D things
palign_prfprf.o: $(INCL) palign.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o palign_prfprf.o -D PRFPRF palign.$(EXTENSION)

palign_prfprf_test.o: $(INCL) palign.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o palign_prfprf_test.o -D PRFPRF -D PRFTEST palign.$(EXTENSION)

palign_test.o: $(INCL) palign.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o palign_test.o  -D PRFTEST palign.$(EXTENSION)

dynprog_msa.o: $(INCL) dynprog_msa.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o dynprog_msa.o -D PRFPRF dynprog_msa.$(EXTENSION)

dynprog_msa_c.o: $(INCL) dynprog_msa.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o dynprog_msa_c.o -D PRFPRF -D CPROFPROF dynprog_msa.$(EXTENSION)

pmembr.o: $(INCL) palign.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o pmembr.o -D PMEMBR palign.$(EXTENSION)

pmembr_list.o: $(INCL) palign_list_new.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o pmembr_list.o -D PMEMBR palign_list_new.$(EXTENSION)

palign_list_prfprf.o: $(INCL) palign_list_new.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o palign_list_prfprf.o -D PRFPRF palign_list_new.$(EXTENSION)

pmembr_hash.o: $(INCL) palign_list_new.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o pmembr_hash.o -DHEUR -D PMEMBR palign_list_new.$(EXTENSION)

palign_hash.o: $(INCL) palign_list_new.$(EXTENSION)
	$(F77) -c $(FFLAG) -I./ -o palign_hash.o -D HEUR palign_list_new.$(EXTENSION)



#
#	Include stuff ....
#
include dependencies.mk
