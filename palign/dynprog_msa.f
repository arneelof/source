C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
c*******************************************************************************
C
C     Should be identical to dynprog.f except that seq is an array (of size aa)
C
C************************************************************************
      real function dynscore_msa(profile,proflen,seq,seqlen,type,seqss,
     $     profss,seqssfreq,profssfreq,numaa,shift,lambda,method,ssmatch
     $     ,win,scaledpsiprf,scaledpsiseq)
      
c     type=0 => local
c type=1 => ends free
c type=10 => global 
      implicit none
#include <dynprog.h>
#include <seqio.h>

      real bestscore,maxscorey,maxscorex
      integer  x, y,type,yminone,numaa,method
      real ssmatch(0:3,0:3), win(25)
      integer seqss(maxproflen),profss(maxproflen)
      real profile(maxproflen,maxaa+2),wgtend
      real seq(maxproflen,maxaa+2)
      real seqskip, profskip(maxseqlen),typefact,minscore
      real prescore(maxseqlen),  lastscore, bestjump
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real score, diagtest, proftest
      real temp,profprof,profprof_probscore_F,profprof_logaver_F
      real lambda,shift
      real scaledpsiprf(maxproflen,maxaa+2),scaledpsiseq(maxproflen,maxaa+2)
      integer seqlen,proflen
      integer xminone,newseqlen,newproflen
      external profprof,profprof_probscore_F,profprof_logaver_F
c      integer n,picIndx,c,size,u,i
c      write(*,*)'dyn psi',scaledpsiprf(1,1),scaledpsiseq(1,1)      
      newseqlen=seqlen+25
      newproflen=proflen+25
      if (method .eq. 5 .or. method .eq. 6 .or. method .eq. 7) then
c         write(*,*)'Making a prob_score matrix2'
          do x=1,newseqlen
            do y=1,newproflen
               if(x .gt. seqlen .or. y .gt. proflen) then
                  Mprob_score(y,x)=0
               else
                  Mprob_score(y,x)=profprof_probscore_F(y,x,profile,seq
     $                 ,profss,seqss,numaa,shift,lambda,ssmatch
     $                 ,profssfreq ,seqssfreq)
               end if
            end do
         end do
      end if


c     init dimensions and bestscore
c     do the bottom row (y=1)
      wgtend=0
      if (type .eq. 10) then
        typefact=1.
        minscore=-9999.0
        bestscore = -9999.0
        profskip(1)=-9999.0
        maxscorey=minscore
C        prescore(1)=profile(1,seq(1))
C        write(*,*) 'TEST1'
        prescore(1)=profprof(1,1,profile,seq,profss,seqss,profssfreq
     $       ,seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen
     $       ,win,scaledpsiprf,scaledpsiseq)
        wgtend=profile(1,penopen)
C        write (*,*) 'test1:',1,1,prescore(1)
C        bestscore = max(bestscore,score)
        do x = 2, seqlen
          wgtend=wgtend+profile(1,penext)
C          prescore(x)=profile(1,seq(x))+wgtend
C        write(*,*) 'TEST2'
          prescore(x)=profprof(1,x,profile,seq,profss,seqss,profssfreq,
     $         seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win,scaledpsiprf,scaledpsiseq)
          bestscore = max(bestscore,prescore(x))
          profskip(x) = -9999.0
C          write (5,'(a)') "test1:",x,1,prescore(x)
          score=prescore(x)
C          write(*,*) "test1:",x,1,score
        end do
      elseif (type .eq. 1) then
        typefact=0.
        minscore=-9999.0
        bestscore = -9999.0
        maxscorex=-9999.0
        maxscorey=-9999.0
        do x = 1, seqlen
C          score=profile(1,seq(x))
C          prescore(x)= max(score,minscore)
C        write(*,*) 'TEST3'
          score=profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $          numaa,shift,lambda,method,ssmatch,proflen,seqlen,win,scaledpsiprf,scaledpsiseq)
          prescore(x)= max(score,minscore)
          profskip(x) = -9999.0
          bestscore = max(bestscore,score)
C          write(*,*) "test1:",x,profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,shift,lambda,method,ssmatch)
          maxscorey = max(maxscorey,score+profile(x,penopen))+profile(1,penext)
        end do
        maxscorey = max(maxscorey,score)

      elseif (type .eq. 0) then
        typefact=0.
        minscore=0.0
        bestscore = minscore
        do x = 1, seqlen
C          score=profile(1,seq(x))
C          prescore(x)= max(score,minscore)
C        write(*,*) 'TEST4',seqlen
          score=profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $          numaa,shift,lambda,method,ssmatch,proflen,seqlen,win,scaledpsiprf,scaledpsiseq)
C        write(*,*) 'TEST5'
          prescore(x)= max(score,minscore)
          profskip(x) = -9999.0
          bestscore = max(bestscore,score)
C          write(*,*) "test1:",x,profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,shift,lambda,method,ssmatch),seq(x,1),profile(x,1),profss(x),seqss(x),profssfreq(x,1),seqssfreq(x,1),numaa,shift,lambda,method,ssmatch
        end do
      else
        call stopprocess('Unknown alignment methods')
      end if

      wgtend = (profile(1,penopen))*typefact
      seqskip=max(minscore,profile(1,penopen)+profile(1,penext))
      score=minscore

      do y = 2, proflen
        yminone=y-1
C        write(*,*)'test1',maxscorey,score
        maxscorey=max(maxscorey,score+profile(yminone,penopen)*typefact)+profile(yminone,penext)*typefact
c     for x=1 
        x = 1

C        score = max(minscore,(profile(y,seq(x))))
C        write(*,*) 'TEST5'
        score = max(minscore,profprof(y,x,profile,seq,profss,seqss,profssfreq,
     $       seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win,scaledpsiprf,scaledpsiseq))
        lastscore = score
        bestscore = max(bestscore,score)
        seqskip = minscore
        wgtend = wgtend+profile(y,penext)*typefact
C        write (*,*) 'test2:',x,y,prescore(x)
        do x = 2, seqlen
          xminone=x-1
          
C          temp = profile(y,seq(x))
C        write(*,*) 'TEST6'
          temp = profprof(y,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $         numaa,shift,lambda,method,ssmatch,proflen,seqlen,win,scaledpsiprf,scaledpsiseq)
          diagtest = prescore(xminone)
          proftest = profskip(xminone)
C          write(*,*)'TEST ',x,y,temp,diagtest,proftest,seqskip
          score = max(max(diagtest,max(proftest,seqskip))+temp,minscore)
          bestscore = max(bestscore,score)
          bestjump = prescore(xminone) + profile(y,penopen)

          seqskip = max(seqskip,bestjump)+ profile(y,penext)

          profskip(xminone) = max(bestjump,profskip(xminone))+ profile(y,penext)
          prescore(xminone) = lastscore
          lastscore = score
          
C          write(*,*) '#rnd\t< ',profprof(y,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,shift,lambda,method,ssmatch),'\t>'
C          write(*,*) 'seqlen proflen',seqlen,proflen
          
       end do
C        prescore(1)=profile(y,seq(1))+wgtend
C        write(*,*) 'TEST7'
        prescore(1)=profprof(y,1,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $      numaa,shift,lambda,method,ssmatch,proflen,seqlen,win,scaledpsiprf,scaledpsiseq)+wgtend*typefact
      end do


C      write(*,*)'dynscore_msa',score,maxscorey,maxscorex,bestscore

      if (type .eq. 10) then
        maxscorex=max(minscore,score)
        temp=profile(proflen,penopen)
        do x = seqlen-1,1,-1
          temp=temp+profile(proflen,penext)
          maxscorex=max(maxscorex,prescore(x)+temp)
        end do
C        write(*,*)'testing',score,maxscorey,maxscorex
        dynscore_msa=max(score,max(maxscorey,maxscorex))
      else if (type .eq. 1) then
C        write(*,*)'testing',score,minscore,bestscore,maxscorey
         dynscore_msa=max(maxscorey,score)
        do x = seqlen-1,1,-1
          dynscore_msa=max(dynscore_msa,prescore(x))
        end do
      elseif (type .eq. 0) then 
        dynscore_msa=bestscore
      end if
      return 

      end

c*******************************************************************************

      subroutine dyntrace_msa(profile,proflen,seq,seqlen,type
     $     ,dynscore_msa,traceback,seqss,profss,seqssfreq,profssfreq
     $     ,numaa,shift,lambda,method,ssmatch,win,scaledpsiprf
     $     ,scaledpsiseq)

c type=0 => local
c type=1 => ends free
c type=10 => global 
      implicit none

#include <dynprog.h>
#include <seqio.h>

      real bestscore,maxscorex,maxscorey,trace_score
      integer x, y, type,bestx,besty,i,maxy,maxx,numaa,method
      real ssmatch(0:3,0:3),win(25)
      integer seqss(maxproflen),profss(maxproflen)
      real profile(maxproflen,maxaa+2),wgtend,dynscore_msa,seq(maxseqlen,maxaa+2)
      real seqskip, profskip(maxseqlen),typefact,minscore
      real prescore(maxseqlen),  lastscore, bestjump
      real score, diagtest,  proftest
      real temp,profprof
      real shift,lambda,profprof_probscore_F,profprof_logaver_F
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real scaledpsiprf(maxproflen,maxaa+2),scaledpsiseq(maxproflen,maxaa+2)
      integer seqlen,proflen
      integer xminone,traceback(maxseqlen),yminone
      integer prex(maxproflen,maxseqlen),prey(maxproflen,maxseqlen)
      integer xlast,ylast(maxproflen),newseqlen,newproflen
      external profprof,profprof_probscore_F,profprof_logaver_F

      newseqlen=seqlen+25
      newproflen=proflen+25
      if (method .eq. 5 .or. method .eq. 6 .or. method .eq. 7) then
         write(*,*)'INFO> Making a prob_score matrix'
         do x=1,newseqlen
            do y=1,newproflen
               if(x .gt. seqlen .or. y .gt. proflen) then
                  Mprob_score(y,x)=0
               else
                  Mprob_score(y,x)=profprof_probscore_F(y,x,profile,seq
     $                 ,profss,seqss,numaa,shift,lambda,ssmatch
     $                 ,profssfreq ,seqssfreq)
               end if
            end do
         end do
      end if

C      do x=1,seqlen
C         do y=1,20
C            write(*,*) "SEQ: ",x,y,seq(x,y)
C         end do
C      end do
C      do x=1,seqlen
C         do y=1,20
C            write(*,*) "Prof: ",x,y,profile(x,y)
C         end do
C      end do

      typefact=0
C
C
c     init dimensions and bestscore
c     do the bottom row (y=1)

      bestx=1
      besty=1
      wgtend=0
      prex(1,1)=0
      prey(1,1)=0
      if (type .eq. 10) then
        typefact=1
        minscore=-9999.0
        bestscore = -9999.0
        profskip(1)=-9999.0
        maxscorey=minscore
C        prescore(1)=profile(1,seq(1))
        prescore(1)=profprof(1,1,profile,seq,profss,seqss,profssfreq,
     $       seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $       ,scaledpsiprf,scaledpsiseq)
C        write (*,*) 'test1:',1,1,prescore(1)
C        bestscore = max(bestscore,score)
        wgtend=profile(1,penopen)
        ylast(1)=1
C        write (*,*) 'test1:',1,1,prescore(1),' ',prex(1,1),' ',prey(1,1)
        do x = 2, seqlen
          ylast(x)=1
          wgtend=wgtend+profile(1,penext)
C          prescore(x)=profile(1,seq(x))+wgtend
C        write(*,*) 'Test-1'
          prescore(x)=profprof(1,x,profile,seq,profss,seqss,profssfreq,
     $         seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $         ,scaledpsiprf,scaledpsiseq)+wgtend
          prex(1,x)=x-1
          prey(1,x)=0
C          bestscore = max(bestscore,score)
          if (prescore(x) .gt. bestscore) then
            bestscore=prescore(x)
            bestx=x
            besty=1
          end if
          profskip(x) = -9999.0
C          write (*,*) 'test1:',x,1,prescore(x),' ',prex(1,x),' ',prey(1,x)
        end do
      elseif (type .eq. 1) then
         maxx=1;
        typefact=0
        minscore=-9999.0
        bestscore = -9999.0
        maxscorex = -9999.0
        do x = 1, seqlen
C          prescore(x) = max(profile(1,seq(x)),minscore)
C        write(*,*) 'Test-2'
           score=profprof(1,x,profile,seq,profss,seqss,profssfreq
     $          ,seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $          ,scaledpsiprf,scaledpsiseq)
C          write(*,*)'test ',score
          if (minscore .gt. score) then
            prescore(x)=minscore
            prex(1,x)=-1
            prey(1,x)=-1
            ylast(x)=0
          else
            prescore(x)=score
            prex(1,x)=x-1
            prey(1,x)=0
            ylast(x)=x
          endif
          profskip(x) = -9999.0
C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
            bestscore=score
            bestx=x
            besty=1
          end if
C          write (*,*) 'test1:',x,1,' ',prex(1,x),' ',prey(1,x),bestx,besty
          if (maxscorey .lt. score+profile(x,penopen)) then
             maxx=x
             maxscorey=score+profile(x,penopen)
          end if
          maxscorey=maxscorey+profile(1,penext)
C          write (*,*) 'test1:',x,1,seq(x),profile(1,seq(x)),' ',prex(1,x),' ',prey(1,x),bestx,besty
        end do
        if (maxscorey .lt. score) then
           maxx=seqlen
           maxscorey=score
        end if

        maxy=1
      elseif  (type .eq. 0) then
        typefact=0
        minscore=0.0
        bestscore = minscore
        do x = 1, seqlen
C          prescore(x) = max(profile(1,seq(x)),minscore)
C        write(*,*) 'Test-3'
           score=profprof(1,x,profile,seq,profss,seqss,profssfreq
     $          ,seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $          ,scaledpsiprf,scaledpsiseq)
C          write(*,*)'test ',score
          if (minscore .gt. score) then
            prescore(x)=minscore
            prex(1,x)=-1
            prey(1,x)=-1
            ylast(x)=0
          else
            prescore(x)=score
            prex(1,x)=x-1
            prey(1,x)=0
            ylast(x)=x
          endif
          profskip(x) = -9999.0
C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
            bestscore=score
            bestx=x
            besty=1
          end if
C          write (*,*) 'test1:',x,1,' ',prex(1,x),' ',prey(1,x),bestx,besty
        end do
      else
        call stopprocess('Unknown alignment methods')
      end if
      score=minscore
      maxy=1
      wgtend = (profile(1,penopen))*typefact
      seqskip=max(minscore,profile(1,penopen)+profile(1,penext))
      do y = 2, proflen
        yminone=y-1
C        maxscorey=max(maxscorey,score+profile(yminone,penopen))+profile(yminone,penext)
        if (maxscorey.lt. score+profile(yminone,penopen)*typefact) then
          maxy=yminone
          maxscorey=score+profile(yminone,penopen)*typefact
        end if
        maxscorey=maxscorey+profile(yminone,penext)*typefact
c     for x=1 
        x = 1
        xlast=1
C        score = max(minscore,(profile(y,seq(x))+wgtend))
C        if (minscore .gt. profile(y,seq(x))) then
C        write(*,*) 'Test-4'
        if (minscore .gt. profprof(y,x,profile,seq,profss,seqss
     $       ,profssfreq,seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $       ,scaledpsiprf,scaledpsiseq))then
            score=minscore
            prex(y,x)=-1
            prey(y,x)=-1
          else
C            score=profile(y,seq(x))
C        write(*,*) 'Test-5'
             score=profprof(y,x,profile,seq,profss,seqss,profssfreq
     $            ,seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $            ,scaledpsiprf,scaledpsiseq)
C             write(*,*)'test ',y,x,score

            prex(y,x)=0
            prey(y,x)=yminone
          endif

        lastscore = score
C        bestscore = max(bestscore,score)
        if (score .gt. bestscore) then
          bestscore=score
          bestx=x
          besty=y
        end if
        seqskip = minscore
C
C        write (*,*) 'TEST3:',y,score,bestscore,bestx,besty
        wgtend = wgtend+profile(1,penext)*typefact
C        write (*,*) 'TEST2:',score,wgtend

C        write (*,*) 'test2:',x,y,score,prescore(x),' ',prex(y,x),' ',prey(y,x)
        do x = 2, seqlen
          xminone=x-1
          
C          temp = profile(y,seq(x))
C        write(*,*) 'Test-6'
          temp = profprof(y,x,profile,seq,profss,seqss,profssfreq
     $         ,seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $         ,scaledpsiprf,scaledpsiseq)
          diagtest = prescore(xminone)
          proftest = profskip(xminone)

C          write (*,*) 'TEST4: ',y,x,diagtest,proftest,seqskip,temp
C          score=max(diagtest,max(proftest,seqskip))+temp
          if (diagtest .ge. seqskip .and. diagtest .ge. proftest) then
            score=diagtest+temp
            if (diagtest.eq.minscore) then
              prex(y,x)=-1
              prey(y,x)=-1
            else
              prex(y,x)=xminone
              prey(y,x)=yminone
              end if
          elseif (proftest .ge. seqskip) then
            score=proftest+temp
            prex(y,x)=xminone
            prey(y,x)=ylast(xminone)
          else
            score=seqskip+temp
            if (minscore .eq. seqskip) then
               prex(y,x)=-1
               prey(y,x)=-1
            else
               prex(y,x)=xlast
               prey(y,x)=yminone
            end if
         end if
C          score = max(score,minscore)
          if (minscore .gt. score) then
            score=minscore
            prex(y,x)=-1
            prey(y,x)=-1
          endif

C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
            bestscore=score
            bestx=x
            besty=y
          end if

          bestjump = prescore(xminone) + profile(y,penopen)

C          seqskip = max(seqskip,bestjump)
          if (bestjump .gt. seqskip) then 
            seqskip =bestjump
            xlast=xminone
C            write  (*,*) 'changed seqskip'
          end if

          seqskip = seqskip + profile(y,penext)

C          profskip(x) = max(bestjump,profskip(xminone))
          if (profskip(xminone) .lt. bestjump) then
            profskip(xminone) = bestjump
            ylast(xminone)=yminone
C            write  (*,*)'changed profskip',x
          endif

          profskip(xminone) = profskip(xminone) + profile(y,penext)
C          write (*,*)'test4:',bestjump,profskip(xminone),xminone
          prescore(xminone) = lastscore
          lastscore = score
C          write (*,*) 'test3:',x,y,prescore(xminone),score,' ',prex(y,x),' ',prey(y,x)
        end do
C        prescore(1)=profile(y,seq(1))+wgtend
C        write(*,*) 'Test-7'
        prescore(1)=profprof(y,1,profile,seq,profss,seqss,profssfreq
     $       ,seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $       ,scaledpsiprf,scaledpsiseq)+wgtend*typefact
      end do


C      maxscorey=max(score,maxscorey+profile(proflen,penopen)+profile(proflen,penext))
C      write(*,*)'dyntrace1',score,maxscorey,maxscorex,bestscore
C      if (maxscorey+profile(proflen,penopen)+seqlen*profile(proflen,penext).lt. score) then
      if (maxscorey .lt. score) then
        maxy=proflen-1
        maxscorey=score
C      else
C        maxscorey=maxscorey
      end if

C      write(*,*)'dyntrace',score,maxscorey,maxscorex,bestscore

      maxscorex=max(minscore,score)
      maxx=seqlen
      temp=profile(proflen,penopen)*typefact
      do x = seqlen-1,1,-1
        temp=temp+profile(proflen,penext)*typefact
C        maxscorex=max(maxscorex,prescore(x)+temp)
        if (maxscorex .lt. prescore(x)+temp) then
C          write(*,*)'test',x,maxscorex,prescore(x),temp
          maxx=x
          maxscorex=prescore(x)+temp
        end if
      end do


C      write(*,*)'TRACE-test> ',score,maxscorey,maxscorex,maxx,maxy,bestx,besty
      if (type .eq. 10 ) then

        if (score .gt. maxscorey .and. score .gt. maxscorex) then
          dynscore_msa=score
          bestx=seqlen
          besty=proflen
        else if ( maxscorey .gt. maxscorex) then
          dynscore_msa=maxscorey
          bestx=seqlen
          besty=maxy
        else 
          dynscore_msa=maxscorex
          bestx=maxx
          besty=proflen
        end if
      else if (type .eq. 1 ) then
        if (score .gt. maxscorey .and. score .gt. maxscorex) then
          dynscore_msa=score
          bestx=seqlen
          besty=proflen
        else if ( maxscorey .gt. maxscorex) then
          dynscore_msa=maxscorey
          bestx=seqlen
          besty=maxy
        else 
          dynscore_msa=maxscorex
          bestx=maxx
          besty=proflen
        end if
      elseif (type .eq. 0) then
        dynscore_msa=bestscore
      end if

      do x=1,seqlen
        traceback(x)=0.
      end do

      y=besty
      x=bestx

      do while (y.gt.0.and.x.gt.0)
         trace_score=profprof(y,x,profile,seq,profss,seqss,profssfreq,
     $        seqssfreq,numaa,shift,lambda,method,ssmatch,proflen,seqlen,win
     $        ,scaledpsiprf,scaledpsiseq)
          traceback(x)=y
          i=x
          x=prex(y,x)
          y=prey(y,i)
      end do
      return
      end



C************************************************************************
      real function profprof(y,x,profile,seq,profss,seqss,profssfreq
     $     ,seqssfreq,num,shift,lambda,method,ssmatch,proflen,seqlen,win
     $     ,scaledpsiprf,scaledpsiseq)

      implicit none
#include <dynprog.h>
#include <seqio.h>


      integer x,y,num,method,i
      integer profss(maxproflen),seqss(maxproflen),proflen,seqlen,numaa
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real shift,lambda
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real ssmatch(0:3,0:3),win(25)
      real profprof_profsim_F,profprof_dp_F,profprof_logaver_F
      real profprof_probscore_F,profprof_nn_F,profprof_nn_raw_F
      real profprof_win_F
      real scaledpsiprf(maxproflen,maxaa+2),scaledpsiseq(maxproflen
     $     ,maxaa+2)

      external profprof_profsim_F,profprof_dp_F,profprof_logaver_F
      external profprof_probscore_F,profprof_nn_F,profprof_nn_raw_F
c      external profprof_win_F,proflen,seqlen,numaa
      external profprof_win_F,numaa


C      write(*,*) "TEST prfprf",method,seq(1,1),profile(1,1)

      if (method .eq. 5) then
         profprof=profprof_nn_F(y,x,profile,seq,profss,seqss,num,shift
     $        ,lambda,ssmatch,profssfreq,seqssfreq,win,scaledpsiprf
     $        ,scaledpsiseq)
      else if (method .eq. 2) then
         profprof=profprof_dp_F(y,x,profile,seq,profss,seqss,num,shift
     $        ,lambda,ssmatch,profssfreq,seqssfreq)
      else if (method .eq. 4) then
         profprof=profprof_probscore_F(y,x,profile,seq,profss,seqss,num
     $        ,shift,lambda,ssmatch,profssfreq,seqssfreq)
      else if (method .eq. 1) then
         profprof=profprof_profsim_F(y,x,profile,seq,profss,seqss,num
     $        ,shift,lambda,ssmatch,profssfreq,seqssfreq)
      else if (method .eq. 6) then
         profprof=profprof_nn_raw_F(y,x,profile,seq,profss,seqss,num
     $        ,shift,lambda,ssmatch,profssfreq,seqssfreq)
      else if (method .eq. 7) then
         profprof=profprof_win_F(y,x,profile,seq,profss,seqss,num
     $        ,shift,lambda,ssmatch,profssfreq,seqssfreq,win)
      else
C         write (*,*) "TEST1",x,y,profile(1,x),seq(1,y),profss(y),seqss(y)
C         write (*,*) "test2",num,shift,lambda
C         write (*,*) "test3",ssmatch(0,0)
C         write (*,*) "test4",profssfreq(y,1),seqssfreq(x,1)
         profprof=profprof_logaver_F(y,x,profile,seq,profss,seqss,num
     $        ,shift,lambda,ssmatch,profssfreq,seqssfreq)
C         write (*,*) "Foo"
      end if
C      write (*,*) "YAH"

      return
      end
c#ifndef CPROFPROF
C************************************************************************
C yona (prof_sim)
C************************************************************************
      real function profprof_profsim_F(y,x,profile,seq,profss,seqss,num
     $     ,shift,lambda,ssmatch,profssfreq,seqssfreq)

C
C Note that the profile has to be converted to a probability profile before
C This routine is called.
C Seems to be working 17-02-2004

      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      integer k
      real s,d,r(maxaa),r2(maxaa),kullbackleiblerf
      real p(maxaa),q(maxaa),tiny
      parameter (tiny=1.e-20)
      external kullbackleiblerf


      do k=1,num
        r(k)=lambda*profile(y,k)+(1-lambda)*seq(x,k)+tiny
        p(k)=profile(y,k)+tiny
        q(k)=seq(x,k)+tiny
      end do

      d=lambda*kullbackleiblerf(p,r,num)+
     $     (1-lambda)*kullbackleiblerf(q,r,num)

      do k=1,num
        r2(k)=lambda*r(k)+(1-lambda)*initfreq(k)
      end do

      s=lambda*kullbackleiblerf(r,r2,num)+
     $     (1-lambda)*kullbackleiblerf(initfreq,r2,num)
      
      profprof_profsim_F=0.5*(1-d)*(1+s)+shift+ssmatch(seqss(x),profss(y
     $     ))
      return
      end
C************************************************************************
      real function kullbackleiblerf(p,q,num)
C Seems to be working 17-02-2004
      implicit none
#include <dynprog.h>
      real logtwo
      integer k,num
      real p(num),q(num)
      parameter (logtwo=1.442695)

      kullbackleiblerf=0

      do k=1,num
        kullbackleiblerf=kullbackleiblerf+p(k)*log(p(k)/q(k))*logtwo
      end do

      return
      end


C************************************************************************
c  picasso (prob_score)
C************************************************************************

   
      real function profprof_probscore_F(y,x,profile,seq,profss,seqss
     $     ,num,shift,lambda,ssmatch,profssfreq,seqssfreq)
C Seems to be working 19-02-2004 
      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
    
      real Pf1(maxaa+2),Sf1(maxaa+2)
      real tiny, log_oddsf,ssScore, weightss
      integer k,j
      parameter (tiny=1.e-20)
      external log_oddsf
      ssScore=0
      weightss=ssmatch(1,1)
      do k=1,num
         Pf1(k)=profile(y,k)+tiny
         Sf1(k)=seq(x,k)+tiny
      end do

      do j=1,3
         ssScore=ssScore+profssfreq(y,j)*seqssfreq(x,j)
      end do
c      ssScore=0
      profprof_probscore_F=log_oddsf(Pf1,Sf1,num,x,y) + log_oddsf(Sf1
     $     ,Pf1,num,x,y) + shift
      
c      write(*,*)'Pscore',profile(y,21),profile(y,22),profile(y,23)
      return
      end      
C************************************************************************
c      real function log_oddsf(v1,v2,num,x,y,lambda)
      real function log_oddsf(v1,v2,num,x,y)

C Seems to be working 17-02-2004 

      implicit none
#include <dynprog.h>
#include <seqio.h>
      real v1(maxaa+2),v2(maxaa+2)
      integer k,num,x,y


      log_oddsf=0
      do k=1,num
         log_oddsf = log_oddsf + v1(k)*log(v2(k)/initfreq(k))
      end do
      return
      end

C************************************************************************
C  ffas (DP)
C************************************************************************
      real function profprof_dp_F(y,x, profile, seq, profss, seqss,num,
     $     shift,lambda,ssmatch,profssfreq,seqssfreq)

C Seems to be working 17-02-2004      

      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)

      integer k
      real Pf1(maxaa+2), Pf3(maxaa+2)
      real Sf1(maxaa+2), Sf3(maxaa+2)
      real scalar_prod
      real Psq,Ssq,Plength,Slength
      real tiny
      parameter (tiny=1.e-20)
      external final_fracf
      scalar_prod=0

      do k=1,num
         Pf1(k)=profile(y,k)
         Sf1(k)=seq(x,k)
      end do

      call final_fracf(Pf1,Pf3, y, num)
      call final_fracf(Sf1,Sf3, x, num)
  
c     test: try to first make the vectors length =1
      if (lambda .ge.1) then
         do k=1,num
            Psq =Psq + Pf3(k)*Pf3(k)
            Ssq =Ssq + Sf3(k)*Sf3(k)
         end do
         Plength=sqrt(Psq)
         Slength=sqrt(Ssq)
         do k=1,num
            Pf3(k)=Pf3(k)/Plength
            Sf3(k)=Sf3(k)/Slength
         end do
      end if
      
      do k=1,num
         scalar_prod = scalar_prod + Pf3(k)*Sf3(k)
      end do
      profprof_dp_F=scalar_prod+shift+ssmatch(seqss(x),profss(y))
      
      return
      end
C************************************************************************
C  final_frac
C************************************************************************


      subroutine final_fracf(prob_frac,f_new,col,num)
C Seems to be working 17-02-2004
      implicit none
#include <dynprog.h>
#include <seqio.h>
      real prob_frac(maxaa+2),f_new(maxaa+2)
      real f1(maxaa+2), f2(maxaa+2)
      real  sqSum_f2
      integer k,col,num
      sqSum_f2=0
      do k=1,num
         f1(k) = prob_frac(k)*initfreq(k)
      end do
      
      do k=1,20
         f2(k)=5*f1(k) + prob_frac(k)
         sqSum_f2 = sqSum_f2 + f2(k)*f2(k)
      end do
      
      do k=1,20
         f_new(k)=f2(k)*f2(k)/sqSum_f2
      end do
      
      end
   
      
C************************************************************************
C     log_aver
C************************************************************************
      
      real function profprof_logaver_F(y,x,profile,seq,profss,seqss,num
     $     ,shift,lambda,ssmatch,profssfreq,seqssfreq)
C Seems to be working 19-02-2004
      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real p(maxaa+2), q(maxaa+2)
      integer k,true
      real scoreAveragef
      external scoreAveragef
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)

      real psum,qsum
      psum=0
      qsum=0
C      write (*,*) "test1 ",num,x,y
      do k=1,num
C         write (*,*) "testk ",k,x,y
C         write (*,*) "test PR ",k,profile(y,k)
C         write (*,*) "test SW ",k,seq(x,k)
         p(k)=profile(y,k)
         q(k)=seq(x,k)
         psum=psum+p(k)
         qsum=qsum+q(k)
      end do

      if(oldy .eq. y)then
         true=0
      end if

C      write (*,*) "test PQ ",p,q,num,x,y
C      write (*,*) "score ",scoreAveragef(p,q,num,true,x,y)
      profprof_logaver_F=log(scoreAveragef(p,q,num,true,x,y))+shift+ssmatch(seqss(x),profss(y))
C      write (*,*) "log ",log(scoreAveragef(p,q,num,true,x,y))

      oldy=y

      return
      end
      
C************************************************************************
      real function scoreAveragef(p,q,num,true,x,y)
C Seems to be working 19-02-2004      
      implicit none
#include <dynprog.h>
#include <seqio.h>

      real p(maxaa+2), q(maxaa+2)
      integer num,true
      real v(maxaa)
      integer k,l,i 
      real logtwo
      parameter (logtwo=0.693147)
      integer x,y
      true=1
      scoreAveragef=0

      do i=1,20
         v(i)=0
         do k=1,20
            v(i)=v(i)+p(k)*exp( (logtwo/2)*(table(i,k)) )
         end do           
      end do
      
      do l=1,20
         scoreAveragef = scoreAveragef + v(l)*q(l)
      end do
      
       return
      end

 
C************************************************************************
C     nn_score 
C     Written by tomas ohlson 2003:2006-03
C     tomas.ohlson@gmail.com
C
C************************************************************************
      real function profprof_nn_F(y,x,profile,seq,profss,seqss,num,shift
     $     ,lambda,ssmatch,profssfreq,seqssfreq,win,scaledpsiprf
     $     ,scaledpsiseq)
      implicit none
#include <dynprog.h>
#include <seqio.h>
      
      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real seqssfreq(maxproflen,maxss),profssfreq(maxproflen,maxss)
      real ssmatch(0:3,0:3),win(25),netfwdf,profprof_win_F,ssScore
      real scaledpsiprf(maxproflen,maxaa+2),scaledpsiseq(maxproflen
     $     ,maxaa+2)
      integer size,gapSize,ssSize,ssGapSize,pal_picSize,palSize
     $     ,pal_1picSize,pic_1ssSize
      integer c,g,h,i,j,n,r,index,palgap_1picsize
      integer SOMSize,SOMpicSize,RespicSize,SOMpicSizeSSnum
     $     ,SOMpicSizeSSall,SOM12picSize,SOM12picSSSize,picSSdist
     $     ,picSSdata,picSSdistdata, SOM9picSSSize
      integer SOMpicSSallDist,SOM12picSSallDist,SOM2d_ss_ssdist_pic
     $     ,SOM2d_ss_pic,SOM4d_ss_ssdist_pic,SOM12picSSallDist_binary
     $     ,SOMdistpicSSallDist,SOM6picSSSize
      external netfwdf,profprof_win_F
      real ann_input(600)
      g=0
      h=0
      i=0
      n=0
      size=0
      ssScore=0

      do i=1,25
         if(win(i)>0) then
            size=size+1
         end if
      end do

      gapSize=3*size
      SOMSize=6*size
      SOMpicSize=6*size+1000000
      SOM12picSize=24*size+1
      SOM12picSSSize=30*size+1
      SOM6picSSSize=20*size
      SOM9picSSSize=26*size
      SOM2d_ss_ssdist_pic=12*size
      SOM4d_ss_ssdist_pic=16*size
      SOM2d_ss_pic=11*size
      RespicSize=13*size
      ssSize=2*size
      pic_1ssSize=size +1
      ssGapSize=4*size  
      pal_picSize=41*size
      palSize=40*size
      pal_1picSize=40*size+1
      palgap_1picSize=42*size + 1
      SOMpicSizeSSnum=7*size +10000000
      SOMpicSizeSSall=12*size +1

      picSSdist=2*size
      picSSdata=7*size
      picSSdistdata=8*size

      SOMpicSSallDist=12*size +2
      SOMdistpicSSallDist=15*size
      SOM12picSSallDist=30*size +2
      SOM12picSSallDist_binary=50*size
      c=((size-1)/2)

      if(picSSdata .eq. nin)then
         index=1
         do n=1,size
            if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
               ann_input(index)=Mprob_score(y+(-c+n-1),x+(-c+n-1))
               index=1+index
               ann_input(index+3)=profssfreq(y+(-c+n-1),3)
c               ann_input(index+4:index+5)=profssfreq(y+(-c+n-1),1:2)
               ann_input(index+4)=profssfreq(y+(-c+n-1),1)
               ann_input(index+5)=profssfreq(y+(-c+n-1),2)
               ann_input(index)=seqssfreq(x+(-c+n-1),3)
c               ann_input(index+1:index+2)=seqssfreq(x+(-c+n-1),1:2)
               ann_input(index+1)=seqssfreq(x+(-c+n-1),1)
               ann_input(index+2)=seqssfreq(x+(-c+n-1),2)
              end if
         end do
            
      elseif(pal_1picSize .eq. nin)then
         if(lambda .eq. 1)then
            index=1
            do n=1,size
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  do r=1,20
                     ann_input(index+19+r)=seq(x+(-c+n-1),r)
                  end do
                  if(y .ne. oldy)then
                     do r=1,20
                     ann_input(index-1+r)=profile(y+(-c+n-1),r)
                  end do
                  end if

               end if
               index=index+40
            end do
            ann_input(index)=Mprob_score(y,x)
         elseif(lambda .eq. 2)then
            index=1
            do n=1,size
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  do r=1,20
                     ann_input(index-1+r)=scaledpsiseq(x+(-c+n-1),r)
                     ann_input(index+19+r)=scaledpsiprf(y+(-c+n-1),r)
                  end do
               else
                  do r=1,40
                     ann_input(index-1+r)=0.5
                  end do
               end if
               index=index+40
            end do
            ann_input(index)=Mprob_score(y,x)
         end if
      elseif(palgap_1picSize .eq. nin)then
         if(lambda .eq. 2)then
            index=1
            do n=1,size
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  do r=1,20
                     ann_input(index-1+r)=scaledpsiseq(x+(-c+n-1),r)
                     ann_input(index+20+r)=scaledpsiprf(y+(-c+n-1),r)
                  end do
                     ann_input(index+20)=1/(1+exp(-scaledpsiseq(x+(-c+n-1),22)))
                     ann_input(index+41)=1/(1+exp(-scaledpsiprf(y+(-c+n-1),22)))
                  else
                     do r=1,40
                        ann_input(index-1+r)=0.5
                     end do
               end if
               index=index+42
            end do
            ann_input(index)=Mprob_score(y,x)
         end if
      elseif( size .eq. nin) then 
         do i=1,size
            if(y+(-c+i-1) .gt. 0 .and. x+(-c+i-1) .gt. 0) then
               ann_input(i)=Mprob_score(y+(-c+i-1),x+(-c+i-1))
            else
               ann_input(i)=0
            end if
         end do
c     prob_score vector with gap working date Tue Jan  4 12:49:47 CET 2005
      elseif( gapSize .eq. nin) then
         do i=1,gapSize
            if(mod(i,3) .eq. 1) then
               n=n+1
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  ann_input(i)=Mprob_score(y+(-c+n-1),x+(-c+n-1))
               else
                  ann_input(i)=0
               end if
            elseif(mod(i,3) .eq. 2) then
               g=g+1
               if(y+(-c+g-1) .gt. 0)then
                  ann_input(i)=profile(y+(-c+g-1),22)
               else
                  ann_input(i)=0
               end if
            elseif(mod(i,3) .eq. 0) then
               h=h+1
               if(y+(-c+h-1) .gt. 0)then
                  ann_input(i)=seq(x+(-c+h-1),22)
               else
                  ann_input(i)=0
               end if
            end if
         end do
c     prob_score vector with ss info  working date Tue Jan  4 12:49:47 CET 2005
      elseif(ssSize .eq. nin)then
         do i=1,ssSize
            if(i .le. size) then
               if(y+(-c+i-1) .gt. 0 .and. x+(-c+i-1) .gt. 0) then
                  ann_input(i)=Mprob_score(y+(-c+i-1),x+(-c+i-1))
            else
               ann_input(i)=0
            end if
            elseif(i .gt. size) then
               j=i-size
               if(y+(-c+j-1) .gt. 0 .and.  x+(-c+j-1) .gt. 0 ) then
                  do r=1,3
                     ssScore=ssScore+profssfreq(y+(-c+j-1),r)
     $                    *seqssfreq(x+(-c+j-1),r)
                     end do
                  end if
                  ann_input(i)=ssScore
                  ssScore=0
               end if
            end do
c     prob_score vector with ss info and gap working date Tue Jan  4 12:49:47 CET 2005
      elseif(ssGapSize .eq. nin)then
         do i=1,ssGapSize
            if(i .le. gapSize)then
                  if(mod(i,3) .eq. 1) then
                     n=n+1
                     if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                        ann_input(i)=Mprob_score(y+(-c+n-1),x+(-c+n-1))
                     else
                        ann_input(i)=0
                     end if
                  elseif(mod(i,3) .eq. 2) then
                     g=g+1
                     if(y+(-c+g-1) .gt. 0 ) then
                        ann_input(i)=profile(y+(-c+g-1),22)
                     else
                        ann_input(i)=0
                     end if
                  elseif(mod(i,3) .eq. 0) then
                     h=h+1
                     if(x+(-c+h-1) .gt. 0) then
                        ann_input(i)=seq(x+(-c+h-1),22)
                     else
                        ann_input(i)=0
                     end if
                  end if
               elseif(i .gt. gapSize) then
                  j=i-gapSize
                  if(y+(-c+j-1) .gt. 0 .and.  x+(-c+j-1) .gt. 0 ) then
                     do r=1,3
                        ssScore=ssScore+profssfreq(y+(-c+j-1),r)
     $                       *seqssfreq(x+(-c+j-1),r)
                     end do
               end if
               ann_input(i)=ssScore
               ssScore=0
            end if
         end do
      elseif(pic_1ssSize .eq. nin) then 
         do i=1,size
            if(y+(-c+i-1) .gt. 0 .and. x+(-c+i-1) .gt. 0) then
               ann_input(i)=Mprob_score(y+(-c+i-1),x+(-c+i-1))
            else
               ann_input(i)=0
            end if
         end do
         do r=1,3
            ssScore=ssScore+profssfreq(y,r)*seqssfreq(x,r)
         end do
         ann_input(size+1)=ssScore
      elseif(pal_picSize .eq. nin)then
         if(y .eq. 29 .and. x .eq. 8)then
         end if
         if(lambda .eq. 1)then
            index=1
            do n=1,size
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  do r=1,20
                     ann_input(index-1+r)=seq(x+(-c+n-1),r)
                     ann_input(index+19+r)=profile(y+(-c+n-1),r)
                  end do
                  ann_input(n*41)=Mprob_score(y+(-c+n-1),x+(-c+n-1))
               end if
               index=index+41
            end do
         elseif(lambda .eq. 2)then
            index=1
            do n=1,size
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  do r=1,20
                     ann_input(index-1+r)=scaledpsiseq(x+(-c+n-1),r)
                     ann_input(index+19+r)=scaledpsiprf(y+(-c+n-1),r)
                  end do
                  ann_input(n*41)=Mprob_score(y+(-c+n-1),x+(-c+n-1))
               end if
               index=index+41
            end do
         end if
      elseif(palSize .eq. nin)then
         if(lambda .eq. 1)then
            index=1
            do n=1,size
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  do r=1,20
                     ann_input(index-1+r)=seq(x+(-c+n-1),r)
                     ann_input(index+19+r)=profile(y+(-c+n-1),r)
                  end do
               end if
               index=index+40
            end do
         elseif(lambda .eq. 2)then
            index=1
            do n=1,size
               if(y+(-c+n-1) .gt. 0 .and. x+(-c+n-1) .gt. 0) then
                  do r=1,20
                     ann_input(index-1+r)=scaledpsiseq(x+(-c+n-1),r)
                     ann_input(index+19+r)=scaledpsiprf(y+(-c+n-1),r)
                  end do
               end if
            index=index+40
         end do
      end if
      
      end if
      
      oldy=y

      profprof_nn_F=netfwdf(ann_input)+shift

      return
      end
      
C************************************************************************    
C************************************************************************
C     win_score 
C************************************************************************
      real function profprof_win_F(y,x,profile,seq,profss,seqss,num
     $     ,shift,lambda,ssmatch,profssfreq,seqssfreq,win)

      implicit none
#include <dynprog.h>
#include <seqio.h>
      
      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3),win(25)
      real p
      integer window,nvalue,size,c,i
      integer j,r,start,null
      real profssfreq(maxproflen,maxss),seqssfreq(maxproflen,maxss)
      real netfwdf,profprof_probscore_F,totweight
      real ssweight,ssScore,ss,matSum,res,inner_diff
      real maxscore
      external netfwdf,profprof_probscore_F
      p=0
      maxscore=-9999
      inner_diff=0
      start=0
      res=0
      null=0
      matSum=0
      size=0
      nvalue=0
      totweight=0
c      index=1
      ssScore=0
      ss=0
      window=nin/40
      do i=1,25
         if(win(i)>0) then
            size=size+1
            totweight=totweight+win(i)
         end if
         matSum=matSum+Mprob_score(i,i)
      end do
      ssweight=ssmatch(1,1)/size

    
      c=((size-1)/2)
      do j=1,size
         p=p+(win(j)/totweight)*Mprob_score(y+(-c+j-1),x+(-c+j-1))
c         write (*,*) 'inside WIN', win(j),Mprob_score(y+(-c+j-1),x+(-c+j-1))

         if(y+(-c+j-1) .gt. 0 .and.  x+(-c+j-1) .gt. 0 ) then
            do r=1,3
               ssScore=ssScore+profssfreq(y+(-c+j-1),r)*seqssfreq(x+(-c+j-1),r)
            end do
         end if
c         write(*,*)'profprof_win_F',Mprob_score(y+(-c+j-1),x+(-c+j-1)),win(j),y+(-c+j-1),x+(-c+j-1),(-c+j-1)
      end do
      
      profprof_win_F = p + shift + ssScore*ssweight
c      profprof_win_F = res + ssScore*ssweight
c      write(*,*)'profprof_win_F',profprof_win_F,x,y,p+shift
      return
      end 
C************************************************************************       
 
      real function profprof_nn_raw_F(y,x,profile,seq,profss,seqss,num,shift,lambda,
     $     ssmatch,profssfreq,seqssfreq)
C     Seems to be working 19-02-2004
      implicit none
#include <dynprog.h>
#include <seqio.h>
      
      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
c      real p(nin+2)
      integer window,nvalue
c,index
      real netfwdf,profprof_probscore_F
      external netfwdf,profprof_probscore_F
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real newscore
      nvalue=0
c      index=0
      window=nin/40

         if (y-2 .lt. 0 .or. x-2 .lt. 0 .and. y-1 .lt. 0 .or. x-1 .lt. 0 ) then
            newscore=0*Mprob_score(y-2,x-2)+0*Mprob_score(y-1,x-1)+0.4
     $           *Mprob_score(y,x)+0.2*Mprob_score(y+1,x+1)+0.1
     $           *Mprob_score(y+2,x+2)
         else if (y-2 .lt. 0 .or. x-2 .lt. 0 ) then
            newscore=0*Mprob_score(y-2,x-2)+0.2*Mprob_score(y-1,x-1)+0.4
     $           *Mprob_score(y,x)+0.2*Mprob_score(y+1,x+1)+0.1
     $           *Mprob_score(y+2,x+2)
         else
            newscore=0.1*Mprob_score(y-2,x-2)+0.2*Mprob_score(y-1,x-1)+0
     $           .4*Mprob_score(y,x)+0.2*Mprob_score(y+1,x+1)+0.1
     $           *Mprob_score(y+2,x+2)
         end if


         profprof_nn_raw_F=newscore+shift
c         write(*,*)'profprof_nn_raw_F',profprof_nn_raw_F
         return
         end
      
C************************************************************************

C      real function netfwdf(values)
C      
C      implicit none
C#include <dynprog.h>
C#include <seqio.h>
C      
C      integer i,j
Cc      real nodsum,values(2*maxaa+4)
C      real nodsum,values(620)
C
C 
C
C         do i=1,nhidden
C            nodsum=0
C            do j=1,nin
C               nodsum=nodsum + w1(j,i)*values(j);
C            end do
Cc            nodsum=dot_product( w1(1:nin,i),values(1:nin))
C            nodsum=nodsum+b1(i,1);
C            nodsum=tanh(nodsum);
C            netfwdf=netfwdf + nodsum*w2(i,1);
C            
C         end do
C         netfwdf=netfwdf+b2(1) 
C 
Cc      write(*,*)'nwt',w1(1,1:21),w1(1:21,1)
C
Cc      write(*,*)netfwdf
C      return 
C      end
      real function netfwdf(values)
      
      implicit none
#include <dynprog.h>
#include <seqio.h>
      
      integer i,j,k,nets
c     real nodsum,values(2*maxaa+4)
      real nodsum,values(620),res
      
      res=0
      do k=1,nets
         do i=1,nhidden
            nodsum=0
            do j=1,nin
               nodsum=nodsum + w1(j+(k-1)*nin,i)*values(j);
            end do
c     nodsum=dot_product( w1(1:nin,i),values(1:nin))
            nodsum=nodsum+b1(i,1+(k-1)*nout);
            nodsum=tanh(nodsum);
            netfwdf=netfwdf + nodsum*w2(i+(k-1)*nhidden,1);
            
         end do
         netfwdf=netfwdf+b2(1+(k-1)*nout) 
         res=res+netfwdf
         netfwdf=0
c     write(*,*)'netfwdf',k,nets,res
      enddo
      netfwdf=res/nets
c      write(*,*)'nwt',w1(1,1:21),w1(1:21,1)
      
      return 
      end
C************************************************************************

