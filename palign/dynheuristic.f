C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
c*******************************************************************************

      real function dynheuristic(profile,proflen,seq,seqlen,type,hash,
     $     position,ktup,gapcost,signcutoff)

      implicit none
#include <dynprog.h>

      real gapcost,minscore
      integer numtop
      parameter (minscore=0)

      
      integer i,j,k,index,shift
      integer type,ktup,start,stop,start2,stop2
      integer hash(maxhash,maxktup),position(maxhash,maxktup)
      real match(-maxseqlen:maxproflen),x
      integer last(-maxseqlen:maxproflen)
      integer first(-maxseqlen:maxproflen)
      real bestmatch(maxtop+1),s2(maxtop),skiptest(0:maxproflen)
      real profile(maxproflen,maxaa+2)
      real score(maxtop),tempscore,bestscore,signcutoff
      integer seqlen,proflen,seq(maxseqlen),length,bestshift(maxtop+1)
C      real s(maxtop,maxproflen)
      integer distance,numsign

C      if (numtop.ge.maxtop) then
C        numtop=maxtop
C      end if


      start2=maxseqlen
      stop2=0
      bestscore=0
      do i=1,maxtop+1
        bestmatch(i)=0
        bestshift(i)=0
      end do
      
      do j=0,maxtop
        s2(j)=0
      end do
      do j=1,maxtop
        s2(j)=0
C        do i=1,max(seqlen,proflen)
C          s(j,i)=0
C        end do
      end do
      do i=0,max(seqlen,proflen)
        skiptest(i)=0
      end do

C
C     First we need to search the hash of ktup-n of the profile
C
      
C a good gutoff could be something like 10
c      l=1
      do i=-seqlen,proflen
        first(i)=seqlen
        last(i)=0
        match(i)=0
      end do

      do i=1,seqlen+1-ktup
        index=seq(i)-1+(seq(i+1)-1)*maxaa+1
        k=1
C        write(*,*) 'match-test: ',i,seq(i),seq(i+1),k,index,position(k,index)
        do while  (position(k,index) .gt. 0) 
          shift=position(k,index)-i
C          write(*,*) 'match: ',i,j,k,shift,index,match(shift)
          match(shift)=match(shift)+hash(k,index)
          first(shift)=min(first(shift),i)
          last(shift)=max(last(shift),i)
C          start(l)=i
C          pos(l)=position(k,index)
          k=k+1
C          l=l+1
        end do
      end do




C
C Secondly we need to find the numtop best diagonals 
C This should be done be checking the density of matches,
C
      do i=-seqlen,proflen
        j=maxtop
        length=max(last(i)-first(i),10)
C        write(*,*)'find-test: ',i,j,match(i),length,bestmatch(j)
        do while (match(i) .gt.bestmatch(j) .and. j.ge.1  .and. match(i).ge.signcutoff)
C          write(*,*)'find-best: ',i,j,bestmatch(j),match(i),length
          j=j-1
        end do
        if (j.lt.maxtop) then
          do k=maxtop-1,j,-1
            bestmatch(k+1)=bestmatch(k)
            bestshift(k+1)=bestshift(k)
          end do
          j=j+1
          bestmatch(j)=match(i)
          bestshift(j)=i
        end if
      end do
C
C     We should only include significant diagonals.
C
      numsign=maxtop
      do while (bestmatch(numsign) .le. signcutoff .and. numsign.gt.0)
        numsign=numsign-1
      end do

C      write(*,*)'TEST',numsign

C      Now we need to find the start and stop of these diagonals....

      
      do i=1,numsign
C        score(i)=0
        start=max(1,first(bestshift(i))-5)
        stop=min(seqlen,last(bestshift(i))+5+ktup)
        start2=min(start,start2)
        stop2=max(stop,stop2)
C     tempscore=0
C        distance=min(bestshift(i),0)+start
C        write(*,*)'test ',i,bestshift(i),start,distance
C        do j=start,stop
C          s(i,distance)=max(minscore,profile(bestshift(i)+j,seq(j)))
C          s(i,distance)=max(minscore,profile(distance,seq(j)))
C     This was the original 
C     s(i,distance)=profile(bestshift(i)+j,seq(j))
C          distance=distance+1
C        end do
      end do

C
C We should now combine regions of the best matches
C
      do j=start2,stop2
        do i=1,numsign
          if (bestshift(i)+j.ge.0) then
            x=profile(bestshift(i)+j,seq(j))
            s2(i)=max(minscore,max(skiptest(j-1),s2(i))+x)
C          write(*,*) 'TEST2: ',i,j,bestshift(i)+j,x,s(i,j)
C          s2(i)=max(minscore,max(skiptest(j-1),s2(i))+s(i,j) )
C          distance=min(bestshift(i)+j,j)
C          s(i,distance)=profile(bestshift(i)+j,seq(j))
C          s2(i)=max(minscore,max(skiptest(j-1),s2(i))+profile(min(bestshift(i)+j,j),seq(j)) )
            bestscore=max(bestscore,s2(i))
            skiptest(j)=max(skiptest(j-1),s2(i)-gapcost)
          end if
        end do
      end do


C A temporary hack to find the best scoring non-gapped region.

      dynheuristic=bestscore

      return
      end

C*****************************************************************************
      subroutine makehash(profile,proflen,hash,position,ktup,cutoff,numaa)

c Create an index of the profile
      implicit none
#include <dynprog.h>

      integer i,j,k,ktup,numaa,index
      integer hash(maxhash,maxktup),position(maxhash,maxktup),num(maxktup)
      real profile(maxproflen,maxaa+2),cutoff,score
      integer proflen

C At present only for ktup == 2
C      write(*,*)'test2',ktup,cutoff,numaa
      do i=1,maxktup
        num(i)=0
      end do

      if (ktup .eq. 2) then
        do i=1,proflen+1-ktup
          do j=1,numaa
            do k=1,numaa
              index=(j-1)+maxaa*(k-1)+1
              score=profile(i,j)+profile(i+1,k)
C              write(*,*) 'H-testing: ',i,j,k,index,score,profile(i,j),profile(i+1,k)
              if (score .gt. cutoff) then
C                write(*,*)'testhash: ',i,j,k,index,score,index,num(index),i,j,k
                if (num(index).le.maxhash-1) then
                  num(index)=num(index)+1
                  hash(num(index),index)=score
                  position(num(index),index)=i
                else
                  write(*,*)'overflow in hashes'
                end if
              end if
            end do
          end do
        end do
      else
        call stopprocess('We only support ktup=2 at the moment')
      end if
      return
      end

