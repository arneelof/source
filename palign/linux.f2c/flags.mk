FFLAG2=      -O9 -f -r8 -w -Nn6416 -Nx400
# FFLAG2= -g

CPPFLAG= -I./  -DBIGREAL  -DSMALLINTEGER  -DGNU \
	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DF2C -DSTLEN_PROBLEMS -DFORTRAN_SECNDS

CCFLAG= -g -O9 -ffast-math     -funroll-loops   $(CPPFLAG) \
	-Dgnu  -m486  -I./

FFLAG=  -u  $(FFLAG2) 
LFLAG=  -lf2c -lf77
EXTRA= cstuff.o 
LINK=	fort77
F77=	fort77
CPP=   /usr/lib/gcc-lib/i386-redhat-linux/2.7.2.3/cpp
CC=	gcc -c $(CCFLAG)
F2C=	f2c
TAR= 	tar
CD=	cd

EXTENSIONS= src
MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino

OBJDIR= .
BINDIR= $(HOME)/source/source5/linux.f2c


# EXTRA=cstuff.o

# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make

.SUFFIXES: .h .o .src .f .c

.src.o:	
	$(CPP) -P $(CPPFLAG) $*.src > $*.f
	$(F2C) $(FFLAG) $*.f
	$(CC)  $*.c
	rm -f $*.f $*.c

.src.f:	
	$(CPP) -P $(CPPFLAG) $*.src > $*.f

.f.o:	$*.f $*.h $*.c
	$(F2C) $(FFLAG) $*.f
	$(CC)  $*.c
	rm -f $*.f $*.c

c.o:	
	$(CC)   $*.c
