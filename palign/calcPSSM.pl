package PSSMmaker;

require MSAmaker;
@ISA = qw(MSAmaker);

use Compress::Zlib;
use BMM::Paths qw|$blast_data_dir|;
use POSIX qw|tmpnam|;

my %matrix = readMatrix("$blast_data_dir/BLOSUM62");

#
# $matrix{CW} = 2;  CYS-TRP score
#

my %aacomp = (
A => 0.0756,
R => 0.0515,
N => 0.0450,
D => 0.0529,
C => 0.0168,
Q => 0.0401,
E => 0.0634,
G => 0.0683,
H => 0.0223,
I => 0.0577,
L => 0.0939,
K => 0.0595,
M => 0.0235,
F => 0.0408,
P => 0.0491,
S => 0.0716,
T => 0.0570,
W => 0.0123,
Y => 0.0318,
V => 0.0654,
);

my @aas = qw(A C D E F G H I K L M N P Q R S T V W Y);
my @blastorder = qw(A R N D C Q E G H I L K M F P S T W Y V);

# calculate qfreqs i vs. j

my $lambda = 0.3176;
my $beta = 10;
my %qfreqs = ();
foreach $aa1 (@aas)
{
  foreach $aa2 (@aas)
  {
    $qfreqs{$aa1.$aa2} = $aacomp{$aa1} *
                         $aacomp{$aa2} *
                         exp($lambda * $matrix{$aa1.$aa2});
  }
}


#
# THIS IS THE MAIN FUNCTION, it returns a string ($pssmstr) which looks like
# a PSI-BLAST PSSM file
#
# calcMc and calcNc are other important functions
# 

sub calcPSSM {
  my ($self) = @_;

  my $pssmstr = '';

  my %msa;
  my @seqids;
  # get alignment (MSAmaker method)
  $self->getMSA(\@seqids, \%msa);
  # and uppercase it
  grep $msa{$_} = uc($msa{$_}), @seqids;

  my $i;
  my $topid = $seqids[0];
  my $topseq = $msa{$topid};
  my $msalen = length($topseq);

  my $master_regexp = $self->getMasterRegexp();

  foreach $aa1 (@blastorder)
  {
    if ($aa1 eq $blastorder[0]) {
      $pssmstr .= "# $aa1 ";
    }
    else {
      $pssmstr .= "  $aa1 ";
    }
  }
  $pssmstr .= "\n";

  my $resnum = -1;
  for ($i=0; $i<$msalen; $i++)
  {
    next if (substr($topseq, $i, 1) eq '-');
    $resnum++;

    my @mcids;  # sequence ids from multiple sequence alignment (msa)
    my @mccols;
    calcMc($i, $msalen, \%msa, \@mcids, \@mccols);
    die "ERROR pssmgen domain $domain col $i\n" unless (@mcids && @mccols);

    # calculate the (whole) sequence weight by Henikoff and Henikoff method
    #

    my %hh;
    foreach $colidx (@mccols)
    {
      my %seen;
      foreach $seqid (@mcids)
      {
        my $aa = substr($msa{$seqid}, $colidx, 1);
        $seen{$aa} = 0 if (not exists $seen{$aa});
        $seen{$aa}++;
      }
      foreach $seqid (@mcids)
      {
        my $aa = substr($msa{$seqid}, $colidx, 1);
        if ($sqrt)
        {
          $hh{$seqid}{$colidx} = 1 / ($seen{$aa} * keys %seen);
        }
        else
        {
          $hh{$seqid}{$colidx} = 1 / ($seen{$aa} * keys %seen);
        }
      }
    }
    my %seqweight;
    my $wtsum = 0;
    foreach $seqid (@mcids)
    {
      my $sum = 0;
      foreach $colidx (@mccols)
      {
        $sum += $hh{$seqid}{$colidx};
      }
      $seqweight{$seqid} = $sum / @mccols;
      $seqweight{$seqid} = sqrt($seqweight{$seqid}) if ($sqrt);

      $wtsum += $seqweight{$seqid};
    }

    if (abs($wtsum-1) > 0.00001) # if they don't add up to 1
    {                            # make sure they do...
      foreach $seqid (@mcids)
      {
        $seqweight{$seqid} /= $wtsum;
      }
    }

    # calculate raw frequencies for each amino acid
    # in this column

    my %aafreqs = map { $_, 0 } ('A' .. 'Z');
    my %aacount;
    my $aa;

    foreach $seqid (@mcids)
    {
      $aa = substr($msa{$seqid}, $i, 1);
      $aafreqs{$aa} += $seqweight{$seqid};
      $aacount{$aa} = 1;
    }

    my $nc = calcNc(\@mcids, \@mccols, \%msa);

    my %logscore;
    my %probscore;
    foreach $aa1 (@aas)
    {
      $g = 0.0;
      foreach $aa2 (@aas)
      {
        $g += $aafreqs{$aa2} * $qfreqs{$aa1.$aa2} / $aacomp{$aa2};
      }
      my $alpha = $nc - 1;
      my $q = ($alpha * $aafreqs{$aa1} + $beta * $g) /
              ($alpha + $beta);

      $logscore{$aa1} = 0;
      $logscore{$aa1} = log($q / $aacomp{$aa1})/$lambda if ($q>0);

      $probscore{$aa1} = $q;
    }
    foreach $aa1 (@blastorder)
    {
      if ($probabilities)
      {
        $pssmstr .= sprintf "%5.3f ", $probscore{$aa1};
      }
      else # normal
      {
        $pssmstr .= sprintf "%3d ", $logscore{$aa1};
      }
    }
    $pssmstr .= "\n";
  }
  return $pssmstr;
}

sub calcMc
{
  my ($i, $msalen, $msa, $mcids, $mccols) = @_;

  my @seqids = keys %$msa;
  @$mcids = ();
  @$mccols = ();

  # work out which seqids are represented with a residue or '-' in the column
  my ($st, $len);
  foreach $seqid (@seqids)
  {
    ($st, $len) = ($i-3, 7);
    if ($st < 0) {
      ($st, $len) = (0, $len+$st);
    }
    push @$mcids, $seqid
      unless (substr($msa->{$seqid}, $st, $len) =~ /-/);
  }
  # work out which columns are occupied in all mcids
  my $maxmcid = @$mcids;
  for ($i=0; $i<$msalen; $i++)
  {
    my $j;
    my $numplus = 0;
    for ($j=0; $j<$maxmcid; $j++) # $seqid (@$mcids)
    {
      # $numplus++ if (substr($msa->{$seqid}, $i, 1) eq '-');
      if (substr($msa->{$mcids->[$j]}, $i, 1) eq '-') {
	$numplus = 1;
        last;
      }
    }
    # put the one with the gap at the head of the queue
    unshift @$mcids, splice @$mcids, $j, 1 if ($j>0 && $j<$maxmcid);
    push @$mccols, $i if ($numplus==0);
  }
}

sub calcNc
{
  my ($mcids, $mccols, $msa) = @_;
  my %seen;
  my $sum = 0;
  foreach $colidx (@$mccols)
  {
    %seen = ();
    foreach $seqid (@$mcids)
    {
      $seen{substr($msa->{$seqid}, $colidx, 1)} = 1;
    }
    $sum += keys %seen;
  }
  return $sum/@$mccols;
}


sub readMatrix {
  my ($filename) = @_;

  my @letters = ();
  my $letter = "";
  my @numbers = ();
  my %matrix = ();

  open(MAT, $filename) || die "readmatrix can't read $filename\n";
  while (<MAT>) {
    next if (/^#/);
    if (@letters==0) {
      @letters = split;
      pop @letters; # we don't want the '*'
    }
    elsif (/^\w/) {
      $letter = substr($_,0,1);
      s/^\w\s*//;
      @numbers = split;
      pop @numbers; # see above
      die "readMatrix format error\n" if (@letters != @numbers);
      my $i;
      for ($i=0; $i<@numbers; $i++) {
        $matrix{$letter.$letters[$i]} = $numbers[$i];
      }
    }
  }
  close(MAT);
  return %matrix;
}

1;

