#include <stdio.h>
#include <math.h>
#include <dynprog_C.h>
#include <seqio_C.h>
//#include <dynprog_C.h>
//int scnt=0;
//int dcnt=0;
//  int box1=0,box2=0,box3=0,box4=0,box5=0,box6=0,box7=0,box8=0,box9=0,box10=0,box11=0,box12=0,box13=0,box14=0,box15=0,box16=0,box17=0,box18=0,box19=0,box20=0;
//*********************************************************************
float kullbackleibler(float p[], float q[], int num)
{    
  float logtwo=0.693147;
  float logtwoinv=1.442695041;
  float KLsum=0;
  int k=0;
  float psum=0,qsum=0;
  
  for(k=0; k<num; k++)
    {
      KLsum+=p[k]*(log(p[k]/q[k]))*logtwoinv;
    }
  //  printf("KLsum %f\n", KLsum); 
  return(KLsum);
}
//************************************************************************
#ifdef GNU
float profprof_profsim__(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#else
float profprof_profsim_(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#endif
{
  //initfreq[20] in alphabetical order
  
  
  int k=0;
  float  r[maxaa+2], r2[maxaa+2], p[maxaa+2], q[maxaa+2];
  float score=0, d=0, PrfPrfsum=0, psum=0;
  const float tiny=1.0e-20; 
  int x=*X-1;
  int y=*Y-1;
  float ssScore=0;
  float similar=0;
  float normp=0, normq=0;
  //printf("%f\n", *shift);
  for(k=0; k<*num; k++)
    {
      r[k]=*lambda* PROFILE[k][y]+(1-*lambda)*SEQ[k][x]+tiny;
      //r[k]=*lambda* initfreq_.initfreq[k]+(1-*lambda)*SEQ[k][x]+tiny;
      p[k]=PROFILE[k][y]+tiny;
      q[k]=SEQ[k][x]+tiny;
      //p[k]=initfreq_.initfreq[k];//neutral match
      //p[k]=q[k];//perfect match
      //      q[k]=initfreq_.initfreq[k];//neutral match
    }


  d = *lambda*kullbackleibler(p,r,*num)+(1-*lambda)*kullbackleibler(q,r,*num); 
 
  for(k=0; k<*num; k++)
    {
      r2[k] = *lambda*r[k]+(1-*lambda)*initfreq_.initfreq[k];
    }


  if(PROFSS[0][y]==SEQSS[0][x] && PROFSS[0][y]!=-1)
    {ssScore=0.20;}// =ge
  else if(PROFSS[0][y]!=-1 && SEQSS[0][x]!=-1)
    {ssScore=-0.00;}

  score = *lambda*kullbackleibler(r,r2,*num)+(1-*lambda)*kullbackleibler(initfreq_.initfreq,r2,*num);
  //  printf("p1 q1 r1 %f %f %f\n", p[0], q[0], r[0]);
  //  if(x==y){

  PrfPrfsum = 0.5*(1-d)*(1+score)+*shift + ssScore;
  //  if(similar <= 0.05 && box1 < 100000){box1++;printf("#bad <\t%f\t>\n",PrfPrfsum);}
  //  if(box1 < 100000){box1++;printf("#100 <\t%f\t>\n",PrfPrfsum);} //perfect match
  //if(box1 < 100000){box1++;printf("#neu <\t%f\t>\n",PrfPrfsum);} //neutral match
 


  //  printf("TEST-t-prfprf <%f> <%f> <%f> <%f>\n",d, score, PrfPrfsum,*lambda);
  return(PrfPrfsum);
}
/*
  Reference:
  Yona G. et al
  J Mol Biol 2002. 315:1257-1275
*/
  

