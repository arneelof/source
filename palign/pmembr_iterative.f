C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      Program Pmembriterative
C
C A heuristic sequence/profile search program. Made for membrane profiles.
C
      implicit none
#include <dynprog.h>

      real profile(maxproflen,maxaa+2),gapopen,gapext,score
      real dynheuristic,dynscore,x(maxproflen),y(maxproflen),z(maxproflen)
      character*320 proffile,seqfile,filename1,filename2
      character*800 line,oldline,dbfile
      character*10 proftype
      character*1 char
      integer hash(maxktup,maxhash),position(maxktup,maxhash),ktup
      integer resnum(maxseqlen),profresnum(maxseqlen),num
      integer wdlen,linelen,i,j,k,proflen,alitype,type
      integer profseq(maxseqlen),profss(maxseqlen),numss,numaa,cfiles,aa
      integer seq(maxseqlen),seqss(maxseqlen),seqlen,traceback(maxseqlen)
      integer seqtoseq,seqtoss,infile,numtop,dbsize
      integer seqfilelen,proffilelen,numtm,seqs
      integer iter,numiter,numhits,maxhits,maxdbsize
      logical proffound,inverse
      real dpcutoff,cutoff,gapcost,signcutoff
C      real cutoff2,score1,score2,gapcost2
      real length,minout,itercutoff,sumd,frac,numseq,aafreq(maxaa)
      real qfreq(maxaa,maxaa),table(maxaa,maxaa),loopgap(3),ssmatch(3,3)
      real alpha,beta,lambda,g,f(maxaa),q
      real evalue,zs_to_e
      external dynscore,seqtoseq,seqtoss,dynheuristic,zs_to_e

      parameter (maxhits=5000)
      parameter (maxdbsize=100000)
      parameter (infile=91)
      parameter (numss=3)
      parameter (numaa=20)
      integer msa(maxproflen,0:maxhits),profnum,newprofnum
      real d(maxhits),s(maxhits,maxhits),a(maxhits,maxhits),w(maxhits)
      real heurscore(maxdbsize)
      real sumscore,sum2score,fscore(maxdbsize),averscore,sd,zscore,oldaverscore
      real profsum,profsum2,newprofsum,newprofsum2,profaver,newprofaver
      real profsd, newprofsd
      integer fseqlen(maxdbsize),fproflen(maxdbsize),indx(maxdbsize),numout
      character*80 name(maxhits)

      
C      call foo(bar)
C default values
      inverse=.false.
      aa=numaa
      ktup=2
      itercutoff=1.e-3
      cutoff=10.
      numiter=3
      dpcutoff=6.
      minout=10
      gapcost=30.
C      numtop=20
      alitype=0
      gapopen=-10
      gapext=-4
      seqss(1)=-1
      profss(1)=-1
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
      ssmatch(1,1)=0
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
      ssmatch(3,3)=0
      call init
      call init_int(position,maxktup*maxhash,0)
      call init_int(hash,maxktup*maxhash,0)
      call init_int(seq,maxseqlen,0)
      call init_int(seqss,maxseqlen,0)
      call init_int(profss,maxseqlen,0)
      call init_int(profseq,maxseqlen,0)
      call init_int(traceback,maxseqlen,0)

      call init_real(profile,maxproflen*(maxaa+2),0.)
      call init_real(x,maxseqlen,0.)
      call init_real(y,maxseqlen,0.)
      call init_real(z,maxseqlen,0.)
      call init_real(table,maxaa*maxaa,0.)


      filename1='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contDistHL'
      filename2='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contEneHL'
C      filename1='/home/gvh/arne/source/palign/parkLevitt/contDistHL'
C      filename2='/home/gvh/arne/source/palign/parkLevitt/contEneHL'

      line='/afs/pdc.kth.se/home/a/arnee/source/palign/pam250.mat'
      call readfastaseqtable(line,table,maxaa)

      cfiles=1 
      call getarg(cfiles,line)
      linelen=len(line)
      call trima(line,linelen)
      type=1

#ifdef IFC
      do while  (line .ne. oldline ) 
        oldline=line
#else
      do while  (line .ne. '' ) 
#endif
        if (line(1:5).eq.'-help') then
          write(*,*) "Usage:"
          write(*,*) "-table file   : Use a sequence table file in blast format"
          write(*,*) "-fasta file   : Use a sequence table file in fasta format (default pam250)"
          write(*,*) "-local        : Use local alignments (default)"
          write(*,*) "-global       : Use global alignment"
          write(*,*) "-inire  file1 file2    : Energy files for threading energy"
          write(*,*) "-loopgap A B C : cost for gaps in differenst SS regions (default 1,1,1)"
          write(*,*) "-ssmatch a b c d e f g h i : Scores for secondary structure regions"
          write(*,*) "                            default 100 010 001 for secstr and"
          write(*,*) "                            000 010 00 for TM and"
          write(*,*) "-go X          : Gap opening cost"
          write(*,*) "-ge X          : Gap  extension cost"
          write(*,*) "-cutoff (12)   Cutoff for hash"
          write(*,*) "-itercutoff (1.e-3)   Cutoff for including in the next iteration"
          write(*,*) "-dpcutoff (8)  Cutoff for full DP search"
C          write(*,*) "-numtop (20)   Number of diagonals to use in the heuristic search"
          write(*,*) "-signcutoff (4*cutoff)  cutoff for diagonals to be used "
          write(*,*) "-gapcost (30)  Cost for gaps in the heuristic search"
          write(*,*) "-minout (10) minimum score for beeing printed"
          write(*,*) "-seq file1 file2 ... - : files to read for sequence info"
          write(*,*) "file1 file2     : sequence and profile-list file"
          write(*,*) "Note somehow atleast one sequence or profile-list file has to be named"

          stop
        else if (line(1:6).eq.'-table') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-fasta') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readfastaseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-local') then
          alitype=0
        else if (line(1:7).eq.'-global') then
          alitype=10
        else if (line(1:8).eq.'-loopgap') then
          do j=1,3
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) loopgap(j)
          end do
        else if (line(1:8).eq.'-ssmatch') then
          do j=1,3
            do k=1,3
              cfiles=cfiles+1
              call getarg(cfiles,line)
              linelen=len(line)
              call trima(line,linelen)
              read (line,*) ssmatch(j,k)
            end do
          end do
        else if (line(1:3).eq.'-go') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapopen
        else if (line(1:3).eq.'-ge') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapext
        else if (line(1:5).eq.'-ktup') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) ktup
        else if (line(1:7).eq.'-cutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) cutoff
        else if (line(1:11).eq.'-itercutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) itercutoff
        else if (line(1:8).eq.'-numiter') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) numiter
        else if (line(1:7).eq.'-minout') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) minout
        else if (line(1:9).eq.'-dpcutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) dpcutoff
        else if (line(1:11).eq.'-signcutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) signcutoff
C        else if (line(1:7).eq.'-numtop') then
C          cfiles=cfiles+1
C          call getarg(cfiles,line)
C          linelen=len(line)
C          call trima(line,linelen)
C          read (line,*) numtop
C          if (numtop.ge.maxtop)  call stopprocess('ERROR> numtop too large !')
        else if (line(1:8).eq.'-gapcost') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapcost
        else if (line(1:5).eq.'-prof') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          do while (line(1:1).ne.'-')
            proffile=line
            proffilelen=len(proffile)
            call trima(proffile,proffilelen)
            call readanyseq(line,profseq,proflen,proffound,profile,profss,x,y,z,proftype,profresnum)
            if (proflen .gt. maxproflen) call stopprocess('ERROR> profile too long !')
            write(*,*)'READING profile type: ',proftype,' len: ',proflen
            cfiles=cfiles+1
            call getarg(cfiles,line)
          end do
          if (profile(1,1).eq.0.) then
            write(*,*)'Making a profile:'
            call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
          else
            do i=1,proflen
              profile(i,penopen)=gapopen+gapext
              profile(i,penext)=gapext
            end do
          end if
          if (profss(1).ne.-1 .and. seqss(1).ne.-1) then
            write(*,*)'Making a secondary structureprofile:'
            call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
            aa=numaa*3
          end if
          type=type+1
          inverse=.true.
        else 
C Now the last argument has to be a database file 
          linelen=len(line)
          call trima(line,linelen)
          dbfile=line
        end if
        cfiles=cfiles+1
        call getarg(cfiles,line)
        linelen=len(line)
        call trima(line,linelen)
      end do

      if (.not. inverse) then 
        call stopprocess('You have to specify a profile')
      end if

C for profiles
      lambda = 0.3176
      beta = 10
      call init_freq(aafreq,qfreq,table,lambda,maxaa,numaa)

      call makehash(profile,proflen,hash,position,ktup,cutoff,maxaa)

      if (signcutoff .eq. 0) then
        signcutoff=4*cutoff
      end if

C We have to find the average and sd values of the profile !!
      profsum=0
      profsum2=0
      profnum=0
      do i=1,proflen
        do j=1,aa
          profsum =profsum+profile(i,j)
          profnum =profnum+1
          profsum2=profsum2+profile(i,j)**2
        end do
      end do
      profaver=profsum/profnum
      profsd=sqrt((profsum2-profsum*profsum/profnum)/(profnum-1))


      j=0
      numhits=0
      do i=1,proflen
        msa(i,0)=profseq(i)
      end do
      do iter=1,numiter
        Open(unit=infile,file=dbfile,form='FORMATTED',status='OLD',
     &       access='SEQUENTIAL',err=777)
        do i=1,proflen
          do j=1,numhits
            msa(i,j)=0
          end do
        end do
        sumscore=0
        sum2score=0
        numhits=0
        numout=0
        num=0
        do while (.true.)
          linelen=len(line)
          read(infile,*,err=777,end=666) line,seqlen,numtm
          call trima(line,linelen)
          if (seqlen .gt. maxseqlen) then
            write(*,*)'Sequence longer than maxsequence, not using the complete sequence'
            seqlen=maxseqlen
          end if
          read(infile,*,err=777,end=666) (seq(i),i=1,seqlen)
          
          length=seqlen*proflen
          
          score=dynheuristic(profile,proflen,seq,seqlen,alitype,
     $         hash,position,ktup,gapcost,signcutoff)

          
C
C Using the Z-score I actually need to read the file twice !!!
C
C
C          if (score/log(length) .ge. min(dpcutoff,itercutoff)  ) then
C            call dyntrace(profile,proflen,seq,seqlen,alitype,score,traceback)
CC Here we create the next generation MSA
C            if (score/log(length) .ge. itercutoff  ) then
C              numhits=numhits+1
C              if (numhits .gt. maxhits) then
C                write(*,*)'Numhits larger than maxhits, sequence ignored'
C              else
CC              write(*,*)'TEST> ',numhits,score,score/log(length)
C                do i=1,seqlen
C                  if (traceback(i).gt.0) then
C                    seqs=seq(i)-numaa*int((seq(i)-1)/numaa)
C                    msa(traceback(i),numhits)=seqs
C                  end if
C                end do
C              end if
C            end if
C            if (numhits .le. maxhits) then
C              fseqlen(numhits)=seqlen
C              fproflen(numhits)=proflen
C              name(numhits)=line
C              fscore(numhits)=score/log(length)
C            end if
C          end if
          num=num+1
          score=score/log(length)
          heurscore(num)=score
          sumscore=sumscore+score
          sum2score=sum2score+score**2
C          if (score .ge. minout  ) then
C            write(*,'(1(a,1x),2(a,1x),2i8,3x,2(f11.5,1x),i8)') 'SCORE1> ',
C     $           proffile(1:proffilelen),
C     $           line(1:linelen),seqlen,proflen,score,score/log(length),iter
C          end if
        end do
 666    continue
        close (unit=infile)

C Now we have to calculate Z-score ignoring all hits with a preliminary
C Z-score higher than 7 orless than 3

        averscore=sumscore/num
C     This produces the wrong result if using single precision and large numbers,
C        sd=sqrt((num*sum2score-sumscore*sumscore)/(num*(num-1)))
        sd=sqrt((sum2score-sumscore*sumscore/num)/(num-1))

        j=1
        oldaverscore=averscore+1
        do while (j .lt. 5 .and. averscore .ne. oldaverscore)
          oldaverscore=averscore
          j=j+1
          dbsize=0
          sumscore=0
          sum2score=0
          do i=1,num
            zscore=(heurscore(i)-averscore)/sd
            if (zscore .lt. 7. .and. zscore .gt. -3.) then
              dbsize=dbsize+1
              sumscore=sumscore+heurscore(i)
              sum2score=sum2score+heurscore(i)**2
            end if
          end do
          averscore=sumscore/dbsize
          sd=sqrt((sum2score-sumscore*sumscore/dbsize)/(dbsize-1))
        end do

        num=0
        Open(unit=infile,file=dbfile,form='FORMATTED',status='OLD',
     &       access='SEQUENTIAL',err=777)
        do while (.true.)
          linelen=len(line)
          read(infile,*,err=777,end=667) line,seqlen,numtm
          call trima(line,linelen)
          if (seqlen .gt. maxseqlen) then
C            write(*,*)'Sequence longer than maxsequence, not using the complete sequence'
            seqlen=maxseqlen
          end if
          read(infile,*,err=777,end=667) (seq(i),i=1,seqlen)
          num=num+1

          score=heurscore(num)
          zscore=(heurscore(num)-averscore)/sd
C
          evalue=zs_to_e(zscore,dbsize)

C          write(*,*)'testing',num,score,zscore,evalue,dbsize,numhits,averscore,sd
          if (zscore .ge. dpcutoff .or. evalue .le. max(itercutoff,minout) ) then
            call dyntrace(profile,proflen,seq,seqlen,alitype,score,traceback)
CC Here we create the next generation MSA
            length=seqlen*proflen
            score=score/log(length)
            zscore=(score-averscore)/sd
            evalue=zs_to_e(zscore,dbsize)
            if ( evalue .le. itercutoff  ) then
              numhits=numhits+1
              if (numhits .gt. maxhits) then
                write(*,*)'Numhits larger than maxhits, sequence ignored'
              else
C              write(*,*)'TEST> ',numhits,score,score/log(length)
                do i=1,seqlen
                  if (traceback(i).gt.0) then
                    seqs=seq(i)-numaa*int((seq(i)-1)/numaa)
                    msa(traceback(i),numhits)=seqs
                  end if
                end do
              end if
            end if

          end if

          if (evalue .le. minout  ) then
            numout=numout+1
            if (numout .gt. maxdbsize) then
              write(*,*)'Numout larger than maxhits, sequence ignored'
            else
              fseqlen(numout)=seqlen
              fproflen(numout)=proflen
              fscore(numout)=score
              name(numout)=line
            end if
          end if

        end do
            
 667    continue

        call indexx(numout,fscore,indx)
        i=min(numout,maxdbsize)
        numhits=min(numhits,maxhits)

        zscore=(fscore(indx(i))-averscore)/sd
        evalue=zs_to_e(zscore,dbsize)
C        write(*,*)'test: ',zscore,minout,evalue,i,numout
        do while(evalue.le.minout .and. i.ge.1)
          length=fseqlen(indx(i))*fproflen(indx(i))
          linelen=80
          call trima(name(indx(i)),linelen)
          evalue=zs_to_e(zscore,dbsize)
          write(*,'(1(a,1x),2(a,1x),f11.5,3i8,3x,2(e11.3,1x),i8)') 'SCORE> ',
     $         proffile(1:proffilelen),name(indx(i))(1:linelen),
     $         fscore(indx(i))*log(length),numout-i+1,
     $         fseqlen(indx(i)),fproflen(indx(i)),fscore(indx(i)),evalue,iter
          i=i-1
          zscore=(fscore(indx(i))-averscore)/sd
          evalue=zs_to_e(zscore,dbsize)
        end do


        close(unit=infile)
        if(numiter .eq. iter) then 
          write(*,*)'Reached maximum number of iterations; ',numiter
          stop
        end if
C
C     Now we have to make the new profile
C     To obtain best performance we should actually weight all sequence. 
C     We use a similar method as in FFAS (Rychlewski et al, 2000)
C
C
C     We should check if the profile changes, otherwise exit..
C
        do j=1,numhits
          do k=j,numhits
            a(j,k)=0
            do i=1,proflen
              a(j,k)=a(j,k)+table(msa(i,j),msa(i,k))
C              if (msa(i,j) .ne. msa(i,k)) write(*,*)'WARNING: ',i,j,k,msa(i,j),msa(i,k)
            end do
            a(k,j)=a(j,k)
          end do
        end do
      
        do j=1,numhits
          do k=j,numhits
            s(j,k)=max( a(j,k)/min(a(j,j),a(k,k)),0.0)
C            write(*,*)'TESTING-PRF',j,k,s(j,k),a(j,k),a(j,j),a(k,k)
          end do
          s(k,j)=s(j,k)
        end do

        sumd=0
        do j=1,numhits
          d(j)=0
          do k=1,j-1
            d(j)=d(j)+s(j,k)**2
          end do
          do k=j+1,numhits
            d(j)=d(j)+s(j,k)**2
          end do
          d(j)=1/(1+d(j))
          sumd=sumd+d(j)
        end do

        do j=1,numhits
          w(j)=d(j)/sumd
C          w(j)=1./numhits
C          write(*,*) 'TEST-prof',j,w(j)
        end do

        do i=1,proflen
          do j=1,numaa
            profile(i,j)=0
            f(j)=0
          end do
          frac=0
          numout=0
          do j=1,numhits
            frac=frac+min(msa(i,j),1)*w(j)
            numout=numout+min(msa(i,j),1)
          end do
          do j=1,numhits
            seqs=msa(i,j)
            f(seqs)=f(seqs)+w(j)/frac
          end do


CC          write(*,*)'TESTing: ',numseq,numhits,frac
C          do j=1,numhits
C            seqs=msa(i,j)
CC            write(*,*)'TEST-msa: ',i,j,msa(i,j),w(j)/frac,seqs,table(seqs,seqs)
C            do k=1,numaa
C              profile(i,k)=profile(i,k)+log(w(j)*table(seqs,k)/frac
C            end do
C          end do
C          write(*,'(a,1x,i4,1x,60(f4.1,1x))') 'PRF: ',i,(profile(i,k),k=1,numaa)
C
C This is taken from Bob/Lawrence
C

          do j=1,numaa
            g = 0.0
            do k=1,numaa
              g =g+ f(k) * qfreq(j,k) / aafreq(k)
C              write(*,*)'test1 ',j,k,f(k),qfreq(j,k),aafreq(k)
            end do
            alpha = numout - 1
            q = (alpha*f(j) + beta* g) / (alpha + beta)
            if (q.gt.0) then
              profile(i,j) = log(q / aafreq(j))/lambda
C              write(*,*)'TEST ',i,j,g,f(j),q,profile(i,j)
            else
              profile(i,j)=0
            end if
          end do


        end do
        if (profss(1).ne.-1) then
C     write(*,*)'Making a secondary structureprofile:'
          call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
        end if
C        newprofsum=0
C        newprofsum2=0
C        newprofnum=0
C        do i=1,proflen
C          do j=1,aa
C            newprofsum =newprofsum+profile(i,j)
C            newprofnum =newprofnum+1
C            newprofsum2=newprofsum2+profile(i,j)**2
C          end do
C        end do
C        newprofaver=newprofsum/newprofnum
C        newprofsd=sqrt((newprofsum2-newprofsum*newprofsum/newprofnum)/(newprofnum-1))
C        write(*,*)'Testing-sd: ',profaver,profsd,newprofaver,newprofsd
C        do i=1,proflen
C          do j=1,aa
C            profile(i,j)=profile(i,j)*profsd/newprofsd+profaver-newprofaver
C          end do
C        end do
          
C        newprofsum=0
C        newprofsum2=0
C        newprofnum=0
C        do i=1,proflen
C          do j=1,aa
C            newprofsum =newprofsum+profile(i,j)
C            newprofnum =newprofnum+1
C            newprofsum2=newprofsum2+profile(i,j)**2
C          end do
C        end do
C        newprofaver=newprofsum/newprofnum
C        newprofsd=sqrt((newprofsum2-newprofsum*newprofsum/newprofnum)/(newprofnum-1))
C        write(*,*)'Testing2-sd: ',profaver,profsd,newprofaver,newprofsd
        call makehash(profile,proflen,hash,position,ktup,cutoff,maxaa)

      end do

      stop
 777  call stopprocess('cannot read list')

      end

***********************************************************************
      subroutine stopprocess(string)
      character*(*) string
      write(*,*) string
      write(*,*)'STOP> Job terminates'
      stop
      end

