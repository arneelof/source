FFLAG2=     -O3 -static    -funroll-loops  -fno-globals\
	-ffixed-line-length-none -finline-functions -fomit-frame-pointer \
	-Wimplicit
#
# FFLAG2= -g -ffixed-line-length-none 

#        options = -s -static -O9 -finline-functions -funroll-loops \
#        -funroll-all-loops -fomit-frame-pointer
# Note:   The static option makes big diff 


#CPPFLAG= -I./ \
#	-L/modules/gnu/gcc/linux/lib/gcc-lib/i586-unknown-linuxaout/2.7.1/ \
#	-I/modules/gnu/gcc/linux/lib/gcc-lib/i586-unknown-linuxaout/2.7.1/include/ \
#	 -DBIGREAL  -DSMALLINTEGER  -DGNU \
#	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS
CPPFLAG= -I./ \
	 -DBIGREAL  -DSMALLINTEGER  -DGNU \
	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS


FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 
# LINK=	/modules/gnu/gcc/linux/bin/g77  -I/modules/gnu/gcc/linux/lib/gcc-lib/i586-unknown-linuxaout/2.7.2.f.1/
# F77=	/modules/gnu/gcc/linux/bin/g77 -I/modules/gnu/gcc/linux/lib/gcc-lib/i586-unknown-linuxaout/2.7.2.f.1/

#LINK= /home/gvh/arne/temp/linuxutils/usr/bin/g77
#F77= /home/gvh/arne/temp/linuxutils/usr/bin/g77
LINK=g77
F77=g77
CPP=  cpp
#CC=	/modules/gnu/gcc/linux/bin/gcc 
CC=gcc
CCFLAG= -g -O $(CPPFLAG) -Dgnu  -I./
CD=	cd

MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino
EXTENSION= F

OBJDIR= .
BINDIR= $(HOME)/source/source5/linux.g77


# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make

.SUFFIXES: .F .h .o .c .src .f
.src.F:	
#	sed "s/\\\T/\	/g" $*.src  | awk -f split.awk > $*.F
#	cp $*.src $*.F
	$(F77) -c $(FFLAG) -I./ $*.F
#	rm -f $*.F

.F.o:	
#	sed "s/\\\T/\	/g" $*.src  | awk -f split.awk > $*.F
#	awk -f split.awk $*.src | sed "s/\\\T/\	/g" > $*.F
#	cp $*.src $*.F
	$(F77) -c $(FFLAG) -I./ $*.F
#	rm -f $*.F


.c.o:	
	$(CC) -c $(CCFLAG)  $*.c
