#!/bin/bash -x

cp linux/palign /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palign
cp linux/palignp /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palignp
cp linux/palignp2 /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palignp2
cp linux/palign2 /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palign2

cp linux.ifc7/palign2 /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palign2.ifc
cp linux.ifc7/palign /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palign.ifc
cp linux.ifc7/palignp /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palignp.ifc
cp linux.ifc7/palignp2 /afs/pdc.kth.se/home/a/arnee/structpredict/SCOP/scop-1.57/bin/palignp2.ifc
