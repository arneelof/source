FFLAG2=      -O9 -f -r8 -w -Nn6416 -Nx400
# FFLAG2= -g

CPPFLAG= -I./  -DBIGREAL  -DSMALLINTEGER  -DGNU \
	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DF2C -DSTLEN_PROBLEMS \
	-I/modules/gnu/gcc/sun4/lib/gcc-lib/sparc-sun-solaris2.4/2.7.1/include/

CCFLAG= -g -O9 -ffast-math     -funroll-loops   $(CPPFLAG) \
	-Dgnu  -mv8

FFLAG=  -u  $(FFLAG2) 
LFLAG=  -lf2c	-L/modules/gnu/gcc/sun4/lib/
EXTRA= cstuff.o 
# LINK=	gcc $(LFLAG)
LINK=	/modules/gnu/gcc/sun4/bin/fort77 -L/modules/gnu/gcc/sun4/lib/
F77=	fort77 
CPP=  cpp
CC=	gcc -c $(CCFLAG)
F2C=	/modules/gnu/gcc/sun4/bin/f2c

CD=	cd

MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino
EXTENSION=src
OBJDIR= .
BINDIR= $(HOME)/source/source5/sun4.f2c


# EXTRA=cstuff.o

# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make

.SUFFIXES:  .src .h .o .c

.src.o:	
	$(CPP) -P $(CPPFLAG) $*.src > $*.f
	$(F2C) $(FFLAG) $*.f
	$(CC)  $*.c
	rm -f $*.f $*.c

.src.f:	
	$(CPP) -P $(CPPFLAG) $*.src > $*.f

.f.o:	$*.f $*.h $*.c
	$(F2C) $(FFLAG) $*.f
	$(CC)  $*.c
	rm -f $*.f $*.c

c.o:	
	$(CC)   $*.c
