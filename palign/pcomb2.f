C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      Program Pcomb
C
C An integrated approach to generate information for a consensus predictions.
C
C Optimized version using the 10 best parameters
C
      implicit none
#include <dynprog.h>
      

      real profile(maxproflen,maxaa+2),gapopen,gapext
      real profile2(maxproflen,maxaa+2),tempprofile(maxproflen,maxaa+2)
      real dynscore,score,x(maxproflen),y(maxproflen),z(maxproflen)
      character*800 line,oldline
      character*160 alfile,pdbfile,proffile,seqfile,filename1,filename2
      character*160 seqstrfile
      character*160 pdbfile2,proffile2
      character*160 seqstrfile2
      character*1 chain2
      integer resnum(maxseqlen)
      integer resnum2(maxseqlen)
      integer cfiles,linelen,i,j,k,proflen,alitype
      integer proflen2,offset2,length,wdlen
      integer profseq(maxseqlen),profss(maxseqlen),numss,numaa 
      integer profseq2(maxseqlen)
      integer seq(maxseqlen),seqss(maxseqlen),seqlen,traceback(maxseqlen)
      integer traceback2(maxseqlen)
      integer tempseq(maxseqlen),seq2(maxseqlen),seqss2(maxseqlen),seqlen2
      integer ssseq(maxseqlen),ssseq2(maxseqlen),ssseqlen,ssseqlen2
      integer seqtoseq,seqtoss,allen,numal,gaps,numgaps,ssgaps,infile
      integer infile1,infile2,num,type
      integer seqfilelen,proffilelen,seqstrfilelen,seqstrfilelen2
      integer pdbfilelen2,proffilelen2
      logical found
      real R(20,20), E(20,20), Etot, Eaa, Eij, Eia,fraction
      real table(maxaa,maxaa),loopgap(3),ssmatch(3,3),go,ge,s,s2
      external dynscore,seqtoseq,seqtoss
      real dataEtot(maxnum,maxtype)
      real dataEaa(maxnum,maxtype)
      real dataEij(maxnum,maxtype)
      real dataEia(maxnum,maxtype)
      real datafraction(maxnum,maxtype)
      real datascore(maxnum,maxtype)
      integer dataallen(maxnum,maxtype)
      integer datagaps(maxnum,maxtype)
      integer datassgaps(maxnum,maxtype)
      integer datanumgaps(maxnum,maxtype)
      integer datanumal(maxnum,maxtype)
      integer dataseqlen(maxnum,maxtype)
      integer dataproflen(maxnum,maxtype)
C
C
C
      parameter (infile=91)
      parameter (infile1=92)
      parameter (infile2=93)
      parameter (numss=3)
      parameter (numaa=20)

      i=1

C      call foo(bar)
C default values
      alfile=''
      alitype=0
      gapopen=-10
      gapext=-4
      seqss(1)=-1
      profss(1)=-1
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
      ssmatch(1,1)=1
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
      ssmatch(3,3)=1
      call init_int(seq,maxseqlen,0)
      call init_int(seqss,maxseqlen,0)
      call init_int(profss,maxseqlen,0)
      call init_int(profseq,maxseqlen,0)
      call init_int(traceback,maxseqlen,0)

      call init_real(profile,maxproflen*(maxaa+2),0.)
      call init_real(x,maxseqlen,0.)
      call init_real(y,maxseqlen,0.)
      call init_real(z,maxseqlen,0.)
      call init_real(table,maxaa*maxaa,0.)
      call init_real(E,400,0.)
      call init_real(R,400,0.)
C
C     Some default files
C
      filename1='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contDistHL'
      filename2='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contEneHL'
C      filename1='/home/ae/arne/source/palign/parkLevitt/contDistHL'
C      filename2='/home/ae/arne/source/palign/parkLevitt/contEneHL'
      call inire(filename1,filename2,R,E)

      line='/home/ae/arne/source/palign/blosum50.mat'
      call readseqtable(line,table,maxaa)
      x(1)=-9999
      alitype=0
      gapopen=-10
      gapext=-4
      seqss(1)=-1
      profss(1)=-1
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
      ssmatch(1,1)=1
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
      ssmatch(3,3)=1

C
C     Input is read from two files one containing the files corresponding to the input proien
C     and one to the library
C     The files are as follows (and should be assured to be identical): 
C     PDB  Seqstr Profile -file
C
C     We still have some parameters that can be passed
C
      cfiles=1 
      call getarg(cfiles,line)
      linelen=len(line)

#ifdef IFC      call trima(line,linelen)
      do while  (line .ne. oldline ) 
        oldline=line
#else
      do while  (line .ne. '' ) 
#endif
        if (line(1:6).eq.'-table') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-fasta') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readfastaseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:7).eq.'-inire') then
          alitype=10
C          filename1 = 'contDistHL'
C          filename2 = 'contEneHL'
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
C          read (line,*) filename1
          filename1=line
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
C          read (line,*) filename2
          filename2=line
          call inire(filename1,filename2,R,E)
C          do i=1,20
C            write(*,'(a,i4,20f8.3)')'R ',i,(R(i,j),j=1,20)
C            write(*,'(a,i4,20f8.3)')'E ',i,(E(i,j),j=1,20)
C          end do
        else if (line(1:7).eq.'-loopgap') then
          do j=1,3
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) loopgap(j)
          end do
        else if (line(1:7).eq.'-ssmatch') then
          do j=1,3
            do k=1,3
              cfiles=cfiles+1
              call getarg(cfiles,line)
              linelen=len(line)
              call trima(line,linelen)
              read (line,*) ssmatch(j,k)
            end do
          end do
        else if (line(1:8).eq.'-alfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) alfile
        else if (line(1:9).eq.'-pdbfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) pdbfile
        else  
          if (i.eq.1) then
            Open(unit=infile1,file=line,form='FORMATTED',status='OLD',
     &           access='SEQUENTIAL',err=777)
            
          elseif (i.eq.2) then
            Open(unit=infile2,file=line,form='FORMATTED',status='OLD',
     &           access='SEQUENTIAL',err=777)
            
          else
            write(*,*) i,line
                call stopprocess('ERROR> extra arguments !')
          end if
          i=i+1
        end if
        cfiles=cfiles+1
        call getarg(cfiles,line)
        linelen=len(line)
        call trima(line,linelen)
      end do
C
C First we read in the query protein.
C
      read(infile1,'(A)',err=777,end=666) line
      length=len(line)
      call trima(line,length)
      seqfilelen=len(seqfile)
      call nextwd(line,length,seqfile,seqfilelen,wdlen)
      call trima(seqfile,seqfilelen)
      seqstrfilelen=len(seqstrfile)
      call nextwd(line,length,seqstrfile,seqstrfilelen,wdlen)
      call trima(seqstrfile,seqstrfilelen)
      proffilelen=len(proffile)
      call nextwd(line,length,proffile,proffilelen,wdlen)
      call trima(proffile,proffilelen)

      write(*,'(4(a,1x))') 'QUERY> ',seqfile,seqstrfile,proffile

C      write (*,*)'TEST-files: ',seqfile,seqstrfile,proffile
C        call readanyseq(seqfile,seq,seqlen,seqfound,profile,seqss,x,y,z,seqtype,resnum)
      call readpsiblastprofile(proffile,profile,profseq,proflen,found)
      if (.not.(found .and. proflen .gt. 0))   then
        write (*,*) proffile
         call stopprocess('ERROR: profile file not found')
      end if

C      call readpdb_ca(pdbfile,chain,seq,seqlen,seqss,found,x,y,z,offset,resnum)
      call readseq(seqfile,seq,seqlen,found)
      if (.not.(found .and. seqlen .gt. 0))   then
         call stopprocess('ERROR: Sequence file is not found')
      end if

      call readpsipred(seqstrfile, ssseq, ssseqlen, seqss, found)
      if (.not.(found .and. ssseqlen .gt. 0))   then
         call stopprocess('ERROR: seqstr file  not found')
      end if

C compare the sequences
      if (seqlen .ne. ssseqlen .or. seqlen .ne. proflen ) then
         call stopprocess('ERROR: Sequence length differs')
      end if
         
      do i=1,seqlen
         if (seq(i) .ne. seq(i) .or. seq(i) .ne. profseq(i)) then
            call stopprocess('ERROR: Sequences differs')
         end if
      end do
C            call addsstoseq(seq,seqss,seqlen,numaa)
C      do i=1,proflen
C         profile(i,penopen)=gapopen+gapext
C         profile(i,penext)=gapext
C      end do
C      call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)

C Now we need start reading the libarary and doing aligments
      num=0
      do while (.true.)
        num=num+1
         type=0
         read(infile2,'(A)',err=777,end=666) line
         length=len(line)
         call trima(line,length)
         pdbfilelen2=len(pdbfile2)
         call nextwd(line,length,pdbfile2,pdbfilelen2,wdlen)
         call trima(pdbfile2,pdbfilelen2)

         seqstrfilelen2=len(seqstrfile2)
         call nextwd(line,length,seqstrfile2,seqstrfilelen2,wdlen)
         call trima(seqstrfile2,seqstrfilelen2)
         proffilelen2=len(proffile2)
         call nextwd(line,length,proffile2,proffilelen2,wdlen)
         call trima(proffile2,proffilelen2)

         write(*,'(4(a,1x))') 'FILES> ',pdbfile2,seqstrfile2,proffile2
C     call readanyseq(seqfile,seq,seqlen,seqfound,profile,seqss,x,y,z,seqtype,resnum)
         call readpsiblastprofile(proffile2,profile2,profseq2,proflen2,found)
         if (.not.(found .and. proflen2 .gt. 0))   then
            call stopprocess('ERROR2: profile file  not found')
         end if

         chain2='x'
         call readpdb_ca(pdbfile2,chain2,seq2,seqlen2,seqss2,found,x,y,z,offset2,resnum)
         if (.not.(found .and. seqlen2 .gt. 0))   then
           write(*,*) 'Pdbfile: ',pdbfile2
            call stopprocess('ERROR2: PDB file is not found')
         end if

         call readstride(seqstrfile2, ssseq2, ssseqlen2, seqss2, found,resnum2,chain2)
         if (.not.(found .and. ssseqlen2 .gt. 0))   then
            call stopprocess('ERROR2: seqstr file  not found')
         end if

C compare the sequences to make sure they are OK
         if (seqlen2 .ne. ssseqlen2 .or. seqlen2 .ne. proflen2 ) then
           write(*,'(a,3(1x,i8))')'ERROR> ',seqlen2,ssseqlen2,proflen2
           call stopprocess('ERROR2: Sequence length differs')
         end if
         
         do i=1,seqlen2
            if (seq2(i) .ne. ssseq2(i) .or. seq2(i) .ne. profseq2(i)) then
              write (*,'(a,i8,3(1x,a))') 'ERROR> ',i,seq2(i),ssseq2(i),profseq2(i)
               call stopprocess('ERROR2: Sequences differs')
            end if
         end do



C     Now we have to do a lot of alignments.
         write(*,'(a)')'INFO> SEQUENCE ALIGNMENT'
C     Sequence-sequence alignments
         call makeseqprof(tempprofile,seq2,proflen2,table,go,ge)               
         do go=-15,-10,5
            do ge=-1,-1,0.5
              do i=1,proflen
                profile2(i,penopen)=go
                profile2(i,penext)=ge
              end do
               do alitype=0,10,10
                 type=type+1 
                 call dyntrace(tempprofile,proflen2,seq,seqlen,alitype,score,traceback)
                 call vdwEnergy(X,Y,Z,seqlen,proflen2,traceback,seq,E,R,Etot,Eaa,Eij,Eia)
C     do i=1,seqlen
C     write (*,*)i,traceback(i),seq(i),profseq2(traceback(i))
C     end do
                 call CountSsMatch(seqss,seqss2,seqlen,traceback,
     &                fraction,gaps,numgaps,ssgaps,allen,numal)
                 call storedata(num,type,allen,numal,seqlen,proflen2,
     &                score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps,dataallen,
     $                datanumal,dataseqlen,dataproflen,datascore,dataEtot,dataEaa,
     $                dataEij,dataEia,datafraction,datagaps,
     $                datassgaps,datanumgaps)
               end do
             end do
           end do
C     sequence-profile alignments (This does not work so we ignore it
C         write(*,'(a)')'INFO> SEQUENCE-PROFILE ALIGNMENT'
C     Q. Should we use other type of gap penalties.
C         do go=-15,-5,5
C            do ge=-2,0,.5          
C              do i=1,proflen
C                profile2(i,penopen)=go
C                profile2(i,penext)=ge
C              end do
C              do alitype=0,10,10
C                type=type+1 
C                call dyntrace(profile2,proflen2,seq,seqlen,alitype,score,traceback)
C                call vdwEnergy(X,Y,Z,seqlen,proflen2,traceback,seq,E,R,Etot,Eaa,Eij,Eia)
C                call CountSsMatch(seqss,seqss2,seqlen,traceback,
C     &               fraction,gaps,numgaps,ssgaps,allen,numal)
C                call storedata(num,type,allen,numal,seqlen,proflen2,
C     &               score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps,dataallen,
C     $               datanumal,dataseqlen,dataproflen,datascore,dataEtot,dataEaa,
C     $               dataEij,dataEia,datafraction,datagaps,
C     $               datassgaps,datanumgaps)
C              end do
C            end do
C          end do
C     profile-sequence alignments
C     Q.  How do we get the traceback to be correct.
C
         write(*,'(a)')'INFO> PROFILE-SEQUENCE ALIGNMENT'
C     Q. Should we use other type of gap penalties.
         do go=-15,-10,5
            do ge=-1,-1,.5          
              do i=1,proflen
                profile2(i,penopen)=go
                profile2(i,penext)=ge
              end do
              do alitype=0,10,10
                type=type+1 
                call dyntrace(profile,seqlen,seq2,seqlen2,alitype,score,traceback)
C     We have to get the traceback to be correct
C
                do i=1,seqlen2
                  traceback2(i)=0
                end do
                do i=1,seqlen
                  traceback2(traceback(i))=i
                end do
                call vdwEnergy(X,Y,Z,seqlen,proflen2,traceback2,seq,E,R,Etot,Eaa,Eij,Eia)
                call CountSsMatch(seqss2,seqss,seqlen2,traceback2,
     &               fraction,gaps,numgaps,ssgaps,allen,numal)
                call storedata(num,type,allen,numal,seqlen,proflen2,
     &               score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps,dataallen,
     $               datanumal,dataseqlen,dataproflen,datascore,dataEtot,dataEaa,
     $               dataEij,dataEia,datafraction,datagaps,
     $               datassgaps,datanumgaps)
              end do
            end do
          end do
C
          write(*,'(a)')'INFO> SEQUENCE+SS ALIGNMENT'
C     seq+ss-seq+ss alignments
          do go=-15,-10,5
            do ge=-1,-1,.5          
              do s=1,2
                do s2=0,-0.1,-0.1
                  ssmatch(1,1)=s
                  ssmatch(1,2)=s2
                  ssmatch(1,3)=s2
                  ssmatch(2,1)=s2
                  ssmatch(2,2)=s
                  ssmatch(2,3)=s2
                  ssmatch(3,1)=s2
                  ssmatch(3,2)=s2
                  ssmatch(3,3)=s
                  
                  do alitype=0,10,10
C     ssmatch numaa parameters etc.
                    type=type+1 
                    call copyseq(seq,tempseq,seqlen)
                    call addsstoseq(tempseq,seqss,seqlen,numaa)
                    call makeseqprof(tempprofile,seq2,proflen2,table,go,ge)               
                    call makessprof(tempprofile,seqss2,seqlen,loopgap,ssmatch,numaa,numss)
                    call dyntrace(tempprofile,proflen2,seq,seqlen,alitype,score,traceback)
                    call vdwEnergy(X,Y,Z,seqlen,proflen2,traceback,seq,E,R,Etot,Eaa,Eij,Eia)
                    call CountSsMatch(seqss,seqss2,seqlen,traceback,
     &                   fraction,gaps,numgaps,ssgaps,allen,numal)
                    call storedata(num,type,allen,numal,seqlen,proflen2,
     &                   score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps,dataallen,
     $                   datanumal,dataseqlen,dataproflen,datascore,dataEtot,dataEaa,
     $                   dataEij,dataEia,datafraction,datagaps,
     $                   datassgaps,datanumgaps)
                  end do
                end do
              end do
            end do
          end do
C     seq+ss-prof+ss alignments
         write(*,'(a)')'INFO> SEQUENCE-PROFIL+ss ALIGNMENT'
C
         call copyprof(profile2,tempprofile,proflen2)
         call makessprof(tempprofile,seqss2,seqlen2,loopgap,ssmatch,numaa,numss)
         do go=-15,-10,5
            do ge=-1,-1,.5          
              do i=1,proflen
                tempprofile(i,penopen)=go
                tempprofile(i,penext)=ge
              end do
              do s=1,2
                do s2=0,-0.1,-0.1
                  ssmatch(1,1)=s
                  ssmatch(1,2)=s2
                  ssmatch(1,3)=s2
                  ssmatch(2,1)=s2
                  ssmatch(2,2)=s
                  ssmatch(2,3)=s2
                  ssmatch(3,1)=s2
                  ssmatch(3,2)=s2
                  ssmatch(3,3)=s
               do alitype=0,10,10
C     ssmatch numaa parameters etc.
                  type=type+1 
                  call copyseq(seq,tempseq,seqlen)
                  call addsstoseq(tempseq,seqss,seqlen,numaa)
                  call dyntrace(tempprofile,proflen2,seq,seqlen,alitype,score,traceback)
                  call vdwEnergy(X,Y,Z,seqlen,proflen2,traceback,seq,E,R,Etot,Eaa,Eij,Eia)
                  call CountSsMatch(seqss,seqss2,seqlen,traceback,
     &                 fraction,gaps,numgaps,ssgaps,allen,numal)
                  call storedata(num,type,allen,numal,seqlen,proflen2,
     &                 score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps,dataallen,
     $                 datanumal,dataseqlen,dataproflen,datascore,dataEtot,dataEaa,
     $                 dataEij,dataEia,datafraction,datagaps,
     $                 datassgaps,datanumgaps)
               end do
            end do
               end do
            end do
         end do
CC     prof+ss-seq+ss alignments
C         write(*,'(a)')'INFO> PROFILE-SEQUENCE+ss ALIGNMENT'
CC     Q. Should we use other type of gap penalties.
C         call copyprof(profile,tempprofile,proflen)
C         call makessprof(tempprofile,seqss,seqlen,loopgap,ssmatch,numaa,numss)
C         do go=-15,-5,5
C            do ge=-2,0,.5          
C              do i=1,proflen
C                profile2(i,penopen)=go
C                profile2(i,penext)=ge
C              end do
C              do alitype=0,10,10
C                type=type+1 
C                call copyseq(seq2,tempseq,seqlen2)
C                call addsstoseq(tempseq,seqss2,seqlen2,numaa)
C                call dyntrace(tempprofile,seqlen,tempseq,seqlen2,alitype,score,traceback)
CC     We have to get the traceback to be correct
C                do i=1,seqlen2
C                  traceback2(i)=0
C                end do
C                do i=1,seqlen
C                  traceback2(traceback(i))=i
C                end do
C                call vdwEnergy(X,Y,Z,seqlen,proflen2,traceback2,seq,E,R,Etot,Eaa,Eij,Eia)
C                call CountSsMatch(seqss2,seqss,seqlen2,traceback2,
C     &               fraction,gaps,numgaps,ssgaps,allen,numal)
C                call storedata(num,type,allen,numal,seqlen,proflen2,
C     &               score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps,dataallen,
C     $               datanumal,dataseqlen,dataproflen,datascore,dataEtot,dataEaa,
C     $               dataEij,dataEia,datafraction,datagaps,
C     $               datassgaps,datanumgaps)
C              end do
C            end do
C          end do
C     profile-profile alignments (not implemented yet)
C     seq-contact alignments (not implemented yet)
      end do
      
 666  continue
      stop
 777  call stopprocess('cannot read list')

      end

***********************************************************************
      subroutine stopprocess(string)
      implicit none
      character*(*) string
      write(*,*) string
      write(*,*)'STOP> Job terminates'
      stop
      end
************************************************************************
      subroutine storedata(num,type,allen,numal,seqlen,proflen,
     &     score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps,dataallen,
     $     datanumal,dataseqlen,dataproflen,datascore,dataEtot,dataEaa,
     $     dataEij,dataEia,datafraction,datagaps,
     $     datassgaps,datanumgaps)
      implicit none
#include <dynprog.h>

      integer num,type,allen,numal,seqlen,proflen
      integer gaps,ssgaps,numgaps
      real score,etot,eaa,eij,eia,fraction
      integer dataallen(maxnum,maxtype)
      integer datanumal(maxnum,maxtype)
      integer dataseqlen(maxnum,maxtype)
      integer dataproflen(maxnum,maxtype)
      real dataEtot(maxnum,maxtype)
      real dataEaa(maxnum,maxtype)
      real dataEij(maxnum,maxtype)
      real dataEia(maxnum,maxtype)
      real datafraction(maxnum,maxtype)
      real datascore(maxnum,maxtype)
      integer datagaps(maxnum,maxtype)
      integer datassgaps(maxnum,maxtype)
      integer datanumgaps(maxnum,maxtype)


      
      dataallen(num,type)=allen
      datanumal(num,type)=numal
      dataseqlen(num,type)=seqlen
      dataproflen(num,type)=proflen
      datascore(num,type)=score
      dataEtot(num,type)=Etot
      dataEaa(num,type)=Eaa
      dataEij(num,type)=Eij
      dataEia(num,type)=Eia
      datafraction(num,type)=fraction
      datagaps(num,type)=gaps
      datassgaps(num,type)=ssgaps
      datanumgaps(num,type)=numgaps


      write(*,'(a,1x,6i8,6e11.3,3i8)') 'SCORE> ',num,type,
     $     allen,numal,seqlen,proflen,
     &     score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps


      return
      end
