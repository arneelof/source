PINFO=	anal.o 	bounded.o build_misc.o \
	energy_area.o energy_atom.o energy_bowie.o \
	energy_dihe.o energy_dist.o  energy_main2.o \
	energy_rose.o membrtest.o \
	init2.o init_main.o \
	io_energy.o io_main.o io_param2.o \
	math.o  pinfo.o pdb_to_dih.o \
	rmsd.o sippl.o surface.o testing.o rmsd_common.o


LIBR= 	stringa.o stringch.o probfcn.o fsubs.o  cart_spher.o \
	numrec.o anal_main.o  \
	timer.o vector.o getenvfor.o mem_alloc.o

include flags.mk

MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino

OBJDIR= .
BINDIR= .



#	 *************** GRAIL ****************

pan: $(PINFO) $(INCL)    $(LIBR) 
	$(MAKE) -f pinfo.mk $(BINDIR)/pinfo

link: 	$(PINFO) $(INCL)   $(LIBR)   $(EXTRA)
	ld -m $(LFLAG)  $(PINFO)     $(LIBR)
	rm -f a.out


#
#  The real stuff .....
#

$(BINDIR)/pinfo: $(INCL) $(PINFO)   $(LIBR) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pinfo $(PINFO) $(LIBR) 

#
#	Include stuff ....
#
include dependencies.mk
