C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      subroutine errhnd(level,routine,mess)
C
C Handle error messages, and possibly terminate program execution
C
      integer level
      character*(*) routine,mess
#include <errcodes.h>
      integer krash,ikrash,errout,ierrout
      real a
      character*1 severity(UNKNERR)
      data krash,errout /WARNING,TERMOUT/
      data severity /'I','W','E','F','U'/
C
      if(level .gt. FATAL .or. level .lt. INFO) level=UNKNERR
      if(errout .gt. 0) write(errout,10) routine,severity(level),mess
      if(errout .lt. 0) write(*,10) routine,severity(level),mess
10    format(' %',A,'-',A,': ',A)
      if(level .ge. krash) then
        if(errout .gt. 0) write(errout,20)
        write(*,20)
20      format(/' *** Crash-level reached. Terminating execution. ***',
     &         /' *** Forcing a trace-back:                       ***')
        a=-3.5
        a=sqrt(a)
        stop 
      endif    
C
      return
c
      entry  setele(ikrash,ierrout)
      if(ikrash .lt. 999) krash=ikrash
      if(ierrout .lt. 999) errout=ierrout
      return
      entry getele(ikrash,ierrout)
      ikrash=krash
      ierrout=errout
      return
      end
C     
*****************************************************************
      subroutine readcom(lin,lout,comline,comlen,MXCOMLN,prompt,
     &     qinter,eof)
C
C Reads command from unit lin into comline, up to maximum of MXCOMLN
C Trims ending & beginning blanks, multiple line input signalled by
C ending line with a '-'. 
C If lout>0 command lines are echoed to unit lout
C If qinter is .true. the prompt string is used to prompt for input
C eof returned as true if end-of-file encountered
C
      integer lin,lout,comlen,MXCOMLN
      character*(*) comline
      character*(*) prompt
      logical qinter,eof
C
#include <errcodes.h>
      integer lpr,icont,llen,nblen,istart,templen
      character*1280 line
      character*1 prepr(2)
      character*1 cntmrk,excl
      character*1000 tempstr
      data prepr/' ','_'/,cntmrk/'-'/,excl/'!'/
C
      comlen=0
      eof=.false.      

      lpr=nblen(prompt)
      icont=1
5       if(qinter)then
          tempstr=prepr(icont)//prompt(1:lpr)//' '

          call getc2(tempstr,llen,line,eof)
          if (eof) goto 900
        else
          read(lin,'(A)',end=900) line
          llen=nblen(line)
        endif 
        if (llen.eq.0) return
        call muc(line)                         
        if(index(line,excl) .gt. 0)then
           llen=index(line,excl)-1
           call trima(line,llen)
        endif 
        if(lout .gt. 0 .and. llen .gt. 0)then
          tempstr=prepr(icont)//prompt(1:lpr)//' '//line(1:llen)
          templen=min(1000,llen+lpr+1+5)
          write(lout,'(A)') tempstr(1:templen)
        endif
        if (llen.eq.0) return
        if(line(llen:llen) .eq. cntmrk)then
          line(llen:llen)=' '
          call trima(line,llen)
          llen=llen+1
          line(llen:llen)=' '
          icont=2
        else
          icont=1 
          call trima(line,llen)
        endif
        if(comlen+llen .gt. MXCOMLN)then
          call errhnd(ERROR,'Readcom','Too long commandline')
        endif  
        llen=min(llen,MXCOMLN-comlen)
        comline(comlen+1:comlen+llen)=line(1:llen)
        comlen=comlen+llen
        if(icont.eq.2 .and. comlen.lt.MXCOMLN) goto 5
800   return
900   eof=.true.
      return
      end
C     
******************************************************************* 
      real function getnum(text,default)
C
C Prompt w/ text & default value. Return either a specified 
C value or (if CR) just the default.
C
C LN May 86
C
      integer MXSTLN
      parameter (MXSTLN=30)
      character*(*) text
      character*80 tempstr
      real default,decodf,r
C
      logical eof
      integer stlen,nblen,n
      external nblen,decodf
      character*(MXSTLN) defstr,answer
C
C  Make default value 'printable'
      call encodf(default,defstr,MXSTLN,stlen)     
10    tempstr=text(1:nblen(text))//' <'//defstr(1:stlen)//'>: '
      call getc2(tempstr,n,answer,eof)
      if(answer .eq. ' ')then
        getnum=default
      else
C  Make sure it is a number
        read(answer,*,err=50) r    
        getnum=decodf(answer,nblen(answer))
      endif
      return
50    write(*,*) answer(1:nblen(answer))//' is not a number'
      goto 10
      end
      SUBROUTINE GETI(PRSTR,N,IVEC)
C
C     Prompt with PRSTR for N integer variables IVEC
C
      CHARACTER* (*) PRSTR
      integer n
      INTEGER IVEC(N)
C
      WRITE(*,100) PRSTR
100   FORMAT('$',A)
      READ(*,*) IVEC
      RETURN
      END
      SUBROUTINE GETR(PRSTR,N,RVEC)
C
C     Prompt with PRSTR for N real variables RVEC
C
      CHARACTER* (*) PRSTR
      integer n
      REAL RVEC(N)
C
      WRITE(*,100) PRSTR
100   FORMAT('$',A)
      READ(*,*) RVEC
      RETURN
      END
      SUBROUTINE GETC2(PRSTR,N,CSTR,eof)
C     
C     Prompt with PRSTR for String CSTR.
C     N returns the number of characters input (.le. len(CSTR))
C     
      CHARACTER* (*) PRSTR,CSTR,SPACE*1
      INTEGER M,N,LENGTH
      logical eof
      DATA SPACE/' '/
C     
      eof=.false.
      LENGTH=LEN(PRSTR)
 20   IF(LENGTH .EQ. 0) goto 200
      IF(PRSTR(LENGTH:LENGTH) .NE. SPACE) GOTO 200
      LENGTH=LENGTH-1
      GOTO 20
 200  WRITE(*,100) PRSTR(1:LENGTH)
 100  FORMAT(A,$)
      READ(*,110,end=900) CSTR
 110  FORMAT(A)
      N=LEN(CSTR)
 10   IF(N .EQ. 0) RETURN
      IF(CSTR(N:N) .NE. SPACE) RETURN
      N=N-1
      GOTO 10
 900  eof=.true.
      return
      END
      SUBROUTINE GETCMD(LINE,NARG,NLEN)
C
C     Gets the command line (LINE), directly if the
C     main program is defined as a foreign command under VMS/DCL,
C     or by asking for it if not. NARG returns the number of arguments
C     (items separated by commas or spaces not within " "), NLEN returns
C     the total number of characters in LINE.
C
C     NOTE: The command line is ALWAYS converted to upper case!
C
C     Examples:
C     Command line                                        NARG NLEN
C     MYPROG                                                 1    6
C     arg1,arg2,,arg3                                        4   15
C     "arg 1 , ,"   arg2 , , arg3                            4   27
C     ,                                                      2    1
C     ARG,                                                   2    4
C      , ARG                                                 2    6 
C
C     1983-05-26/LN
C
      CHARACTER*(*) LINE
      CHARACTER*1 COMMA,SPACE,QUOTE,C,C1,c2
      integer i,nlen,istat,narg,nblen
      external nblen
      LOGICAL QUOFLG
      DATA COMMA,SPACE,QUOTE/',',' ','"'/
C
      NARG=0
C      ISTAT=LIB$GET_FOREIGN(LINE,'Command-line_:',NLEN)
      istat=0
      IF(NLEN .EQ. 0) RETURN
      NLEN=NBLEN(LINE)
      C1=SPACE
      C2=COMMA
      QUOFLG=.FALSE.
      DO I=1,NLEN
         C=LINE(I:I) 
         IF(C .EQ. QUOTE)THEN
#ifdef F2C
           if (QUOFLG) then
             QUOFLG=.false.
           else
             QUOFLG=.TRUE.
           endif
#else
            QUOFLG=(QUOFLG .XOR. .TRUE.)
#endif
         ELSEIF(.NOT. QUOFLG)THEN
            IF(C .EQ. SPACE)THEN
               IF(C1 .NE. COMMA  .AND. C1 .NE. SPACE)THEN
                  NARG=NARG+1
               ENDIF
            ELSEIF(C .EQ. COMMA)THEN
               IF(C1 .NE. SPACE .OR. C2 .EQ. COMMA) NARG=NARG+1
               C2=COMMA
            ELSE
               C2=SPACE
            ENDIF
         ENDIF
      C1=C
      IF(I .EQ. NLEN) NARG=NARG+1
      ENDDO
      RETURN
      END
#ifdef VMS
      SUBROUTINE GETARG(LINE,ARGSTR,NARG,NLEN)
C
C     Gets the command line (LINE) argument # NARG and returns it in
C     ARGSTR. The argument length is in NLEN.
C     NOTE: The command line has to be extracted BEFORE calling GETARG;
C     this can be done by using routine GETCMD.
C
C     Examples:
C     Command line                         ARGSTR         NARG NLEN
C     MYPROG                               MYPROG            1    6
C     arg1,arg2,,arg3                      ARG2              2    4
C     "arg 1 , ,"   arg2 , , arg3                            3    0
C     arg1, "a r , g2 " , arg3             "A R , G2 "       2   11
C
C     The command line is ALWAYS converted to upper case!
C
C     1983-05-26/LN
C
      CHARACTER*(*) LINE,ARGSTR
      CHARACTER*1 COMMA,SPACE,QUOTE,C,C1,c2
      LOGICAL QUOFLG
      DATA COMMA,SPACE,QUOTE/',',' ','"'/
C
      NLEN=0   
      NCLEN=NBLEN(LINE)
      IF(NCLEN .EQ. 0) RETURN
      C1=SPACE
      C2=COMMA
      NCARG=0
      QUOFLG=.FALSE.
      J1=1
      DO I=1,NCLEN
         C=LINE(I:I) 
         IF(C .EQ. QUOTE)THEN
            QUOFLG=QUOFLG .XOR. .TRUE.
         ELSEIF(.NOT. QUOFLG)THEN
            IF(C .EQ. SPACE)THEN
               IF(C1 .NE. COMMA  .AND. C1 .NE. SPACE)THEN
                  NCARG=NCARG+1
                  IF(NCARG .EQ. NARG-1) J1=I
               ENDIF
            ELSEIF(C .EQ. COMMA)THEN
               IF(C1 .NE. SPACE .OR. C2 .EQ. COMMA)THEN
                  NCARG=NCARG+1
                  IF(NCARG .EQ. NARG-1) J1=I
                  C2=COMMA
               ELSE
                  C2=COMMA
               ENDIF
            ELSE
               C2=SPACE
            ENDIF
         ENDIF
      C1=C
      IF(I .EQ. NCLEN)THEN
         NCARG=NCARG+1
         IF(NCARG .EQ. NARG-1) J1=I
      ENDIF 
      IF(NCARG .EQ. NARG)THEN
         J2=I
         GOTO 10
      ENDIF
      ENDDO
      RETURN
10    J1=J1
      J2=J2
      DO K=J1,J2
         C=LINE(K:K)
         IF(C .NE. SPACE .AND. C .NE. COMMA) GOTO 20
      ENDDO
      RETURN
20    QUOFLG=.FALSE.
      DO L=K,J2
         C=LINE(L:L)
         IF(C .EQ. QUOTE)  QUOFLG= QUOFLG .XOR. .TRUE.
         IF((C .EQ. SPACE .OR. C .EQ. COMMA) .AND. .NOT. QUOFLG) RETURN
         NLEN=NLEN+1
         ARGSTR(NLEN:NLEN)=C
      ENDDO
      RETURN
      END
#endif
      subroutine switch(n,label,string,flag,ival)
C      
C     Go through string and look for /c where c is a character
C     in label. If found set the corresponding flag .true..
C     If /-c is found set flag .false.. The construct /cn where
C     c is in label and n is a numeric value places this value in ival.
C     In all cases replace  with spaces. Case is ignored.
C      
      character*(*) label,string
      character*1 ch
      integer n
      integer ival(n),num,inext,iv,isl2,ikey,match,isl1,isl,nblen,lenstr
      logical flag(n),value,isdig
      external num,match,nblen
C      
      lenstr=nblen(string)
      if(string(lenstr:lenstr) .eq. '/') string(lenstr:lenstr)=' '
1     isl=index(string,'/')
      if(isl .eq. 0) return
      isl1=isl+1
      string(isl:isl)=' '
      ch=string(isl1:isl1)
      if(ch .eq. '-')then
         isl1=isl1+1
         ch=string(isl1:isl1)
         value=.false.
      else
         value=.true.
      endif
      ikey=match(label,ch)
      if(ikey .gt. 0 .and. ikey .le. n) flag(ikey)=value
      isl2=isl1+1
      if(isdig(string(isl2:isl2)).and. isl2 .le. lenstr)then
         iv=num(string,isl2,inext)
         isl1=inext-1
         if(ikey .gt. 0 .and. ikey .le. n) ival(ikey)=iv
      endif
      string(isl1:isl1)=' '
      goto 1
      end
C     
******************************************************************
      integer function num(st,istart,inext)
      character*(*) st
      character*1 ch
      logical isdig
      integer istart,n,inext,nblen,i
      external nblen
      n=nblen(st)
      num=0
      do i= istart,n
         ch=st(i:i)
         if(.not. isdig(ch)) goto 10
         num=num*10+ichar(ch)-48
      enddo
      inext=n+1
      return
10    inext=i
      return
      end
C     
*****************************************************
      logical function isdig(ch)
      character ch
      if(ichar(ch) .ge. 48 .and. ichar(ch) .le. 57)then
         isdig=.true.
      else
         isdig=.false.
      endif
      return
      end
