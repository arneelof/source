C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      integer chartonumdb(26)
      integer oldy
      character*1 numtochardb(20)
      real aafreq(20)
      real initfreq(60)
      real table(maxaa,maxaa)

      integer nlayers,nin,nhidden,nout,nwts
      real w1(MAXNNINP,MAXNNHIDDEN),w2(MAXNNHIDDEN,MAXNNOUT)
      real b1(MAXNNHIDDEN,MAXNNOUT),b2(MAXNNOUT)
      real Mprob_score(maxproflen,maxseqlen) 
      common /nlayers/ nlayers
      common /nin/ nin
      common /nhidden/ nhidden
      common /nout/ nout
      common /nwts/ nwts
      common /w1/ w1
      common /w2/ w2
      common /b1/ b1
      common /b2/ b2
      common /chartonumdb/ chartonumdb
      common /numtochardb/ numtochardb
      common /aafreq/ aafreq
      common /initfreq/ initfreq
      common /oldy/ oldy
      common /table/ table
      common /Mprob_score/ Mprob_score

C From
C http://h18009.www1.hp.com/fortran/docs/vf-html/pg/pgwuscom.htm
C
C Conversely, Fortran can declare the variable global (COMMON) 
C  and other languages can reference it as external:
C  ! Fortran declaring PI global
C   REAL PI
C   COMMON /PI/ PI ! Common Block and variable have the same name
C 
C In C, the variable is referenced as an external with the statement:
C 
C   //C code with external reference to PI
C   extern float PI;
