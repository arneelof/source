profconvert_(float profile[62][6500],int *len,int *numaa){
  // Converts a log-profile to a probability profile
  // the profiles are sorted after the 20(or more) amino acids (i.e. A C D E F ...)

  float aafreq[20]={ 7.588652e-02, 1.661404e-02,
		     5.288085e-02, 6.371048e-02,
		     4.091684e-02, 6.844532e-02,
		     2.241501e-02, 5.813866e-02,
		     5.958784e-02, 9.426655e-02,
		     2.372756e-02, 4.455974e-02,
		     4.908899e-02, 3.974479e-02,
		     5.169278e-02, 7.125817e-02, 
		     5.677277e-02, 6.585309e-02,
		     1.235821e-02, 3.188801e-02};
  
  float sum=0.0;//, lamb=0.0;
  int i=0,j=0;
  // This paramter is for psiblast profiles !!
  const float lamb = 0.3176;

  for(i=0; i<*numaa; i++){ // row
   sum=0.0;
   for(j=0; j<*len; j++){  // col
      profile[i][j]=aafreq[i]*exp(profile[i][j]*lamb);
      sum+=profile[i][j];
    }
  
    for(j=0; j<*len; j++){ // col
      profile[i][j]=profile[i][j]/sum;
      } 
    }
}
