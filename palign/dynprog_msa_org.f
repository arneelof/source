C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
c*******************************************************************************
C
C     Should be identical to dynprog.f except that seq is an array (of size aa)
C
C************************************************************************
      real function dynscore_msa(profile,proflen,seq,seqlen,type,seqss,
     $     profss,seqssfreq,profssfreq,numaa,shift,lambda,method,ssmatch)

c type=0 => local
c type=1 => ends free
c type=10 => global 
      implicit none
#include <dynprog.h>
#include <seqio.h>

      real bestscore,maxscorey,maxscorex
      integer  x, y,type,yminone,numaa,method
      real ssmatch(0:3,0:3)
      integer seqss(maxproflen),profss(maxproflen)
      real profile(maxproflen,maxaa+2),wgtend
      real seq(maxproflen,maxaa+2)
      real seqskip, profskip(maxseqlen),typefact,minscore
      real prescore(maxseqlen),  lastscore, bestjump
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real score, diagtest, proftest
      real temp,profprof,profprof_probscore_F
      real lambda,shift
      integer seqlen,proflen
      integer xminone
      external profprof,profprof_probscore_F


      if (method .eq. 5) then
         do x=1,seqlen
            do y=1,proflen
            Mprob_score(y,x)=profprof_probscore_F(y,x,profile,seq,profss,seqss,numaa,shift,lambda,ssmatch,profssfreq,seqssfreq)
         end do
      end do
      end if


c     init dimensions and bestscore
c     do the bottom row (y=1)
      wgtend=0
      if (type .eq. 10) then
        typefact=1.
        minscore=-9999.0
        bestscore = -9999.0
        profskip(1)=-9999.0
        maxscorey=minscore
C        prescore(1)=profile(1,seq(1))
        prescore(1)=profprof(1,1,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $       numaa,shift,lambda,method,ssmatch)
        wgtend=profile(1,penopen)
C        write (*,*) 'test1:',1,1,prescore(1)
C        bestscore = max(bestscore,score)
        do x = 2, seqlen
          wgtend=wgtend+profile(1,penext)
C          prescore(x)=profile(1,seq(x))+wgtend
          prescore(x)=profprof(1,x,profile,seq,profss,seqss,profssfreq,
     $         seqssfreq,numaa,shift,lambda,method,ssmatch)
          bestscore = max(bestscore,prescore(x))
          profskip(x) = -9999.0
C          write (5,'(a)') "test1:",x,1,prescore(x)
          score=prescore(x)
C          write(*,*) "test1:",x,1,score
        end do
      elseif (type .eq. 1) then
        typefact=0.
        minscore=-9999.0
        bestscore = -9999.0
        maxscorex=-9999.0
        maxscorey=-9999.0
        do x = 1, seqlen
C          score=profile(1,seq(x))
C          prescore(x)= max(score,minscore)
          score=profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $          numaa,shift,lambda,method,ssmatch)
          prescore(x)= max(score,minscore)
          profskip(x) = -9999.0
          bestscore = max(bestscore,score)
C          write(*,*) "test1:",x,profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,shift,lambda,method,ssmatch)
          maxscorey = max(maxscorey,score+profile(x,penopen))+profile(1,penext)
        end do
        maxscorey = max(maxscorey,score)

      elseif (type .eq. 0) then
        typefact=0.
        minscore=0.0
        bestscore = minscore
        do x = 1, seqlen
C          score=profile(1,seq(x))
C          prescore(x)= max(score,minscore)
          score=profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $          numaa,shift,lambda,method,ssmatch)
          prescore(x)= max(score,minscore)
          profskip(x) = -9999.0
          bestscore = max(bestscore,score)
C          write(*,*) "test1:",x,profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,shift,lambda,method,ssmatch),seq(x,1),profile(x,1),profss(x),seqss(x),profssfreq(x,1),seqssfreq(x,1),numaa,shift,lambda,method,ssmatch
        end do
      else
        call stopprocess('Unknown alignment methods')
      end if

      wgtend = (profile(1,penopen))*typefact
      seqskip=max(minscore,profile(1,penopen)+profile(1,penext))
      score=minscore

      do y = 2, proflen
        yminone=y-1
C        write(*,*)'test1',maxscorey,score
        maxscorey=max(maxscorey,score+profile(yminone,penopen)*typefact)+profile(yminone,penext)*typefact
c     for x=1 
        x = 1

C        score = max(minscore,(profile(y,seq(x))))
        score = max(minscore,profprof(y,x,profile,seq,profss,seqss,profssfreq,
     $       seqssfreq,numaa,shift,lambda,method,ssmatch))
        lastscore = score
        bestscore = max(bestscore,score)
        seqskip = minscore
        wgtend = wgtend+profile(y,penext)*typefact
C        write (*,*) 'test2:',x,y,prescore(x)
        do x = 2, seqlen
          xminone=x-1
          
C          temp = profile(y,seq(x))
          temp = profprof(y,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $         numaa,shift,lambda,method,ssmatch)
          diagtest = prescore(xminone)
          proftest = profskip(xminone)
C          write(*,*)'TEST ',x,y,temp,diagtest,proftest,seqskip
          score = max(max(diagtest,max(proftest,seqskip))+temp,minscore)
          bestscore = max(bestscore,score)
          bestjump = prescore(xminone) + profile(y,penopen)

          seqskip = max(seqskip,bestjump)+ profile(y,penext)

          profskip(xminone) = max(bestjump,profskip(xminone))+ profile(y,penext)
          prescore(xminone) = lastscore
          lastscore = score
          
C          write(*,*) '#rnd\t< ',profprof(y,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,shift,lambda,method,ssmatch),'\t>'
C          write(*,*) 'seqlen proflen',seqlen,proflen
          
       end do
C        prescore(1)=profile(y,seq(1))+wgtend
        prescore(1)=profprof(y,1,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $      numaa,shift,lambda,method,ssmatch)+wgtend*typefact
      end do


C      write(*,*)'dynscore_msa',score,maxscorey,maxscorex,bestscore

      if (type .eq. 10) then
        maxscorex=max(minscore,score)
        temp=profile(proflen,penopen)
        do x = seqlen-1,1,-1
          temp=temp+profile(proflen,penext)
          maxscorex=max(maxscorex,prescore(x)+temp)
        end do
C        write(*,*)'testing',score,maxscorey,maxscorex
        dynscore_msa=max(score,max(maxscorey,maxscorex))
      else if (type .eq. 1) then
C        write(*,*)'testing',score,minscore,bestscore,maxscorey
         dynscore_msa=max(maxscorey,score)
        do x = seqlen-1,1,-1
          dynscore_msa=max(dynscore_msa,prescore(x))
        end do
      elseif (type .eq. 0) then 
        dynscore_msa=bestscore
      end if
      return 

      end

c*******************************************************************************

      subroutine dyntrace_msa(profile,proflen,seq,seqlen,type,dynscore_msa,traceback,
     &     seqss,profss,seqssfreq,profssfreq,numaa,shift,lambda,method,ssmatch)

c type=0 => local
c type=1 => ends free
c type=10 => global 
      implicit none

#include <dynprog.h>
#include <seqio.h>

      real bestscore,maxscorex,maxscorey,trace_score
      integer x, y, type,bestx,besty,i,maxy,maxx,numaa,method
      real ssmatch(0:3,0:3)
      integer seqss(maxproflen),profss(maxproflen)
      real profile(maxproflen,maxaa+2),wgtend,dynscore_msa,seq(maxseqlen,maxaa+2)
      real seqskip, profskip(maxseqlen),typefact,minscore
      real prescore(maxseqlen),  lastscore, bestjump
      real score, diagtest,  proftest
      real temp,profprof
      real shift,lambda,profprof_probscore_F
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      integer seqlen,proflen
      integer xminone,traceback(maxseqlen),yminone
      integer prex(maxproflen,maxseqlen),prey(maxproflen,maxseqlen)
      integer xlast,ylast(maxproflen)
      external profprof,profprof_probscore_F


      if (method .eq. 5) then
         write(*,*)'Making a prob_score matrix'
         do x=1,seqlen
            do y=1,proflen
            Mprob_score(y,x)=profprof_probscore_F(y,x,profile,seq,profss,seqss,numaa,shift,lambda,ssmatch,profssfreq,seqssfreq)
         end do
      end do
c      write(*,*)'Mp_s',Mseqlen,Mproflen,Mprob_score(1,1)
c      write(*,*)'_____'
      end if


      typefact=0
C
C
c     init dimensions and bestscore
c     do the bottom row (y=1)

      bestx=1
      besty=1
      wgtend=0
      prex(1,1)=0
      prey(1,1)=0
      if (type .eq. 10) then
        typefact=1
        minscore=-9999.0
        bestscore = -9999.0
        profskip(1)=-9999.0
        maxscorey=minscore
C        prescore(1)=profile(1,seq(1))
        prescore(1)=profprof(1,1,profile,seq,profss,seqss,profssfreq,
     $       seqssfreq,numaa,shift,lambda,method,ssmatch)
        write (*,*) 'test1:',1,1,prescore(1)
C        bestscore = max(bestscore,score)
        wgtend=profile(1,penopen)
        ylast(1)=1
        write (*,*) 'test1:',1,1,prescore(1),' ',prex(1,1),' ',prey(1,1)
        do x = 2, seqlen
          ylast(x)=1
          wgtend=wgtend+profile(1,penext)
C          prescore(x)=profile(1,seq(x))+wgtend
          prescore(x)=profprof(1,x,profile,seq,profss,seqss,profssfreq,
     $         seqssfreq,numaa,shift,lambda,method,ssmatch)+wgtend
          prex(1,x)=x-1
          prey(1,x)=0
C          bestscore = max(bestscore,score)
          if (prescore(x) .gt. bestscore) then
            bestscore=prescore(x)
            bestx=x
            besty=1
          end if
          profskip(x) = -9999.0
C          write (*,*) 'test1:',x,1,prescore(x),' ',prex(1,x),' ',prey(1,x)
        end do
      elseif (type .eq. 1) then
         maxx=1;
        typefact=0
        minscore=-9999.0
        bestscore = -9999.0
        maxscorex = -9999.0
        do x = 1, seqlen
C          prescore(x) = max(profile(1,seq(x)),minscore)
          score=profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,
     $          shift,lambda,method,ssmatch)
C          write(*,*)'test ',score
          if (minscore .gt. score) then
            prescore(x)=minscore
            prex(1,x)=-1
            prey(1,x)=-1
            ylast(x)=0
          else
            prescore(x)=score
            prex(1,x)=x-1
            prey(1,x)=0
            ylast(x)=x
          endif
          profskip(x) = -9999.0
C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
            bestscore=score
            bestx=x
            besty=1
          end if
C          write (*,*) 'test1:',x,1,' ',prex(1,x),' ',prey(1,x),bestx,besty
          if (maxscorey .lt. score+profile(x,penopen)) then
             maxx=x
             maxscorey=score+profile(x,penopen)
          end if
          maxscorey=maxscorey+profile(1,penext)
C          write (*,*) 'test1:',x,1,seq(x),profile(1,seq(x)),' ',prex(1,x),' ',prey(1,x),bestx,besty
        end do
        if (maxscorey .lt. score) then
           maxx=seqlen
           maxscorey=score
        end if

        maxy=1
      elseif  (type .eq. 0) then
        typefact=0
        minscore=0.0
        bestscore = minscore
        do x = 1, seqlen
C          prescore(x) = max(profile(1,seq(x)),minscore)
          score=profprof(1,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $          numaa,shift,lambda,method,ssmatch)
C          write(*,*)'test ',score
          if (minscore .gt. score) then
            prescore(x)=minscore
            prex(1,x)=-1
            prey(1,x)=-1
            ylast(x)=0
          else
            prescore(x)=score
            prex(1,x)=x-1
            prey(1,x)=0
            ylast(x)=x
          endif
          profskip(x) = -9999.0
C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
            bestscore=score
            bestx=x
            besty=1
          end if
C          write (*,*) 'test1:',x,1,' ',prex(1,x),' ',prey(1,x),bestx,besty
        end do
      else
        call stopprocess('Unknown alignment methods')
      end if
      score=minscore
      maxy=1
      wgtend = (profile(1,penopen))*typefact
      seqskip=max(minscore,profile(1,penopen)+profile(1,penext))
      do y = 2, proflen
        yminone=y-1
C        maxscorey=max(maxscorey,score+profile(yminone,penopen))+profile(yminone,penext)
        if (maxscorey.lt. score+profile(yminone,penopen)*typefact) then
          maxy=yminone
          maxscorey=score+profile(yminone,penopen)*typefact
        end if
        maxscorey=maxscorey+profile(yminone,penext)*typefact
c     for x=1 
        x = 1
        xlast=1
C        score = max(minscore,(profile(y,seq(x))+wgtend))
C        if (minscore .gt. profile(y,seq(x))) then
        if (minscore .gt. profprof(y,x,profile,seq,profss,seqss,profssfreq,
     $       seqssfreq,numaa,shift,lambda,method,ssmatch))
     $       then
            score=minscore
            prex(y,x)=-1
            prey(y,x)=-1
          else
C            score=profile(y,seq(x))
            score=profprof(y,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $            numaa,shift,lambda,method,ssmatch)
C             write(*,*)'test ',y,x,score

            prex(y,x)=0
            prey(y,x)=yminone
          endif

        lastscore = score
C        bestscore = max(bestscore,score)
        if (score .gt. bestscore) then
          bestscore=score
          bestx=x
          besty=y
        end if
        seqskip = minscore
C
C        write (*,*) 'TEST3:',y,score,bestscore,bestx,besty
        wgtend = wgtend+profile(1,penext)*typefact
C        write (*,*) 'TEST2:',score,wgtend

C        write (*,*) 'test2:',x,y,score,prescore(x),' ',prex(y,x),' ',prey(y,x)
        do x = 2, seqlen
          xminone=x-1
          
C          temp = profile(y,seq(x))
          temp = profprof(y,x,profile,seq,profss,seqss,profssfreq,seqssfreq,numaa,
     $         shift,lambda,method,ssmatch)
          diagtest = prescore(xminone)
          proftest = profskip(xminone)

C          write (*,*) 'TEST4: ',y,x,diagtest,proftest,seqskip,temp
C          score=max(diagtest,max(proftest,seqskip))+temp
          if (diagtest .ge. seqskip .and. diagtest .ge. proftest) then
            score=diagtest+temp
            if (diagtest.eq.minscore) then
              prex(y,x)=-1
              prey(y,x)=-1
            else
              prex(y,x)=xminone
              prey(y,x)=yminone
              end if
          elseif (proftest .ge. seqskip) then
            score=proftest+temp
            prex(y,x)=xminone
            prey(y,x)=ylast(xminone)
          else
            score=seqskip+temp
            if (minscore .eq. seqskip) then
               prex(y,x)=-1
               prey(y,x)=-1
            else
               prex(y,x)=xlast
               prey(y,x)=yminone
            end if
         end if
C          score = max(score,minscore)
          if (minscore .gt. score) then
            score=minscore
            prex(y,x)=-1
            prey(y,x)=-1
          endif

C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
            bestscore=score
            bestx=x
            besty=y
          end if

          bestjump = prescore(xminone) + profile(y,penopen)

C          seqskip = max(seqskip,bestjump)
          if (bestjump .gt. seqskip) then 
            seqskip =bestjump
            xlast=xminone
C            write  (*,*) 'changed seqskip'
          end if

          seqskip = seqskip + profile(y,penext)

C          profskip(x) = max(bestjump,profskip(xminone))
          if (profskip(xminone) .lt. bestjump) then
            profskip(xminone) = bestjump
            ylast(xminone)=yminone
C            write  (*,*)'changed profskip',x
          endif

          profskip(xminone) = profskip(xminone) + profile(y,penext)
C          write (*,*)'test4:',bestjump,profskip(xminone),xminone
          prescore(xminone) = lastscore
          lastscore = score
C          write (*,*) 'test3:',x,y,prescore(xminone),score,' ',prex(y,x),' ',prey(y,x)
        end do
C        prescore(1)=profile(y,seq(1))+wgtend
        prescore(1)=profprof(y,1,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $       numaa,shift,lambda,method,ssmatch)+wgtend*typefact
      end do


C      maxscorey=max(score,maxscorey+profile(proflen,penopen)+profile(proflen,penext))
C      write(*,*)'dyntrace1',score,maxscorey,maxscorex,bestscore
C      if (maxscorey+profile(proflen,penopen)+seqlen*profile(proflen,penext).lt. score) then
      if (maxscorey .lt. score) then
        maxy=proflen-1
        maxscorey=score
C      else
C        maxscorey=maxscorey
      end if

C      write(*,*)'dyntrace',score,maxscorey,maxscorex,bestscore

      maxscorex=max(minscore,score)
      maxx=seqlen
      temp=profile(proflen,penopen)*typefact
      do x = seqlen-1,1,-1
        temp=temp+profile(proflen,penext)*typefact
C        maxscorex=max(maxscorex,prescore(x)+temp)
        if (maxscorex .lt. prescore(x)+temp) then
C          write(*,*)'test',x,maxscorex,prescore(x),temp
          maxx=x
          maxscorex=prescore(x)+temp
        end if
      end do


C      write(*,*)'TRACE-test> ',score,maxscorey,maxscorex,maxx,maxy,bestx,besty
      if (type .eq. 10 ) then

C         write(*,*)'TEST ',score,maxscorey,maxscorex
C        dynscore_msa=max(score,max(maxscorey,maxscorex))
        if (score .gt. maxscorey .and. score .gt. maxscorex) then
          dynscore_msa=score
          bestx=seqlen
          besty=proflen
        else if ( maxscorey .gt. maxscorex) then
          dynscore_msa=maxscorey
          bestx=seqlen
          besty=maxy
        else 
          dynscore_msa=maxscorex
          bestx=maxx
          besty=proflen
        end if
      else if (type .eq. 1 ) then
C         write(*,*)'TEST ',score,maxscorey,maxscorex
        if (score .gt. maxscorey .and. score .gt. maxscorex) then
          dynscore_msa=score
          bestx=seqlen
          besty=proflen
        else if ( maxscorey .gt. maxscorex) then
          dynscore_msa=maxscorey
          bestx=seqlen
          besty=maxy
        else 
          dynscore_msa=maxscorex
          bestx=maxx
          besty=proflen
        end if
      elseif (type .eq. 0) then
        dynscore_msa=bestscore
      end if

      do x=1,seqlen
        traceback(x)=0.
      end do

      y=besty
      x=bestx

      do while (y.gt.0.and.x.gt.0)
         trace_score=profprof(y,x,profile,seq,profss,seqss,profssfreq,
     $        seqssfreq,numaa,shift,lambda,method,ssmatch)
C         write(*,'(a,5i6)')'TRACE> ',x,y,prex(y,x),prey(y,x),trace_score
C         write(*,*)'TRACE> ',x,y,prex(y,x),prey(y,x),trace_score
          traceback(x)=y
          i=x
          x=prex(y,x)
          y=prey(y,i)
      end do

C      write(*,*) 'testing ',dynscore_msa

      return
      end



C************************************************************************
      real function profprof(y,x,profile,seq,profss,seqss,profssfreq,seqssfreq,
     $     num,shift,lambda,method,ssmatch)

      implicit none
#include <dynprog.h>
#include <seqio.h>


      integer x,y,num,method,count
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real shift,lambda
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real ssmatch(0:3,0:3)
      real profprof_profsim_F,profprof_dp_F,profprof_logaver_F
      real profprof_probscore_F,profprof_nn_F,profprof_nn_raw_F
      external profprof_profsim_F,profprof_dp_F,profprof_logaver_F
      external profprof_probscore_F,profprof_nn_F,profprof_nn_raw_F
      

c      write(*,*)'test ',y,x,profile(1,y),seq(1,x),method,shift
      if (method .eq. 1) then
         profprof=profprof_profsim_F(y,x,profile,seq,profss,seqss,num,shift,lambda,ssmatch)
      else if (method .eq. 2) then
         profprof=profprof_dp_F(y,x,profile,seq,profss,seqss,num,shift,lambda,ssmatch)
      else if (method .eq. 4) then
         profprof=profprof_probscore_F(y,x,profile,seq,profss,seqss,num,shift,lambda,ssmatch,profssfreq,seqssfreq)
      else if (method .eq. 5) then
         profprof=profprof_nn_F(y,x,profile,seq,profss,seqss,num,shift,lambda,
     $        ssmatch,profssfreq,seqssfreq)
      else if (method .eq. 6) then
         profprof=profprof_nn_raw_F(y,x,profile,seq,profss,seqss,num,shift,lambda,
     $        ssmatch,profssfreq,seqssfreq)
      else
         profprof=profprof_logaver_F(y,x,profile,seq,profss,seqss,num,shift,lambda,ssmatch,profssfreq,seqssfreq)
      end if


      return
      end
c#ifndef CPROFPROF
C************************************************************************
C yona (prof_sim)
C************************************************************************
      real function profprof_profsim_F(y,x,profile,seq,profss,seqss,num,shift,lambda,ssmatch)
C
C Note that the profile has to be converted to a probability profile before
C This routine is called.
C Seems to be working 17-02-2004

      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)

      integer k
      real s,d,r(maxaa),r2(maxaa),kullbackleiblerf
      real p(maxaa),q(maxaa),tiny
      parameter (tiny=1.e-20)
      external kullbackleiblerf

      if (x .eq. 5 .and.  y.eq.5) then
      write(*,*)'p-p-s-s-m',profile(y,1),seq(x,1),profss(y),seqss(x),ssmatch(seqss(x),profss(y))           
      end if
      do k=1,num
        r(k)=lambda*profile(y,k)+(1-lambda)*seq(x,k)+tiny
        p(k)=profile(y,k)+tiny
        q(k)=seq(x,k)+tiny
      end do
 
      d=lambda*kullbackleiblerf(p,r,num)+
     $     (1-lambda)*kullbackleiblerf(q,r,num)

      do k=1,num
        r2(k)=lambda*r(k)+(1-lambda)*initfreq(k)
      end do

      s=lambda*kullbackleiblerf(r,r2,num)+
     $     (1-lambda)*kullbackleiblerf(initfreq,r2,num)
      
      profprof_profsim_F=0.5*(1-d)*(1+s)+shift+ssmatch(seqss(x),profss(y))

      return
      end
C************************************************************************
      real function kullbackleiblerf(p,q,num)
C Seems to be working 17-02-2004
      implicit none
#include <dynprog.h>
      real logtwo
      integer k,num
      real p(num),q(num)
      parameter (logtwo=1.442695)

      kullbackleiblerf=0

      do k=1,num
        kullbackleiblerf=kullbackleiblerf+p(k)*log(p(k)/q(k))*logtwo
      end do

      return
      end


C************************************************************************
c  picasso (prob_score)
C************************************************************************

   
      real function profprof_probscore_F(y,x,profile,seq,profss,seqss
     $     ,num,shift,lambda,ssmatch,profssfreq,seqssfreq)
C Seems to be working 19-02-2004 
      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
    
      real Pf1(maxaa+2),Sf1(maxaa+2)
      real tiny, log_oddsf,ssScore
      integer k,i,j
      parameter (tiny=1.e-20)
      external log_oddsf
      ssScore=0
      do k=1,num
         Pf1(k)=profile(y,k)+tiny
         Sf1(k)=seq(x,k)+tiny
      end do
      do i=1,3
         do j=1,3
            ssScore=ssScore+ssmatch(i,j)*profssfreq(y,i)*seqssfreq(x,j)
c            write(*,*)'ssScore',ssScore,ssmatch(i,j),profssfreq(y,i),seqssfreq(x,j)
         end do
      end do
      profprof_probscore_F=log_oddsf(Pf1,Sf1,num,x,y) + log_oddsf(Sf1,Pf1,num,x,y) + shift + ssScore
C      write(*,*)'Pscore',profprof_probscore_F,i,j,y,x
      return
      end      
C************************************************************************
      real function log_oddsf(v1,v2,num,x,y)

C Seems to be working 17-02-2004 

      implicit none
#include <dynprog.h>
#include <seqio.h>
      real v1(maxaa+2),v2(maxaa+2)
      integer k,num,x,y

      log_oddsf=0
      do k=1,num
c        write(*,*)'LOG?!!!?',v1(k),v2(k),log_oddsf,k
         log_oddsf = log_oddsf + v1(k)*log(v2(k)/initfreq(k))
      end do
c     write(*,*)'LOG???',log_oddsf
      return
      end

C************************************************************************
C  ffas (DP)
C************************************************************************
      real function profprof_dp_F(y,x, profile, seq, profss, seqss,num, shift,lambda,ssmatch)

C Seems to be working 17-02-2004      

      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      
      integer k
      real Pf1(maxaa+2), Pf3(maxaa+2)
      real Sf1(maxaa+2), Sf3(maxaa+2)
      real scalar_prod
      real Psq,Ssq,Plength,Slength
      real tiny
      parameter (tiny=1.e-20)
      external final_fracf
      scalar_prod=0

      do k=1,num
         Pf1(k)=profile(y,k)
         Sf1(k)=seq(x,k)
C         write(*,*)'P',profile(y,k),'S',seq(x,k)
      end do

      call final_fracf(Pf1,Pf3, y, num)
      call final_fracf(Sf1,Sf3, x, num)
  
c     test: try to first make the vectors length =1
      if (lambda .ge.1) then
         do k=1,num
            Psq =Psq + Pf3(k)*Pf3(k)
            Ssq =Ssq + Sf3(k)*Sf3(k)
         end do
         Plength=sqrt(Psq)
         Slength=sqrt(Ssq)
         do k=1,num
            Pf3(k)=Pf3(k)/Plength
            Sf3(k)=Sf3(k)/Slength
         end do
      end if
      
      do k=1,num
         scalar_prod = scalar_prod + Pf3(k)*Sf3(k)
      end do
      profprof_dp_F=scalar_prod+shift+ssmatch(seqss(x),profss(y))
      
      return
      end
C************************************************************************
C  final_frac
C************************************************************************


      subroutine final_fracf(prob_frac,f_new,col,num)
C Seems to be working 17-02-2004
      implicit none
#include <dynprog.h>
#include <seqio.h>
      real prob_frac(maxaa+2),f_new(maxaa+2)
      real f1(maxaa+2), f2(maxaa+2)
      real  sqSum_f2
      integer k,col,num
      sqSum_f2=0
      do k=1,num
         f1(k) = prob_frac(k)*initfreq(k)
      end do
      
      do k=1,20
         f2(k)=5*f1(k) + prob_frac(k)
         sqSum_f2 = sqSum_f2 + f2(k)*f2(k)
      end do
      
      do k=1,20
         f_new(k)=f2(k)*f2(k)/sqSum_f2
      end do
      
      end
   
      
C************************************************************************
C     log_aver
C************************************************************************
      
      real function profprof_logaver_F(y,x,profile,seq,profss,seqss,num,shift,lambda,ssmatch,profssfreq,seqssfreq)
C Seems to be working 19-02-2004
      implicit none
#include <dynprog.h>
#include <seqio.h>

      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real p(maxaa+2), q(maxaa+2)
      integer k,true
      real scoreAveragef
      external scoreAveragef
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)

      real psum,qsum
      psum=0
      qsum=0
      do k=1,num
         p(k)=profile(y,k)
         q(k)=seq(x,k)
         psum=psum+p(k)
         qsum=qsum+q(k)
      end do
      if(psum < .99 .or. qsum < .99)then
c      write(*,*)'Psum Qsum',psum,qsum
      end if
      if(oldy .eq. y)then
         true=0
      end if
c      write(*,*)'profss ',profssfreq(y,1),profssfreq(y,2),profssfreq(y,3),profss(y)
c      write(*,*)'seqss ',seqssfreq(x,1),seqssfreq(x,2),seqssfreq(x,3),seqss(x)
      profprof_logaver_F=log(scoreAveragef(p,q,num,true,x,y))+shift+ssmatch(seqss(x),profss(y))

      oldy=y

      return
      end
      
C************************************************************************
      real function scoreAveragef(p,q,num,true,x,y)
C Seems to be working 19-02-2004      
      implicit none
#include <dynprog.h>
#include <seqio.h>

      real p(maxaa+2), q(maxaa+2)
      integer num,true
      real v(maxaa)
      integer k,l,i 
      real logtwo
      parameter (logtwo=0.693147)
      integer x,y
      true=1
      scoreAveragef=0

c      if(true .ne. 0) then
      do i=1,20
         v(i)=0
         do k=1,20
            v(i)=v(i)+p(k)*exp( (logtwo/2)*(table(i,k)) )
         end do           
      end do
c     end if
      
      
      do l=1,20
         scoreAveragef = scoreAveragef + v(l)*q(l)
      end do
      
     
c      else 
c         do l=1,20
c            scoreAveragef = scoreAveragef + v(l)*q(l)
c         end do
c      end if

      return
      end

 
C************************************************************************
C     nn_score 
C************************************************************************
      real function profprof_nn_F(y,x,profile,seq,profss,seqss,num,shift,lambda,
     $     ssmatch,profssfreq,seqssfreq)
C     Seems to be working 19-02-2004
      implicit none
#include <dynprog.h>
#include <seqio.h>
      
      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real p(nin+2)
      integer window,nvalue,inner,index
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real netfwdf,profprof_probscore_F
      external netfwdf,profprof_probscore_F

      nvalue=0
      index=0
      window=nin/40

      do inner=0,nin-1
         index=index+1
         if (y-(nin-1)/2+inner .lt. 0 .and. x-(nin-1)/2+inner .lt. 0) then
            p(index)=0
         else 
            p(index)=Mprob_score((y-(nin-1)/2+inner),x-(nin-1)/2+inner)
         end if
c         write(*,*)'new p',index,y,x,p(index)
c         write(*,*)'old p',index,y,x,p(index)
      end do
c     read window (=nin/40) positions into one vector (p) from each profile
      nvalue=0
      index=0
      window=nin/40
      profprof_nn_F=netfwdf(p)+shift
c      write(*,*)'profprof_nn_F',profprof_nn_F
      return
      end
C************************************************************************      
      real function profprof_nn_raw_F(y,x,profile,seq,profss,seqss,num,shift,lambda,
     $     ssmatch,profssfreq,seqssfreq)
C     Seems to be working 19-02-2004
      implicit none
#include <dynprog.h>
#include <seqio.h>
      
      integer x,y,num
      real shift,lambda
      integer profss(maxproflen),seqss(maxproflen)
      real profile(maxproflen,maxaa+2),seq(maxseqlen,maxaa+2)
      real ssmatch(0:3,0:3)
      real p(nin+2)
      integer window,nvalue,inner,index
      real netfwdf,profprof_probscore_F
      external netfwdf,profprof_probscore_F
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real zero(20);
      zero(1)=0
      zero(2)=0
      zero(3)=0
      zero(4)=0
      zero(5)=0
      zero(6)=0
      zero(7)=0
      zero(8)=0
      zero(9)=0
      zero(10)=0
      zero(11)=0
      zero(12)=0
      zero(13)=0
      zero(14)=0
      zero(15)=0
      zero(16)=0
      zero(17)=0
      zero(18)=0
      zero(19)=0
      zero(20)=0

      nvalue=0
      index=0
      window=nin/40

      do inner=0,nin-1
         index=index+1
         if (y-(nin-1)/2+inner .lt. 0 .and. x-(nin-1)/2+inner .lt. 0) then
            p(index)=0
         else 
            p(index)=Mprob_score((y-(nin-1)/2+inner),x-(nin-1)/2+inner)
         end if
c         write(*,*)'new p',index,y,x,p(index)
c         write(*,*)'old p',index,y,x,p(index)
      end do


c     read window (=nin/40) positions into one vector (p) from each profile
      nvalue=0
      index=0
      window=nin/40
c     do inner=0,window-1
c        do k=1,num
c           index=index+1
c           write(*,*)'idx',index
c           if (y-((window-1)/2 - inner) .gt. 0) then
c              p(index)=profile(y-((window-1)/2 - inner),k)
c           else
c               write(*,*)'tt',y-((window-1)/2 - inner)
c              p(index)=zero(k)
c           end if
c
c           if (x-((window-1)/2 - inner) .gt. 0) then
c              p(index+(nin/2))=seq(x-((window-1)/2 - inner),k)
c           else 
c              p(index+(nin/2))=zero(k)
c           end if
c        end do
c     end do
c This is the old way:
c      do inner=0,nin-1
c         index=index+1
c         if (y-(nin-1)/2+inner .gt. 0 .and. x-(nin-1)/2+inner .gt. 0) then
c            p(index)=profprof_probscore_F(y-(nin-1)/2+inner,x-(nin-1)/2+inner,profile,seq,profss,seqss,num,shift,lambda,ssmatch)
c         else
c            p(index)=0
c         end if
c         write(*,*)'old p',index,y,x,p(index)
c      end do
c
      profprof_nn_raw_F=netfwdf(p)+shift
c      write(*,*)'profprof_nn_raw_F',profprof_nn_raw_F
      return
      end
      
C************************************************************************

      real function netfwdf(values)
      
      implicit none
#include <dynprog.h>
#include <seqio.h>
      
      integer i,j
      real nodsum,values(2*maxaa+4)

      do i=1,nhidden
         nodsum=0
         do j=1,nin
            nodsum=nodsum + w1(j,i)*values(j);
         end do
         
         nodsum=nodsum+b1(i,1);
         nodsum=tanh(nodsum);
         netfwdf=netfwdf + nodsum*w2(i,1);
       
      end do
      netfwdf=netfwdf+b2(1) 
c      write(*,*)'x',netfwdf
c      write(*,*)'x',nin,nhidden,values
      return 
      end

C************************************************************************

