#include <stdio.h>
#include <math.h>
#include <dynprog_C.h>
#include <seqio_C.h>
int scnt=0;
int dcnt=0;
int box1=0;

#define max(A,B) ((A) > (B) ? (A) : (B))
#define min(A,B) ((A) < (B) ? (A) : (B))

//***************************************************
log_odds(float v1[], float v2[], int num)
{
  float initfreq[20]={7.588652e-02, 1.661404e-02,
		      5.288085e-02, 6.371048e-02,
		      4.091684e-02, 6.844532e-02,
		      2.241501e-02, 5.813866e-02,
		      5.958784e-02, 9.426655e-02,
		      2.372756e-02, 4.455974e-02,
		      4.908899e-02, 3.974479e-02,
		      5.169278e-02, 7.125817e-02, 
		      5.677277e-02, 6.585309e-02,
		      1.235821e-02, 3.188801e-02};
 
  float S=0.0;
  int k;
 
  for(k = 0; k < num; k++)
    {
      S += v1[k]*log(v2[k]/initfreq[k]);
    }
  return(S);
}

#ifdef GNU
float profprof_picasso__(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#else
float profprof_picasso_(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#endif
{
  float Pf1[maxaa+2];
  float Sf1[maxaa+2];
  float Score=0.0;
  int k;  
  int x=*X-1; 
  int y=*Y-1;
  int aanum=*num;
  float ssScore=0;
  float similar=0;
  const float tiny=1.0e-20; 

  for(k=0; k<*num; k++)
    {
      Pf1[k]=PROFILE[k][y]+tiny;
      Sf1[k]=SEQ[k][x]+tiny;
    }
    
  log_odds(Pf1,Sf1,aanum);
  log_odds(Sf1,Pf1,aanum);
  
  if(PROFSS[0][y] == SEQSS[0][x] && PROFSS[0][y]!=-1)
    {ssScore=0.0;}
  else if(PROFSS[0][y]!=-1 && SEQSS[0][x]!=-1)
    {ssScore=-0.0;}

  Score=log_odds(Pf1,Sf1,aanum) + log_odds(Sf1,Pf1,aanum) + *shift;
  // printf("score %f\n",Score);
  // if(box1 < 1000000 ){box1++;printf("#rnd <\t%f\t>\n",Score);}
 return(Score);
}
/*
  Reference:
  Holm, Heger
*/
