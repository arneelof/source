#FFLAG2=    -O3 -static    -funroll-loops -Wall  \
#	-ffixed-line-length-none -finline-functions -fomit-frame-pointer -Wimplicit -DGNU

FFLAG2= -O3 -132 -rcd -tpp6  -w95  -fpp  -Vaxlib -DIFC -w
#
#FFLAG2=   -g -ffixed-line-length-none  -fomit-frame-pointer -Wimplicit  -DGNU 


#CPPFLAG= -I./ \
#	 -DBIGREAL  -DSMALLINTEGER  -DGNU \
#	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS


FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 

LINK=ifc
F77=ifc
CPP=  cpp
#CC=	/modules/gnu/gcc/linux/bin/gcc 
CC=gcc
CCFLAG= -O $(CPPFLAG) -Dgnu  -I./
LFLAG= $(FFLAG2)
CD=	cd

EXTENSION= f

OBJDIR= .
BINDIR= .

MAKE= make

.f.o:	
	$(F77) -c $(FFLAG) -I./ $*.f

.c.o:	
	$(CC) -c $(CCFLAG)  $*.c

