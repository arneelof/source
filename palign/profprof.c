#include <stdio.h>
#include <math.h>
//*********************************************************************
float kullbackleibler(float p[], float q[], int num)
{    
  float logtwo = 1.442695;
  float KLsum = 0.0;
  int k;

for(k = 0; k < num; k++)
    {
      KLsum += p[k]*log(p[k]/q[k])*logtwo;
    }

// printf("KL = %f\n", KLsum);
  return(KLsum);
}

//************************************************************************
//float profprof(int y, int x, float profile[maxproflen][maxaa+2], float seq[maxseqlen][maxaa+2], int num)
float profprof_(int *Y, int *X, float PROFILE[62][6500], float SEQ[62][6500],
		int  PROFSS[1][6500], int SEQSS[1][6500], int *num)
{
 
 float initfreq[20] = {7.588652e-02, 1.661404e-02,
		        5.288085e-02, 6.371048e-02,
		        4.091684e-02, 6.844532e-02,
		        2.241501e-02, 5.813866e-02,
		        5.958784e-02, 9.426655e-02,
		        2.372756e-02, 4.455974e-02,
		        4.908899e-02, 3.974479e-02,
		        5.169278e-02, 7.125817e-02, 
		        5.677277e-02, 6.585309e-02,
		        1.235821e-02, 3.188801e-02};
     
  int k;
  float  r[62], r2[62],p[62], q[62];
  float score, d, PrfPrfsum = 0.0;
  const float lambda = 0.5;
  const float shift = 0.45;
  const float tiny = 1.0e-20; 
  int x = *X - 1;
  int y = *Y - 1;
  float pSum=0, qSum=0, rSum=0;

/*    for(k=0; k<*num;k++)
    {

      pSum += PROFILE[k][y];
         printf("prof %d %d %f\n",y,k,PROFILE[k][y]);
    }

   printf("SUMPROFILE = %f\n", pSum);
  //    printf("prof/seq content = %f, %f\n", PROFILE[1][0], SEQ[1][0]);
  */
   for(k = 0; k < *num; k++)
    {
      r[k] = lambda* PROFILE[k][y] + (1-lambda)* SEQ[k][x] + tiny;
      rSum += r[k];
      p[k] = PROFILE[k][y] + tiny;
      pSum += p[k];
      q[k] = SEQ[k][x] + tiny;
      qSum += q[k];
      //  printf("r[%d] =%f, p[] =%f, q[] =%f\n",k, r[k], p[k], q[k]);
    }
   //   printf("TEST-prfprf p,q,r,Sum %f %f %f\n",pSum, qSum, rSum);
   //        printf("prof/seq content = %f, %f\n", PROFILE[0][0], SEQ[0][0]);
   //	  printf("r[0] =%f, p[] =%f, q[] =%f\n", r[0], p[0], q[0]);
   //  printf("r[1] =%f, p[] =%f, q[] =%f\n", r[1], p[1], q[1]);
  
   d = lambda*kullbackleibler(p,r,*num) + (1-lambda)*kullbackleibler(q,r,*num); 
   //   printf("d=%f\n", d);
      
    for(k = 0; k < *num; k++)
    {
        r2[k] = lambda*r[k]+(1-lambda)*initfreq[k];
    }

   score = lambda*kullbackleibler(r,r2,*num)+ (1-lambda)*kullbackleibler(initfreq,r2,*num); 
   //   printf("score=%f\n", score);
   PrfPrfsum = 0.5*(1-d)*(1+score)-shift;
   //   printf("PrfPrfsum=%f\n", PrfPrfsum);

   //   printf("TEST-prfprf %f\t",PrfPrfsum);
   return(PrfPrfsum);
      
 }

  
