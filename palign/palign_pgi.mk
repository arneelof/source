PALIGN=	palign.o 

PCOMB=	pcomb.o 

PCOMB2=	pcomb2.o 

PCOMBT=	pcomb_thorsten.o 

PCOMBT2= pcomb_test.o 

PMEMBR=	pmembr.o 

PALIGN2= palign_list.o 

PALIGNJ= palign_jeanette.o

TEST=	test.o 

LIBR= 	dynprog.o stringa.o stringch.o numrec.o anal_main.o probfcn.o vector.o\
	vdw.o alignanal.o io.o profiles.o

TRACE= dyntrace.o

LIBRMSA=dynprog_msa.o 

include flags_pgi.mk

OBJDIR= .
BINDIR= .




#
#  The real stuff .....
#

$(BINDIR)/test: $(INCL) $(TEST)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/test $(TEST) $(LIBR) $(TRACE) 

$(BINDIR)/palign: $(INCL) $(PALIGN)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palign $(PALIGN) $(LIBR) $(TRACE) 

$(BINDIR)/pmembr: $(INCL) $(PMEMBR)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pmembr $(PMEMBR) $(LIBR) $(TRACE) 

$(BINDIR)/pcomb: $(INCL) $(PCOMB)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pcomb $(PCOMB) $(LIBR) $(TRACE) 

$(BINDIR)/pcomb2: $(INCL) $(PCOMB2)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pcomb2 $(PCOMB2) $(LIBR) $(TRACE) 

$(BINDIR)/pcomb_thorsten: $(INCL) $(PCOMBT)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pcomb_thorsten $(PCOMBT) $(LIBR) $(TRACE) 

$(BINDIR)/pcomb_test: $(INCL) $(PCOMBT2)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/pcomb_test $(PCOMBT2) $(LIBR) $(TRACE) 

$(BINDIR)/palign2: $(INCL) $(PALIGN2)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palign2 $(PALIGN2) $(LIBR) $(TRACE) 

$(BINDIR)/palignj: $(INCL) $(PALIGNJ)   $(LIBR) $(TRACE) 
	$(LINK) $(LFLAG) -o $(BINDIR)/palignj $(PALIGNJ) $(LIBR) $(TRACE) 

#
#	Include stuff ....
#
include dependencies.mk
