C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
C
C      STRING.FCM
C
C     COMMON FILE FOR TEMPORARY STRING MANIPULATION VARIABLES
C     NOTE: SCRMAX must match MXCMSZ of COMAND.FCM
C
      integer*4  scrmax,scrlen,swdmax,swdlen,fmtmax,fmtlen
      parameter (scrmax=800,swdmax=20,fmtmax=20)
c   temporary increase for some test cases - brb
c     parameter (scrmax=1536,swdmax=20,fmtmax=20)
      character*(swdmax) swdtch
      character*(fmtmax) fmtwd
      character*(scrmax) scrtch
      common/string/ swdtch,fmtwd,scrtch
      common/strngi/ swdlen,fmtlen,scrlen
 
