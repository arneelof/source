C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      program test_of_palign
      implicit none
#include <dynprog.h>
      
      real table(maxaa,maxaa),loopgap(3),ssmatch(0:3,0:3)
      real profile(maxproflen,maxaa+2),test
      character*800 line,proffile,seqfile,filename1,filename2
      integer proflen,seqlen,seq(maxseqlen),type,traceback(maxseqlen),i
      real dynscore
      external dynscore

      call init
      profile(1,1)=0.
      profile(1,2)=1.
      profile(1,3)=0.
      profile(1,4)=0.
      profile(1,penopen)=0.
      profile(1,penext)=0.

      profile(2,1)=0.
      profile(2,2)=0.
      profile(2,3)=1.
      profile(2,4)=0.
      profile(2,penopen)=0.
      profile(2,penext)=0.

      profile(3,1)=0.
      profile(3,2)=0.
      profile(3,3)=0.
      profile(3,4)=1.
      profile(3,penopen)=0.
      profile(3,penext)=0.

      proflen=3


      seq(1)=1
      seq(2)=2
      seq(3)=4
      seqlen=3

 
      line='/afs/pdc..se/home/a/arnee/source/palign/pam250.mat'
      call readfastaseqtable(line,table,maxaa)
      line='foo.mat'
      call writeseqtable(table,maxaa)
      stop
      type=10
      test=dynscore(profile,proflen,seq,seqlen,type)
      write(*,*) 'DYNSCORE> ',test

      call dyntrace(profile,proflen,seq,seqlen,type,test,traceback)
      write(*,*) 'DYNTRACE> ',test
      do i=1,seqlen
        write(*,*)'trace> ',i,traceback(i)
      end do
      write(*,*)'***** local ******'
      type=1
      test=dynscore(profile,proflen,seq,seqlen,type)
      write(*,*) 'DYNSCORE> ',test

      call dyntrace(profile,proflen,seq,seqlen,type,test,traceback)
      write(*,*) 'DYNTRACE> ',test
      do i=1,seqlen
        write(*,*)'trace> ',i,traceback(i)
      end do
      end

***********************************************************************
      Subroutine StopProcess(string)
      character*(*) string
      write(*,*) string
      write(*,*)'STOP> Job terminates'
      stop
      end

