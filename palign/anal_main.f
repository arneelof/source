C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      subroutine init
#include <dynprog.h>
#include <seqio.h>
      oldy=-99999;
      chartonumdb(1)= 1
      chartonumdb(2)= 21 
      chartonumdb(3)= 2 
      chartonumdb(4)= 3 
      chartonumdb(5)= 4 
      chartonumdb(6)= 5 
      chartonumdb(7)= 6 
      chartonumdb(8)= 7 
      chartonumdb(9)= 8 
      chartonumdb(10)=21 
      chartonumdb(11)=9 
      chartonumdb(12)=10 
      chartonumdb(13)=11 
      chartonumdb(14)=12 
      chartonumdb(15)=21 
      chartonumdb(16)=13 
      chartonumdb(17)=14 
      chartonumdb(18)=15 
      chartonumdb(19)=16
      chartonumdb(20)=17 
      chartonumdb(21)=21 
      chartonumdb(22)=18 
      chartonumdb(23)=19 
      chartonumdb(24)=21
      chartonumdb(25)=20 
      chartonumdb(26)=21
      numtochardb(1)='A'
      numtochardb(2)='C'
      numtochardb(3)='D'
      numtochardb(4)='E'
      numtochardb(5)='F'
      numtochardb(6)='G'
      numtochardb(7)='H'
      numtochardb(8)='I'
      numtochardb(9)='K'
      numtochardb(10)='L'
      numtochardb(11)='M'
      numtochardb(12)='N'
      numtochardb(13)='P'
      numtochardb(14)='Q'
      numtochardb(15)='R'
      numtochardb(16)='S'
      numtochardb(17)='T'
      numtochardb(18)='V'
      numtochardb(19)='W'
      numtochardb(20)='Y'


      aafreq(1)=7.588652e-02
      aafreq(2)=1.661404e-02
      aafreq(3)=5.288085e-02
      aafreq(4)=6.371048e-02
      aafreq(5)=4.091684e-02
      aafreq(6)=6.844532e-02
      aafreq(7)=2.241501e-02
      aafreq(8)=5.813866e-02
      aafreq(9)=5.958784e-02
      aafreq(10)=9.426655e-02
      aafreq(11)=2.372756e-02
      aafreq(12)=4.455974e-02
      aafreq(13)=4.908899e-02
      aafreq(14)=3.974479e-02
      aafreq(15)=5.169278e-02
      aafreq(16)=7.125817e-02
      aafreq(17)=5.677277e-02
      aafreq(18)=6.585309e-02
      aafreq(19)=1.235821e-02
      aafreq(20)=3.188801e-02

      initfreq(1)=7.588652e-02
      initfreq(2)=1.661404e-02
      initfreq(3)=5.288085e-02
      initfreq(4)=6.371048e-02
      initfreq(5)=4.091684e-02
      initfreq(6)=6.844532e-02
      initfreq(7)=2.241501e-02
      initfreq(8)=5.813866e-02
      initfreq(9)=5.958784e-02
      initfreq(10)=9.426655e-02
      initfreq(11)=2.372756e-02
      initfreq(12)=4.455974e-02
      initfreq(13)=4.908899e-02
      initfreq(14)=3.974479e-02
      initfreq(15)=5.169278e-02
      initfreq(16)=7.125817e-02
      initfreq(17)=5.677277e-02
      initfreq(18)=6.585309e-02
      initfreq(19)=1.235821e-02
      initfreq(20)=3.188801e-02
      return
      end
C************************************************************************
      subroutine initssfreq
#include <dynprog.h>
#include <seqio.h>

      initfreq(1)= 0.042920
      initfreq(2)= 0.022059
      initfreq(3)= 0.013399
      initfreq(4)= 0.019115
      initfreq(5)= 0.004740
      initfreq(6)= 0.018356
      initfreq(7)= 0.033980
      initfreq(8)= 0.012746
      initfreq(9)= 0.007720
      initfreq(10)=0.021275
      initfreq(11)=0.043069
      initfreq(12)=0.026052
      initfreq(13)=0.009688
      initfreq(14)=0.014079
      initfreq(15)=0.008502
      initfreq(16)=0.018083
      initfreq(17)=0.016055
      initfreq(18)=0.005275
      initfreq(19)=0.012157
      initfreq(20)=0.022897
      initfreq(21)=0.013609
      initfreq(22)=0.009909
      initfreq(23)=0.006310
      initfreq(24)=0.007442
      initfreq(25)=0.004636
      initfreq(26)=0.006755
      initfreq(27)=0.010097
      initfreq(28)=0.011051
      initfreq(29)=0.004956
      initfreq(30)=0.020847
      initfreq(31)=0.021094
      initfreq(32)=0.010672
      initfreq(33)=0.004686
      initfreq(34)=0.012415
      initfreq(35)=0.004928
      initfreq(36)=0.011345
      initfreq(37)=0.015019
      initfreq(38)=0.003814
      initfreq(39)=0.010740
      initfreq(40)=0.028037
      initfreq(41)=0.025627
      initfreq(42)=0.018448
      initfreq(43)=0.024516
      initfreq(44)=0.032437
      initfreq(45)=0.006931
      initfreq(46)=0.013383
      initfreq(47)=0.022956
      initfreq(48)=0.050211
      initfreq(49)=0.010020
      initfreq(50)=0.014187
      initfreq(51)=0.024515
      initfreq(52)=0.024205
      initfreq(53)=0.006811
      initfreq(54)=0.012487
      initfreq(55)=0.033178
      initfreq(56)=0.029757
      initfreq(57)=0.024984
      initfreq(58)=0.004309
      initfreq(59)=0.011476
      initfreq(60)=0.019026
      return
      end
      
*************************************************************
      
      subroutine traceinvers(seqlen,trace1,trace2)
      
      Implicit None
      integer i,seqlen,trace1(seqlen),trace2(seqlen)

      do i=1,seqlen
        trace2(i)=0
      end do
      do i=1,seqlen
        if (trace1(i) .gt. 0) trace2(trace1(i))=i
      end do

      return
      end
*************************************************************
      
      real Function GetMW(Seq)
      
      Implicit None
      
      Character Seq(*)*1
      integer   N
      
      GetMW = 0
      
      N=1
      Do While ( Seq(N) .ne. Char(0))
         if (Seq(N) .eq. 'A' ) GetMW = GetMW + 71
         if (Seq(N) .eq. 'R' ) GetMW = GetMW +  157
         if (Seq(N) .eq. 'N' ) GetMW = GetMW +  114
         if (Seq(N) .eq. 'D' ) GetMW = GetMW +  114 
         if (Seq(N) .eq. 'C' ) GetMW = GetMW +  103 
         if (Seq(N) .eq. 'Q' ) GetMW = GetMW +  128 
         if (Seq(N) .eq. 'E' ) GetMW = GetMW +  128 
         if (Seq(N) .eq. 'G' ) GetMW = GetMW +  57 
         if (Seq(N) .eq. 'H' ) GetMW = GetMW +  137 
         if (Seq(N) .eq. 'I' ) GetMW = GetMW +  113
         if (Seq(N) .eq. 'L' ) GetMW = GetMW +  113
         if (Seq(N) .eq. 'K' ) GetMW = GetMW +  129
         if (Seq(N) .eq. 'M' ) GetMW = GetMW +  131
         if (Seq(N) .eq. 'F' ) GetMW = GetMW +  147
         if (Seq(N) .eq. 'P' ) GetMW = GetMW +  97
         if (Seq(N) .eq. 'S' ) GetMW = GetMW +  87
         if (Seq(N) .eq. 'T' ) GetMW = GetMW +  101
         if (Seq(N) .eq. 'W' ) GetMW = GetMW +  186
         if (Seq(N) .eq. 'Y' ) GetMW = GetMW +  163
         if (Seq(N) .eq. 'V' ) GetMW = GetMW +  99
         
         N = N + 1
         
      End Do
      
      END

**********************************************************************
      Subroutine CenterOfMass(TotalAtoms,X,Y,Z,XCen,YCen,ZCen)

      Implicit None

      integer TotalAtoms,a,number
      real    X(*),Y(*),Z(*),XCen,YCen,ZCen
      real    XSum,YSum,ZSum

	XSum = 0
	YSum = 0
	ZSum = 0
        number=0
	Do a=1,TotalAtoms

           if (x(a).ne.9999.0) then
              XSum = XSum + X(a)
              YSum = YSum + Y(a)
              ZSum = ZSum + Z(a)
              number=number+1
           end if
	End Do

	XCen = XSum/FLOAT(number)
	YCen = YSum/FLOAT(number)
	ZCen = ZSum/FLOAT(number)

      END

*******************************************************************
      
      Subroutine ExtractCA(TotalAtoms,AtNam,X,Y,Z,CAX,CAY,CAZ,N)
      
      Implicit None
      
      Character  AtNam(*)*3
      integer    a,N,TotalAtoms
      real	   X(*),Y(*),Z(*),CAX(*),CAY(*),CAZ(*)
      
      
      N=0
      Do a = 1,TotalAtoms
         
         if(AtNam(a) .eq. 'CA ') then
            N = N+1
            CAX(N) = X(a)
            CAY(N) = Y(a)
            CAZ(N) = Z(a)
         end if
         
      End Do
      
      END
      
C     
************************************************************************
      integer function whatsecstr(phi,psi)
      Implicit None
      real phi,psi
      if ( (Phi.ge.-82.) .and.
     &     (Phi.le.-42.) .and. 
     &     (Psi.ge.-62.) .and. 
     &     (Psi.le.-22.) ) then
         whatsecstr=1
      else if ( (Phi.ge.-170.) .and.
     &        (Phi.le. -50.) .and. 
     &        (Psi.ge.  90.) .and. 
     &        (Psi.le. 180.) ) then
         whatsecstr=2
      else
         whatsecstr=3
      end if
      
      end



************************************************************************
      integer function seqtoseq(i,aa)
      Implicit None
      integer i,aa
      seqtoseq=i-aa*int((i-1)/aa)
C      write(*,*)i,aa,seqtoseq,i-aa*int((i-1)/aa)
      end

************************************************************************
      integer function seqtoss(i,aa)
      Implicit None
      integer i,aa
      seqtoss=int((i-1)/aa)+1
      end
***************************************************************

      integer Function CharToNum(ResNam)

      Implicit None

      Character ResNam*1
      integer i,j(2)
#include <dynprog.h>
#include <seqio.h>

C      j(1)=(1,2)
      i=ichar(resnam)-64
      chartonum=chartonumdb(i)
      return
      END


***************************************************************

      integer Function CharToSS(seqtype)

      Implicit None

      Character Seqtype*1

      Chartoss = 0

      call muc(seqtype)

      if (Seqtype .eq. 'A') then
        Chartoss = 1
      else if (Seqtype .eq. 'H') then
        Chartoss = 1
      else if (Seqtype .eq. 'G') then
        Chartoss = 1
      else if (Seqtype .eq. 'E') then
        Chartoss = 2
      else if (Seqtype .eq. 'B') then
        Chartoss = 2
      else if (Seqtype .eq. '.') then
        Chartoss = 3
      else if (Seqtype .eq. 'I') then
        Chartoss = 3
      else if (Seqtype .eq. 'L') then
        Chartoss = 3
      else if (Seqtype .eq. 'T') then
        Chartoss = 3
      else if (Seqtype .eq. 'C') then
        Chartoss = 3
      else if (Seqtype .eq. '-') then
        Chartoss = 3
      else if (Seqtype .eq. ' ') then
        Chartoss = 3
      end if

      END
***************************************************************

      subroutine  sstoChar(seqtype,seqnum)

      Implicit None
      integer seqnum
      Character Seqtype*1

      if (Seqnum .eq. 0) then 
        seqtype='X'
      else if(Seqnum .eq. 1) then
        seqtype='H'
      else if(Seqnum .eq. 2) then
        seqtype='E'
      else if(Seqnum .eq. 3) then
        seqtype='L'
      end if

      END
      
**************************************************************

      logical Function levitt_hyd(ResNum)
C
C     Just tells if a residue is hydrophobic or not (Tyr is not)
C

      Implicit None

      integer resnum

      levitt_hyd=.false.

C      if (ResNam .eq. 'ALA') then
C        NamToNum = 1
C      else if (ResNam .eq. 'ARG') then
C        NamToNum = 2
C      else if (ResNam .eq. 'ASN') then
C        NamToNum = 3
C      else if (ResNam .eq. 'ASP') then
C        NamToNum = 4
C      else if (ResNam .eq. 'CYS') then
C        NamToNum = 5
      if (resnum .eq. 5) then
        levitt_hyd = .true.

C      else if (ResNam .eq. 'GLN') then
C        NamToNum = 6
C      else if (ResNam .eq. 'GLU') then
C        NamToNum = 7
C      else if (ResNam .eq. 'GLY') then
C        NamToNum = 8
C      else if (ResNam .eq. 'HIS') then
C        NamToNum = 9
C      else if (ResNam .eq. 'ILE') then
C        NamToNum = 10
      else if (resnum .eq. 10) then
        levitt_hyd = .true.

C      else if (ResNam .eq. 'LEU') then
C        NamToNum = 11
      else if (resnum .eq. 11) then
        levitt_hyd = .true.

C      else if (ResNam .eq. 'LYS') then
C        NamToNum = 12
C      else if (ResNam .eq. 'MET') then
C        NamToNum = 13
      else if (resnum .eq. 14) then
        levitt_hyd = .true.

C      else if (ResNam .eq. 'PHE') then
C        NamToNum = 14
      else if (resnum .eq. 14) then
        levitt_hyd = .true.

C      else if (ResNam .eq. 'PRO') then
C        NamToNum = 15
C      else if (ResNam .eq. 'SER') then
C        NamToNum = 16
C      else if (ResNam .eq. 'THR') then
C        NamToNum = 17
C      else if (ResNam .eq. 'TRP') then
C        NamToNum = 18
      else if (resnum .eq. 18) then
        levitt_hyd = .true.

C      else if (ResNam .eq. 'TYR') then
C        NamToNum = 19
C      else if (ResNam .eq. 'VAL') then
C        NamToNum = 20
      else if (resnum .eq. 20) then
        levitt_hyd = .true.
      end if
C      end if

      END

C     
***************************************************************

      real Function hydrophobicity(ResNam)

C
C     from Kyte and doolittle
C
      Implicit None

      Character ResNam*3

      Hydrophobicity = 0

      if (ResNam .eq. 'ALA') then
        Hydrophobicity = 1.8
      else if (ResNam .eq. 'ARG') then
        Hydrophobicity = -4.5
      else if (ResNam .eq. 'ASN') then
        Hydrophobicity = -3.5
      else if (ResNam .eq. 'ASP') then
        Hydrophobicity = -3.5
      else if (ResNam .eq. 'CYS') then
        Hydrophobicity = 2.5
      else if (ResNam .eq. 'GLN') then
        Hydrophobicity = -3.5
      else if (ResNam .eq. 'GLU') then
        Hydrophobicity = -3.5
      else if (ResNam .eq. 'GLY') then
        Hydrophobicity = -0.4
      else if (ResNam .eq. 'HIS') then
        Hydrophobicity = -3.2
      else if (ResNam .eq. 'ILE') then
        Hydrophobicity = 4.5
      else if (ResNam .eq. 'LEU') then
        Hydrophobicity = 3.8
      else if (ResNam .eq. 'LYS') then
        Hydrophobicity = -3.9
      else if (ResNam .eq. 'MSE') then
        Hydrophobicity = 1.9
      else if (ResNam .eq. 'MET') then
        Hydrophobicity = 1.9
      else if (ResNam .eq. 'PHE') then
        Hydrophobicity = 2.8
      else if (ResNam .eq. 'PRO') then
        Hydrophobicity = -1.6
      else if (ResNam .eq. 'SER') then
        Hydrophobicity = -0.8
      else if (ResNam .eq. 'THR') then
        Hydrophobicity = -0.7
      else if (ResNam .eq. 'TRP') then
        Hydrophobicity = -0.9
      else if (ResNam .eq. 'TYR') then
        Hydrophobicity = -1.3
      else if (ResNam .eq. 'VAL') then
        Hydrophobicity = 4.2
      end if
      
      END
C     
***************************************************************

      real Function hydrophobicity2(ResNam)

C
C     from Engelman et al.
C
      Implicit None

      Character ResNam*3

      Hydrophobicity2 = 0

      if (ResNam .eq. 'ALA') then
        Hydrophobicity2 = 1.6
      else if (ResNam .eq. 'ARG') then
        Hydrophobicity2 = -12.3
      else if (ResNam .eq. 'ASN') then
        Hydrophobicity2 = -4.8
      else if (ResNam .eq. 'ASP') then
        Hydrophobicity2 = -9.2
      else if (ResNam .eq. 'CYS') then
        Hydrophobicity2 = 2.0
      else if (ResNam .eq. 'GLN') then
        Hydrophobicity2 = -4.1
      else if (ResNam .eq. 'GLU') then
        Hydrophobicity2 = -8.2
      else if (ResNam .eq. 'GLY') then
        Hydrophobicity2 = 1.0
      else if (ResNam .eq. 'HIS') then
        Hydrophobicity2 = -3.0
      else if (ResNam .eq. 'ILE') then
        Hydrophobicity2 = 3.1
      else if (ResNam .eq. 'LEU') then
        Hydrophobicity2 = 2.8
      else if (ResNam .eq. 'LYS') then
        Hydrophobicity2 = -8.8
      else if (ResNam .eq. 'MSE') then
        Hydrophobicity2 = 3.4
      else if (ResNam .eq. 'MET') then
        Hydrophobicity2 = 3.4
      else if (ResNam .eq. 'PHE') then
        Hydrophobicity2 = 3.7
      else if (ResNam .eq. 'PRO') then
        Hydrophobicity2 = -0.2
      else if (ResNam .eq. 'SER') then
        Hydrophobicity2 = 0.6
      else if (ResNam .eq. 'THR') then
        Hydrophobicity2 = 1.2
      else if (ResNam .eq. 'TRP') then
        Hydrophobicity2 = 1.9
      else if (ResNam .eq. 'TYR') then
        Hydrophobicity2 = -0.7
      else if (ResNam .eq. 'VAL') then
        Hydrophobicity2 = 2.6
      end if
      
      END
C     
***************************************************************

      real Function atom_hydrophobicity(atnam,resnam)

C
C     from Kyte and doolittle
C
      Implicit None

      Character ResNam*3,atnam*3

      atom_hydrophobicity = 0

      if (atnam(1:1).eq."C") then
        atom_hydrophobicity=16.0

      else if (atnam(1:1).eq."S") then
        atom_hydrophobicity=21.0

      else if (atnam(1:1).eq."N") then
        atom_hydrophobicity=-6.
        if (resnam.eq."LYS" .and. atnam.eq."NZ") atom_hydrophobicity=-50.
        if (resnam.eq."ARG" .and. atnam.eq."NH1")atom_hydrophobicity=-50.
C        if (resnam.eq."HIS" .and. )

      else if (atnam(1:1).eq."O") then
        atom_hydrophobicity=-6.
        if (resnam.eq."ASP" .and. atnam.eq."OD1") atom_hydrophobicity=-24.
        if (resnam.eq."GLU" .and. atnam.eq."OE1") atom_hydrophobicity=-24.

      end if

      
      END
C     
***************************************************************

	Subroutine NumToChar(Num,ResNam)

	Implicit None

	Character ResNam*1
	integer   Num
#include <dynprog.h>
#include <seqio.h>
        resnam=numtochardb(num)
        return
        end
C     
*****************************************************************

	Subroutine oneToThree(OneSeq,ThreeSeq)


	Implicit None

	Character OneSeq*1,ThreeSeq*3

        ThreeSeq = 'XXX'
	if (OneSeq .eq. 'A' ) then
          ThreeSeq = 'ALA' 
        else if (OneSeq .eq. 'R' ) then
          ThreeSeq = 'ARG' 
        else if (OneSeq .eq. 'N' ) then
          ThreeSeq = 'ASN' 
        else if (OneSeq .eq. 'D' ) then
          ThreeSeq = 'ASP' 
        else if (OneSeq .eq. 'C' ) then
          ThreeSeq = 'CYS' 
        else if (OneSeq .eq. 'Q' ) then
          ThreeSeq = 'GLN' 
        else if (OneSeq .eq. 'E' ) then
          ThreeSeq = 'GLU' 
        else if (OneSeq .eq. 'G' ) then
          ThreeSeq = 'GLY' 
        else if (OneSeq .eq. 'H' ) then
          ThreeSeq = 'HIS' 
        else if (OneSeq .eq. 'I' ) then
          ThreeSeq = 'ILE'
        else if (OneSeq .eq. 'L' ) then
          ThreeSeq = 'LEU'
        else if (OneSeq .eq. 'K' ) then
          ThreeSeq = 'LYS'
        else if (OneSeq .eq. 'M' ) then
          ThreeSeq = 'MET'
        else if (OneSeq .eq. 'F' ) then
          ThreeSeq = 'PHE'
        else if (OneSeq .eq. 'P' ) then
          ThreeSeq = 'PRO'
        else if (OneSeq .eq. 'S' ) then
          ThreeSeq = 'SER'
        else if (OneSeq .eq. 'T' ) then
          ThreeSeq = 'THR'
        else if (OneSeq .eq. 'W' ) then
          ThreeSeq = 'TRP'
        else if (OneSeq .eq. 'Y' ) then
          ThreeSeq = 'TYR'
        else if (OneSeq .eq. 'V' ) then
          ThreeSeq = 'VAL'
        end if

	END


*****************************************************************
      Subroutine ThreeToONE(OneSeq,ThreeSeq)


      Implicit None
        
      Character OneSeq*1,ThreeSeq*3
      Oneseq='X'
      if (Threeseq .eq. 'ALA' ) then
        OneSeq = 'A' 
      else if (Threeseq .eq. 'ARG' ) then
        OneSeq = 'R' 
      else if (Threeseq .eq. 'ASN' ) then
        OneSeq = 'N' 
      else if (Threeseq .eq. 'ASP' ) then
        OneSeq = 'D' 
      else if (Threeseq .eq. 'CYS' ) then
        OneSeq = 'C' 
      else if (Threeseq .eq. 'GLN' ) then
        OneSeq = 'Q' 
      else if (Threeseq .eq. 'GLU' ) then
        OneSeq = 'E' 
      else if (Threeseq .eq. 'GLY' ) then
        OneSeq = 'G' 
      else if (Threeseq .eq. 'HIS' ) then
        OneSeq = 'H' 
      else if (Threeseq .eq. 'ILE' ) then
        OneSeq = 'I'
      else if (Threeseq .eq. 'LEU' ) then
        OneSeq = 'L'
      else if (ThreeSeq .eq. 'LYS' ) then
        OneSeq = 'K'
      else if (ThreeSeq .eq. 'MSE' ) then
        OneSeq = 'M'
      else if (ThreeSeq .eq. 'MET' ) then
        OneSeq = 'M'
      else if (ThreeSeq .eq. 'PHE' ) then
        OneSeq = 'F'
      else if (ThreeSeq .eq. 'PRO' ) then
        OneSeq = 'P'
      else if (ThreeSeq .eq. 'SER' ) then
        oneSeq = 'S'
      else if (ThreeSeq .eq. 'THR' ) then
        oneSeq = 'T'
      else if (ThreeSeq .eq. 'TRP' ) then
        OneSeq = 'W'
      else if (ThreeSeq .eq. 'TYR' ) then
        OneSeq = 'Y'
      else if (ThreeSeq .eq. 'VAL' ) then
        OneSeq = 'V'
      endif

      END


******************************************************************

      real Function GetDistance(X1,Y1,Z1,X2,Y2,Z2)

      Implicit None

      real     X1,Y1,Z1,X2,Y2,Z2
      real     XDiff,YDiff,ZDiff

      
      XDiff = X1-X2
      YDiff = Y1-Y2
      ZDiff = Z1-Z2
      

      GetDistance = SQRT(XDiff**2 + YDiff**2 + ZDiff**2)


      END

******************************************************************
*     CHECKDISTANCE
*     
*     Returns True if pt1 within 'range' of pt2
**********************************************************************

      Logical Function CheckDistance(X1,Y1,Z1,X2,Y2,Z2,Range,Dist)

      Implicit None

      real X1,Y1,Z1,X2,Y2,Z2,Range,XDiff,YDiff,ZDiff,Dist


      CheckDistance=.FALSE.
      XDiff = ABS(X1-X2)
      YDiff = ABS(Y1-Y2)
      ZDiff = ABS(Z1-Z2)

      if(XDiff .le. range .and. YDiff .le. range .and. Zdiff .le. range) then

        Dist= XDiff**2 + YDiff**2 + ZDiff**2

        if(Dist .le. range**2) CheckDistance=.TRUE.

      end if	

      END  
C     End of CheckDistance


**********************************************************************
* GETFREEAREA
*
*  Returns the Area of a residue in a Gly_X_Gly reference tripeptide
**********************************************************************

      REAL FUNCTION GetFreeArea(Res)

	Implicit None
	
	Character*3  Res
	
	 GetFreeArea=10000
	 
	if(Res .eq. 'ARG') then
          GetFreeArea = 208
        else if(Res .eq. 'LYS') then
          GetFreeArea = 179
        else if(Res .eq. 'ASP') then
          GetFreeArea = 116
        else if(Res .eq. 'GLU') then
          GetFreeArea = 151
        else if(Res .eq. 'ASN') then
          GetFreeArea = 123
        else if(Res .eq. 'GLN') then
          GetFreeArea = 151
        else if(Res .eq. 'SER') then
          GetFreeArea = 86
        else if(Res .eq. 'GLY') then
          GetFreeArea = 40
        else if(Res .eq. 'HIS') then
          GetFreeArea = 175
        else if(Res .eq. 'ALA') then
          GetFreeArea = 71
        else if(Res .eq. 'THR') then
          GetFreeArea = 113
        else if(Res .eq. 'PRO') then
          GetFreeArea = 123
        else if(Res .eq. 'TYR') then
          GetFreeArea = 197
        else if(Res .eq. 'MSE') then
          GetFreeArea = 172
        else if(Res .eq. 'MET') then
          GetFreeArea = 172
        else if(Res .eq. 'VAL') then
          GetFreeArea = 130
        else if(Res .eq. 'CYS') then
          GetFreeArea = 56
        else if(Res .eq. 'LEU') then
          GetFreeArea = 154
        else if(Res .eq. 'ILE') then
          GetFreeArea = 157
        else if(Res .eq. 'PHE') then
          GetFreeArea = 189
        else if(Res .eq. 'TRP') then
          GetFreeArea = 234
        end if
	END  
************************************************************************
      Subroutine Normalize(Angle)
      
      Implicit None
      
      real  Angle
      
      if (angle.ne.500.0) then
        if(Angle .gt. 180) Angle = Angle - 360.*float( int( (angle+180)/360 ) )
        if(Angle .lt. -180) Angle = Angle - 360.*float( int( (angle-180)/360 ) )
      end if
      
      END
***********************************************************************

      real function  Normangle(Angle)

      Implicit None
      
      real  Angle
      
      if (angle.ne.500.0) then
        if(Angle .gt. 180) Angle = Angle - 360.*float( int( (angle+180)/360 ) )
        if(Angle .lt. -180) Angle = Angle - 360.*float( int( (angle-180)/360 ) )
      end if
      normangle=angle
      END
C     
**********************************************************************
      real function dihdiff(a,b)
      Implicit None
      real a,b
C      integer i,j
      dihdiff=abs(a-b)
      dihdiff=min(dihdiff,abs(dihdiff-360))
      end
C
C     these functions check if a residue (or a stretch of residues
C     are in a given conformation. Assumed normalised energu functions)
C

      logical function checkhelix(phi,psi)
      real phi,psi,temp1,temp2
      temp1=phi+64
      temp2=psi+57
      checkhelix =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15) checkhelix =.true.
      end

      logical function checksheet(phi,psi)
      real phi,psi,temp1,temp2
      temp1=phi+120
      temp2=psi-120
      checksheet =.false.
      if ( abs(temp1).lt.30 .and. abs(temp2).lt.30) checksheet =.true.
      end

      logical function checkturn1(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1+60
      temp2=psi1+30
      temp3=phi2+90
      temp4=psi2
      temp5=phi3-90
      temp6=psi3
      checkturn1 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn1 =.true.
      end

      logical function checkturn2(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1+60
      temp2=psi1+30
      temp3=phi2+90
      temp4=psi2
      temp5=phi3+90
      temp6=psi3-120
      checkturn2 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn2 =.true.
      end

      logical function checkturn3(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1+90
      temp2=psi1-120
      temp3=phi2+60
      temp4=psi2+30
      temp5=phi3+90
      temp6=psi3
      checkturn3 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn3 =.true.
      end

      logical function checkturn4(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1-60
      temp2=psi1-30
      temp3=phi2-90
      temp4=psi2
      temp5=phi3+90
      temp6=psi3-120
      checkturn4 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn4 =.true.
      end

      logical function checkturn5(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1+60
      temp2=psi1+120
      temp3=phi2-80
      temp4=psi2
      temp5=phi3+90
      temp6=psi3-120
      checkturn5 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn5 =.true.
      end

      logical function checkturn6(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1+90
      temp2=psi1-120
      temp3=phi2+60
      temp4=psi2-120
      temp5=phi3-80
      temp6=psi3
      checkturn6 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn6 =.true.
      end

      logical function checkturn7(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1-60
      temp2=psi1+120
      temp3=phi2+80
      temp4=psi2
      temp5=phi3+90
      temp6=psi3-120
      checkturn7 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn7 =.true.
      end

      logical function checkturn8(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1+80
      temp2=psi1-80
      temp3=phi2-80
      temp4=psi2+80
      temp5=phi3+90
      temp6=psi3-120
      checkturn8 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15
     $     ) checkturn8 =.true.
      end

      logical function checkturn9(phi1,psi1,phi2,psi2,phi3,psi3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      temp1=phi1+100
      temp2=psi1+150
      temp3=phi2-110
      temp4=psi2+160
      temp5=phi3+90
      temp6=psi3+140
      checkturn9 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15 
     $     ) checkturn9 =.true.
      end

      logical function checkturn10(phi1,psi1,omega1,phi2,psi2,omega2,phi3,psi3,omega3)
      real phi1,psi1,temp1,temp2,phi2,psi2,temp3,temp4,phi3,psi3,temp5,temp6
      real omega1,omega2,omega3,temp7,temp8,temp9
      temp1=phi1+120
      temp2=psi1-120
      temp3=omega1
      temp4=phi2+70
      temp5=psi2-150
      temp6=omega2
      temp7=phi3+90
      temp8=psi3-120
      temp9=omega3
      checkturn10 =.false.
      if ( abs(temp1).lt.15 .and. abs(temp2).lt.15 .and.
     $     abs(temp3).lt.15 .and. abs(temp4).lt.15 .and.
     $     abs(temp5).lt.15 .and. abs(temp6).lt.15 .and.
     $     abs(temp7).lt.15 .and. abs(temp8).lt.15 .and.
     $     abs(temp9).lt.15  
     $     )checkturn10 =.true.
      end

C
C     This file include math functions that are specific for grail.
C     Numerical recipies studd should be in numrec.f

*********************************************************
C     C GAUSS--GENERATE A GAUSSIAN RANDOM VARIABLE
      real Function Gauss(Mean,Sigma,Random)
      Implicit None
      integer  Seed
      real     Mean,Sigma,Jump
      real     Random
      real randomn
      external randomn
      Seed = int(Random*100000+1231)
      random=max((1.e-7)*(2*3.14159*Sigma),random+1.e-30)
C     Y= ( 1/(2*3.14159*Sigma) )*Random
C     C	if(Y .eq. 0) Y=1E-7
C     Jump = SQRT(-2.0*(Sigma**2)*LOG(Y*2*3.14159*Sigma) )
      jump = sqrt(-2.0*(Sigma**2)*LOG(random))
      Random = RANdomn(Seed)
      Random = RANdomn(Seed)
      if (Random .lt. 0.5) Jump = -1.0*Jump
      Gauss = Mean + Jump
      END
***********************************************************************
      integer Function IMax(a,b)
      Implicit None
      integer a,b
      if (a .gt. b) then
        IMax = a
      else
        IMax = b
      end if
      END

***********************************************************************

      integer Function IMin(a,b)
      Implicit None
      integer a,b
      if (a .lt. b) then
        IMin = a
      else
        IMin = b
      end if
      END

***   StrToreal  ***************************************************************
*
*     This converts an real value in a string to an integer.  True
*     is returned if the real value was recovered successfully.
*
******************************************************************************

      Function StrToReal( String, Value )

      Implicit None
      Character String(*)
      real Value
      real NValue, Divide
      Logical StrToReal
      Character Nstr(132)
      integer Pos
      Logical ChIsDigit
      Logical Neg, HaveDecimal
      integer DigCount,strtoken1,strtoken
      external strtoken
      Character Eol

      Eol=Char(0)
      NValue = 0.0
      Pos=1
      strtoken1= StrToken(Nstr,String,Pos)
C     trim space off the start, end.
      Pos=1
      Neg = .false.
      If( Nstr(1).eq.'+' ) Then
        Pos=Pos+1
      Else
        If( Nstr(1).eq.'-' ) Then
          Pos=Pos+1
          Neg = .true.
        EndIf
      End If
      Divide = 1.0
      HaveDecimal=.false.
      DigCount=0
      Do While (Nstr(Pos).ne.Eol)
        If (ChIsDigit(Nstr(Pos))) Then
          DigCount=DigCount+1
          If (HaveDecimal) Divide=10.0*Divide
          NValue=10*NValue+IChar(Nstr(Pos))-IChar('0')
          Pos=Pos+1
        Else
          If (Nstr(Pos).eq.'.') Then
            HaveDecimal=.true.
            Pos=Pos+1
          Else
            Goto 100
          End If
        End If
      End Do
      If(DigCount.eq.0) goto 100
      NValue=Nvalue/Divide
      If (Neg) NValue=-NValue
      Value=NValue
      StrToReal=.true.
      Return

 100  StrToReal=.false.

      Return
      End

C     
***   StrToken  ****************************************************************
*
*     This extracts a token, a number of characters delimited by white
*     space (spaces or tabs).  Pos is used to indicate where the scanning
*     should start.  On return, Pos points to the start of the following
*     whitespace or the end of the string, so it can be called repeatedly
*     to strip off words or numbers separted by spaces.
*     The return value of the function is the number of characters in
*     the function.
*
*******************************************************************************
      integer Function StrToken(Token, String, Pos)

      Implicit None

      Character Token(*), String(*)
      integer Pos

      integer OutPos
      Character Eol, Space, Tab

      eol=char(0)
      space=char(32)
      tab=char(9)

      OutPos = 1
      If ( Pos.eq.0 ) Pos = 1
      Do While ( String(Pos).eq.Tab .or. String(Pos).eq.Space )
        Pos = Pos + 1
      End Do
      Do While( String(Pos).ne.Eol    .and.
     $     String(Pos).ne.Tab    .and.
     $     String(Pos).ne.Space         )
        Token(OutPos) = String(Pos)
        OutPos = OutPos + 1
        Pos = Pos + 1
      End Do
      Token(OutPos) = Eol

      StrToken = OutPos - 1

      Return
      End
C     of StrToken

C     
***   ChIsDigit  *******************************************************
*
*     This logical function returns true if the character given is
*     a digit ( 0..9 ).
*
******************************************************************************

      Function ChIsDigit( OneChar )
      Implicit None

      Character OneChar
      Logical ChIsDigit

      ChIsDigit=(OneChar.ge.'0').and.(OneChar.le.'9')

      return
      End
C     of ChIsDigit


******************************************************************
      Subroutine GetAsps(TotalAtoms,AtNam,ResNam,Asp)

      Implicit None

      Character AtNam(*)*3,ResNam(*)*3
      integer   TotalAtoms,b
      real  Asp(*)




      Do b=1,TotalAtoms

        if( AtNam(b)(1:1) .eq. 'C') Asp(b) = 18.
        if(AtNam(b)(1:1) .eq. 'S') Asp(b) = -5.
        if(AtNam(b) .eq. 'O  ') Asp(b) = -9.

        if(AtNam(b)(1:1) .eq. 'O' .and. AtNam(b) .ne. 'O  ') then

          if (INDEX(ResNam(b),'ASP') .ne. 0 .or.
     &         INDEX(ResNam(b),'GLU') .ne. 0) then
            Asp(b) = -23.
          else
            Asp(b) = -9.

          end if

        end if

        if(AtNam(b) .eq. 'N  ') Asp(b) = -9.

        if(AtNam(b)(1:1) .eq. 'N' .and. AtNam(b) .ne. 'N  ') then
          if( (INDEX(ResNam(b),'ARG') .ne. 0 .and. AtNam(b) .ne. 'NE ') 
     &         .or.    INDEX(ResNam(b),'HIS') .ne. 0) then
            Asp(b) = -23.
          else
            Asp(b) = -9.
          end if
        end if
C     write(*,*)'TEST-Name',ResNam(b),AtNam(b),Asp(b)
      End Do

      END

***********************************************************************
      subroutine init_int(vect,vectlen,num)
      implicit none
      integer vect(*), i,vectlen,num

      do i = 1,vectlen
         vect(i) = num
      enddo
      end
***********************************************************************
      subroutine init_real(vect,vectlen,num)
      implicit none
      real vect(*),num
      integer i,vectlen

      do i = 1,vectlen
         vect(i) = num
      enddo
      end
***********************************************************************
      subroutine numtonam(num,nam)
      implicit none
      integer num
      character*3 nam
      character*1 c
      call numtochar(num,c)
      call onetothree(c,nam)
      end
************************************************************************
      integer Function NamToNum(ResNam)

      Implicit None
      Character ResNam*3,c*1
      integer chartonum
      external chartonum
      call threetoone(c,resnam)
      namtonum=chartonum(c)
C      write(*,*) "TEST: ",resnam,c,namtonum

      return
      end
************************************************************************
C
C Old functions
C
***************************************************************

      integer Function Namtonumold(ResNam)

      Implicit None

      Character ResNam*3

      Namtonumold = 21

      if (ResNam .eq. 'ALA') then
        Namtonumold = 1
        else if (ResNam .eq. 'ARG') then
          Namtonumold = 2
        else if (ResNam .eq. 'ASN') then
          Namtonumold = 3
        else if (ResNam .eq. 'ASP') then
          Namtonumold = 4
        else if (ResNam .eq. 'CYS') then
          Namtonumold = 5
        else if (ResNam .eq. 'GLN') then
          Namtonumold = 6
        else if (ResNam .eq. 'GLU') then
          Namtonumold = 7
        else if (ResNam .eq. 'GLY') then
          Namtonumold = 8
        else if (ResNam .eq. 'HIS') then
          Namtonumold = 9
        else if (ResNam .eq. 'ILE') then
          Namtonumold = 10
        else if (ResNam .eq. 'LEU') then
          Namtonumold = 11
        else if (ResNam .eq. 'LYS') then
          Namtonumold = 12
        else if (ResNam .eq. 'MSE') then
          Namtonumold = 13
        else if (ResNam .eq. 'MET') then
          Namtonumold = 13
        else if (ResNam .eq. 'PHE') then
          Namtonumold = 14
        else if (ResNam .eq. 'PRO') then
          Namtonumold = 15
        else if (ResNam .eq. 'SER') then
          Namtonumold = 16
        else if (ResNam .eq. 'THR') then
          Namtonumold = 17
        else if (ResNam .eq. 'TRP') then
          Namtonumold = 18
        else if (ResNam .eq. 'TYR') then
          Namtonumold = 19
        else if (ResNam .eq. 'VAL') then
          Namtonumold = 20
        end if

	END


      integer Function Chartonumold(ResNam)

      Implicit None

      Character ResNam*1

      Chartonumold = 21

      if (ResNam .eq. 'A') then
        Chartonumold = 1
        else if (ResNam .eq. 'R') then
          Chartonumold = 2
        else if (ResNam .eq. 'N') then
          Chartonumold = 3
        else if (ResNam .eq. 'D') then
          Chartonumold = 4
        else if (ResNam .eq. 'C') then
          Chartonumold = 5
        else if (ResNam .eq. 'Q') then
          Chartonumold = 6
        else if (ResNam .eq. 'E') then
          Chartonumold = 7
        else if (ResNam .eq. 'G') then
          Chartonumold = 8
        else if (ResNam .eq. 'H') then
          Chartonumold = 9
        else if (ResNam .eq. 'I') then
          Chartonumold = 10
        else if (ResNam .eq. 'L') then
          Chartonumold = 11
        else if (ResNam .eq. 'K') then
          Chartonumold = 12
        else if (ResNam .eq. 'M') then
          Chartonumold = 13
        else if (ResNam .eq. 'F') then
          Chartonumold = 14
        else if (ResNam .eq. 'P') then
          Chartonumold = 15
        else if (ResNam .eq. 'S') then
          Chartonumold = 16
        else if (ResNam .eq. 'T') then
          Chartonumold = 17
        else if (ResNam .eq. 'W') then
          Chartonumold = 18
        else if (ResNam .eq. 'Y') then
          Chartonumold = 19
        else if (ResNam .eq. 'V') then
          Chartonumold = 20
        end if

	END

***************************************************************

	Subroutine Numtocharold(Num,ResNam)

	Implicit None

	Character ResNam*1
	integer   Num

	if (Num .eq. 1) then
          ResNam = 'A' 
        else if (Num .eq. 2) then
          ResNam = 'R' 
        else if (Num .eq. 3) then
          ResNam = 'N' 
        else if (Num .eq. 4) then
          ResNam = 'D' 
        else if (Num .eq. 5) then
          ResNam = 'C' 
        else if (Num .eq. 6) then
          ResNam = 'Q' 
        else if (Num .eq. 7) then
          ResNam = 'E' 
        else if (Num .eq. 8) then
          ResNam = 'G' 
        else if (Num .eq. 9) then
          ResNam = 'H' 
        else if (Num .eq. 10) then
          ResNam = 'I'
        else if (Num .eq. 11) then
          ResNam = 'L'
        else if (Num .eq. 12) then
          ResNam = 'K'
        else if (Num .eq. 13) then
          ResNam = 'M'
        else if (Num .eq. 14) then
          ResNam = 'F'
        else if (Num .eq. 15) then
          ResNam = 'P'
        else if (Num .eq. 16) then
          ResNam = 'S'
        else if (Num .eq. 17) then
          ResNam = 'T'
        else if (Num .eq. 18) then
          ResNam = 'W'
        else if (Num .eq. 19) then
          ResNam = 'Y'
        else if (Num .eq. 20) then
          ResNam = 'V'
        end if
	END

************************************************************************
      subroutine init_freq(freq,qfreq,table,lambda,maxaa,numaa)
      Implicit None
      integer numaa,maxaa,i,j
      real freq(maxaa),lambda,qfreq(maxaa,maxaa),table(maxaa,maxaa)

C
C for creating profiles
C
      freq(1)=  0.0756
      freq(2)=  0.0168
      freq(3)=  0.0529
      freq(4)=  0.0634
      freq(5)=  0.0408
      freq(6)=  0.0683
      freq(7)=  0.0223
      freq(8)=  0.0577
      freq(9)=  0.0595
      freq(10)= 0.0939
      freq(11)= 0.0235
      freq(12)= 0.0450
      freq(13)= 0.0491
      freq(14)= 0.0401
      freq(15)= 0.0515
      freq(16)= 0.0716
      freq(17)= 0.0570
      freq(18)= 0.0654
      freq(19)= 0.0123
      freq(20)= 0.0318

      do i=1,numaa
        do j=1,numaa
          qfreq(i,j) = freq(i)*freq(j)*exp(lambda * table(i,j))
        end do
      end do
      return
      end

C************************************************************************
C To calculate E and Z-score
C************************************************************************

      real function e_to_zs(e,num)
      real e, z
      integer num
      
      z = (log(e/num)+0.577216)/(-1.28255)
      e_to_zs= z 
C*10.0+50.0

      return
      end
C************************************************************************
C* computes 1.0 - E value for a given z value,
C		    assuming extreme value distribution 
      real function zs_to_e(zs,num) 
      integer num
      real e, z,zs

      if (num .lt. 5) then
        zs_to_e=9999.0
        return 
      end if
C      z = (zs - 50.0)/10.0
      z = zs
      e =  exp(-1.2825498 * z - .577216)
      if (e .gt. .01) then
        zs_to_e=num * (1-exp(-e))
      else
        zs_to_e=num *  e
      end if

      return 

      end
