C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http:
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      Program Palign
C
C A hack of panal so I can do it all on one line,oldline !
C
      implicit none
C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http:
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      integer maxseqlen,maxaa,maxproflen,penopen,penext,maxtype,maxnum,maxktup
      integer maxhash,maxtop
      parameter (maxtop=50)
      parameter (maxproflen=6500)
      parameter (maxseqlen=6500)
      parameter (maxaa=60)
      parameter (maxtype=100)
      parameter (maxnum=2000)
      parameter (penopen=maxaa+1)
      parameter (penext=maxaa+2)
      parameter (maxktup=maxaa*maxaa)
      parameter (maxhash=201)


      real profile(maxproflen,maxaa+2),gapopen,gapext
      real dynscore,score,x(maxproflen),y(maxproflen),z(maxproflen)
      real x2(maxproflen),y2(maxproflen),z2(maxproflen)
      real averscore,sumscore,sum2score,zscore,tempscore,sd
      real gapmax,gapdist
      character*800 line,proffile,seqfile,filename1,filename2
      character*800 alfile,pdbfile,alfile2,pdbfile2,oldline
      character*10 seqtype,proftype
      character*4 resnum(maxseqlen),profresnum(maxseqlen),nam
C      real score2
      character*1 char,char1,char2
      integer cfiles,linelen,i,j,k,proflen,alitype,resnumflag,type,num
      integer profseq(maxseqlen),profss(maxseqlen),numss,numaa
      integer seq(maxseqlen),seqss(maxseqlen),seqlen,traceback(maxseqlen),traceback2(maxseqlen)
      integer seqtoseq,seqtoss,allen,numal,gaps,numgaps,ssgaps
      integer seq2(maxseqlen)
      logical seqfound,proffound,printali,gaplinear
      real R(20,20), E(20,20), Etot, Eaa, Eij, Eia,fraction
      real table(maxaa,maxaa),loopgap(3),ssmatch(3,3)
      external dynscore,seqtoseq,seqtoss

      parameter (numss=3)
      parameter (numaa=20)

C      call foo(bar)
C default values
      call init
      call init_int(seq,maxseqlen,0)
      call init_int(seqss,maxseqlen,0)
      call init_int(profss,maxseqlen,0)
      call init_int(profseq,maxseqlen,0)
      call init_int(traceback,maxseqlen,0)
C      call init_int(resnum,maxseqlen,0)
C      call init_int(profresnum,maxseqlen,0)

      call init_real(profile,maxproflen*(maxaa+2),0.)
      call init_real(x,maxseqlen,0.)
      call init_real(y,maxseqlen,0.)
      call init_real(z,maxseqlen,0.)
      call init_real(table,maxaa*maxaa,0.)
      call init_real(E,400,0.)
      call init_real(R,400,0.)

      profile(1,1)=-9999
      filename1='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contDistHL'
      filename2='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contEneHL'
      call inire(filename1,filename2,R,E)


      line='/afs/pdc.kth.se/home/a/arnee/source/palign/pam250.mat'
      call readfastaseqtable(line,table,maxaa)
      type=1
      gaplinear=.false.
      seqfound=.false.
      proffound=.false.
      x(1)=-9999
      x2(1)=-9999
      alitype=0
      gapopen=-10
      gapext=-4
      seqss(1)=-1
      profss(1)=-1
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
C
C     This is the only difference from pmembr
C
      printali=.false.
      ssmatch(1,1)=1
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
      ssmatch(3,3)=1
      alfile='xxx'
      pdbfile='xxx'
      alfile2='xxx'
      pdbfile2='xxx'
      do i=1,maxseqlen
        write(resnum(i),*),i
        write(profresnum(i),*),i
      end do

C Start the program
      cfiles=1 
      call getarg(cfiles,line)
      linelen=len(line)
      call trima(line,linelen)
      i=1

      do while  (line .ne. '' ) 
        if (line(1:2).eq.'-h') then
          write(*,*) "Usage:"
          write(*,*) "-table file   : Use a sequence table file in blast format"
          write(*,*) "-fasta file   : Use a sequence table file in fasta format (default pam250)"
          write(*,*) "-local        : Use local alignments (default)"
          write(*,*) "-global       : Use global alignment"
          write(*,*) "-inire  file1 file2    : Energy files for threading energy"
          write(*,*) "-loopgap A B C : cost for gaps in different SS regions (default 1,1,1)"
          write(*,*) "-ssmatch a b c d e f g h i : Scores for secondary structure regions"
          write(*,*) "                            default 100 010 001 for secstr and"
          write(*,*) "                            000 010 000 for TM and"
          write(*,*) "-go X          : Gap opening cost"
          write(*,*) "-ge X          : Gap  extension cost"
          write(*,*) "-gapmax X Y    : Increased/decreased gap costs"
          write(*,*) "-ali        : alignment is printed to stdout"
          write(*,*) "-alfile  foo      : file to print alignment"
          write(*,*) "-alifile foo      : file to print alignment"
          write(*,*) "-caspfile foo     : file to print alignment"
          write(*,*) "-pdbfile  foo     : file to print structure"
          write(*,*) "-seq file1 file2 ... - : files to read for sequence info"
          write(*,*) "-prof file1 file2 ... - : files to read for profiel info"
          write(*,*) "file1 file2     : sequence and profile file"
          write(*,*) "Note somehow atleast one sequence or profile file has to be named"
          stop
        else if (line(1:6).eq.'-table') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-fasta') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readfastaseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-local') then
          alitype=0
        else if (line(1:7).eq.'-global') then
          alitype=10
        else if (line(1:7).eq.'-inire') then
          alitype=10
C          filename1 = 'contDistHL'
C          filename2 = 'contEneHL'
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
C          read (line,*) filename1
          filename1=line
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
C          read (line,*) filename2
          filename2=line
          call inire(filename1,filename2,R,E)
C          do i=1,20
C            write(*,'(a,i4,20f8.3)')'R ',i,(R(i,j),j=1,20)
C            write(*,'(a,i4,20f8.3)')'E ',i,(E(i,j),j=1,20)
C          end do
        else if (line(1:8).eq.'-loopgap') then
          do j=1,3
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) loopgap(j)
          end do
        else if (line(1:8).eq.'-ssmatch') then
          do j=1,3
            do k=1,3
              cfiles=cfiles+1
              call getarg(cfiles,line)
              linelen=len(line)
              call trima(line,linelen)
              read (line,*) ssmatch(j,k)
            end do
          end do
        else if (line(1:3).eq.'-go') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapopen
        else if (line(1:3).eq.'-ge') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapext
        else if (line(1:7).eq.'-gapmax') then
          gaplinear=.true.
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapmax
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapdist
        else if (line(1:7).eq.'-alfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile
        else if (line(1:9).eq.'-caspfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile
        else if (line(1:8).eq.'-alifile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile
        else if (line(1:4).eq.'-ali') then
          printali=.true.
        else if (line(1:8).eq.'-pdbfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') pdbfile
        else if (line(1:9).eq.'-al2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile2
        else if (line(1:9).eq.'-noresnum') then
          resnumflag=1
        else if (line(1:10).eq.'-casp2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile2
        else if (line(1:9).eq.'-ali2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile2
        else if (line(1:9).eq.'-pdb2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') pdbfile2
        else if (line(1:4).eq.'-seq') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          do while (line(1:1).ne.'-')
            seqfile=line
C            call trima(seqfile,linelen)
            call readanyseq(line,seq,seqlen,seqfound,profile,seqss,x,y,z,seqtype,resnum)
            if (seqlen .gt. maxseqlen) call stopprocess('ERROR> Sequence too long !')
            write(*,*)'READING sequence type: ',seqtype,' len: ',seqlen
            cfiles=cfiles+1
            call getarg(cfiles,line)
          end do
        else if (line(1:5).eq.'-prof') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          do while (line(1:1).ne.'-')
            proffile=line
C            call trima(proffile,linelen)
            call readanyseq(line,profseq,proflen,proffound,profile,profss,x2,y2,z2,proftype,profresnum)
            if (proflen .gt. maxproflen) call stopprocess('ERROR> profile too long !')
            write(*,*)'READING profile (2) type: ',proftype,' len: ',proflen
            cfiles=cfiles+1
            call getarg(cfiles,line)
          end do
          if (profile(1,1) .eq. -9999) then
            write(*,*)'Making a profile (1):'
            call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
          else
            do i=1,proflen
              profile(i,penopen)=gapopen+gapext
              profile(i,penext)=gapext
            end do
          end if
        else
          if (type.eq.1 .and. .not. seqfound) then
            seqfile=line
            call readanyseq(line,seq,seqlen,seqfound,profile,seqss,x,y,z,seqtype,resnum)
            if (seqlen .gt. maxseqlen) call stopprocess('ERROR> Sequence too long !')
            write(*,*)'READING sequence type (3): ',seqtype,' len: ',seqlen
          elseif (.not. proffound) then
            proffile=line
            call readanyseq(line,profseq,proflen,proffound,profile,profss,x2,y2,z2,proftype,profresnum)
            if (proflen .gt. maxproflen) call stopprocess('ERROR> Profile too long !')
            write(*,*)'READING profile type (4): ',proftype,' len: ',proflen
            if (profile(1,1) .eq. -9999) then
              write(*,*)'Making a profile (4):'
              call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
            else
              if (gapopen .gt. 0) gapopen=-gapopen
              if (gapext .gt. 0) gapext=-gapext
              do i=1,proflen
                profile(i,penopen)=gapopen+gapext
                profile(i,penext)=gapext
              end do
            end if
          end if
          type=type+1
        end if
        cfiles=cfiles+1
        call getarg(cfiles,line)
        linelen=len(line)
        call trima(line,linelen)
      end do


      if (profss(1).ne.-1 .and. seqss(1).ne.-1) then
        write(*,*)'Using secondary structure predictions (4):'
        call addsstoseq(seq,seqss,seqlen,numaa)
        write(*,*)'Making a secondary structureprofile (4):'
        call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
C     call writeprofile(profile,profseq,proflen,60)
      end if

      
      if (gaplinear) then
        if (gapmax .gt. 0) then
          do i=1,proflen
            profile(i,penopen)=profile(i,penopen)*(1+gapmax/(i*gapdist))
            profile(i,penext)=profile(i,penext)*(1+gapmax/(i*gapdist))
          end do
        else
          do i=1,proflen
            profile(i,penopen)=profile(i,penopen)*(1-gapmax/((proflen-i)*gapdist))
            profile(i,penext)=profile(i,penext)*(1-gapmax/((proflen-i)*gapdist))
          end do
        end if
      end if


C      call foo(bar)
C      do i=1,seqlen
C        call numtonam(seqtoseq(seq(i),numaa),nam)
C        write(*,*)'test1',i,seq(i),nam,seqss(i),seqtoss(seq(i),numaa)
C        call numtonam(seqtoseq(profseq(i),numaa),nam)
C        write(*,*)'test2',i,profseq(i),nam,profss(i),seqtoss(profseq(i),numaa)
C      end do
C
C
C      write(*,*) 'SCORE> ',dynscore(profile,proflen,seq,seqlen,alitype)
C      do i=1,proflen
C        call numtochar(profseq(i),char)
C        write(*,'(A3,1x,20(f3.0,1x)),2x,20(f3.0,1x)),2x,20(f3.0,1x)),2x,2(f3.0,1x))')
C     &       char,(profile(i,j),j=1,numaa),profile(i,maxaa+1),profile(i,maxaa+2)
C      end do
      call dyntrace(profile,proflen,seq,seqlen,alitype,score,traceback)

C Let us also calculate a Z-score
      call copyseq(seq,seq2,seqlen)
      num=100
      do i=1,num
        call randomizeseq(seq2,seqlen)
        tempscore=dynscore(profile,proflen,seq2,seqlen,alitype)
        sumscore=sumscore+tempscore
        sum2score=sum2score+tempscore**2
      end do
      averscore=sumscore/num
      sd=sqrt((sum2score-sumscore*sumscore/num)/(num-1))
      zscore=(score-averscore)/sd


C      write(*,'(A,e11.3)') 'SCORE> ',dynscore(profile,proflen,seq,seqlen,alitype)
C      write(*,*) 'FILES> ',seqfile,profile
      write(*,'(A,e11.3,1x,e11.3)') 'SCORE> ',score,zscore

      if (resnumflag .eq. 1) then
        do i=1,maxseqlen
          write(resnum(i),*),i
          write(profresnum(i),*),i
        end do
      end if
      
C      write(*,*)'test:',pdbfile,':',alfile,':',x(1)
      if (alfile.ne.'xxx') call writealfile(alfile,seq,seqlen,profseq,proffile,traceback,resnum,profresnum)
      if (pdbfile.ne.'xxx' .and. x2(1).ne.-9999.0) 
     &     call writepdbfile(pdbfile,seq,seqlen,traceback,x2,y2,z2,resnum)

      call traceinvers(maxseqlen,traceback,traceback2)
      if (alfile2.ne.'xxx') call writealfile(alfile2,profseq,proflen,seq,seqfile,traceback2,profresnum,resnum)
      
      if (pdbfile2.ne.'xxx' .and. x(1).ne.-9999.0) 
     &     call writepdbfile(pdbfile2,profseq,proflen,traceback2,x,y,z,profresnum)

      if (printali) then
        do i=1,seqlen
          call numtochar(seqtoseq(seq(i),numaa),char1)
          if (seq(i) .eq. 0) char1='X'
          if (traceback(i).gt.0) then
            call numtochar(seqtoseq(profseq(traceback(i)),numaa),char2)
            if (profseq(traceback(i)) .eq. 0) char2='X'
          else
            char2='.'
          end if
C          if (trace(i).gt.0) then
C     write(*,*)'test',i,seq(i),trace(i),resnum(trace(i)),profseq(trace(i))
            write(*,('(3x,a,4x,a4,6x,a,1x,i7)'))char1,resnum(i),char2,traceback(i)
C     else
C     write(*,*)'trace> ',i,' ',char1,'   -   '
C          end if
        end do
      end if
C           
C From ylva  
C

      if (x2(1).ne.-9999) then
        call vdwEnergy(X2,Y2,Z2,seqlen,proflen,traceback,seq,E,R,Etot,Eaa,Eij,Eia)
        call CountSsMatch(seqss,profss,seqlen,traceback,
     &       fraction,gaps,numgaps,ssgaps,allen,numal)
        write(*,'(a,4i8,6e11.3,3i8)') 'SCORES> ',allen,numal,seqlen,proflen,
     &       score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps
      end if

      if (x(1).ne.-9999) then
        call vdwEnergy(X,Y,Z,proflen,seqlen,traceback2,profseq,E,R,Etot,Eaa,Eij,Eia)
        call CountSsMatch(profss,seqss,proflen,traceback2,
     &       fraction,gaps,numgaps,ssgaps,allen,numal)
        write(*,'(a,4i8,6e11.3,3i8)') 'SCORES2> ',allen,numal,seqlen,proflen,
     &       score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps
      end if
C      do i=1,seqlen
C        call numtochar(seqtoseq(seq(i),numaa),char1)
C        if (traceback(i).gt.0) then
C          call numtochar(seqtoseq(profseq(traceback(i)),numaa),char2)
C          score=profile(traceback(i),seq(i))
C          score2=table(seq(i),profseq(traceback(i)))
C        else
C          char2='.'
C          score=profile(i,penopen)+profile(i,penext)
C          score2=0
C        end if
C        if (traceback(i).gt.0) then
C          write(*,*)'trace> ',i,' ',char1,' ',seq(i),seqtoss(seq(i),numaa),' ',
C     &         traceback(i),' ',char2,' ',profseq(traceback(i)),
C     &         seqtoss(profseq(traceback(i)),numaa),
C     &         ' ',score,score2
C          else
C            write(*,*)'trace> ',i,' ',char1,'   -   '
C        end if
C      end do


      stop
      end

***********************************************************************
      subroutine stopprocess(string)
      character*(*) string
      write(*,*) string
      write(*,*)'STOP> Job terminates'
      stop
      end

