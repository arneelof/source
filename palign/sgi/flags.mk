FFLAG2=    -O3 -LNO 

#
#FFLAG2=   -g -ffixed-line-length-none  -fomit-frame-pointer -Wimplicit  -DGNU 


CPPFLAG= -I ./ -DIFC 
#	 -DBIGREAL  -DSMALLINTEGER  -DGNU \
#	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS


FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 

LINK=f77
F77=f77
CPP=  /usr/sbin/cppstdin 
#CC=	/modules/gnu/gcc/linux/bin/gcc 
CC=cc
CCFLAG= -O $(CPPFLAG) -Dgnu 
LFLAG= $(FFLAG2)
CD=	cd

EXTENSION=src
OBJDIR= .
BINDIR= .

# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make


#  xlf should recognize CPP and long lines I guess in .F files
#
#  -WF,<cpp option>,<cpp option>,...
#
#

#.src.o:	

.src.cpp:	
	$(CPP)  $(CPPFLAG)  < $*.src > $*.cpp
.cpp.f:
	awk -f split.awk $*.cpp > $*.f
.f.o:
	$(F77) -c $(FFLAG) -I./ $*.f
#	rm -f $*.cpp
#	rm -f $*.f


.c.o:	
	$(CC) -c $(CCFLAG)  $*.c

# Special dependencies for .src files 


.SUFFIXES: .cpp .src .f .h .o .fcm .FCM

