EXTRA= malloc.o


LFLAG= 
#LINK=	f77 $(FFLAG) $(LFLAG)
LINK= f77 $(LFLAG)
F77=	f77 
CC=	cc 
CCFLAG= -g -O
CD=	cd
ADDR= -DSMALLADDR
TYPE= -DSGI

TARGET= -DTARGETSIZE $(FFLAG)
FFLAG= -r8 -i4 -DBIGREAL  -DSMALLINTEGER -g -O  \
	-extend_source -DHAVE_MALLOC $(TYPE) $(ADDR)  -DTRIMA_PROBLEM \
	 -DHAVE_MALLOC  -DHAVE_POINTERS
CCFLAG=	-g -O $(TYPE) $(ADDR)

EXTENSION= f

OBJDIR= .
BINDIR= .


MAKE= make
.f.o:
	$(F77) -c $(FFLAG)  $(SIZE) -I./ $*.f

.c.o:	
	$(CC) -c $(CCFLAG) -I./  $*.c



.SUFFIXES: .f .h .o .fcm .FCM
