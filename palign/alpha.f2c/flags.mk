FFLAG2=      -O9 -f -r8 -w
# FFLAG2= -g

CPPFLAG= -I./  -DBIGREAL  -DSMALLINTEGER   -DALPHA -DGNU \
	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DF2C -DSTLEN_PROBLEMS

#CCFLAG= -g -O9 -ffast-math     -funroll-loops   $(CPPFLAG) \
#	-Dgnu  

CCFLAG=  -O4 -g3 $(CPPFLAG)
GCCFLAG= -O9  -ffast-math     -funroll-loops -w  $(CPPFLAG)
FFLAG=  -u  $(FFLAG2) 
LFLAG=  
EXTRA= cstuff.o 
#LINK=	/modules/gnu/gcc/alpha/bin/fort77
#F77=	/modules/gnu/gcc/alpha/bin/fort77

#LINK=	/usr/local/bin/fort77
F77=	/usr/local/bin/fort77 -L/modules/gnu/gcc/alpha/lib/
LINK=	/home/gvh/arne/source/source5/alpha/fort77

CPP=  cpp
#CC=	cc -c $(CCFLAG)
CC=	gcc -c $(GCCFLAG)
F2C=	/modules/gnu/gcc/alpha/bin/f2c

CD=	cd

MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino
EXTENSION=src
OBJDIR= .
BINDIR= .


# EXTRA=cstuff.o

# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make

.SUFFIXES: .h .o .src .f .c

.src.o:	
	$(CPP) -P $(CPPFLAG) $*.src > $*.f
	$(F2C) $(FFLAG) $*.f
	$(CC)  $*.c
#	rm -f $*.f $*.c

.src.f:	
	$(CPP) -P $(CPPFLAG) $*.src > $*.f

.f.o:	$*.f $*.h $*.c
	$(F2C) $(FFLAG) $*.f
	$(CC)  $*.c
#	rm -f $*.f $*.c

c.o:	
	$(CC)   $*.c
