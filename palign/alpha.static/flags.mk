FFLAG2= -real_size 64 -integer_size 32 -DBIGREAL  -DSMALLINTEGER \
	-g -O -fast  -extend_source -cpp   -DBIGADDR  -DBIG  \
	 -DHAVE_MALLOC  -DHAVE_POINTERS -DALPHA -DDEC

FFLAG=  -u  $(FFLAG2)  -check_bounds 
LFLAG=  
LINK=	f77 $(FFLAG) -non_shared
F77=	f77 
CC=	cc 
CCFLAG=	-g3 -O
CD=	cd
TAR=	gtar
LIBDIR= .

BINDIR= .

EXTENSION= f

MAKE= make
.f.o:
	$(F77) -c  $(FFLAG) -I./  $*.f

.c.o:	
	$(CC) -c  $(CCFLAG) -I./  $*.c



