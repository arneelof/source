C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      Program Pmemrdheuristic
C
C A heuristic sequence/profile search program. Made for membrane profiles.
c Inverted, i.e. sequence search for 
C
      implicit none
#include <dynprog.h>
      integer maxseqs
      parameter (maxseqs=100000)

      real profile(maxproflen,maxaa+2),gapopen,gapext,score
      real dynheuristic,dynscore,x(maxproflen),y(maxproflen),z(maxproflen)
      character*320 proffile,seqfile,filename1,filename2
      character*800 line,oldline
      character*10 seqtype,proftype
      character*1 char
      integer hash(maxktup,maxhash),position(maxktup,maxhash),ktup
      integer resnum(maxseqlen),profresnum(maxseqlen)
      integer wdlen,linelen,i,j,k,proflen,alitype,type
      integer profseq(maxseqlen),profss(maxseqlen),numss,numaa,cfiles
      integer seq(maxseqlen),seqss(maxseqlen),seqlen,traceback(maxseqlen)
      integer seqtoseq,seqtoss,infile,numtop
      integer seqfilelen,proffilelen,numtm
      logical seqfound,proffound,inverse
      real dpcutoff,cutoff,gapcost
C      real cutoff2,score1,score2,gapcost2
      real length,minout,signcutoff
      real table(maxaa,maxaa),loopgap(3),ssmatch(3,3)
      external dynscore,seqtoseq,seqtoss,dynheuristic

      parameter (infile=91)
      parameter (numss=3)
      parameter (numaa=20)

C      call foo(bar)
C default values
      inverse=.false.
      ktup=2
      cutoff=12.
      dpcutoff=8.
      minout=7.
      gapcost=30.
C      numtop=20
      alitype=0
      gapopen=-10
      gapext=-4
      seqss(1)=-1
      profss(1)=-1
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
      ssmatch(1,1)=0
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
      ssmatch(3,3)=0
      call init
      call init_int(position,maxktup*maxhash,0)
      call init_int(hash,maxktup*maxhash,0)
      call init_int(seq,maxseqlen,0)
      call init_int(seqss,maxseqlen,0)
      call init_int(profss,maxseqlen,0)
      call init_int(profseq,maxseqlen,0)
      call init_int(traceback,maxseqlen,0)

      call init_real(profile,maxproflen*(maxaa+2),0.)
      call init_real(x,maxseqlen,0.)
      call init_real(y,maxseqlen,0.)
      call init_real(z,maxseqlen,0.)
      call init_real(table,maxaa*maxaa,0.)

      filename1='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contDistHL'
      filename2='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contEneHL'
C      filename1='/home/gvh/arne/source/palign/parkLevitt/contDistHL'
C      filename2='/home/gvh/arne/source/palign/parkLevitt/contEneHL'

      line='/afs/pdc.kth.se/home/a/arnee/source/palign/pam250.mat'
      call readfastaseqtable(line,table,maxaa)

      cfiles=1 
      call getarg(cfiles,line)
      linelen=len(line)
      call trima(line,linelen)
      type=1

#ifdef IFC
      do while  (line .ne. oldline ) 
        oldline=line
#else
      do while  (line .ne. '' ) 
#endif
        if (line(1:5).eq.'-help') then
          write(*,*) "Usage:"
          write(*,*) "-table file   : Use a sequence table file in blast format"
          write(*,*) "-fasta file   : Use a sequence table file in fasta format (default pam250)"
          write(*,*) "-local        : Use local alignments (default)"
          write(*,*) "-global       : Use global alignment"
          write(*,*) "-inire  file1 file2    : Energy files for threading energy"
          write(*,*) "-loopgap A B C : cost for gaps in differenst SS regions (default 1,1,1)"
          write(*,*) "-ssmatch a b c d e f g h i : Scores for secondary structure regions"
          write(*,*) "                            default 100 010 001 for secstr and"
          write(*,*) "                            000 010 00 for TM and"
          write(*,*) "-go X          : Gap opening cost"
          write(*,*) "-ge X          : Gap  extension cost"
          write(*,*) "-cutoff (12)   Cutoff for hash"
          write(*,*) "-dpcutoff (8)  Cutoff for full DP search"
C          write(*,*) "-numtop (20)   Number of diagonals to use in the heuristic search"
          write(*,*) "-signcutoff (4*cutoff)  cutoff for diagonals to be used "
          write(*,*) "-gapcost (30)  Cost for gaps in the heuristic search"
          write(*,*) "-minout (7) minimum score for beeing printed"
          write(*,*) "-seq file1 file2 ... - : files to read for sequence info"
          write(*,*) "file1 file2     : sequence and profile-list file"
          write(*,*) "Note somehow atleast one sequence or profile-list file has to be named"

          stop
        else if (line(1:6).eq.'-table') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-fasta') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readfastaseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-local') then
          alitype=0
        else if (line(1:7).eq.'-global') then
          alitype=10
        else if (line(1:8).eq.'-loopgap') then
          do j=1,3
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) loopgap(j)
          end do
        else if (line(1:8).eq.'-ssmatch') then
          do j=1,3
            do k=1,3
              cfiles=cfiles+1
              call getarg(cfiles,line)
              linelen=len(line)
              call trima(line,linelen)
              read (line,*) ssmatch(j,k)
            end do
          end do
        else if (line(1:3).eq.'-go') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapopen
        else if (line(1:3).eq.'-ge') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapext
        else if (line(1:5).eq.'-ktup') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) ktup
        else if (line(1:7).eq.'-cutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) cutoff
        else if (line(1:9).eq.'-dpcutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) dpcutoff
        else if (line(1:11).eq.'-signcutoff') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) signcutoff
C        else if (line(1:7).eq.'-numtop') then
C          cfiles=cfiles+1
C          call getarg(cfiles,line)
C          linelen=len(line)
C          call trima(line,linelen)
C          read (line,*) numtop
C          if (numtop.ge.maxtop)  call stopprocess('ERROR> numtop too large !')
        else if (line(1:8).eq.'-gapcost') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapcost
        else if (line(1:5).eq.'-prof') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          do while (line(1:1).ne.'-')
            proffile=line
            proffilelen=len(proffile)
            call trima(proffile,proffilelen)
            call readanyseq(line,profseq,proflen,proffound,profile,profss,x,y,z,proftype,profresnum)
            if (proflen .gt. maxproflen) call stopprocess('ERROR> profile too long !')
            write(*,*)'READING profile type: ',proftype,' len: ',proflen
            cfiles=cfiles+1
            call getarg(cfiles,line)
          end do
          if (profile(1,1).eq.0.) then
            write(*,*)'Making a profile:'
            call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
          else
            do i=1,proflen
              profile(i,penopen)=gapopen+gapext
              profile(i,penext)=gapext
            end do
          end if
          if (profss(1).ne.-1 .and. seqss(1).ne.-1) then
            write(*,*)'Making a secondary structureprofile:'
            call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
          end if
          type=type+1
          inverse=.true.
        else 
          if (type.eq.1) then
            seqfile=line
            call readanyseq(line,seq,seqlen,seqfound,profile,seqss,x,y,z,seqtype,resnum)
            if (seqlen .gt. maxseqlen) call stopprocess('ERROR> Sequence too long !')
C            write(*,*)'READING sequence type: ',seqtype,' len: ',seqlen
            if (seqss(1).ne.-1) then
C              write(*,*)'Using secondary structure predictions:'
              call addsstoseq(seq,seqss,seqlen,numaa)
            end if
          else if (type.eq.2) then
            linelen=len(line)
            call trima(line,linelen)
C            write(*,*) "TESTING:",line(1:linelen),":"
C            proffile=line
            Open(unit=infile,file=line,form='FORMATTED',status='OLD',
     &           access='SEQUENTIAL',err=777)
          else
            call stopprocess('ERROR> extra arguments !')
          end if
          type=type+1
        end if
        cfiles=cfiles+1
        call getarg(cfiles,line)
        linelen=len(line)
        call trima(line,linelen)
      end do
      call makehash(profile,proflen,hash,position,ktup,cutoff,maxaa)
      if (signcutoff .eq. 0) then
        signcutoff=4*cutoff
      end if

      if (.not. inverse) then 
        call stopprocess('You have to specify a profile')
      end if
      j=0
      do while (.true.)
        linelen=len(line)
        read(infile,*,err=777,end=666) line,seqlen,numtm
        call trima(line,linelen)
        if (seqlen .gt. maxseqlen) then
          write(*,*)'Sequence longer than maxsequence, not using the complete sequence'
          seqlen=maxseqlen
        end if
        read(infile,*,err=777,end=666) (seq(i),i=1,seqlen)
        j=j+1

        length=seqlen*proflen

        score=dynheuristic(profile,proflen,seq,seqlen,alitype,
     $     hash,position,ktup,gapcost,signcutoff)

        if (score/log(length) .ge. dpcutoff) then
          score=dynscore(profile,proflen,seq,seqlen,alitype)
        end if
        if (score/log(length) .ge. minout) then
          write(*,'(3(a,1x),2i8,3x,1(f11.5,1x))') 'SCORES> ',proffile(1:proffilelen),line(1:linelen),
     &         seqlen,proflen,score
C     $         ,dpscore
        end if
      end do

 666  continue
      stop
 777  call stopprocess('cannot read list')

      end

***********************************************************************
      subroutine stopprocess(string)
      character*(*) string
      write(*,*) string
      write(*,*)'STOP> Job terminates'
      stop
      end

