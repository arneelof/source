C     The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      Program Pmembr2
C     
C     A heuristic sequence/profile search program. Made for membrane profiles.
C     
      implicit none
#include <dynprog.h>
      
      real profile(maxproflen,maxaa+2),gapopen,gapext,score
      real dynscore,x(maxproflen),y(maxproflen),z(maxproflen)
      character*320 proffile,filename1,filename2
      character*800 oldline,line,dbfile
      character*10 proftype
      character*4 profresnum(maxseqlen),resnum(maxseqlen)
C      character*1 char
      integer hash(maxktup,maxhash),position(maxktup,maxhash),ktup,templen
      integer num,stlen
      integer linelen,i,j,k,proflen,alitype,type
      integer profseq(maxseqlen),profss(maxseqlen),numss,numaa,cfiles,aa
      integer seq(maxseqlen),seqss(maxseqlen),seqlen,traceback(maxseqlen)
      integer seqtoseq,seqtoss,infile,dbsize
      integer proffilelen,numtm
      integer maxhits,maxdbsize
      logical proffound,inverse
C     real cutoff2,score1,score2,gapcost2
      real length,minout,aafreq(maxaa)
      real qfreq(maxaa,maxaa),table(maxaa,maxaa),loopgap(3),ssmatch(3,3)
      real evalue,zs_to_e,lambda,beta
      external dynscore,seqtoseq,seqtoss,zs_to_e
      
      parameter (maxhits=5000)
      parameter (maxdbsize=100000)
      parameter (infile=91)
      parameter (numss=3)
      parameter (numaa=20)
      real heurscore(maxdbsize)
      real sumscore,sum2score,fscore(maxdbsize),averscore,sd,zscore,oldaverscore
      integer fseqlen(maxdbsize),fproflen(maxdbsize),indx(maxdbsize),numout
      character*160 name(maxhits)
      
      
C     call foo(bar)
C     default values
      inverse=.false.
      aa=numaa
      ktup=2
      minout=10
C     numtop=20
      alitype=0
      gapopen=-10
      gapext=-4
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
      ssmatch(1,1)=0
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
      ssmatch(3,3)=0
      call init
      call init_int(position,maxktup*maxhash,0)
      call init_int(hash,maxktup*maxhash,0)
      call init_int(seq,maxseqlen,0)
      call init_int(seqss,maxseqlen,-1)
      call init_int(profss,maxseqlen,-1)
      call init_int(profseq,maxseqlen,0)
      call init_int(traceback,maxseqlen,0)
      
      call init_real(profile,maxproflen*(maxaa+2),0.)
      call init_real(x,maxseqlen,0.)
      call init_real(y,maxseqlen,0.)
      call init_real(z,maxseqlen,0.)
      call init_real(table,maxaa*maxaa,0.)
      
      
      filename1='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contDistHL'
      filename2='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contEneHL'
C     filename1='/home/gvh/arne/source/palign/parkLevitt/contDistHL'
C     filename2='/home/gvh/arne/source/palign/parkLevitt/contEneHL'
      
      line='/afs/pdc.kth.se/home/a/arnee/source/palign/pam250.mat'
      call readfastaseqtable(line,table,maxaa)
      
      cfiles=1 
      call getarg(cfiles,line)
      linelen=len(line)
      call trima(line,linelen)
      type=1

      do i=1,maxseqlen
        call encodi(i,resnum(i),4,stlen)
        call encodi(i,profresnum(i),4,stlen)
      end do

#ifdef IFC      
      do while  (line .ne. oldline ) 
        oldline=line
#else
      do while  (line .ne. '' ) 
#endif
        if (line(1:5).eq.'-help') then
          write(*,*) "Usage:"
          write(*,*) "-table file   : Use a sequence table file in blast format"
          write(*,*) "-fasta file   : Use a sequence table file in fasta format (default pam250)"
          write(*,*) "-local        : Use local alignments (default)"
          write(*,*) "-global       : Use global alignment"
          write(*,*) "-inire  file1 file2    : Energy files for threading energy"
          write(*,*) "-loopgap A B C : cost for gaps in differenst SS regions (default 1,1,1)"
          write(*,*) "-ssmatch a b c d e f g h i : Scores for secondary structure regions"
          write(*,*) "                            default 100 010 001 for secstr and"
          write(*,*) "                            000 010 00 for TM and"
          write(*,*) "-go X          : Gap opening cost"
          write(*,*) "-ge X          : Gap  extension cost"
C     write(*,*) "-numtop (20)   Number of diagonals to use in the heuristic search"
          write(*,*) "-minout (10) minimum score for beeing printed"
          write(*,*) "-seq file1 file2 ... - : files to read for sequence info"
          write(*,*) "file1 file2     : sequence and profile-list file"
          write(*,*) "Note somehow atleast one sequence or profile-list file has to be named"
          
          stop
        else if (line(1:6).eq.'-table') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readseqtable(line,table,maxaa)
C     call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-fasta') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readfastaseqtable(line,table,maxaa)
C     call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-local') then
          alitype=0
        else if (line(1:7).eq.'-global') then
          alitype=10
        else if (line(1:8).eq.'-loopgap') then
          do j=1,3
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) loopgap(j)
          end do
        else if (line(1:8).eq.'-ssmatch') then
          do j=1,3
            do k=1,3
              cfiles=cfiles+1
              call getarg(cfiles,line)
              linelen=len(line)
              call trima(line,linelen)
              read (line,*) ssmatch(j,k)
            end do
          end do
        else if (line(1:3).eq.'-go') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapopen
        else if (line(1:3).eq.'-ge') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapext
        else if (line(1:5).eq.'-ktup') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) ktup
        else if (line(1:7).eq.'-minout') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) minout
C     else if (line(1:7).eq.'-numtop') then
C     cfiles=cfiles+1
C     call getarg(cfiles,line)
C     linelen=len(line)
C     call trima(line,linelen)
C     read (line,*) numtop
C     if (numtop.ge.maxtop)  call stopprocess('ERROR> numtop too large !')
        else if (line(1:5).eq.'-prof') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          do while (line(1:1).ne.'-')
            proffile=line
            proffilelen=len(proffile)
            call trima(proffile,proffilelen)
            call readanyseq(line,profseq,proflen,proffound,profile,profss,x,y,z,proftype,profresnum)
            if (proflen .gt. maxproflen) call stopprocess('ERROR> profile too long !')
            if (templen .ne. 0 .and. proflen .ne. templen) then
              call stopprocess('ERROR> sequences of different length !')
            end if
            templen=proflen
            write(*,*)'READING profile type: ',proftype,' len: ',proflen
            cfiles=cfiles+1
            call getarg(cfiles,line)
          end do
          if (proftype(1:8).ne.'psiblast') then
            write(*,*)'Making a profile:'
            call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
          else
            if (gapopen .gt. 0) gapopen=-gapopen
            if (gapext .gt. 0) gapext=-gapext
            do i=1,proflen
              profile(i,penopen)=gapopen+gapext
              profile(i,penext)=gapext
            end do
          end if
          if (profss(1).ne.-1 .and. seqss(1).ne.-1) then
            write(*,*)'Making a secondary structureprofile:'
            call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss)
            aa=numaa*3
          end if
          type=type+1
          inverse=.true.
        else 
C     Now the last argument has to be a database file 
          linelen=len(line)
          call trima(line,linelen)
          if (linelen .gt. 0)   dbfile=line
        end if
        cfiles=cfiles+1
        call getarg(cfiles,line)
        linelen=len(line)
        call trima(line,linelen)
      end do
      
      if (.not. inverse) then 
        call stopprocess('You have to specify a profile')
      end if
      
C     for profiles
      lambda = 0.3176
      beta = 10
      call init_freq(aafreq,qfreq,table,lambda,maxaa,numaa)
      
      
      j=0
      Open(unit=infile,file=dbfile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      sumscore=0
      sum2score=0
      numout=0
      num=0
      do while (.true.)
        linelen=len(line)
        read(infile,*,err=777,end=666) line,seqlen,numtm
        call trima(line,linelen)
        if (seqlen .gt. maxseqlen) then
          write(*,*)'Sequence longer than maxsequence, not using the complete sequence'
          seqlen=maxseqlen
        end if
        read(infile,*,err=777,end=666) (seq(i),i=1,seqlen)
        
        length=seqlen*proflen
        
        score=dynscore(profile,proflen,seq,seqlen,alitype)
        
        num=num+1
        score=score/log(length)
        heurscore(num)=score
        sumscore=sumscore+score
        sum2score=sum2score+score**2
      end do
 666  continue
      close (unit=infile)
      
      averscore=sumscore/num
      sd=sqrt((sum2score-sumscore*sumscore/num)/(num-1))
      
      j=1
      oldaverscore=averscore+1
      do while (j .lt. 5 .and. averscore .ne. oldaverscore)
        oldaverscore=averscore
        j=j+1
        dbsize=0
        sumscore=0
        sum2score=0
        do i=1,num
          zscore=(heurscore(i)-averscore)/sd
C          if (zscore .lt. 7. .and. zscore .gt. -3.) then
          if (zscore .lt. 7. ) then
            dbsize=dbsize+1
            sumscore=sumscore+heurscore(i)
            sum2score=sum2score+heurscore(i)**2
          end if
        end do
        if (sumscore.gt.0 .and. dbsize.gt.5) then
          averscore=sumscore/dbsize
          sd=sqrt((sum2score-sumscore*sumscore/dbsize)/(dbsize-1))
        end if
C      write(*,*)'test',j,oldaverscore,averscore,sd,dbsize
      end do


      Open(unit=infile,file=dbfile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      do num=1,num 
        linelen=len(line)
        read(infile,*,err=777,end=667) line,seqlen,numtm
        call trima(line,linelen)
        if (seqlen .gt. maxseqlen) then
C     write(*,*)'Sequence longer than maxsequence, not using the complete sequence'
          seqlen=maxseqlen
        end if
        read(infile,*,err=777,end=667) (seq(i),i=1,seqlen)

        
        score=heurscore(num)
        zscore=(heurscore(num)-averscore)/sd
        evalue=zs_to_e(zscore,dbsize)
        
C        write(*,*) 'test1',num,score,zscore,evalue,numout

        if (evalue .le. minout  ) then
          numout=numout+1
          if (numout .gt. maxdbsize) then
            write(*,*)'Numout larger than maxhits, sequence ignored'
          else
C            write(*,*)'test3: ',numout,score,averscore,sd,dbsize
            fseqlen(numout)=seqlen
            fproflen(numout)=proflen
            fscore(numout)=score
            name(numout)=line
          end if
        end if
      end do
        
 667  continue
      if (numout .eq. 0) call stopprocess('no hits found')
        
      call indexx(numout,fscore,indx)
      i=min(numout,maxdbsize)
      
      zscore=(fscore(indx(i))-averscore)/sd
      evalue=zs_to_e(zscore,dbsize)
C      write(*,*)'test2: ',zscore,minout,evalue,i,numout
      do while(evalue.le.minout .and. i.ge.1)
        length=fseqlen(indx(i))*fproflen(indx(i))
        linelen=80
        call trima(name(indx(i)),linelen)
        evalue=zs_to_e(zscore,dbsize)
C        write(*,'(1(a,1x),2(a,1x),f11.5,3i8,3x,3(e11.3,1x))') 'SCORE> ',
C     $       proffile(1:proffilelen),name(indx(i))(1:linelen),
C     $       fscore(indx(i))*log(length),numout-i+1,
C     $       fproflen(indx(i)),fseqlen(indx(i)),fscore(indx(i)),zscore,evalue
        write(*,*) 'SCORE> ',
     $       proffile(1:proffilelen),name(indx(i))(1:linelen),
     $       fscore(indx(i))*log(length),numout-i+1,
     $       fproflen(indx(i)),fseqlen(indx(i)),fscore(indx(i)),zscore,evalue
        i=i-1
        if(i.ge.1) then
          zscore=(fscore(indx(i))-averscore)/sd
        else
          zscore=-9999
        end if
        evalue=zs_to_e(zscore,dbsize)
      end do
      
      
      close(unit=infile)
      stop
 777  call stopprocess('cannot read list')
      
      end
      
***********************************************************************
      subroutine stopprocess(string)
      character*(*) string
      write(*,*) string
      write(*,*)'STOP> Job terminates'
      stop
      end
      
