C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      INTEGER FUNCTION MATCH(STRING,PATT)
C
C     Finds a match of PATT in STRING.
C     MATCH=0  if no match
C     MATCH=first position in STRING that matches PATT
C     Trailing blanks in PATT are NOT significant and case does NOT
C     matter UNLESS QUOTES are used!  (PATT=ABc and PATT=ABC are equal but
C     PATT="ABc  ", PATT="ABC  " and PATT="ABC " are all unequal)
C     PATT=ABC"kk" is EQUAL to PATT=ABC"KK"; here the quotes have no special 
C     meaning.
C     PATT=AbC, STRING=AbDefGABCDEAbCDe  => MATCH=7
C
C     MATCH is very similar to the standard INDEX function, but MATCH is 
C     a little more general in that it also handles the "ignore case" case.
C
C     1983-05-02/LN
C
      CHARACTER*(*) PATT,STRING
      LOGICAL CASE
      CHARACTER*1 P,S,UC,QUOTE
      INTEGER I,J,K,M,N,NS,NP,nblen,l
      external nblen
      DATA QUOTE/'"'/
C
      MATCH=0
      NS=LEN(STRING)
      NP=NBLEN(PATT)
      IF(PATT(1:1) .EQ. QUOTE) THEN
         CASE=.FALSE.
         M=2  
         IF(PATT(NP:NP) .EQ. QUOTE) NP=NP-1
         NP=NP-1
         IF(NP .LE. 0) THEN
            WRITE(*,*) 'ERROR! MATCH=0. PATTERN IS " OR "" (EMPTY)'
            RETURN
         ENDIF
      ELSE  
         CASE=.TRUE.
         M=1
      ENDIF
      N=NS-NP+1
      IF(N .LE. 0) RETURN
      L=1
5     P=PATT(M:M)
      IF(CASE) P=UC(P)
      I=L
7     IF(I .GT. N) GOTO 9
         S=STRING(I:I)
         IF(CASE) S=UC(S)
         IF(S .EQ. P) GOTO 10
         I=I+1
         GOTO 7

C    IF WE COME OUT THIS WAY THERE WAS NO MATCH 

9     RETURN

C    BUT HERE WE HAVE A NEW MATCH

10    IF(NP .EQ. 1) GOTO 20
      J=M
      K=I+1
15    IF(K .GT. I+NP-1) GOTO 20
         J=J+1
         P=PATT(J:J)
         IF(CASE) P=UC(P)
         S=STRING(K:K)
         IF(CASE) S=UC(S)
         IF(S .NE. P) GOTO 30
         K=K+1
         GOTO 15
20    MATCH=I
      RETURN
30    L=I+1
      IF(L .GT. N) RETURN
C
C     ELSE TRY AGAIN
C
      GOTO 5
      END
************************************
      CHARACTER FUNCTION UC(C)
C
C     UC=Upper Case of C
C
      CHARACTER*1 C,tab

      INTEGER IDIFF
      DATA IDIFF/32/
      tab=char(9)
C
      IF((C .LT. 'a') .OR. (C .GT. 'z')) THEN
         UC=C
      ELSE
         UC=CHAR(ICHAR(C)-IDIFF)
      ENDIF
      if (C .eq. tab) uc = ' '
      RETURN
      END
C     
********************************************
      SUBROUTINE MUC(STRING)
C
C     Makes Upper Case of String
C     This Excludes everything within '' or ""
C     And places a space their.
C     Not specially advanced but easy..
C
      CHARACTER*(*) STRING
      INTEGER N,I
      CHARACTER*1 UC
      logical inside
C
      N=LEN(STRING)
      if (n.eq.1 .and. ichar(string(1:1)).eq.32) return
      inside=.false.
      I=1
 10   IF(STRING(I:I).eq.char(34) .or. STRING(I:I).eq.char(39)) then
         inside=.not.inside
         string(i:i)=' '
      ENDIF
      IF(.not.inside) STRING(I:I)=UC(STRING(I:I))
         I=I+1
         IF(I .LE. N) GOTO 10
      RETURN
      END
**************************************
      CHARACTER FUNCTION LC(C)
C
C     LC=Upper Case of C
C
      CHARACTER*1 C,tab
      INTEGER IDIFF

      DATA IDIFF/32/
      tab=char(9)
C
      IF((C .LT. 'A') .OR. (C .GT. 'Z')) THEN
        LC=C
      ELSE
        LC=CHAR(ICHAR(C)+IDIFF)
      ENDIF
      if (C .eq. tab ) lc=' '
      RETURN
      END
C     
*******************************************
      SUBROUTINE MLC(STRING)
C
C     Makes Upper Case of String
C     This Excludes everything within '' or ""
C     And places a space their.
C     Not specially advanced but easy..
C
      CHARACTER*(*) STRING
      INTEGER N,I
      CHARACTER*1 LC
      logical inside
C
      N=LEN(STRING)
      if (n.eq.1 .and. ichar(string(1:1)).eq.32) return
      inside=.false.
      I=1
10    IF(STRING(I:I).eq.char(34) .or. STRING(I:I).eq.char(39)) then
        inside=.not.inside
        string(i:i)=' '
      ENDIF
      IF(.not.inside) STRING(I:I)=LC(STRING(I:I))
      I=I+1
      IF(I .LE. N) GOTO 10
      RETURN 

      END
C     
****************************************
      INTEGER FUNCTION NBLEN(STRING)
C
C     NBLEN=Number of characters in STRING up to (incl.) the last NON-BLANK
C     character.
C
C     1983-05-02/LN
C
      CHARACTER*(*) STRING
      CHARACTER SPACE
      DATA SPACE/' '/
C
      NBLEN=LEN(STRING)
5        IF(NBLEN .EQ. 0) RETURN
         IF(STRING(NBLEN:NBLEN) .NE. SPACE) RETURN
         NBLEN=NBLEN-1
         GOTO 5
      END
