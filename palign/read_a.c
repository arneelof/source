/* no memory checks! */
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>

#define Max_hom 4096
#define Max_len 4096
#define Verbose 0
#define Aggressive 1
#define Farhigh 1
#define Cluster 1
#define Filter 1
#define Max_sim 0.97
#define Max_eval 0.001
#define Max_comp 0.05
#define Seq_obj 21
#define Seq_max 24
#define Prf_mark "G	A	S	C	V	T	I	P	M	D	N	L	K	E	Q	R	H	F	Y	W	."

#define Max(a,b) ((a)>=(b)?(a):(b))
#define Min(a,b) ((a)<=(b)?(a):(b))
#define Gets(line,len) \
  { char* c=line+len-2;\
    *c='\0';\
    if(fgets(line,len,stdin)==NULL){\
      fprintf(stderr,"unexpected end of file, fatal\n");exit(-1);}\
    while(!iscntrl(*c))\
      *c=getc(stdin);\
  }
#define Format(l) {fprintf(stderr,"format error, fatal:\n%s\n",l);exit(-1);}

 typedef float prof_t[Seq_obj+1];
 const int seq2num[256]={
 /* 0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,A ,B ,C ,D ,E ,F */
    20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,/* 00 - 0F */
    20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,/* 10 - 1F */
    21,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,/* 20 - 2F */
    20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,/* 30 - 3F */
    20, 1,20, 3, 9,13,17, 0,16, 6,20,12,11, 8,10,20,/* 40 - 4F */
     7,14,15, 2, 5,20, 4,19,20,18,20,20,20,20,20,20,/* 50 - 5F */
    20, 1,20, 3, 9,13,17, 0,16, 6,20,12,11, 8,10,20,/* 60 - 6F */
     7,14,15, 2, 5,20, 4,19,20,18,20,20,20,20,20,20 /* 70 - 7F */
    };
 /* not used yet */
 const char num2seq[22]=
   {'G','A','S','C','V','T','I','P','M','D',
    'N','L','K','E','Q','R','H','F','Y','W','-',' '};
 struct text_s{
   int len;
   float e_val;
   char *text;
   struct text_s *prev;
   struct text_s *next;};

void* mem(unsigned long size)
{ void *p=malloc(size);
  if(p==NULL&&size>0)
  { fprintf(stderr,"no memory for %lu bytes, fatal\n",size);
    abort();
  }
  return(p);
}
float norm1(float* x,int n)
{ int m=n;
  float sum=0.0;
  for(;--n>=0;)
    sum+=x[n];
  if(sum==0.0)
    return(1.0);
  for(;--m>=0;)
    x[m]/=sum;
  return(sum);
}
/*
void set_comp(float* comp,char* seq,int len)
{ int sum=0;
  memset(comp,0,sizeof(float)*Seq_obj);
  for(;--len>=0;)
    if(seq2num[seq[len]]<Seq_obj){
      comp[seq2num[seq[len]]]++;
      sum++;}
  for(len=Seq_obj-1;len>=0;len--)
    comp[len]/=(float)sum;
}
float comp_div(const float* comp1,const float* comp2)
{ int i;
  float x=0.0;
  for(i=Seq_obj-2;i>=0;i--)
    x+=(comp1[i]-comp2[i])*(comp1[i]-comp2[i]);
  if(x==0.0) return(0.0);
  return(sqrt(x/(float)(Seq_obj-1)));
}
*/
float com_div(char* seq1,char* seq2,int len)
{ float div=0.0;
  float com1[Seq_obj];
  float com2[Seq_obj];
  int sum1=1;
  int sum2=1;
  memset(com1,0,Seq_obj*sizeof(float));
  memset(com2,0,Seq_obj*sizeof(float));
  for(;--len>=0;){
    if(seq1[len]==' '||seq2[len]==' ')
      continue;
    if(seq2num[seq1[len]]<Seq_obj){
      com1[seq2num[seq1[len]]]++;
      sum1++;}
    if(seq2num[seq2[len]]<Seq_obj){
      com2[seq2num[seq2[len]]]++;
      sum2++;}}
  /* gaps ignored */
  for(len=Seq_obj-2;len>=0;len--){
    float x=com2[len]/(float)sum2-com1[len]/(float)sum1;
    div+=x*x;}
  return(sqrt(div/(float)(Seq_obj-1)));
}
/* Try asymmetric solution !!! */
float hom_div(char* seq1,char* seq2,int len)
{ int div=0,sum=1;
  for(;--len>=0;){
    /* if(seq1[len]==' '||seq2[len]==' ') symmetric */
    if(seq2[len]==' ') /* non symmetric */
      continue;
    if(seq1[len]!=seq2[len])
      div++;
    sum++;}
  return(1.0-(float)div/(float)sum);
}
const int match[Seq_max][Seq_max]={
  { 4,-1,-2,-2, 0,-1,-1, 0,-2,-1,-1,-1,-1,-2,-1, 1, 0,-3,-2, 0,-2,-1,-1, 0},
  {-1, 5, 0,-2,-3, 1, 0,-2, 0,-3,-2, 2,-1,-3,-2,-1,-1,-3,-2,-3,-1, 0,-1, 0},
  {-2, 0, 6, 1,-3, 0, 0, 0, 1,-3,-3, 0,-2,-3,-2, 1, 0,-4,-2,-3, 3, 0,-1, 0},
  {-2,-2, 1, 6,-3, 0, 2,-1,-1,-3,-4,-1,-3,-3,-1, 0,-1,-4,-3,-3, 4, 1,-1, 0},
  { 0,-3,-3,-3, 9,-3,-4,-3,-3,-1,-1,-3,-1,-2,-3,-1,-1,-2,-2,-1,-3,-3,-1, 0},
  {-1, 1, 0, 0,-3, 5, 2,-2, 0,-3,-2, 1, 0,-3,-1, 0,-1,-2,-1,-2, 0, 3,-1, 0},
  {-1, 0, 0, 2,-4, 2, 5,-2, 0,-3,-3, 1,-2,-3,-1, 0,-1,-3,-2,-2, 1, 4,-1, 0},
  { 0,-2, 0,-1,-3,-2,-2, 6,-2,-4,-4,-2,-3,-3,-2, 0,-2,-2,-3,-3,-1,-2,-1, 0},
  {-2, 0, 1,-1,-3, 0, 0,-2, 8,-3,-3,-1,-2,-1,-2,-1,-2,-2, 2,-3, 0, 0,-1, 0},
  {-1,-3,-3,-3,-1,-3,-3,-4,-3, 4, 2,-3, 1, 0,-3,-2,-1,-3,-1, 3,-3,-3,-1, 0},
  {-1,-2,-3,-4,-1,-2,-3,-4,-3, 2, 4,-2, 2, 0,-3,-2,-1,-2,-1, 1,-4,-3,-1, 0},
  {-1, 2, 0,-1,-3, 1, 1,-2,-1,-3,-2, 5,-1,-3,-1, 0,-1,-3,-2,-2, 0, 1,-1, 0},
  {-1,-1,-2,-3,-1, 0,-2,-3,-2, 1, 2,-1, 5, 0,-2,-1,-1,-1,-1, 1,-3,-1,-1, 0},
  {-2,-3,-3,-3,-2,-3,-3,-3,-1, 0, 0,-3, 0, 6,-4,-2,-2, 1, 3,-1,-3,-3,-1, 0},
  {-1,-2,-2,-1,-3,-1,-1,-2,-2,-3,-3,-1,-2,-4, 7,-1,-1,-4,-3,-2,-2,-1,-1, 0},
  { 1,-1, 1, 0,-1, 0, 0, 0,-1,-2,-2, 0,-1,-2,-1, 4, 1,-3,-2,-2, 0, 0,-1, 0},
  { 0,-1, 0,-1,-1,-1,-1,-2,-2,-1,-1,-1,-1,-2,-1, 1, 5,-2,-2, 0,-1,-1,-1, 0},
  {-3,-3,-4,-4,-2,-2,-3,-2,-2,-3,-2,-3,-1, 1,-4,-3,-2,11, 2,-3,-4,-3,-1, 0},
  {-2,-2,-2,-3,-2,-1,-2,-3, 2,-1,-1,-2,-1, 3,-3,-2,-2, 2, 7,-1,-3,-2,-1, 0},
  { 0,-3,-3,-3,-1,-2,-2,-3,-3, 3, 1,-2, 1,-1,-2,-2, 0,-3,-1, 4,-3,-2,-1, 0},
  {-2,-1, 3, 4,-3, 0, 1,-1, 0,-3,-4, 0,-3,-3,-2, 0,-1,-4,-3,-3, 4, 1,-1, 0},
  {-1, 0, 0, 1,-3, 3, 4,-2, 0,-3,-3, 1,-1,-3,-1, 0,-1,-3,-2,-2, 1, 4,-1, 0},
  {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1, 0},
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
int seq_num(const char c)
{ switch(c){
    case('a'):return(0);
    case('r'):return(1);
    case('n'):return(2);
    case('d'):return(3);
    case('c'):return(4);
    case('q'):return(5);
    case('e'):return(6);
    case('g'):return(7);
    case('h'):return(8);
    case('i'):return(9);
    case('l'):return(10);
    case('k'):return(11);
    case('m'):return(12);
    case('f'):return(13);
    case('p'):return(14);
    case('s'):return(15);
    case('t'):return(16);
    case('w'):return(17);
    case('y'):return(18);
    case('v'):return(19);
    case('b'):return(20);
    case('z'):return(21);
    case('x'):return(22);
    case('A'):return(0);
    case('R'):return(1);
    case('N'):return(2);
    case('D'):return(3);
    case('C'):return(4);
    case('Q'):return(5);
    case('E'):return(6);
    case('G'):return(7);
    case('H'):return(8);
    case('I'):return(9);
    case('L'):return(10);
    case('K'):return(11);
    case('M'):return(12);
    case('F'):return(13);
    case('P'):return(14);
    case('S'):return(15);
    case('T'):return(16);
    case('W'):return(17);
    case('Y'):return(18);
    case('V'):return(19);
    case('B'):return(20);
    case('Z'):return(21);
    case('X'):return(22);
    case('-'):return(22);
    case('.'):return(22);
    default:;}
  return(23);
}
int set_sim(const char* seq1,const char* seq2,int len)
{ int score=0;
  for(;--len>=0;)
    score+=match[seq_num(seq1[len])][seq_num(seq2[len])];
  if(score<0)
    return(0);
  return(score);
}
/* adding text, sorting on the fly */
void add_text(struct text_s *t,const char* text,const float e_val)
{ assert(t!=NULL);
  t=t->prev;
  while(t->e_val>e_val&&t->e_val>=0.0){
    assert(t!=NULL);
    t=t->prev;}
  assert(t!=NULL);
  t=t->next;
  assert(t->e_val>=0.0);
  /* allocating memory */
  t->prev->next=mem(sizeof(struct text_s));
  t->prev->next->prev=t->prev;
  t->prev->next->next=t;
  t->prev=t->prev->next;
  t=t->prev;
  t->len=strlen(text);
  if(text[t->len-1]=='\n')
    t->len--;
  t->e_val=e_val;
  t->text=mem(sizeof(char)*(t->len+1));
  memcpy(t->text,text,sizeof(char)*(t->len));
  t->text[t->len]='\0';
}
struct text_s* del_text(struct text_s *t)
{ struct text_s *q=t;
  if(t==NULL)
    return(NULL);
  if(t->prev!=NULL)
    (t->prev)->next=t->next;
  if(t->next!=NULL){
    (t->next)->prev=t->prev;
    t=t->next;}
  else
    t=NULL;
  free(q->text);
  free(q);
  return(t);
}
#define Para_gap1 10
#define Para_gap2 1
#define AA 1 /* ali to ali */
#define AG 2 /* ali to gap */
#define VH 4 /* vertical */
#define ZE 8 /* zero */
#define Loctest(a,k) \
  if(a<0.0){a=0.0;smpp[k]|=ZE;} \
  else{if(a>score){score=a;be=k;}}
#define Glotest(a,g,k)
int alipath(const int* seq1,const int* seq2,const int lp1,const int lp2,int* pathp)
{ int be=0;
  int score=match[seq1[0]][seq2[0]];
  char *smpp=mem(sizeof(char)*lp1*lp2);
  if(smpp==NULL || !lp1 || !lp2)
    return(-1.0);
  { int l1,l2,k=0;
    int *al=mem(sizeof(int)*lp2);
    int *gl=mem(sizeof(int)*lp2);
    if(al==NULL||gl==NULL){
      free(smpp);
      return(-1.0);}
    for(l2=0;l2<lp2;l2++,k++){
      al[l2]=match[seq1[0]][seq2[l2]];
      gl[l2]=-Para_gap1;
      smpp[k]=AA|VH;
      Loctest(al[l2],k)}
    for(l1=1;l1<lp1;l1++){
      int gd=-Para_gap1;
      int ad=match[seq1[l1]][seq2[0]];
      smpp[k]=AA;
      Loctest(ad,k)
      for(l2=1,k++;l2<lp2;l2++,k++){
        int an=match[seq1[l1]][seq2[l2]];
        int gn=al[l2]-Para_gap1;

        smpp[k]=AG|VH;
        if(gn<gl[l2]-Para_gap2){
          gn=gl[l2]-Para_gap2;
          smpp[k]=VH;}
        if(gn<gd-Para_gap2){
          gn=gd-Para_gap2;
          smpp[k]=0;}
        if(gn<ad-Para_gap1){
          gn=ad-Para_gap1;
          smpp[k]=AG;}
        if(al[l2-1]>gl[l2-1]){
          an+=al[l2-1];
          smpp[k]|=AA;}
        else
          an+=gl[l2-1];
        al[l2-1]=ad;
        gl[l2-1]=gd;
        ad=an;
        gd=gn;
        Loctest(ad,k)}
      Glotest(ad,gd,k-1)}
    assert(k==lp2*lp1);
    free(al);
    free(gl);
  }
  if(pathp!=NULL)
  { int stat=1,l1,l2,k;
    if(be<0){
      be=-be;
      stat=0;}
    l1=be/lp2;
    l2=be%lp2;
    for(k=l1+1;k<lp1;k++)
      pathp[k]=0;
    for(;;){
      if(stat){
        pathp[l1]=l2+1;
        stat=smpp[be]&AA;
        if(--l1<0)
          break;
        if(--l2<0)
          break;
        be-=lp2+1;
        if(smpp[be]&ZE)
          break;
        }
      else{
        pathp[l1]=0;
        stat=smpp[be]&AG;
        if(smpp[be]&VH){
          if(--l1<0)
            break;
          be-=lp2;}
        else{
          if(--l2<0)
            break;
          be--;}}}
    for(;l1>=0;l1--)
      pathp[l1]=0;
  }
  free(smpp);
  return(score);
}
#undef Loctest
#undef Glotest
#undef AA
#undef AG
#undef VH
#undef ZE
#undef Para_gap1
#undef Para_gap2

int main(int argc,char** argv)
{ int slen=0,uslen=0,nlen=0,nhom=0;
  char line[Max_len];
  char seq[Max_len];
  char useq[Max_len];
  char name[Max_len];
  struct text_s homseq,*hs;
  struct text_s homnam,*hn;
  float homdiv[Max_hom];
  prof_t bprof[Max_len];
  float simm[Max_hom];
  float *sim[Max_hom];
  float max_eval=Max_eval;
  float max_sim=Max_sim;
  int verbose=Verbose;
  int filter=Filter;
  int aggressive=Aggressive;
  int farhigh=Farhigh;
  int cluster=Cluster;

  hs=mem(sizeof(struct text_s));
  homseq.len=0;
  homseq.e_val=-1.0;
  homseq.prev=NULL;
  homseq.next=hs;
  hs->len=-1;
  hs->e_val=11.0;
  hs->prev=&homseq;
  hs->next=NULL;
  hn=mem(sizeof(struct text_s));
  homnam.len=0;
  homnam.e_val=-1.0;
  homnam.prev=NULL;
  homnam.next=hn;
  hn->len=-1;
  hn->e_val=11.0;
  hn->prev=&homnam;
  hn->next=NULL;

  memset(seq,'\0',Max_len*sizeof(char));
  memset(useq,'\0',Max_len*sizeof(char));
  if(argc>1){
    FILE *file=fopen(argv[1],"r");
    if(file==NULL){
      fprintf(stderr,"Could not open file %s , fatal \n",argv[1]);
      exit(-1);}
    while(NULL!=fgets(line,Max_len,file)){
      if(!strncmp("#VERBOSE",line,8)){
        verbose=1;
        fprintf(stderr,"<pre>verbose set\n");
        continue;}
      if(!strncmp("#NO_VERBOSE",line,11)){
        if(verbose)
          fprintf(stderr,"<pre>no verbose\n");
        verbose=0;
        continue;}
      if(!strncmp("#AGGRESSIVE",line,11)){
        aggressive=1;
        if(verbose)
          fprintf(stderr,"<pre>aggressive set\n");
        continue;}
      if(!strncmp("#NO_AGGRESSIVE",line,14)){
        aggressive=0;
        if(verbose)
          fprintf(stderr,"<pre>no aggressive\n");
        continue;}
      if(!strncmp("#FARHIGH",line,8)){
        farhigh=1;
        if(verbose)
          fprintf(stderr,"<pre>farhigh set\n");
        continue;}
      if(!strncmp("#NO_FARHIGH",line,11)){
        farhigh=0;
        if(verbose)
          fprintf(stderr,"<pre>no farhigh\n");
        continue;}
      if(!strncmp("#CLUSTER",line,8)){
        cluster=1;
        if(verbose)
          fprintf(stderr,"<pre>clustering set\n");
        continue;}
      if(!strncmp("#NO_CLUSTER",line,11)){
        cluster=0;
        if(verbose)
          fprintf(stderr,"<pre>no clustering\n");
        continue;}
      if(!strncmp("#FILTER",line,7)){
        filter=1;
        if(verbose)
          fprintf(stderr,"<pre>filtering set\n");
        continue;}
      if(!strncmp("#NO_FILTER",line,10)){
        filter=0;
          if(verbose)
        fprintf(stderr,"<pre>no filtering\n");
        continue;}
      if(!strncmp("#MAX_EVAL",line,9)){
        max_eval=atof(line+10);
        if(max_eval<0.0){
          max_eval=Max_eval;}
        if(verbose)
          fprintf(stderr,"<pre>max_eval %.3f\n",max_eval);
        continue;}
      if(!strncmp("#MAX_SIM",line,8)){
        filter=1;
        max_sim=atof(line+9);
        if(max_sim<0.0){
          max_sim=Max_sim;}
        if(verbose)
          fprintf(stderr,"<pre>max_sim %.3f\n",max_sim);
        continue;}
      if(*line=='#'){
        continue;}
      if(!uslen && *line!='>'){
        strcpy(useq,line);
        for(;;uslen++){
          if(!isalpha(useq[uslen]))
            break;
          useq[uslen]=toupper(useq[uslen]);}
        useq[uslen]='\0';
        continue;}
      if(!nlen){
        strcpy(name,line);
        nlen=strlen(name);}} /* with newline */
    fclose(file);}
  if(verbose)
    fprintf(stderr,"<pre>\n");
  if(!nlen){
    strcpy(name,"query\n");
    nlen=strlen(name);}

  /* reading input start */
  for(nhom=0;nhom<Max_hom;nhom++){
    char name[Max_len];
    char hseq[Max_len];
    int len=0,i=0,j=0;
    if(fgets(line,Max_len,stdin)==NULL){
      break;}
    len=strlen(line); 
    for(i=0;i<len-1;i++){
      if(line[i]>='a'&&line[i]<='z'){
        continue;}
      hseq[j]=line[i];
      if(seq[j]=='\0'){
        if(j>slen)slen=j+1;
        seq[j]=line[i];} 
      j++;}
    hseq[j]='\0';
    add_text(hs,hseq,0.0);
    sprintf(name,">0.0 %d",nhom+1);
    add_text(hn,name,0.0);}
  /* reading input end */

  if(filter)
  { int n;
    /*float scomp[Seq_obj];
    float hcomp[Seq_obj];
    set_comp(scomp,seq,slen);*/
    hs=homseq.next;
    hn=homnam.next;
    for(n=0;n<nhom;){
      float e_val;
      assert(hs->text!=NULL);
      assert(hn->text!=NULL);
      sscanf(hn->text,">%f",&e_val);
      if(e_val>max_eval){
        if(verbose)
          fprintf(stderr,"<font color=red>%*s e_val!(%.3f) %.100s</font>\n",
            -slen,hs->text,e_val,hn->text);
        hs=del_text(hs);
        hn=del_text(hn);
        nhom--;
        continue;}
      /*set_comp(hcomp,hs->text,hs->len);*/
      if((homdiv[n]=com_div(seq,hs->text,hs->len))>Max_comp){
        if(verbose)
          fprintf(stderr,"<font color=red>%*s comp!(%.3f) %.100s\n</font>",
            -slen,hs->text,homdiv[n],hn->text);
        hs=del_text(hs);
        hn=del_text(hn);
        nhom--;
        continue;}
      { int i;
        struct text_s *is;
        struct text_s *in;
        float x=hom_div(seq,hs->text,hs->len);
        if(x>max_sim){
          if(verbose)
            fprintf(stderr,"<font color=red>%*s sim!(%.3f) %.100s\n</font>",
              -slen,hs->text,x,hn->text);
          hs=del_text(hs);
          hn=del_text(hn);
          nhom--;
          goto NEXT;}
        is=homseq.next;
        in=homnam.next;
        for(i=0;i<n;i++,is=is->next,in=in->next){
          x=hom_div(is->text,hs->text,Min(is->len,hs->len));
          if(x>max_sim){

/*
            float old_e_val=-1.0;
            sscanf(in->text,">%f",&old_e_val);
            if(old_e_val>e_val){
              if(verbose){
                fprintf(stderr,"%*s sim[%d]!(%.3f) %.100s (replaced)\n",
                  -slen,is->text,i+1,x,in->text);
                fprintf(stderr,"%*s [%d] %.100s\n",
                  -slen,hs->text,n,hn->text);}
              del_text(is);
              del_text(in);
              nhom--;
              goto OK;}
*/

            if(verbose)
              fprintf(stderr,"<font color=red>%*s sim[%d]!(%.3f) %.100s\n</font>",
                -slen,hs->text,i+1,x,hn->text);
            hs=del_text(hs);
            hn=del_text(hn);
            nhom--;
            goto NEXT;}}
      }
      if(verbose)
        fprintf(stderr,"%*s [%d] %.100s\n",-slen,hs->text,n+1,hn->text);
      n++;
/*
      OK:
*/
      hs=hs->next;
      hn=hn->next;
      NEXT:;}
  }
  if(cluster)
  { int i,n,max;
    max=(float)set_sim(seq,seq,slen);
    hs=homseq.next;
    for(n=0;n<nhom;n++,hs=hs->next){
      sim[n]=(float*)mem(sizeof(float)*nhom);
      sim[n][n]=(float)set_sim(seq,hs->text,Min(hs->len,slen));
      simm[n]=(float)set_sim(hs->text,hs->text,hs->len);
      hn=homseq.next;
      for(i=0;i<n;i++,hn=hn->next)
        sim[i][n]=sim[n][i]=
          (float)set_sim(hn->text,hs->text,Min(hn->len,hs->len));}
    for(n=0;n<nhom;n++){
      sim[n][n]=sim[n][n]/Min(max,simm[n]);
      for(i=0;i<n;i++)
        sim[i][n]=sim[n][i]=sim[n][i]/Min(simm[i],simm[n]);}
    for(n=0;n<nhom;n++){
      simm[n]=sim[n][n];
      sim[n][n]=1.0;
      for(i=0;i<n;i++){
        sim[i][n]=sim[n][i]=1.0-sim[n][i];}}
    if(verbose){
      float weighta[Max_hom];
      float weightb[Max_hom];
      float weightc[Max_hom];
      float weightd[Max_hom];
      for(n=0;n<nhom;n++){
        float x=0.0,y=0.0,z=0.0;
        for(i=0;i<nhom;i++){
          x+=sim[n][i];
          y+=1.0-sim[n][i];
          z+=(1.0-sim[n][i])*(1.0-sim[n][i]);}
        y=1.0/(1.0+y);
        z=1.0/(1.0+z);
        weighta[n]=x*simm[n];
        weightb[n]=x;
        weightc[n]=y;
        weightd[n]=z;}
      norm1(weighta,nhom);
      norm1(weightb,nhom);
      norm1(weightc,nhom);
      norm1(weightd,nhom);
      hs=homseq.next;
      hn=homnam.next;
      for(n=0;n<nhom;n++,hn=hn->next,hs=hs->next){
        fprintf(stderr,"%3d: <font color=green>",n+1);
        for(i=0;i<nhom;i++){
          fprintf(stderr," %.2f",sim[n][i]);}
        fprintf(stderr,"</font>\n%3d: [%.3f,%.3f,%.3f,%.3f] %.100s\n",n+1,weighta[n]*nhom,weightb[n]*nhom,weightc[n]*nhom,weightd[n]*nhom,hn->text);}}}
#ifdef DEBUG
  hs=homseq.next;
  hn=homnam.next;
  for(n=0;n<nhom;n++,hn=hn->next,hs=hs->next){
    assert(hn->e_val==hs->e_val);
    assert(hn->e_val<=(hn->next)->e_val);}
#endif
  { int s,n;
    int iseq[Max_len];
    int iuseq[Max_len];
    int pathp[Max_len];
    int score;
    memset(bprof,0,sizeof(prof_t)*slen);
    fprintf(stdout,"%s\n",Prf_mark);
    /* align useq with seq */
    if(uslen>0){
      for(s=uslen-1;s>=0;s--)
        iuseq[s]=seq_num(useq[s]);
      for(s=slen-1;s>=0;s--)
        iseq[s]=seq_num(seq[s]);
      score=alipath(iuseq,iseq,uslen,slen,pathp);}
    else{
      strcpy(useq,seq);
      uslen=slen;
      for(s=slen-1;s>=0;s--)
        pathp[s]=s+1;}
    for(s=0;s<uslen;s++){
      int p=pathp[s]-1;
      float x,prof[Seq_obj]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
      if(cluster){
        int occ[Max_hom];
        float weight[Max_hom];
        float ss=1.0,dd=1.0,da=1.0,xx=0.0;
        if(p>=0){
          hs=homseq.next;
          for(n=0;n<nhom;n++,hs=hs->next){
            weight[n]=0;
            if(simm[n]>0&&hs->len>p){
              occ[n]=seq2num[(int)hs->text[p]];
              assert(occ[n]>=0);
/* if(occ[n]<0){ fprintf(stderr,"fatal: %c[%d]\n->%s<-\n",hs->text[p],p,hs->text); exit(-1);} */
              if(occ[n]<Seq_obj){
                weight[n]=1.0;
                ss+=simm[n];
                dd+=1.0-simm[n];}}}
          for(n=0;n<nhom;n++){
            float x=0.0;
            float y=0.0;
            float z=0.0;
            int i;
            if(!weight[n])
              continue;
            for(i=0;i<nhom;i++){
              if(weight[i]>0){
                x+=sim[n][i];
                y+=1.0-sim[n][i];
                z+=(1.0-sim[n][i])*(1.0-sim[n][i]);}}
            if(aggressive){
              if(farhigh){
                da+=(1.0-simm[n])*(1.0-simm[n]);
                xx+=z=1.0/(1.0+z);
                prof[occ[n]]+=z;}
              else{
                da+=1.0-simm[n];
                xx+=y=1.0/(1.0+y);
                prof[occ[n]]+=y;}}
            else{
              if(farhigh){
                prof[occ[n]]+=x;}
              else{
                prof[occ[n]]+=simm[n]*x;}}
            }}
        if(aggressive){
          prof[seq2num[useq[s]]]+=1.0/da;
          norm1(prof,Seq_obj);
          x=xx+1.0;}
        else{
          prof[seq2num[useq[s]]]+=dd;
          if(farhigh){
            x=norm1(prof,Seq_obj)/(1.0+nhom);}
          else{
            x=norm1(prof,Seq_obj)/ss;}}}
      else{
        prof[seq2num[useq[s]]]=1.0;
        if(p>=0){
          hs=homseq.next;
          for(n=0;n<nhom;n++,hs=hs->next){
            int i=seq2num[(hs->text)[p]];
            if(i<Seq_obj)
              prof[i]++;}}
        x=norm1(prof,Seq_obj);}
      /* print profile */
      for(n=0;n<Seq_obj;n++){
        if(prof[n])
          fprintf(stdout,"%.5f\t",prof[n]);
        else
          fprintf(stdout,"0\t");}
      fprintf(stdout,"%.3f\t%c\n",x,useq[s]);
      memcpy(bprof+s,prof,sizeof(float)*Seq_obj);
      bprof[s][Seq_obj]=x;}
  }
  if(nlen>1){
    char *file="bank";
    int fd;
    if(argc>2)
      file=argv[2];
    fd=open(file,O_WRONLY|O_APPEND|O_CREAT,0644);    
    if(fd<0){
      fprintf(stderr,"Could not open %s\n",file);
      exit(-1);}
    write(fd,&uslen,sizeof(int));
    write(fd,&nlen,sizeof(int));
    write(fd,name,sizeof(char)*nlen);
    write(fd,useq,sizeof(char)*uslen);
    write(fd,bprof,sizeof(prof_t)*uslen);
    close(fd);}
}
