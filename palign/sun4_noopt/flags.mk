FFLAG2=     -O2 -C -i4 -r8 -e -w


# >= -O3 doesn't work with pointers

CPPFLAG= -I./  -DBIGREAL  -DSMALLINTEGER  -DSUN \
	-DSMALLADDR  -DTRIMA_PROBLEM  -DHAVE_MALLOC  -DHAVE_POINTERS
FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 
LINK=	f77 $(FFLAG) -lV77
F77=	f77
CPP=  cpp
CC=	cc 
CCFLAG= -g -O $(CPPFLAG)
CD=	cd

MAINDIR= /home/md/ae/source/source5
LIBDIR= ../../prgrm/kino



OBJDIR= .
BINDIR= $(HOME)/source/source5/sun4_noopt


# HOMEDIR = /franklin2/users/arne/bowie/source
# HOMEDIR = /escher/users/arne/grail/source

MAKE= make

.SUFFIXES: .f .h .o .fcm .FCM .src

sippl.o: sippl.f sippl.src
	$(CPP) -P $(CPPFLAG) $(SIZE) sippl.src > sippl.cpp
#	awk -f split.awk sippl.cpp > sippl.f
	sed "s/\\\T/\	/g" sippl.cpp > sippl.f
	$(F77) -u -O2 -C -i4 -r8 -e -silent  -c  -I./ sippl.f
	rm -f sippl.cpp
	rm -f sippl.f


.src.f:	
	$(CPP) -P $(CPPFLAG) $(SIZE) $*.src > $*.cpp
#	awk -f split.awk $*.cpp > $*.f
	sed "s/\\\T/\	/g"  $*.cpp > $*.f
.f.o:
	$(F77) -c $(FFLAG) -I./ $*.f
	rm -f $*.cpp
	rm -f $*.f

.src.o:	
	$(CPP) -P $(CPPFLAG) $(SIZE) $*.src > $*.cpp
#	awk -f split.awk $*.cpp > $*.f
	sed "s/\\\T/\	/g"  $*.cpp > $*.f
	$(F77) -c $(FFLAG) -I./ $*.f
	rm -f $*.cpp
	rm -f $*.f

.c.o:	
	$(CC) -c $(CCFLAG)  $*.c

# Special dependencies for .src files 
