#include <stdio.h>
#include <math.h>
#include <dynprog_C.h>
#include <seqio_C.h>
//#include <dynprog_C.h>
//int oldy_.oldy=-99999;
float v[maxaa+2];
//int scnt=0;
//int dcnt=0;
//int box1=0;
//*********************************************************************
float scoreAverage(float p[], float q[], int num, int true)
{  // scoreAverage returns is the dot prod between p & q, multiplied with a probability matrix.
  

  int BLOSUM62[20][20]={4, 0,-2,-1,-2, 0,-2,-1,-1,-1,-1,-2,-1,-1,-1, 1, 0, 0,-3,-2, 
			0, 9,-3,-4,-2,-3,-3,-1,-3,-1,-1,-3,-3,-3,-3,-1,-1,-1,-2,-2, 
		       -2,-3, 6, 2,-3,-1,-1,-3,-1,-4,-3, 1,-1, 0,-2, 0, 1,-3,-4,-3, 
		       -1,-4, 2, 5,-3,-2, 0,-3, 1,-3,-2, 0,-1, 2, 0, 0,-1,-2,-3,-2, 
		       -2,-2,-3,-3, 6,-3,-1, 0,-3, 0, 0,-3,-4,-3,-3,-2,-2,-1, 1, 3, 
			0,-3,-1,-2,-3, 6,-2,-4,-2,-4,-3, 0,-2,-2,-2, 0,-2,-3,-2,-3, 
		       -2,-3,-1, 0,-1,-2, 8,-3,-1,-3,-2, 1,-2, 0, 0,-1,-2,-3,-2, 2, 
		       -1,-1,-3,-3, 0,-4,-3, 4,-3, 2, 1,-3,-3,-3,-3,-2,-1, 3,-3,-1, 
		       -1,-3,-1, 1,-3,-2,-1,-3, 5,-2,-1, 0,-1, 1, 2, 0,-1,-2,-3,-2, 
		       -1,-1,-4,-3, 0,-4,-3, 2,-2, 4, 2,-3,-3,-2,-2,-2,-1, 1,-2,-1, 
		       -1,-1,-3,-2, 0,-3,-2, 1,-1, 2, 5,-2,-2, 0,-1,-1,-1, 1,-1,-1, 
		       -2,-3, 1, 0,-3, 0, 1,-3, 0,-3,-2, 6,-2, 0, 0, 1, 0,-3,-4,-2, 
		       -1,-3,-1,-1,-4,-2,-2,-3,-1,-3,-2,-2, 7,-1,-2,-1,-1,-2,-4,-3, 
		       -1,-3, 0, 2,-3,-2, 0,-3, 1,-2, 0, 0,-1, 5, 1, 0,-1,-2,-2,-1, 
		       -1,-3,-2, 0,-3,-2, 0,-3, 2,-2,-1, 0,-2, 1, 5,-1,-1,-3,-3,-2, 
			1,-1, 0, 0,-2, 0,-1,-2, 0,-2,-1, 1,-1, 0,-1, 4, 1,-2,-3,-2, 
			0,-1,-1,-1,-2,-2,-2,-1,-1,-1,-1, 0,-1,-1,-1, 1, 5, 0,-2,-2, 
			0,-1,-3,-2,-1,-3,-3, 3,-2, 1, 1,-3,-2,-2,-3,-2, 0, 4,-3,-1, 
		       -3,-2,-4,-3, 1,-2,-2,-3,-3,-2,-1,-4,-4,-2,-3,-3,-2,-3,11, 2, 
		       -2,-2,-3,-2, 3,-3, 2,-1,-2,-1,-1,-2,-3,-1,-2,-2,-2,-1, 2, 7}; 

  float logtwo=0.693147;
  //  float score=0.0;
  float sum1=0.0, sum=0.0;  
  int k, l, i;
  float s;
  /*Below is the true scoring!*/
    
  if(true!=0) // if we're using a new profile vector do
  {//printf("not true\n");
  
    for(i=0;i<20;i++)
      {v[i]=0;
      for(k=0;k<20;k++){v[i]+=p[k]*exp((logtwo/2)*BLOSUM62[k][i]);} 
      }
    for(l=0;l<20; l++){sum += v[l]*q[l];}
    }else // if we're using the previous profile vector only do this
      {//printf("true\n");
	for(l=0;l<20; l++){sum += v[l]*q[l];}
      }
  
/*
    for(i=0;i<20;i++)
    {s=0;
    if(p[i]==0&&q[i]!=0){p[i]=1.e-10;}
    if(p[i]==0 && q[i]==0)
      {s=0;
      }else{
	s=q[i]*log(p[i]);} 
    sum1+=s;
    }
  
	
  */

  for(k=0; k<20; k++)//*num or 20?
    {
      for(l=0; l<20; l++)//*num or 20?
	{
	  sum += p[k]*q[l]*exp((logtwo/2)*BLOSUM62[k][l]);// max k,l = 20 for BLOSUM62
	  //	  printf("%d, %d, %f, %f, %f\n",k, l, p[k], q[l], sum);
	}
    }
  
  //  sum=exp(sum1);
  //  printf("--------------------%f\n",sum);
  return(sum);
}
#ifdef GNU
float profprof_logaver__(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#else
float profprof_logaver_(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#endif
{
  float p[maxaa+2], q[maxaa+2], t[20];
  float score=0.0;
  int k=0;
  int x=*X-1;
  int y=*Y-1;
  int true=1;
  float similar=0;
  float ssScore=0.00, psum=0, qsum=0;

  float normp=0,normq=0;
//initfreq_.initfreq[20] in alphabetical order

//  for(k=0; k<*num; k++){   printf("%d %f\n",k,initfreq_.initfreq[k]);  }
  //  exit(0);
  for(k=0; k<*num; k++)
    {
      //p[k]=PROFILE[k][y];
      q[k]=SEQ[k][x];
      p[k]=SEQ[k][x];
      //q[k]=initfreq_.initfreq[k];
      //psum+=p[k]*p[k];
      //qsum+=q[k]*q[k];

      //      similar+=(p[k]*q[k]);
      //normp+=p[k]*p[k];
      //normq+=q[k]*q[k];
    }
  //normp=sqrt(normp);
  //  normq=sqrt(normq);
  //similar=(similar/(normp*normq));
  
  if(PROFSS[0][y] == SEQSS[0][x] && PROFSS[0][y]!=-1)
    {ssScore=0.12;} // =ge
  else if(PROFSS[0][y]!=-1 && SEQSS[0][x]!=-1)
    {ssScore=-0.12;}

  
    if(oldy_.oldy==y){
   true=0; }

  // testing a thing score is really as seen below!!
  // score=log(scoreAverage(p,q,*num,true))+*shift + ssScore; // natural logarithm
  score=log(scoreAverage(p,q,*num,true))+*shift ; // natural logarithm
  
  //score=scoreAverage(p,q,*num,true)+*shift;// test GM

  oldy_.oldy=y;
  
  //  printf("%f %f %f %f %f\n",PROFILE[0][0],SEQ[0][0], ssScore,score,log(scoreAverage(p,q,*num,true))+*shift );
  // printf("%f %f \n",psum, qsum);
  //printf("%f\n",score);

    //  if(similar <= 0.05 && box1 < 100000){box1++;printf("#bad <\t%f\t>\n",score);}
  //  if(box1 < 100000){box1++;printf("#100 <\t%f\t>\n",score);} //perfect match
    //  if(box1 < 100000){box1++;printf("#neu <%f>\n",score);} //neutral match
  return(score);
}
/*
  Reference:
  von �hsen N. et al
  WABI 2001.
  Improving prof-prof alignm via log average scoring
*/
  
 
