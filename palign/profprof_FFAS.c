#include <stdio.h>
#include <math.h>
#include <dynprog_C.h>
#include <seqio_C.h>
//int scnt=0;
//int dcnt=0;
//int box1=0;
//-------- Fix matrix A, using DP.-------------
// A = BLOSUM62
// A should be a matrix with alignment scores between seqs
// i.e. A[i][j] = alignm score of seqs i and j calc with BLOSUM62

// i = seq nbr, num = # seqs

#define max(A,B) ((A) > (B) ? (A) : (B))
#define min(A,B) ((A) < (B) ? (A) : (B))

//float diversity_score(int i, int num)
//{
/*
  int A[20][20]={4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0,
		-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3,
       		-2,  0,  6,  1, -3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3,
		-2, -2,  1,  6, -3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3,
		 0, -3, -3, -3,  9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, 
		-1,  1,  0,  0, -3,  5,  2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2,
		-1,  0,  0,  2, -4,  2,  5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2, 
		 0, -2,  0, -1, -3, -2, -2,  6, -2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3,
		-2,  0,  1, -1, -3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3,
		-1, -3, -3, -3, -1, -3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3,
	      	-1, -2, -3, -4, -1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, 
		-1,  2,  0, -1, -3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2, 
		-1, -1, -2, -3, -1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1,
		-2, -3, -3, -3, -2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1,
		-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2,
		 1, -1,  1,  0, -1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2,
		 0, -1,  0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0,
		-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3,
		-2, -2, -2, -3, -2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1,
		 0, -3, -3, -3, -1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4};
  float score=0.0, sumS=0.0, tmpSum=0.0;
  int j;

  for(j=0; j<num; j++)
    {
      tmpSum=max(  A[i][j]/min(A[i][i],A[j][j]), 0);
      sumS += tmpSum*tmpSum;
    }
  
  score=1/(1+sumS);
  return(score);
}
*/

//***************************************************
final_frac(float prob_frac[], float f_new[], int col, int num)
{
 int BLOSUM62[20][20]={ 4, 0,-2,-1,-2, 0,-2,-1,-1,-1,-1,-2,-1,-1,-1, 1, 0, 0,-3,-2, 
			0, 9,-3,-4,-2,-3,-3,-1,-3,-1,-1,-3,-3,-3,-3,-1,-1,-1,-2,-2, 
		       -2,-3, 6, 2,-3,-1,-1,-3,-1,-4,-3, 1,-1, 0,-2, 0, 1,-3,-4,-3, 
		       -1,-4, 2, 5,-3,-2, 0,-3, 1,-3,-2, 0,-1, 2, 0, 0,-1,-2,-3,-2, 
		       -2,-2,-3,-3, 6,-3,-1, 0,-3, 0, 0,-3,-4,-3,-3,-2,-2,-1, 1, 3, 
			0,-3,-1,-2,-3, 6,-2,-4,-2,-4,-3, 0,-2,-2,-2, 0,-2,-3,-2,-3, 
		       -2,-3,-1, 0,-1,-2, 8,-3,-1,-3,-2, 1,-2, 0, 0,-1,-2,-3,-2, 2, 
		       -1,-1,-3,-3, 0,-4,-3, 4,-3, 2, 1,-3,-3,-3,-3,-2,-1, 3,-3,-1, 
		       -1,-3,-1, 1,-3,-2,-1,-3, 5,-2,-1, 0,-1, 1, 2, 0,-1,-2,-3,-2, 
		       -1,-1,-4,-3, 0,-4,-3, 2,-2, 4, 2,-3,-3,-2,-2,-2,-1, 1,-2,-1, 
		       -1,-1,-3,-2, 0,-3,-2, 1,-1, 2, 5,-2,-2, 0,-1,-1,-1, 1,-1,-1, 
		       -2,-3, 1, 0,-3, 0, 1,-3, 0,-3,-2, 6,-2, 0, 0, 1, 0,-3,-4,-2, 
		       -1,-3,-1,-1,-4,-2,-2,-3,-1,-3,-2,-2, 7,-1,-2,-1,-1,-2,-4,-3, 
		       -1,-3, 0, 2,-3,-2, 0,-3, 1,-2, 0, 0,-1, 5, 1, 0,-1,-2,-2,-1, 
		       -1,-3,-2, 0,-3,-2, 0,-3, 2,-2,-1, 0,-2, 1, 5,-1,-1,-3,-3,-2, 
			1,-1, 0, 0,-2, 0,-1,-2, 0,-2,-1, 1,-1, 0,-1, 4, 1,-2,-3,-2, 
			0,-1,-1,-1,-2,-2,-2,-1,-1,-1,-1, 0,-1,-1,-1, 1, 5, 0,-2,-2, 
			0,-1,-3,-2,-1,-3,-3, 3,-2, 1, 1,-3,-2,-2,-3,-2, 0, 4,-3,-1, 
		       -3,-2,-4,-3, 1,-2,-2,-3,-3,-2,-1,-4,-4,-2,-3,-3,-2,-3,11, 2, 
		       -2,-2,-3,-2, 3,-3, 2,-1,-2,-1,-1,-2,-3,-1,-2,-2,-2,-1, 2, 7}; 
//initfreq[20] in alphabetical order
 
  float f1[maxaa+2], f2[maxaa+2];
  float sum_D=0.0, sqSum_f2=0.0;
  int k;
 
  for(k = 0; k < num; k++)//f'_a, prob_frac[] = f_a
    {
      f1[k] = prob_frac[k]*initfreq_.initfreq[k];
    }
 
  for(k=0; k<20; k++)// 20 or *num?
    {
      f2[k]=5*f1[k] + prob_frac[k];//*sum_D;
      sqSum_f2 += f2[k]*f2[k];
    }
  
  for(k=0; k<20; k++)// 20 or *num?
    {
      f_new[k]=f2[k]*f2[k]/sqSum_f2;
    }
}

#ifdef GNU
float profprof_ffas__(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#else
float profprof_ffas_(int *Y, int *X, float PROFILE[maxaa+2][maxproflen], float SEQ[maxaa+2][maxproflen], int  PROFSS[1][maxproflen], int SEQSS[1][maxproflen], int *num, float *shift, float *lambda)
#endif
{
  float Pf1[maxaa+2], Pf3[maxaa+2];
  float Sf1[maxaa+2], Sf3[maxaa+2];
  float Comp_score=0.0;
  float scalar_prod=0.0;
  int k;  
  int x=*X-1; 
  int y=*Y-1;
  int aanum=*num;
  float ssScore=0;
  float similar=0;
  float Psq=0,Ssq=0,Plength=0,Slength=0;
  float normp=0,normq=0;
//initfreq[20] in alphabetical order

  for(k=0; k<*num; k++)
    {
      Pf1[k]=PROFILE[k][y];
      //    Pf1[k]=initfreq_.initfreq[k];
      Sf1[k]=SEQ[k][x];
      //      Sf1[k]=PROFILE[k][y];
      //      Sf1[k]=initfreq_.initfreq[k];
    }
    
  final_frac(Pf1,Pf3, y, aanum);
  final_frac(Sf1,Sf3, x, aanum);
  //  printf("%d, %d",PROFSS[0][y],SEQSS[0][x]);
  
  if(PROFSS[0][y] == SEQSS[0][x] && PROFSS[0][y]!=-1)
    {ssScore=0.0;}
  else if(PROFSS[0][y]!=-1 && SEQSS[0][x]!=-1)
    {ssScore=-0.0;}
  
  // test: try to first make the vectors length =1
  if (*lambda >1){
    for(k=0; k<*num; k++)// determine the length of the vectors
    {
      Psq+=Pf3[k]*Pf3[k];
      Ssq+=Sf3[k]*Sf3[k];
    }
  Plength=sqrt(Psq);
  Slength=sqrt(Ssq);
  for(k=0; k<*num; k++)// normalize the vectors
    {
      Pf3[k]=Pf3[k]/Plength;
      Sf3[k]=Sf3[k]/Slength;
    }
  }
  for(k=0; k<*num; k++)// 20 or num as upper limit?
    {// dot product between vectors from the two profiles.
      scalar_prod += Pf3[k]*Sf3[k];
      //scalar_prod += Sf3[k]*Sf3[k];
    }
  Comp_score=scalar_prod+*shift + ssScore;
  // if(x==y){
  //  printf("TEST-t-prfprf %d %d %f\n",x, y, scalar_prod);}
 

  //  if(similar <= 0.05 && box1 < 100000){box1++;printf("#bad <\t%f\t>\n",Comp_score);}
  //  if(box1 < 100000){box1++;printf("#100 <\t%f\t>\n",Comp_score);} //perfect match
  //if(box1 < 100000){box1++;printf("#neu <\t%f\t>\n",Comp_score);} //neutral match
 return(Comp_score);
}
/*
  Reference:
  Rychlewski L. et al
  Prot sci 2000. 9:232-241
*/
