//***************************************************
// The FFAS method by Rychlewski et al.
//***************************************************

// i = seq nbr, num = # seqs
float diversity_score(int i, int num)
{
  float score, sumS = 0.0, tmpSum = 0.0;
  int j;

  extern float A[][62]; //  <-------------------------- ?

  for(j = 0; j < num; j++)
    {
      tmpSum = max(  A[i][j]/min(A[i][i], A[j][j]) , 0);
      sumS += tmpSum*tmpSum;
    }

  score = 1/(1+sumS);
  return(score);
}


//***************************************************
float final_frac(float prob_frac[], int num)
{
  float f2[], f3[];
  float sum_D = 0.0, sum_f2 = 0.0;
  extern float aa_frac[];//  <------------------ ?
  int k;

  for(k = 0; k < num; k++)
    {
      sum_D += diversity_score(k,num);
    }

  for(k = 0; k < 20; k++)
    {
      f2[k] = 5*prob_frac[k] + aa_frac[k]*sum_D;
      sum_f2 += f2[k]*f2[k];
    }

  for(k = 0; k < 20; k++)
    {
      f3[k] = f2[k]*f2[k] / sum_f2;
    }

  return(f3);
}

float FFAS_profprof_(int *Y, int *X, float PROFILE[62][6500], float SEQ[62][6500], int *num)
{
  float  Pf1[62], Pf3[62];
  float  Sf1[62], Sf3[62];
  float Comp_score = 0.0;
  int k;  
  int x = *X - 1; 
  int y = *Y - 1;

  for(k = 0; k < *num; k++)
    {
      Pf1[k] = PROFILE[k][y];
      Sf1[k] = SEQ[k][x];
    }

  Pf3 = final_frac(Pf1, num);
  Sf3 = final_frac(Sf1, num);

  for(k = 0; k < 20; k++)// 20 or num as upper limit?
    {
      Comp_score += Pf3[k]*Sf3[k];
    }
 
   return(Comp_score);
 }
