C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
c*******************************************************************************

      subroutine dyntrace(profile,proflen,seq,seqlen,type,dynscore,traceback)

c type=0 => local
c type=1 => ends free
c type=10 => global 

      implicit none
#include <dynprog.h>
      
      real bestscore,maxscorex,maxscorey
      integer x, y, type,bestx,besty,i,maxy,maxx
      real profile(maxproflen,maxaa+2),wgtend,dynscore
      real seqskip, profskip(maxseqlen),typefact,minscore
      real prescore(maxseqlen),  lastscore, bestjump
      real score, diagtest,  proftest
      real temp   
      integer seqlen,proflen,seq(maxseqlen)
      integer xminone,traceback(maxseqlen),yminone
      integer prex(maxproflen,maxseqlen),prey(maxproflen,maxseqlen)
      integer xlast,ylast(maxproflen)
      typefact=0
      

C
c     init dimensions and bestscore
c     do the bottom row (y=1)
      bestx=1
      besty=1
      wgtend=0
      prex(1,1)=0
      prey(1,1)=0
      if (type .eq. 10) then
        typefact=1
        minscore=-9999.0
        bestscore = -9999.0
        profskip(1)=-9999.0
        prescore(1)=profile(1,seq(1))
        maxscorey=minscore
        maxscorex=minscore
C        write (*,*) 'test1:',1,1,prescore(1)
C        bestscore = max(bestscore,score)
        wgtend=profile(1,penopen)
        ylast(1)=1
C        write (*,*) 'test1:',1,1,prescore(1),' ',prex(1,1),' ',prey(1,1)
        do x = 2, seqlen
          ylast(x)=1
          wgtend=wgtend+profile(1,penext)
          prescore(x)=profile(1,seq(x))+wgtend
          prex(1,x)=x-1
          prey(1,x)=0
C          bestscore = max(bestscore,score)
          if (prescore(x) .gt. bestscore) then
            bestscore=prescore(x)
            bestx=x
            besty=1
          end if
          profskip(x) = -9999.0
C          write (*,*) 'test1:',x,1,prescore(x),' ',prex(1,x),' ',prey(1,x)
        end do
      elseif (type .eq. 1) then
         maxx=1;
        typefact=0
        minscore=-9999.0
        bestscore = -9999.0
        maxscorex = -9999.0
        maxscorey = 0
C        write (*,*) 'test1:',1,1,prescore(1),' ',prex(1,1),' ',prey(1,1)
        do x = 1, seqlen
C          prescore(x) = max(profile(1,seq(x)),minscore)
          score=profile(1,seq(x))
          if (minscore .gt. score) then
            prescore(x)=minscore
            prex(1,x)=-1
            prey(1,x)=-1
            ylast(x)=0
          else
            prescore(x)=score
            prex(1,x)=x-1
            prey(1,x)=0
            ylast(x)=x
          endif
          profskip(x) = -9999.0

C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
C            write(*,*)'TESTING',x,score
            bestscore=score
            bestx=x
            besty=1
          end if
C          maxscorey = max(maxscorey,score+profile(x,penopen))+profile(1,penext)
          if (maxscorey .lt. score+profile(x,penopen)) then
             maxx=x
             maxscorey=score+profile(x,penopen)
          end if
          maxscorey=maxscorey+profile(1,penext)
C          write (*,*) 'test1:',x,1,seq(x),profile(1,seq(x)),' ',prex(1,x),' ',prey(1,x),bestx,besty
        end do
C        maxscorey = max(maxscorey,score)
        if (maxscorey .lt. score) then
           maxx=seqlen
           maxscorey=score
        end if

        maxy=1
      elseif  (type .eq. 0) then
        typefact=0
        minscore=0.0
        bestscore = minscore
        do x = 1, seqlen
C          prescore(x) = max(profile(1,seq(x)),minscore)
          score=profile(1,seq(x))
          if (minscore .gt. score) then
            prescore(x)=minscore
            prex(1,x)=-1
            prey(1,x)=-1
            ylast(x)=0
          else
            prescore(x)=score
            prex(1,x)=x-1
            prey(1,x)=0
            ylast(x)=x
          endif
          profskip(x) = -9999.0

C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
C            write(*,*)'TESTING',x,score
            bestscore=score
            bestx=x
            besty=1
          end if
C          write (*,*) 'test1:',x,1,seq(x),profile(1,seq(x)),' ',prex(1,x),' ',prey(1,x),bestx,besty
        end do
      else
        call stopprocess('Unknown alignment methods')
      end if
C
      score=minscore
      maxy=1
      wgtend = (profile(1,penopen))*typefact
      seqskip=max(minscore,profile(1,penopen)+profile(1,penext))
C      write(*,*) 'TEST0',bestscore,type,profile(1,penopen),profile(1,penext),bestx,besty
      do y = 2, proflen
        yminone=y-1
C        maxscorey=max(maxscorey,score+profile(yminone,penopen))+profile(yminone,penext)
        if (maxscorey.lt. score+profile(yminone,penopen)*typefact) then
          maxy=yminone
          maxscorey=score+profile(yminone,penopen)*typefact
        end if
        maxscorey=maxscorey+profile(yminone,penext)*typefact
c     for x=1 
        x = 1
        xlast=1
C        score = max(minscore,(profile(y,seq(x))+wgtend))
        if (minscore .gt. profile(y,seq(x))) then
            score=minscore
            prex(y,x)=-1
            prey(y,x)=-1
          else
C     Shouldn't it be
            score=profile(y,seq(x))
C            score=profile(yminone,seq(x))
            prex(y,x)=0
            prey(y,x)=yminone
          endif

        lastscore = score
C        bestscore = max(bestscore,score)
        if (score .gt. bestscore) then
          bestscore=score
          bestx=x
          besty=y
        end if
        seqskip = minscore
C
C        write (*,*) 'TEST3:',y,score,bestscore,bestx,besty
        wgtend = wgtend+profile(1,penext)*typefact
C        write (*,*) 'TEST2:',score,wgtend,startfact

C        write (*,*) 'TEST2:',x,y,score,prescore(x),' ',prex(y,x),' ',prey(y,x)
        do x = 2, seqlen
          xminone=x-1

C          write(*,*)'test: ',y,x,seq(x),profile(y,seq(x))
          temp = profile(y,seq(x))
          diagtest = prescore(xminone)
          proftest = profskip(xminone)

C          write (*,*) 'TEST: ',y,x,seq(x),diagtest,proftest,seqskip,temp
C          score=max(diagtest,max(proftest,seqskip))+temp
          if (diagtest .ge. seqskip .and. diagtest .ge. proftest) then
            score=diagtest+temp
            if (diagtest.eq.minscore) then
              prex(y,x)=-1
              prey(y,x)=-1
            else
              prex(y,x)=xminone
              prey(y,x)=yminone
              end if
          elseif (proftest .ge. seqskip) then
            score=proftest+temp
            prex(y,x)=xminone
            prey(y,x)=ylast(xminone)
          else
            score=seqskip+temp
            if (minscore .eq. seqskip) then
               prex(y,x)=-1
               prey(y,x)=-1
            else
               prex(y,x)=xlast
               prey(y,x)=yminone
            end if
         end if
C          score = max(score,minscore)
          if (minscore .gt. score) then
            score=minscore
            prex(y,x)=-1
            prey(y,x)=-1
          endif

C          bestscore = max(bestscore,score)
          if (score .gt. bestscore) then
            bestscore=score
            bestx=x
            besty=y
          end if

          bestjump = prescore(xminone) + profile(y,penopen)

C          seqskip = max(seqskip,bestjump)
          if (bestjump .gt. seqskip) then 
            seqskip =bestjump
            xlast=xminone
C            write  (*,*) 'changed seqskip'
          end if

          seqskip = seqskip + profile(y,penext)

C          profskip(x) = max(bestjump,profskip(xminone))
          if (profskip(xminone) .lt. bestjump) then
            profskip(xminone) = bestjump
            ylast(xminone)=yminone
C            write  (*,*)'changed profskip',x
          endif

          profskip(xminone) = profskip(xminone) + profile(y,penext)
C          write (*,*)'test4:',bestjump,profskip(xminone),xminone
          prescore(xminone) = lastscore
          lastscore = score
C          write (*,*) 'TEST3:',x,y,score,prescore(x),prex(y,x),prey(y,x),maxscorey,maxscorex
        end do
        prescore(1)=profile(y,seq(1))+wgtend*typefact
C        profskip(1)=profile(y,seq(x))+wgtend
C       maxscorex=max(maxscorex,score)        
C        if (maxscorex .lt. score) then
C           
C        end if
      end do


C      maxscorey=max(score,maxscorey+profile(proflen,penopen)+profile(proflen,penext))
C      write(*,*)'dyntrace1',score,maxscorey,maxscorex,bestscore
C      if (maxscorey+profile(proflen,penopen)+seqlen*profile(proflen,penext).lt. score) then
      if (maxscorey .lt. score) then
        maxy=proflen-1
        maxscorey=score
C      else
C        maxscorey=maxscorey
      end if

C      write(*,*)'dyntrace',score,maxscorey,maxscorex,bestscore

      maxscorex=max(minscore,score)
      maxx=seqlen
      temp=profile(proflen,penopen)*typefact
C Not really sure if this is correct for global alignmens /AE 9/10
      do x = seqlen-1,1,-1
        temp=temp+profile(proflen,penext)*typefact
C        maxscorex=max(maxscorex,prescore(x)+temp)
C        write(*,*)'MAXX-test',x,maxscorex,prescore(x),temp
        if (maxscorex .le. prescore(x)+temp) then
          maxx=x
          maxscorex=prescore(x)+temp
        end if
      end do


C      write(*,*)'TRACE-test> ',score,maxscorey,maxscorex,maxx,maxy,bestx,besty
      if (type .eq. 10 ) then

C         write(*,*)'TEST ',score,maxscorey,maxscorex
C        dynscore=max(score,max(maxscorey,maxscorex))
        if (score .gt. maxscorey .and. score .gt. maxscorex) then
C           write(*,*) 'TEST: all length'
          dynscore=score
          bestx=seqlen
          besty=proflen
        else if ( maxscorey .gt. maxscorex) then
C           write(*,*) 'TEST: maxscorey',maxy
          dynscore=maxscorey
          bestx=seqlen
          besty=maxy
        else 
C           write(*,*) 'TEST: maxscorex',maxx
          dynscore=maxscorex
          bestx=maxx
          besty=proflen
        end if
      else if (type .eq. 1 ) then
C         write(*,*)'TEST ',score,maxscorey,maxscorex
        if (score .gt. maxscorey .and. score .gt. maxscorex) then
          dynscore=score
          bestx=seqlen
          besty=proflen
        else if ( maxscorey .gt. maxscorex) then
          dynscore=maxscorey
          bestx=seqlen
          besty=maxy
        else 
          dynscore=maxscorex
          bestx=maxx
          besty=proflen
        end if
      elseif (type .eq. 0) then
C         write(*,*)'TEST ',bestscore,score,maxscorey,maxscorex
        dynscore=bestscore
      end if

      do x=1,seqlen
        traceback(x)=0.
      end do

      y=besty
      x=bestx

      do while (y.gt.0.and.x.gt.0)
C          write(*,'(a,4i6)')'TRACE> ',x,y,prex(y,x),prey(y,x)
          traceback(x)=y
          i=x
          x=prex(y,x)
          y=prey(y,i)
      end do

      return
      end

