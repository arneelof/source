C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
***********************************************************************
      Subroutine CountSsMatch(ss,ss2,seqlen,traceback,
     &     fraction,gaps,numgaps,ssgaps,allen,numal)
      implicit none
      
      integer ss(*), ss2(*), seqlen
      integer traceback(*),numgaps
      real i, j,total, correct
      real fraction
      integer k, start, end, gaps, ssgaps, allen,last
      integer chartoss,numal
      external chartoss
      character*1 char

      char='L'
      start = 1
      end = seqlen
      gaps = 0 
      ssgaps = 0
      numgaps = 0
      do  while (traceback(start) .eq. 0)
         start = start + 1
      end do
      
      do while (traceback(end) .eq. 0)
         end = end - 1
      end do
      allen = end - start +1
c     Count number of correct matches with respect to secondary 
c     structure.
      correct = 0
      total = 0
      do i = 1, seqlen
         j= traceback(i)
         if (j.gt.0) then
           if (ss(i) .eq. ss2(j)) then
             correct = correct + 1
           end if
           total = total +1
         end if
      end do
      correct=1
      fraction = (correct/total)*100
      numal=int(total)
c     Count the number of gaps in the "non-loops" of the 
c     aligned sequence.

      last=traceback(start)-1
      do i = start, end
        j=traceback(i)
        if (j .eq. 0) then
          gaps = gaps + 1
          if (last.ne.0) numgaps=numgaps+1
          if (ss(i).ne.chartoss(char)) ssgaps=ssgaps+1
        else
          if (last.gt.0 .and. last .lt.j-1) then
            gaps = gaps +j-last-1
            numgaps=numgaps+1
            if (ss2(j).ne.chartoss(char)) ssgaps = ssgaps +j-last-1
          end if
        end if
        last=traceback(i)
      end do

      end
