#FFLAG2=    -O3 -static    -funroll-loops -Wall  \
#	-ffixed-line-length-none -finline-functions -fomit-frame-pointer -Wimplicit -DGNU

#FFLAG2= -O3 -132 -rcd -tpp6  -w95  -fpp  -Vaxlib -DIFC -w
#FFLAG2= -O3 -132 -rcd -tpp6 -axiMK -xiMK -rcd -ipo -w95  -fpp  -Vaxlib -DIFC -w
OPTFLAG= -O3  -unroll  -rcd -tpp6 -ipo
FFLAG2=  $(OPTFLAG)  -132 -w95  -fpp  -Vaxlib  -w
# To test -pc32  -opt_report
#FFLAG2=  -132  -w95  -fpp  -Vaxlib -DIFC -w


#CPPFLAG= -I./ \
#	 -DBIGREAL  -DSMALLINTEGER  -DGNU \
#	-DSMALLADDR  -DPSCAN_SMALL -DSMALL -DSTLEN_PROBLEMS


FFLAG=  -u  $(FFLAG2) $(CPPFLAG)
LFLAG=  
EXTRA= cstuff.o 

LINK=ifort
F77=ifort
CPP=  cpp
#CC=	/modules/gnu/gcc/linux/bin/gcc 
CC=icc
CCFLAG=  $(CPPFLAG) -DICC $(OPTFLAG)  -w -I./
LFLAG= $(FFLAG2)
CD=	cd

EXTENSION= f

OBJDIR= .
BINDIR= .

MAKE= make

.f.o:	
	$(F77) -c $(FFLAG) -I./ $*.f

.c.o:	
	$(CC) -c $(CCFLAG)  $*.c
