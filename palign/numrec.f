************************************************************************
* From Numerical Recipies
* To use you need your own copu, if not please do not use
************************************************************************
      SUBROUTINE JACOBI(A,N,NP,D,V,NROT)
      INTEGER n,np,nrot,NMAX
      PARAMETER (NMAX=100)
      real A(NP,NP),D(NP),V(NP,NP),B(NMAX),Z(NMAX)
      INTEGER i,ip,iq,j
      REAL c,g,h,s,sm,t,tau,theta,tresh
      DO 12 IP=1,N
        DO 11 IQ=1,N
          V(IP,IQ)=0.
11      CONTINUE
        V(IP,IP)=1.
12    CONTINUE
      DO 13 IP=1,N
        B(IP)=A(IP,IP)
        D(IP)=B(IP)
        Z(IP)=0.
13    CONTINUE
      NROT=0
      DO 24 I=1,50
        SM=0.
        DO 15 IP=1,N-1
          DO 14 IQ=IP+1,N
            SM=SM+ABS(A(IP,IQ))
14        CONTINUE
15      CONTINUE
        IF(SM.EQ.0.)RETURN
        IF(I.LT.4)THEN
          TRESH=0.2*SM/N**2
        ELSE
          TRESH=0.
        ENDIF
        DO 22 IP=1,N-1
          DO 21 IQ=IP+1,N
            G=100.*ABS(A(IP,IQ))
            IF((I.GT.4).AND.(ABS(D(IP))+G.EQ.ABS(D(IP)))
     *         .AND.(ABS(D(IQ))+G.EQ.ABS(D(IQ))))THEN
              A(IP,IQ)=0.
            ELSE IF(ABS(A(IP,IQ)).GT.TRESH)THEN
              H=D(IQ)-D(IP)
              IF(ABS(H)+G.EQ.ABS(H))THEN
                T=A(IP,IQ)/H
              ELSE
                THETA=0.5*H/A(IP,IQ)
                T=1./(ABS(THETA)+SQRT(1.+THETA**2))
                IF(THETA.LT.0.)T=-T
              ENDIF
              C=1./SQRT(1+T**2)
              S=T*C
              TAU=S/(1.+C)
              H=T*A(IP,IQ)
              Z(IP)=Z(IP)-H
              Z(IQ)=Z(IQ)+H
              D(IP)=D(IP)-H
              D(IQ)=D(IQ)+H
              A(IP,IQ)=0.
              DO 16 J=1,IP-1
                G=A(J,IP)
                H=A(J,IQ)
                A(J,IP)=G-S*(H+G*TAU)
                A(J,IQ)=H+S*(G-H*TAU)
16            CONTINUE
              DO 17 J=IP+1,IQ-1
                G=A(IP,J)
                H=A(J,IQ)
                A(IP,J)=G-S*(H+G*TAU)
                A(J,IQ)=H+S*(G-H*TAU)
17            CONTINUE
              DO 18 J=IQ+1,N
                G=A(IP,J)
                H=A(IQ,J)
                A(IP,J)=G-S*(H+G*TAU)
                A(IQ,J)=H+S*(G-H*TAU)
18            CONTINUE
              DO 19 J=1,N
                G=V(J,IP)
                H=V(J,IQ)
                V(J,IP)=G-S*(H+G*TAU)
                V(J,IQ)=H+S*(G-H*TAU)
19            CONTINUE
              NROT=NROT+1
            ENDIF
21        CONTINUE
22      CONTINUE
        DO 23 IP=1,N
          B(IP)=B(IP)+Z(IP)
          D(IP)=B(IP)
          Z(IP)=0.
23      CONTINUE
24    CONTINUE
      call stopprocess('JACOBI> 50 iterations should never happen')
      RETURN
      END

***************************************************************************
*Modified from Numerical Recipes

      real Function QROMB(FUNC,A,B)
      INTEGER JMAX,JMAXP,K,KM,j,l
      REAL a,b,func,ss,EPS,dss
      EXTERNAL func
      PARAMETER(EPS=1.E-6,JMAX=20,JMAXP=JMAX+1,K=5,KM=4)
      real S(JMAXP),H(JMAXP)
      H(1)=1.
      DO 11 J=1,JMAX
        CALL TRAPZD(FUNC,A,B,S(J),J)
        IF (J.GE.K) THEN
          L=J-KM
          CALL POLINT(H(L),S(L),K,0.,SS,DSS)

	  QROMB = SS

          IF (ABS(DSS).LT.EPS*ABS(SS)) RETURN
        ENDIF
        S(J+1)=S(J)
        H(J+1)=0.25*H(J)
11    CONTINUE
      call stopprocess('QROMB> Too many steps.')
      END

***************************************************************
* From Numerical Recipes

      SUBROUTINE POLINT(XA,YA,N,X,Y,DY)
      INTEGER n,NMAX
      PARAMETER (NMAX=10) 
      REAL dy,x,y,xa(n),ya(n)
      INTEGER i,m,ns
      REAL den,dif,dift,ho,hp,w,c(NMAX),d(NMAX)
      NS=1
      DIF=ABS(X-XA(1))
      DO 11 I=1,N 
        DIFT=ABS(X-XA(I))
        IF (DIFT.LT.DIF) THEN
          NS=I
          DIF=DIFT
        ENDIF
        C(I)=YA(I)
        D(I)=YA(I)
11    CONTINUE
      Y=YA(NS)
      NS=NS-1
      DO 13 M=1,N-1
        DO 12 I=1,N-M
          HO=XA(I)-X
          HP=XA(I+M)-X
          W=C(I+1)-D(I)
          DEN=HO-HP
          IF(DEN.EQ.0.)call stopprocess('Numrec>')
          DEN=W/DEN
          D(I)=HP*DEN
          C(I)=HO*DEN
12      CONTINUE
        IF (2*NS.LT.N-M)THEN
          DY=C(NS+1)
        ELSE
          DY=D(NS)
          NS=NS-1
        ENDIF
        Y=Y+DY
13    CONTINUE
      RETURN
      END

*******************************************************************
* From Numerical Recipes

      SUBROUTINE TRAPZD(FUNC,A,B,S,N)
      INTEGER n
      REAL a,b,s,func
      EXTERNAL func
      INTEGER it,j
      REAL del,sum,tnm,x
      IF (N.EQ.1) THEN
        S=0.5*(B-A)*(FUNC(A)+FUNC(B))
        IT=1
      ELSE
        TNM=IT
        DEL=(B-A)/TNM
        X=A+0.5*DEL
        SUM=0.
        DO 11 J=1,IT
          SUM=SUM+FUNC(X)
          X=X+DEL
11      CONTINUE
        S=0.5*(S+(B-A)*SUM/TNM)
        IT=2*IT
      ENDIF
      RETURN
      END

**********************************************************************
* From NumRecipes

      real FUNCTION CEL(QQC,PP,AA,BB)
      real ca, pio2, a,b,p,e,aa,bb,pp,qc,em,g,qqc,f,q
      PARAMETER (CA=.0003, PIO2=1.5707963268)
      IF(QQC.EQ.0.)call stopprocess('Numrec> failure in CEL')
      QC=ABS(QQC)
      A=AA
      B=BB
      P=PP
      E=QC
      EM=1.
      IF(P.GT.0.)THEN
        P=SQRT(P)
        B=B/P
      ELSE
        F=QC*QC
        Q=1.-F
        G=1.-P
        F=F-P
        Q=Q*(B-A*P)
        P=SQRT(F/G)
        A=(A-B)/G
        B=-Q/(G*G*P)+A*P
      ENDIF
1     F=A
      A=A+B/P
      G=E/P
      B=B+F*G
      B=B+B
      P=G+P
      G=EM
      EM=QC+EM
      IF(ABS(G-QC).GT.G*CA)THEN
        QC=SQRT(E)
        QC=QC+QC
        E=QC*EM
        GO TO 1
      ENDIF
      CEL=PIO2*(B+A*EM)/(EM*(EM+P))
      RETURN
      END

C     
*******************************************************************************
      subroutine pearsn(x,y,n,r,prob,z)
      real tiny
      integer n,j
      parameter (tiny=1.e-20)
      real x(n),y(n),z,prob,r,ax,ay,sxx,syy,xt,yt,sxy,df
      real betai,t
      external betai
      ax=0.
      ay=0.
      do 11 j=1,N
        ax=ax+x(j)
        ay=ay+y(j)
11    continue
      ax=ax/n
      ay=ay/n
      sxx=0.
      syy=0.
      sxy=0.
      do 12 j=1,n
        xt=x(j)-ax
        yt=y(j)-ay
        sxx=sxx+xt**2
        syy=syy+yt**2
        sxy=sxy+xt*yt
12    continue
      r=sxy/sqrt(sxx*syy+tiny)
      z=0.5*log(((1.+r)+tiny)/((1.-r)+tiny))
      df=float(n-2)
      t=r*sqrt(df/(((1.-r)+tiny)*((1.+r)+tiny)))
      prob=betai(0.5*df,0.5,df/(df+t**2))
c     prob=erfcc(abs(z*sqrt(n-1.))/1.4142136)
      return
      end
C     
*****************************************************************************
      real function betai(a,b,x)
      real gammln,a,b,x,bt,betacf
      external gammln,betacf
      if(x.lt.0..or.x.gt.1.)call stopprocess('Numrec> bad argument x in BETAI')
      if(x.eq.0..or.x.eq.1.)then
        bt=0.
      else
        bt=exp(gammln(a+b)-gammln(a)-gammln(b)
     *      +a*log(x)+b*log(1.-x))
      endif
      if(x.lt.(a+1.)/(a+b+2.))THEN
        BETAI=BT*BETACF(A,B,X)/A
        RETURN
      ELSE
        BETAI=1.-BT*BETACF(B,A,1.-X)/B
        RETURN
      ENDIF
      END
C     
*****************************************************************************
      real FUNCTION BETACF(A,B,X)
      integer itmax,m,tem,em
      real eps,a,b,x,am,bm,az,qab,qap,qam,bz,d,ap,bp,app,bpp,aold
      PARAMETER (ITMAX=100,EPS=3.E-7)
      AM=1.
      BM=1.
      AZ=1.
      QAB=A+B
      QAP=A+1.
      QAM=A-1.
      BZ=1.-QAB*X/QAP
      DO 11 M=1,ITMAX
        EM=M
        TEM=EM+EM
        D=EM*(B-M)*X/((QAM+TEM)*(A+TEM))
        AP=AZ+D*AM
        BP=BZ+D*BM
        D=-(A+EM)*(QAB+EM)*X/((A+TEM)*(QAP+TEM))
        APP=AP+D*AZ
        BPP=BP+D*BZ
        AOLD=AZ
        AM=AP/BPP
        BM=BP/BPP
        AZ=APP/BPP
        BZ=1.
        IF(ABS(AZ-AOLD).LT.EPS*ABS(AZ))GO TO 1
11    CONTINUE
      call stopprocess('Numrec> A or B too big, or ITMAX too small')
1     BETACF=AZ
      RETURN
      END
C     
************************************************************************
      SUBROUTINE FIT(X,Y,NDATA,SIG,MWT,A,B,SIGA,SIGB,CHI2,Q)
      integer ndata
      real a,b,chi2,siga,sigb,sigdat,ss,st2,sx,sxoss,sy,t,wt,gammq
      real X(NDATA),Y(NDATA),SIG(*),mwt,q
      integer i
      SX=0.
      SY=0.
      ST2=0.
      B=0.
      IF(MWT.NE.0) THEN
        SS=0.
        DO 11 I=1,NDATA
          WT=1./(SIG(I)**2)
          SS=SS+WT
          SX=SX+X(I)*WT
          SY=SY+Y(I)*WT
11      CONTINUE
      ELSE
        DO 12 I=1,NDATA
          SX=SX+X(I)
          SY=SY+Y(I)
12      CONTINUE
        SS=FLOAT(NDATA)
      ENDIF
      SXOSS=SX/SS
      IF(MWT.NE.0) THEN
        DO 13 I=1,NDATA
          T=(X(I)-SXOSS)/SIG(I)
          ST2=ST2+T*T
          B=B+T*Y(I)/SIG(I)
13      CONTINUE
      ELSE
        DO 14 I=1,NDATA
          T=X(I)-SXOSS
          ST2=ST2+T*T
          B=B+T*Y(I)
14      CONTINUE
      ENDIF
      B=B/ST2
      A=(SY-SX*B)/SS
      SIGA=SQRT((1.+SX*SX/(SS*ST2))/SS)
      SIGB=SQRT(1./ST2)
      CHI2=0.
      IF(MWT.EQ.0) THEN
        DO 15 I=1,NDATA
          CHI2=CHI2+(Y(I)-A-B*X(I))**2
15      CONTINUE
        Q=1.
        SIGDAT=SQRT(CHI2/(NDATA-2))
        SIGA=SIGA*SIGDAT
        SIGB=SIGB*SIGDAT
      ELSE
        DO 16 I=1,NDATA
          CHI2=CHI2+((Y(I)-A-B*X(I))/SIG(I))**2
16      CONTINUE
        Q=GAMMQ(0.5*(NDATA-2),0.5*CHI2)
      ENDIF
      RETURN
      END

C====================================================
      real FUNCTION FSUM2(X1,X2,B,A,MA,MA1,MA2)
C     Fourier sumation with real numbers (array storing cosine 
C     and sine terms alternatively)
C     this Fourier series has a period of 2
      integer ma,ma1,ma2,i1,j1,i,j,k
      REAL A(MA),b,dtor,pi,x1,x2
C     set constant value
      DTOR = ATAN(1.0)/45.0
      PI = 180.0*DTOR
C     
      FSUM2=0.
C     
      K=4

      DO I1=0,2*MA1,2
         IF(I1.EQ.0) THEN
            I=I1
         ELSE
            I=I1-1
         END IF
         DO J1=-2*(MA2-1),2*MA2,2
            IF(J1.LT.0) THEN
               J=J1+1
            ELSE IF(J1.EQ.0) THEN
               J=J1
            ELSE
               J=J1-1
            END IF

            IF(I.EQ.0 .AND. J.LE.0) GOTO 100
            IF(I.EQ.0 .AND. J.EQ. (2*MA2-1))GOTO 100
            IF(I.EQ.(2*MA1-1) .AND. J.GE.0) GOTO 100

            K=K+1
            IF(K.GT.MA) GOTO 100
            FSUM2=FSUM2+A(K)*COS(-PI*(I*X1+J*X2))*EXP(-B*(I**2+J**2))
            K=K+1
            IF(K.GT.MA) GOTO 100
            FSUM2=FSUM2+A(K)*SIN(-PI*(I*X1+J*X2))*EXP(-B*(I**2+J**2))
 100        CONTINUE
         END DO
      END DO
      FSUM2 = FSUM2 + A(1)*0.5
     &     + A(2)*0.5*COS(-PI*((2*MA2-1)*X2))*EXP(-B*(2*MA2-1)**2)
     &     + A(3)*0.5*COS(-PI*((2*MA1-1)*X1))*EXP(-B*(2*MA1-1)**2)
     &     + A(4)*0.5*COS(-PI*((2*MA1-1)*X1+(2*MA2-1)*X2))
     &     *EXP(-B*((2*MA1-1)**2+(2*MA2-1)**2))
      RETURN
      END
CC====================================================
CC Fourier sumation with complex numbers
C      FUNCTION FXSUM(X1,X2,DATA,NN,NDIM)
C     integer nn,ndim,i,j,i1,j1
C      real NN(NDIM),x1,x2
C      COMPLEX DATA(*)
CC set constant value
C      DTOR = ATAN(1.0)/45.0
C      PI = 180.0*DTOR
CC
C      X1=X1/NN(1)
C      X2=X2/NN(2)
C      FXSUM=0.
CC
C      DO J=-NN(2)/2+1,NN(2)/2
C      J1=MOD(J+NN(2),NN(2))+1
C      DO I=0,NN(1)/2
C      I1=MOD(I+NN(1),NN(1))+1
C
C      IF(I.EQ.0 .AND. J.LE.0) GOTO 100
C      IF(I.EQ.0 .AND. J.EQ.NN(2)/2)GOTO 100
C      IF(I.EQ.NN(1)/2 .AND. J.GE.0) GOTO 100
C
C      FXSUM = FXSUM +  REAL(DATA(I1+(J1-1)*NN(1)))*COS(-2*PI*(I*X1+J*X2))
C     &              - AIMAG(DATA(I1+(J1-1)*NN(1)))*SIN(-2*PI*(I*X1+J*X2))
C
C100   CONTINUE
C      END DO
C      END DO
C      FXSUM = FXSUM*2
C     &      + REAL(DATA(1))
C     &      + REAL(DATA(1+NN(2)/2*NN(1)))*COS(-2*PI*(NN(2)/2*X2))
C     &      + REAL(DATA(NN(1)/2+1))*COS(-2*PI*(NN(1)/2*X1))
C     &      + REAL(DATA(NN(1)/2+1+NN(2)/2*NN(1)))
C     &       *COS(-2*PI*(NN(1)/2*X1+NN(2)/2*X2))
C      FXSUM = FXSUM/(NN(1)*NN(2))
C      RETURN
C      END
C====================================================
      real FUNCTION FXSUM1(X1,X2,DATA,NN,NDIM)
      integer ndim,i,j,j1,i1
      real NN(NDIM),x1,x2,dtor,pi
      COMPLEX DATA(*),FCXSUM
C set constant value
      DTOR = ATAN(1.0)/45.0
      PI = 180.0*DTOR
C
      FCXSUM=CMPLX(0.,0.)
      X1=X1/NN(1)
      X2=X2/NN(2)
C
      DO J=-NN(2)/2+1,NN(2)/2
      J1=MOD(J+NN(2),NN(2))+1
      DO I=-NN(1)/2+1,NN(1)/2
      I1=MOD(I+NN(1),NN(1))+1
      FCXSUM = FCXSUM + DATA(I1+(J1-1)*NN(1))*
     & CMPLX(COS(-2*PI*(I*X1+J*X2)),SIN(-2*PI*(I*X1+J*X2)))
      END DO
      END DO
      FXSUM1 = REAL(FCXSUM)/(NN(1)*NN(2))
      RETURN
      END
C======================================================
      SUBROUTINE matrixinv(temp,data,size)
      integer size,i,indx(100),j
      real data(size,size),temp(size,size)
      real d
      do i=1,size
         do j=1,size
            temp(i,j)=0.
         end do
      end do
      
      do i=1,5
         write(*,*) 'POTTTEST 1> ',(data(i,j),j=1,5)
      end do
      call ludcmp(data,size,size,indx,d)
      write(*,*) 'POTTTEST 2> ',(indx(j),j=1,5)
      do i=1,5
         write(*,*) 'POTTTEST 3> ',(data(i,j),j=1,5)
      end do
      
      do j=1,size
         call lubksb(data,size,size,indx,temp(1,j))
         write(*,'(a,5(i5))') 'POTTTEST 2> ',(indx(i),i=1,5)
         write(*,*) 'POTTTEST 4> ',(temp(1,i),i=1,5)
      end do
      end
C======================================================
      SUBROUTINE matrixinv2(Temp,data,size)
      integer size,i,j
      real data(size,size),temp(size,size)
      do i=1,size
        do j=i+1,size
          temp(i,j)=1.
        end do
        temp(i,i)=1.
      end do
C     do i=1,5
C     write(*,*) 'POTTTEST 1> ',(data(i,j),j=1,5)
C     end do
      call gaussj(data,size,size,temp,size,size)
      do i=1,size
        do j=1,size
          temp(i,j)=data(i,j)
        end do
      end do
C     do i=1,5
C     write(*,*) 'POTTTEST 2> ',(data(i,j),j=1,5)
c      end do

      

      end


C======================================================
      SUBROUTINE ludcmp(a,n,np,indx,d)
      INTEGER n,np,indx(n),NMAX
      REAL d,a(np,np),TINY
      PARAMETER (NMAX=500,TINY=1.0e-20)
      INTEGER i,imax,j,k
      REAL aamax,dum,sum,vv(NMAX)
      d=1.
      do 12 i=1,n
         aamax=0.
         do 11 j=1,n
            if (abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
 11      continue
         if (aamax.eq.0.) call stopprocess ('LUDCMP> singular matrix in ludcmp')
         vv(i)=1./aamax
 12   continue
      do 19 j=1,n
         do 14 i=1,j-1
            sum=a(i,j)
            do 13 k=1,i-1
               sum=sum-a(i,k)*a(k,j)
 13         continue
            a(i,j)=sum
 14      continue
         aamax=0.
         do 16 i=j,n
            sum=a(i,j)
            do 15 k=1,j-1
               sum=sum-a(i,k)*a(k,j)
 15         continue
            a(i,j)=sum
            dum=vv(i)*abs(sum)
            if (dum.ge.aamax) then
               imax=i
               aamax=dum
            endif
 16      continue
         if (j.ne.imax)then
            do 17 k=1,n
               dum=a(imax,k)
               a(imax,k)=a(j,k)
               a(j,k)=dum
 17         continue
            d=-d
            vv(imax)=vv(j)
         endif
         indx(j)=imax
         if(a(j,j).eq.0.)a(j,j)=TINY
         if(j.ne.n)then
            dum=1./a(j,j)
            do 18 i=j+1,n
               a(i,j)=a(i,j)*dum
 18         continue
         endif
 19   continue
      return
      END
C======================================================
      SUBROUTINE lubksb(a,n,np,indx,b)
      INTEGER n,np,indx(n)
      REAL a(np,np),b(n)
      INTEGER i,ii,j,ll
      REAL sum
      ii=0
      do 12 i=1,n
         ll=indx(i)
         sum=b(ll)
         b(ll)=b(i)
         if (ii.ne.0)then
            do 11 j=ii,i-1
               sum=sum-a(i,j)*b(j)
 11         continue
         else if (sum.ne.0.) then
            ii=i
         endif
         b(i)=sum
 12   continue
      do 14 i=n,1,-1
         sum=b(i)
         do 13 j=i+1,n
            sum=sum-a(i,j)*b(j)
 13      continue
         b(i)=sum/a(i,i)
 14   continue
      return
      END
C     (C) Copr. 1986-92 Numerical Recipes Software 5#Y.
C======================================================
      SUBROUTINE gaussj(a,n,np,b,m,mp)
      INTEGER m,mp,n,np,NMAX
      REAL a(np,np),b(np,mp)
      PARAMETER (NMAX=50)
      INTEGER i,icol,irow,j,k,l,ll,indxc(NMAX),indxr(NMAX),ipiv(NMAX)
      REAL big,dum,pivinv
      do 11 j=1,n
        ipiv(j)=0
 11   continue
      do 22 i=1,n
        big=0.
        do 13 j=1,n
          if(ipiv(j).ne.1)then
            do 12 k=1,n
              if (ipiv(k).eq.0) then
                if (abs(a(j,k)).ge.big)then
                  big=abs(a(j,k))
                  irow=j
                  icol=k
                endif
              else if (ipiv(k).gt.1) then
C                call stopprocess('GAUSSJ> singular matrix in gaussj 1')
                write(*,*)'GAUSSJ> singular matrix in gaussj 1'
                return
              endif
 12         continue
          endif
 13     continue
        ipiv(icol)=ipiv(icol)+1
        if (irow.ne.icol) then
          do 14 l=1,n
            dum=a(irow,l)
            a(irow,l)=a(icol,l)
            a(icol,l)=dum
 14       continue
          do 15 l=1,m
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
 15       continue
        endif
        indxr(i)=irow
        indxc(i)=icol
        if (a(icol,icol).eq.0.) then 
C     call stopprocess('GAUSSJ> singular matrix in gaussj 2')
          write(*,*)'GAUSSJ> singular matrix in gaussj 2'
          return
        end if
        pivinv=1./a(icol,icol)
        a(icol,icol)=1.
        do 16 l=1,n
          a(icol,l)=a(icol,l)*pivinv
 16     continue
        do 17 l=1,m
          b(icol,l)=b(icol,l)*pivinv
 17     continue
        do 21 ll=1,n
          if(ll.ne.icol)then
            dum=a(ll,icol)
            a(ll,icol)=0.
            do 18 l=1,n
              a(ll,l)=a(ll,l)-a(icol,l)*dum
 18         continue
            do 19 l=1,m
              b(ll,l)=b(ll,l)-b(icol,l)*dum
 19         continue
          endif
 21     continue
 22   continue
      do 24 l=n,1,-1
        if(indxr(l).ne.indxc(l))then
          do 23 k=1,n
            dum=a(k,indxr(l))
            a(k,indxr(l))=a(k,indxc(l))
            a(k,indxc(l))=dum
 23       continue
        endif
 24   continue
      return
      END
C     (C) Copr. 1986-92 Numerical Recipes Software 5#Y.
C     
***************************************************************************
      SUBROUTINE mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,nca,chisq,funcs,alamda)
      INTEGER ma,nca,ndata,ia(ma),MMAX
      REAL alamda,chisq,funcs,a(ma),alpha(nca,nca),covar(nca,nca),
     *     sig(ndata),x(ndata),y(ndata)
      PARAMETER (MMAX=20)
C     USES covsrt,gaussj,mrqcof
      INTEGER j,k,l,m,mfit
      REAL ochisq,atry(MMAX),beta(MMAX),da(MMAX)
      SAVE ochisq,atry,beta,da,mfit
      if(alamda.lt.0.)then
         mfit=0
         do 11 j=1,ma
            if (ia(j).ne.0) mfit=mfit+1
 11      continue
         alamda=0.001
         call mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nca,chisq,funcs)
         ochisq=chisq
         do 12 j=1,ma
            atry(j)=a(j)
 12      continue
      endif
      j=0
      do 14 l=1,ma
         if(ia(l).ne.0) then
            j=j+1
            k=0
            do 13 m=1,ma
               if(ia(m).ne.0) then
                  k=k+1
                  covar(j,k)=alpha(j,k)
               endif
 13         continue
            covar(j,j)=alpha(j,j)*(1.+alamda)
            da(j)=beta(j)
         endif
 14   continue
      call gaussj(covar,mfit,nca,da,1,1)
      if(alamda.eq.0.)then
         call covsrt(covar,nca,ma,ia,mfit)
         return
      endif
      j=0
      do 15 l=1,ma
         if(ia(l).ne.0) then
            j=j+1
            atry(l)=a(l)+da(j)
         endif
 15   continue
      call mrqcof(x,y,sig,ndata,atry,ia,ma,covar,da,nca,chisq,funcs)
      if(chisq.lt.ochisq)then
         alamda=0.1*alamda
         ochisq=chisq
         j=0
         do 17 l=1,ma
            if(ia(l).ne.0) then
               j=j+1
               k=0
               do 16 m=1,ma
                  if(ia(m).ne.0) then
                     k=k+1
                     alpha(j,k)=covar(j,k)
                  endif
 16            continue
               beta(j)=da(j)
               a(l)=atry(l)
            endif
 17      continue
      else
         alamda=10.*alamda
         chisq=ochisq
      endif
      return
      END
C     
*************************************************************************
C     (C) Copr. 1986-92 Numerical Recipes Software 5#Y.
      SUBROUTINE mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,nalp,chisq,funcs)
      INTEGER ma,nalp,ndata,ia(ma),MMAX
      REAL chisq,a(ma),alpha(nalp,nalp),beta(ma),sig(ndata),x(ndata),
     *     y(ndata)
      EXTERNAL funcs
      PARAMETER (MMAX=20)
      INTEGER mfit,i,j,k,l,m
      REAL dy,sig2i,wt,ymod,dyda(MMAX)
      mfit=0
      do 11 j=1,ma
         if (ia(j).ne.0) mfit=mfit+1
 11   continue
      do 13 j=1,mfit
         do 12 k=1,j
            alpha(j,k)=0.
 12      continue
         beta(j)=0.
 13   continue
      chisq=0.
      do 16 i=1,ndata
         call funcs(x(i),a,ymod,dyda,ma)
         sig2i=1./(sig(i)*sig(i))
         dy=y(i)-ymod
         j=0
         do 15 l=1,ma
            if(ia(l).ne.0) then
               j=j+1
               wt=dyda(l)*sig2i
               k=0
               do 14 m=1,l
                  if(ia(m).ne.0) then
                     k=k+1
                     alpha(j,k)=alpha(j,k)+wt*dyda(m)
                  endif
 14            continue
               beta(j)=beta(j)+dy*wt
            endif
 15      continue
         chisq=chisq+dy*dy*sig2i
 16   continue
      do 18 j=2,mfit
         do 17 k=1,j-1
            alpha(k,j)=alpha(j,k)
 17      continue
 18   continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 5#Y.
****************************************************************
*  This Routine is from Numerical Recipes
*
      SUBROUTINE INDEXI(N,ARRIN,INDX)

      INTEGER ARRIN(*),INDX(*)
      integer i,j,l,ir,q,indxt,n
      DO 11 J=1,N
        INDX(J)=J
11    CONTINUE
      L= FLOAT(N)/2+1
      IR=N
10    CONTINUE
        IF(L.GT.1)THEN
          L=L-1
          INDXT=INDX(L)
          Q=ARRIN(INDXT)
        ELSE
          INDXT=INDX(IR)
          Q=ARRIN(INDXT)
          INDX(IR)=INDX(1)
          IR=IR-1
          IF(IR.EQ.1)THEN
            INDX(1)=INDXT
            RETURN
          ENDIF
        ENDIF
        I=L
        J=L+L
20      IF(J.LE.IR)THEN
          IF(J.LT.IR)THEN
            IF(ARRIN(INDX(J)).LT.ARRIN(INDX(J+1)))J=J+1
          ENDIF
          IF(Q.LT.ARRIN(INDX(J)))THEN
            INDX(I)=INDX(J)
            I=J
            J=J+J
          ELSE
            J=IR+1
          ENDIF
        GO TO 20
        ENDIF
        INDX(I)=INDXT
      GO TO 10
      END
C     
*****************************************************************
      SUBROUTINE covsrt(covar,npc,ma,ia,mfit)
      INTEGER ma,mfit,npc,ia(ma)
      REAL covar(npc,npc)
      INTEGER i,j,k
      REAL swap
      do 12 i=mfit+1,ma
        do 11 j=1,i
          covar(i,j)=0.
          covar(j,i)=0.
11      continue
12    continue
      k=mfit
      do 15 j=ma,1,-1
        if(ia(j).ne.0)then
          do 13 i=1,ma
            swap=covar(i,k)
            covar(i,k)=covar(i,j)
            covar(i,j)=swap
13        continue
          do 14 i=1,ma
            swap=covar(k,i)
            covar(k,i)=covar(j,i)
            covar(j,i)=swap
14        continue
          k=k-1
        endif
15    continue
      return
      END
C     
***************************************************************
C  (C) Copr. 1986-92 Numerical Recipes Software 5#Y.
      SUBROUTINE indexx(n,arr,indx)
      INTEGER n,indx(n),M,NSTACK
      REAL arr(n)
      PARAMETER (M=7,NSTACK=50)
      INTEGER i,indxt,ir,itemp,j,jstack,k,l,istack(NSTACK)
      REAL a
      do 11 j=1,n
        indx(j)=j
11    continue
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 13 j=l+1,ir
          indxt=indx(j)
          a=arr(indxt)
          do 12 i=j-1,1,-1
            if(arr(indx(i)).le.a)goto 2
            indx(i+1)=indx(i)
12        continue
          i=0
2         indx(i+1)=indxt
13      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        itemp=indx(k)
        indx(k)=indx(l+1)
        indx(l+1)=itemp
        if(arr(indx(l+1)).gt.arr(indx(ir)))then
          itemp=indx(l+1)
          indx(l+1)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l)).gt.arr(indx(ir)))then
          itemp=indx(l)
          indx(l)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l+1)).gt.arr(indx(l)))then
          itemp=indx(l+1)
          indx(l+1)=indx(l)
          indx(l)=itemp
        endif
        i=l+1
        j=ir
        indxt=indx(l)
        a=arr(indxt)
3       continue
          i=i+1
        if(arr(indx(i)).lt.a)goto 3
4       continue
          j=j-1
        if(arr(indx(j)).gt.a)goto 4
        if(j.lt.i)goto 5
        itemp=indx(i)
        indx(i)=indx(j)
        indx(j)=itemp
        goto 3
5       indx(l)=indx(j)
        indx(j)=indxt
        jstack=jstack+2
        if(jstack.gt.NSTACK)pause 'NSTACK too small in indexx'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 5#Y.

***********************************************************************
C
C     We have to add a rand function that deals with big and small reals.
C
CHARMM Element source/util/random.src 1.1
C
C     
      REAL FUNCTION RANDOMn(ISEED)
C-----------------------------------------------------------------------
C     RANDOM NUMBER GENERATOR: UNIFORM DISTRIBUTION (0,1)
C     ISEED: SEED FOR GENERATOR. ON THE FIRST CALL THIS HAS TO
C     HAVE A VALUE IN THE EXCLUSIVE RANGE (1, 2147483647)
C     AND WILL BE REPLACED BY A NEW VALUE TO BE USED IN
C     FOLLOWING CALL.
C
C     REF: Lewis, P.A.W., Goodman, A.S. & Miller, J.M. (1969)
C     "Pseudo-random number generator for the System/360", IBM
C     Systems Journal 8, 136.
C
C     This is a "high-quality" machine independent generator.
C     INTEGERS are supposed to be 32 bits or more.
C     The same algorithm is used as the basic IMSL generator.
C
C      Author: Lennart Nilsson
C

      INTEGER ISEED
      REAL DSEED,DIVIS,DENOM,MULTIP
C     ##IF INTEL
C      REAL DINMOD
C     ##ENDIF
      DATA  DIVIS/2147483647.D0/
      DATA  DENOM /2147483711.D0/
      DATA  MULTIP/16807.D0/
C
      IF(ISEED.LE.1) ISEED=314159
      DSEED=MULTIP*ISEED
C     #IF INTEL
C      DSEED=DINMOD(DSEED,DIVIS)
C     #ELSE
      DSEED=MOD(DSEED,DIVIS)
C     #ENDIF
      RANDOMn=DSEED/DENOM
      ISEED=DSEED
C
      RETURN
      END

C     
#ifdef TJOBALKALA
      real function randomn(seed)
      integer seed
      real*4 random
#ifdef SGI
      real*4 ran
#endif
#ifdef RS6000
      real*4 ran
      call srand(random)
      random=rand()
#else
      random=ran(seed)
#endif
#ifdef BIGREAL
      randomn=dble(random)
#else
      randomn=random
#endif
      end

#endif
C************************************************************************
      SUBROUTINE sort(n,arr)
      INTEGER n,M,NSTACK
      REAL arr(n)
      PARAMETER (M=7,NSTACK=50)
      INTEGER i,ir,j,jstack,k,l,istack(NSTACK)
      REAL a,temp
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          do 11 i=j-1,1,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
11        continue
          i=0
2         arr(i+1)=a
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l+1).gt.arr(l))then
          temp=arr(l+1)
          arr(l+1)=arr(l)
          arr(l)=temp
        endif
        i=l+1
        j=ir
        a=arr(l)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        goto 3
5       arr(l)=arr(j)
        arr(j)=a
        jstack=jstack+2
        if(jstack.gt.NSTACK)pause 'NSTACK too small in sort'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END
