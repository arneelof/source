C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
********************************************************************
      real function scalarprod(A,B)
      real A(3),b(3)
      integer i,j,k
      scalarprod=0
      do k=1,3
         scalarprod=scalarprod+A(k)*B(k)
      end do
      end
********************************************************************
      subroutine normal(A,size)
      integer size,i
      real A(size),sum
  
      sum=0
      do i=1,size
        sum=sum+a(i)**2
      end do
      sum=sqrt(sum)
      if (sum.eq.0) return
      do i=1,size
        a(i)=a(i)/sum
      end do

      end
********************************************************************
      Subroutine MatrixMult(A,B,C)
      real A(3,3),b(3,3),c(3,3)
      integer i,j,k
      do i=1,3
         do j=1,3
            C(i,j)=0
            do k=1,3
               C(i,j)=C(i,j)+A(i,k)*B(k,j)
            end do
         end do
      end do
      end
**********************************************************************
      Subroutine  rightvectmatrixmult(A,B,C)
      real A(3,3),b(3),c(3)
      integer i,j,k

      do i=1,3
         C(i)=0
         do k=1,3
            C(i)=C(i)+A(i,k)*B(k)
         end do
      end do
      end
********************************************************************
      real function lengthvect(A)
      real A(3)
      lengthvect=SQRT(A(1)**2+A(2)**2+A(3)**2)
      end
**********************************************************************
      real function cosangle(x1,y1,z1,x2,y2,z2,x3,y3,z3)
      real  x1,y1,z1,x2,y2,z2,x3,y3,z3
      real atom1(3),atom2(3),atom3(3)
      real vect1(3),vect2(3)
      real scalarprod,lengthvect
      external scalarprod,lengthvect

C        atom1(1) = x1
C        atom1(2) = y1
C        atom1(3) = z1
C        atom1(1) = x2
C        atom2(2) = y2
C        atom2(3) = z2
C        atom3(1) = x3
C        atom3(2) = y3
C        atom3(3) = z3

      vect1(1) = x2 - x1
      vect1(2) = y2 - y1
      vect1(3) = z2 - z1
      vect2(1) = x3 - x2
      vect2(2) = y3 - y2
      vect2(3) = z3 - z2
      
      cosangle= scalarprod(vect1,vect2)/(lengthvect(vect1)*lengthvect(vect2)+1.e-20)
      
      end 

C     
*******************************************************************
C
C	Title:		CROSS_PROD.FOR
C
	SUBROUTINE CROSS_PROD (A,b,c)

	IMPLICIT NONE
	REAL A(3),B(3),C(3)

	C(1) = A(2)*B(3) - A(3)*B(2)
	C(2) = A(3)*B(1) - A(1)*B(3)
	C(3) = A(1)*B(2) - A(2)*B(1)

	END
C     
****************************************************************
      subroutine rotatx(r1,n,r2,theta)
      real r1(3),n(3),r2(3),theta,rot(3)
      real r1xn(3),nr1
      real cost,sint,fact
      real scalarprod
      external scalarprod
      integer j
      
c     from goldstein page 165
c     r2 = r1 cos(theta) + n (n.r1)(1-cos(theta)) + (r1 x n)sin(theta)
c     
      cost=cos(theta)
      sint=sin(theta)
      call normal(n,3)
      call cross_prod(r1,n,r1xn)
c     call dotpr(n,r1,3,nr1)
      nr1=scalarprod(n,r1)
      fact=nr1*(1.0d0-cost)
      do 1 j=1,3
        rot(j)=r1(j)*cost+n(j)*fact+r1xn(j)*sint
    1 continue
      
      do 2 j=1,3
        r2(j)=rot(j)
    2 continue
      return
      end
