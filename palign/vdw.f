C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
***********************************************************************
      subroutine vdwEnergy (X,Y,Z,nres,nres1,traceback,seq2,E,R,Etot,
     &     Eaa,Eij,Eia)

      implicit none
#include <dynprog.h>

      real X(*), Y(*), Z(*)
      integer traceback(*), seq2(*), invtrace(maxseqlen)
      integer nres, nres1
      real rr, rra, rraa
      real xi, yi, zi, xj, yj, zj, xx, xy, xz, yx, yy, yz
      real E(20,20), R(20,20), Etot, Eaa, Eij, Eia
      integer i, j, k, l, m, n
      real A, B
      
      do i=1,maxseqlen,1
         invtrace(i) = 0
      enddo
      
      do i=1,nres1,1
         j = traceback(i)
         if (j .ne. 0) then 
            invtrace(j) = i
         endif
      end do

c      write(*,*)'nres:',nres,' nres1:',nres1

c      do i=1,nres,1
c         write(*,*)'invtrace(',i,') :',invtrace(i),' traceback(',i,'):',
c     &        traceback(i)
c      enddo

      Etot = 0
      Eaa = 0
      Eij = 0
      Eia = 0
      do i=1,nres,1
         do j=i+4,nres,1
            k = invtrace(i)
            l = invtrace(j)
            if(k .ne. 0 .and. l .ne. 0) then
               rraa = (X(k)-X(l))**2+(Y(k)-Y(l))**2+(Z(k)-Z(l))**2
               rraa = rraa**2
               A = -1*E(1,20)*(R(1,20)**8)
               B = -2*E(1,20)*(R(1,20)**4)
               Eaa = Eaa+A/(rraa**2)-B/rraa
               Etot = Etot+A/(rraa**2)-B/rraa
C              write(*,'(a,4i5,3e10.3)')'test',i,j,k,l,rraa,Eaa,Etot
            endif
         enddo
      enddo

      do i=1,nres,1
        do j=i+4,nres,1
           k = invtrace(i)
           l = invtrace(j)
           if(k .ne. 0 .and. l .ne. 0) then
              xi = X(k)
              yi = Y(k)
              zi = Z(k)
              xj = X(l)
              yj = Y(l)
              zj = Z(l)
              
              rr = (xi-xj)**2+(yi-yj)**2+(zi-zj)**2
              rr = rr**2
              call indexes(seq2(i),m)
              call indexes(seq2(j),n)
              A = -1*E(m,21-n)*((R(m,21-n))**8)
              B = -2*E(m,21-n)*((R(m,21-n))**4)
              Eij = Eij+A/(rr**2)-B/rr
              Etot = Etot+A/(rr**2)-B/rr
           endif
        enddo
      enddo
      
      do i=1,nres,1
         k = invtrace(i)
         if (k .ne. 0) then
            xi = X(k)
            yi = Y(k)
            zi = Z(k)
        
            do j=i+3,nres,1
               l = invtrace(j) 
               if (l .ne. 0) then
                  rra=(xi-X(l))**2+(yi-Y(l))**2+(zi-Z(l))**2
                  rra = rra**2
                  call indexes(seq2(i), m)
                  A = -1*E(1,21-m)*((R(1,21-m))**8)
                  B = -2*E(1,21-m)*((R(1,21-m))**4)
                  Eia = Eia+A/(rra**2)-B/rra
                  Etot = Etot+A/(rra**2)-B/rra
               end if
            enddo
            do j=i-3,1,-1
               l = invtrace(j)
               if (l .ne. 0) then
                  rra = (xi-X(l))**2+(yi-Y(l))**2+(zi-Z(l))**2
                  rra = rra**2
                  call indexes(seq2(i),m)
                  A = -1*E(1,21-m)*((R(1,21-m))**8)
                  B = -2*E(1,21-m)*((R(1,21-m))**4)
                  Eia = Eia+A/(rra**2)-B/rra
                  Etot = Etot+A/(rra**2)-B/rra
               endif
            enddo
         endif
      enddo
      end

**********************************************************************
      subroutine iniRE (filename1,filename2,R, E)

      implicit none
      character*(*) filename1, filename2
      real R(20,20), E(20,20)
      integer filenum
      parameter (filenum=90)
      
C      filename1 = 'contDistHL'
C      filename2 = 'contEneHL'
C      path = 'parkLevitt/'

c     �ppna contactDistancesHL och contactEnergiesHL
C      write(*,*)'test1',filename1
      open(unit=filenum,file = filename1,Form='FORMATTED',
     &     status ='OLD',access='SEQUENTIAL',err=777)

   
      do while (.true.)
         read(filenum,*,end = 666,err=777) R
      end do

 666  close(unit = filenum)

      open(unit=filenum,file = filename2,Form='FORMATTED',
     &     status ='OLD',access='SEQUENTIAL',err=777)
      
      do while(.true.)
         read(filenum,*,end = 667,err=777) E
      end do
 
 667  close(unit = filenum)
      return

 777  continue
C      call stopprocess('ERROR - iniRE: Files not found')
      return

      end

**********************************************************************
      subroutine indexes(in, out)

      integer in, out
      character*3 aa

      if (in .eq. 1) aa = 'ALA'
      if (in .eq. 2) aa = 'ARG'
      if (in .eq. 3) aa = 'ASN'
      if (in .eq. 4) aa = 'ASP'
      if (in .eq. 5) aa = 'CYS'
      if (in .eq. 6) aa = 'GLN'
      if (in .eq. 7) aa = 'GLU'
      if (in .eq. 8) aa = 'GLY'
      if (in .eq. 9) aa = 'HIS'
      if (in .eq. 10) aa = 'ILE'
      if (in .eq. 11) aa = 'LEU'
      if (in .eq. 12) aa = 'LYS'
      if (in .eq. 13) aa = 'MET'
      if (in .eq. 14) aa = 'PHE'
      if (in .eq. 15) aa = 'PRO'
      if (in .eq. 16) aa = 'SER'
      if (in .eq. 17) aa = 'THR'
      if (in .eq. 18) aa = 'TRP'
      if (in .eq. 19) aa = 'TYR'
      if (in .eq. 20) aa = 'VAL'

      if (aa .eq. 'ALA') out = 2
      if (aa .eq. 'ARG') out = 12
      if (aa .eq. 'ASN') out = 9
      if (aa .eq. 'ASP') out = 7
      if (aa .eq. 'CYS') out = 16
      if (aa .eq. 'GLN') out = 10
      if (aa .eq. 'GLU') out = 8
      if (aa .eq. 'GLY') out = 1
      if (aa .eq. 'HIS') out = 19
      if (aa .eq. 'ILE') out = 5
      if (aa .eq. 'LEU') out = 4
      if (aa .eq. 'LYS') out = 11
      if (aa .eq. 'MET') out = 15
      if (aa .eq. 'PHE') out = 20
      if (aa .eq. 'PRO') out = 6
      if (aa .eq. 'SER') out = 13
      if (aa .eq. 'THR') out = 14
      if (aa .eq. 'TRP') out = 18
      if (aa .eq. 'TYR') out = 17
      if (aa .eq. 'VAL') out = 3


      end
**********************************************************************
