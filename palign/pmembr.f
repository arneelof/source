C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
      Program Palign
C
C A hack of panal so I can do it all on one line,oldline !
C
      implicit none
#include <dynprog.h>
#include <seqio.h>
#ifdef PRFPRF
      real seq2profile(maxproflen,maxaa+2),seqprofile(maxproflen,maxaa+2)
      real scaleseq2profile(maxproflen,maxaa+2),prfprf
      real scaledpsiseq2profile(maxproflen,maxaa+2)
#endif
      real dynscore_msa,sum,profprof
      real profile(maxproflen,maxaa+2),gapopen,gapext
      real dynscore,score,x(maxproflen),y(maxproflen),z(maxproflen)
      real x2(maxproflen),y2(maxproflen),z2(maxproflen)
      real seqssfreq(maxproflen,maxss)
      real profssfreq(maxproflen,maxss)
      real averscore,sumscore,sum2score,zscore,tempscore,sd
      real gapmax,gapdist
      real scaledpsiseq(maxproflen,maxaa+2),scaledpsiprf(maxproflen,maxaa+2)
      character*800 line,proffile,seqfile,filename1,filename2
      character*800 alfile,pdbfile,alfile2,pdbfile2,oldline
      character*10 seqtype,proftype
      character*4 resnum(maxseqlen),profresnum(maxseqlen),nam
C      real score2
      character*1 char,char1,char2
      integer cfiles,linelen,i,j,k,proflen,alitype,resnumflag,type,num
      integer profseq(maxseqlen),profss(maxseqlen),numss,numaa,stlen,aa
      integer seq(maxseqlen),seqss(maxseqlen),seqlen,traceback(maxseqlen),traceback2(maxseqlen)
      integer seqtoseq,seqtoss,allen,numal,gaps,numgaps,ssgaps
      integer profcol,seqcol
      integer seq2(maxseqlen),templen,numzscore,method,winsize
      logical seqfound,proffound,printali,gaplinear,convert,normprof
     $     ,scalepsiblast
      real R(20,20), E(20,20), Etot, Eaa, Eij, Eia,fraction
      real loopgap(3),ssmatch(0:3,0:3),win(25)
      real lambda,shift
      external dynscore,seqtoseq,seqtoss
      external dynscore_msa
     
      parameter (numss=3)
      parameter (numaa=20)

C      call foo(bar)
C default values
c      do i=1,25
c         win(i)=0
c      end do
      
      aa=numaa
      lambda=0.5
      shift=0.45
      normprof=.false.
   
      call init
      call init_int(seq,maxseqlen,0)
      call init_int(seqss,maxseqlen,0)
      call init_int(profss,maxseqlen,0)
      call init_int(profseq,maxseqlen,0)
      call init_int(traceback,maxseqlen,0)

C      call init_int(resnum,maxseqlen,0)
C      call init_int(profresnum,maxseqlen,0)

      call init_real(profile,maxproflen*(maxaa+2),0.)
#ifdef PRFPRF
      call init_real(seqprofile,maxproflen*(maxaa+2),0.)
      seqprofile(1,1)=-9999
      method=0
#endif
      call init_real(x,maxseqlen,0.)
      call init_real(y,maxseqlen,0.)
      call init_real(z,maxseqlen,0.)
      call init_real(table,maxaa*maxaa,0.)
      call init_real(E,400,0.)
      call init_real(R,400,0.)

      profile(1,1)=-9999
      filename1='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contDistHL'
      filename2='/afs/pdc.kth.se/home/a/arnee/source/palign/parkLevitt/contEneHL'
      call inire(filename1,filename2,R,E)


      call defaulttable_blosum62(table,maxaa)
      type=1
      gaplinear=.false.
      printali=.false.
      seqfound=.false.
      proffound=.false.
      convert=.true.
      scalepsiblast=.false.
#ifdef PRFPRF
      seqtype="psiblastfreq"
      proftype="psiblastfreq"
      gapopen=-2
      gapext=-.2
      numzscore=10
#else
      gapopen=-11
      gapext=-1
      numzscore=100
#endif
      x(1)=-9999
      x2(1)=-9999
      alitype=0
      seqss(1)=-1
      profss(1)=-1
      loopgap(1)=1
      loopgap(2)=1
      loopgap(3)=1
C
C     This is the only difference from pmembr
C
      printali=.false.
      ssmatch(0,0)=0
#ifdef PMEMBR
      ssmatch(1,1)=0
#else
      ssmatch(1,1)=0
#endif
      ssmatch(1,2)=0
      ssmatch(1,3)=0
      ssmatch(2,1)=0
      ssmatch(2,2)=1
      ssmatch(2,3)=0
      ssmatch(3,1)=0
      ssmatch(3,2)=0
#ifdef PMEMBR
      ssmatch(3,3)=0
#else
      ssmatch(3,3)=0
#endif
      alfile='xxx'
      pdbfile='xxx'
      alfile2='xxx'
      pdbfile2='xxx'
      do i=1,maxseqlen
        call encodi(i,resnum(i),4,stlen)
        call encodi(i,profresnum(i),4,stlen)
      end do

C Start the program
      cfiles=1 
      call getarg(cfiles,line)
      linelen=len(line)
      call trima(line,linelen)
      i=1

#ifdef IFC
      do while  (line .ne. oldline ) 
        oldline=line
#else
      do while  (line .ne. '' ) 
#endif
        if (line(1:5).eq.'-help' .or. line(1:2).eq.'-h' .or. line(1:2).eq.'-?' ) then 
          write(*,*) "Usage:"
          write(*,*) "-table file   : Use a sequence table file in blast format"
          write(*,*) "-fasta file   : Use a sequence table file in fasta format (default pam250)"
          write(*,*) "-local        : Use local alignments (default)"
          write(*,*) "-global       : Use global alignment"
          write(*,*) "-gloloc       : Use global-local alignment"
          write(*,*) "-inire  file1 file2    : Energy files for threading energy"
          write(*,*) "-loopgap A B C : cost for gaps in different SS regions (default 1,1,1)"
          write(*,*) "-ssmatch a b c d e f g h i : Scores for secondary structure regions"
          write(*,*) "                            default 100 010 001 for secstr and"
          write(*,*) "                            000 010 000 for TM and"
          write(*,*) "-go X          : Gap opening cost"
          write(*,*) "-ge X          : Gap  extension cost"
#ifdef PRFPRF
          write(*,*) "-readmsa          : Do not use seqtable when creating profile"
          write(*,*) "-readnn file      : read a neural network from a file"
          write(*,*) "-winsize X        : specify the size of the window"
          write(*,*) "-win X Y ..       : the window weights, has to be -win numbers"
          write(*,*) "-somsize X        : specify the number of som values"
          write(*,*) "-win X Y ..       : the som weights by which each som score will be divided by"
          write(*,*) "-shift X          : Factor shift See Yona and Levitt"
          write(*,*) "-lambda Y         : Factor Lambda See Yona and Levitt"
          write(*,*) "-profsim          : use profsim scoring"
          write(*,*) "-ffas             : use ffas scoring"
          write(*,*) "-picasso          : use picasso3 scoring"
          write(*,*) "-logaver          : use logaver scoring (default)"
          write(*,('(a)')) "-normprof          : normalize profiles to length 1"
#endif
#ifdef PRFTEST
          write(*,*) "-cols          : two columns to test Seq,prof"
#endif
          write(*,*) "-gapmax X Y    : Increased/decreased gap costs"
          write(*,*) "-ali        : alignment is printed to stdout"
          write(*,*) "-alfile  foo      : file to print alignment"
          write(*,*) "-alifile foo      : file to print alignment"
          write(*,*) "-caspfile foo     : file to print alignment"
          write(*,*) "-pdbfile  foo     : file to print structure"
          write(*,*) "-numzscore  X     : Number of randomization for Z-score calculation (100)"
          write(*,*) "-seq file1 file2 ... - : files to read for sequence info"
          write(*,*) "-prof file1 file2 ... - : files to read for profiel info"
          write(*,*) "file1 file2     : sequence and profile file"
          write(*,*) "Note somehow atleast one sequence or profile file has to be named"
          stop
        else if (line(1:6).eq.'-table') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-fasta') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readfastaseqtable(line,table,maxaa)
C          call writeseqtable(table,maxaa)
        else if (line(1:6).eq.'-local') then
          alitype=0
#ifdef PRFPRF
        else if (line(1:8).eq.'-readmsa') then
          convert=.false.
        else if (line(1:7).eq.'-readnn') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          call readnn(line)
          method=5
c          write(*,*)'readnn'
        else if (line(1:6).eq.'-shift') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) shift
        else if (line(1:7).eq.'-lambda') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) lambda
c          write(*,*)'lambda = ',lambda
        else if (line(1:8).eq.'-profsim') then
          method=1
        else if (line(1:5).eq.'-ffas') then
          method=2
        else if (line(1:8).eq.'-picasso') then
          method=4
        else if (line(1:10).eq.'-probscore') then
          method=4
        else if (line(1:8).eq.'-logaver') then
          method=3
        else if (line(1:9).eq.'-normprof') then
          normprof=.true.
#endif
#ifdef PRFTEST
        else if (line(1:5).eq.'-cols') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) profcol
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) seqcol
#endif
        else if (line(1:7).eq.'-global') then
          alitype=10
        else if (line(1:7).eq.'-gloloc') then
          alitype=1
        else if (line(1:7).eq.'-locglo') then
          alitype=1
        else if (line(1:9).eq.'-endsfree') then
          alitype=1
        else if (line(1:7).eq.'-inire') then
          alitype=10
C          filename1 = 'contDistHL'
C          filename2 = 'contEneHL'
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
C          read (line,*) filename1
          filename1=line
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
C          read (line,*) filename2
          filename2=line
          call inire(filename1,filename2,R,E)
C          do i=1,20
C            write(*,'(a,i4,20f8.3)')'R ',i,(R(i,j),j=1,20)
C            write(*,'(a,i4,20f8.3)')'E ',i,(E(i,j),j=1,20)
C          end do
        else if (line(1:8).eq.'-loopgap') then
          do j=1,3
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) loopgap(j)
          end do
        else if (line(1:8).eq.'-ssmatch') then
          do j=1,3
            do k=1,3
              cfiles=cfiles+1
              call getarg(cfiles,line)
              linelen=len(line)
              call trima(line,linelen)
              read (line,*) ssmatch(j,k)
C              write(*,*)'TEST ',j,k,ssmatch(j,k)
           end do
        end do 
      else if (line(1:8).eq.'-winsize') then
         cfiles=cfiles+1
         call getarg(cfiles,line)
         linelen=len(line)
         call trima(line,linelen)
         read (line,*) winsize
      else if (line(1:4).eq.'-win') then
         do j=1,winsize
            cfiles=cfiles+1
            call getarg(cfiles,line)
            linelen=len(line)
            call trima(line,linelen)
            read (line,*) win(j)
            method=7
         end do
      else if(line(1:10).eq.'-scalepsib')then
          scalepsiblast=.true.
       else if (line(1:3).eq.'-go') then
         cfiles=cfiles+1
         call getarg(cfiles,line)
         linelen=len(line)
         call trima(line,linelen)
         read (line,*) gapopen
      else if (line(1:10).eq.'-numzscore') then
         cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) numzscore
          numzscore=max(2,numzscore)
        else if (line(1:3).eq.'-ge') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapext
        else if (line(1:7).eq.'-gapmax') then
          gaplinear=.true.
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapmax
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,*) gapdist
        else if (line(1:7).eq.'-alfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile
        else if (line(1:9).eq.'-caspfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile
        else if (line(1:8).eq.'-alifile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile
        else if (line(1:4).eq.'-ali') then
          printali=.true.
        else if (line(1:8).eq.'-pdbfile') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') pdbfile
        else if (line(1:9).eq.'-al2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile2
#ifndef IFC
        else if (line(1:9).eq.'-noresnum') then
          resnumflag=1
#endif
        else if (line(1:10).eq.'-casp2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile2
        else if (line(1:9).eq.'-ali2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') alfile2
        else if (line(1:9).eq.'-pdb2file') then
          cfiles=cfiles+1
          call getarg(cfiles,line)
          linelen=len(line)
          call trima(line,linelen)
          read (line,'(a)') pdbfile2
        else if (line(1:4).eq.'-seq') then
           if (seqfound) call stopprocess
     $          ('ERROR> A Sequence already read !')
           templen=0
           cfiles=cfiles+1
           call getarg(cfiles,line)
           do while (line(1:1).ne.'-')
              seqfile=line
C            call trima(seqfile,linelen)
#ifdef PRFPRF

              if (convert) then
                 call readanyseq(line,seq,seqlen,seqfound,seqprofile
     $                ,seqss,seqssfreq,x,y,z,seqtype,resnum)
              else
                 call readpsiblastfreq(line,seqprofile,seq,seqlen
     $                ,seqfound)
              end if

#else
      call readanyseq(line,seq,seqlen,seqfound,profile,seqss ,seqssfreq
     $     ,x,y,z,seqtype,resnum)
#endif
      if (seqlen .gt. maxseqlen) call stopprocess
     $     ('ERROR> Sequence too long !')
      write(*,*)'READING sequence type: ',seqtype,' len: ',seqlen
      if (templen .ne. 0 .and. seqlen .ne. templen) then
         call stopprocess('ERROR> sequences of different length !')
      end if
      templen=seqlen
      cfiles=cfiles+1
      call getarg(cfiles,line)
      end do
#ifdef PRFPRF
      if (seqprofile(1,1) .eq. -9999) then
         write(*,*)'Making a profile (1):'
         call makeseqprof(seqprofile,seq,seqlen,table,gapopen,gapext)
          else
            if (gapopen .gt. 0) gapopen=-gapopen
            if (gapext .gt. 0) gapext=-gapext
            do i=1,proflen
              seqprofile(i,penopen)=gapopen+gapext
              seqprofile(i,penext)=gapext
            end do
          end if
#endif
       else if (line(1:5).eq.'-prof') then
          if (proffound) call stopprocess
     $         ('ERROR> A Profile already read !')
          cfiles=cfiles+1
          call getarg(cfiles,line)
          templen=0
          do while (line(1:1).ne.'-')
             proffile=line
C     call trima(proffile,linelen)
#ifdef PRFPRF
             if (convert) then
                call readanyseq(line,profseq,proflen,proffound,profile
     $               ,profss,profssfreq,x2,y2,z2,proftype,profresnum)
             else
                call readpsiblastfreq(line,profile,profseq,proflen
     $               ,proffound)
             end if
#else
             call readanyseq(line,profseq,proflen,proffound,profile
     $            ,profss,profssfreq,x2,y2,z2,proftype ,profresnum)
            write(*,*)'pepaldfdf111->',proflen,seqlen

#endif
            if (proflen .gt. maxproflen) call stopprocess('ERROR> profile too long !')
            write(*,*)'READING profile (2) type: ',proftype,' len: ',proflen
            if (templen .ne. 0 .and. seqlen .ne. templen) then
              call stopprocess('ERROR> profiles of different length !')
            end if
            templen=seqlen
            cfiles=cfiles+1
            call getarg(cfiles,line)
          end do
          if (profile(1,1) .eq. -9999) then
            write(*,*)'Making a profile (1):'
            call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
          else
            if (gapopen .gt. 0) gapopen=-gapopen
            if (gapext .gt. 0) gapext=-gapext
            do i=1,proflen
              profile(i,penopen)=gapopen+gapext
              profile(i,penext)=gapext
            end do
          end if
        else
          if (type.eq.1 .and. .not. seqfound) then
            seqfile=line
#ifdef PRFPRF
            if (convert) then
               call readanyseq(line,seq,seqlen,seqfound,seqprofile,seqss
     $              ,seqssfreq,x,y,z,seqtype,resnum)
            else
              call readpsiblastfreq(line,seqprofile,seq,seqlen,seqfound)
            end if
#else
            call readanyseq(line,seq,seqlen,seqfound,profile,seqss
     $           ,seqssfreq,x,y,z,seqtype,resnum)
#endif
            if (seqlen .gt. maxseqlen) call stopprocess('ERROR> Sequence too long !')
            write(*,*)'READING sequence type (3): ',seqtype,' len: ',seqlen
#ifdef PRFPRF
            if (seqprofile(1,1).eq. -9999) then
              write(*,*)'Makeing a profile (4):'
              call makeseqprof(seqprofile,seq,seqlen,table,gapopen,gapext)
            else
              do i=1,seqlen
                seqprofile(i,penopen)=gapopen+gapext
                seqprofile(i,penext)=gapext
              end do
            end if
#endif
#ifdef IFC
            oldline=''
#endif
          elseif (.not. proffound) then
            proffile=line
#ifdef PRFPRF
            if (convert) then
               call readanyseq(line,profseq,proflen,proffound,profile
     $              ,profss,profssfreq,x2,y2,z2,proftype ,profresnum)
            else
              call readpsiblastfreq(line,profile,profseq,proflen,proffound)
            end if
#else
            call readanyseq(line,profseq,proflen,proffound,profile
     $           ,profss,profssfreq,x2,y2,z2,proftype ,profresnum)
#endif
            if (proflen .gt. maxproflen) call stopprocess('ERROR> Profile too long !')
            write(*,*)'READING profile type (4): ',proftype,' len: ',proflen
            if (profile(1,1) .eq. -9999) then
              write(*,*)'Making a profile (4):'
              call makeseqprof(profile,profseq,proflen,table,gapopen,gapext)
            else
              if (gapopen .gt. 0) gapopen=-gapopen
              if (gapext .gt. 0) gapext=-gapext
              do i=1,proflen
                profile(i,penopen)=gapopen+gapext
                profile(i,penext)=gapext
              end do
            end if
          end if
          type=type+1
        end if
        cfiles=cfiles+1
        call getarg(cfiles,line)
        linelen=len(line)
        call trima(line,linelen)
      end do

#ifdef PRFPRF
C We need to convert the profiles to probabilities
C
   
      
      if (convert) then
         write(*,*) 'INFO> converting profiles'
         if(scalepsiblast)then
            call copyprof(profile,scaledpsiprf,proflen)
            call copyprof(seqprofile,scaledpsiseq,seqlen)
         end if
         call profconvert(profile,proflen,aa)
         call profconvert(seqprofile,seqlen,aa)
      end if
      
      if(scalepsiblast)then
         write(*,*) 'INFO> making profnet profiles'
         if (.not. convert) then
            call copyprof(profile,scaledpsiprf,proflen)
            call copyprof(seqprofile,scaledpsiseq,seqlen)
         end if
         call readscalepsiblast(scaledpsiprf,proflen)                              
         call readscalepsiblast(scaledpsiseq,seqlen)
      end if


      if (normprof) then
         write(*,*) 'INFO> normalizing profiles'
         call profnorm(profile,proflen,aa)
         call profnorm(seqprofile,seqlen,aa)
      end if
      if (profss(1).ne.-1 .and. seqss(1).ne.-1) then
        write(*,*)'Adding loopgap info:'
        call makeloopgap(profile,profss,proflen,loopgap,ssmatch,numaa,numss,win)
      end if
   

#else
      if (profss(1).ne.-1 .and. seqss(1).ne.-1) then
        write(*,*)'Using secondary structure predictions (4):'
        call addsstoseq(seq,seqss,seqlen,numaa)
        write(*,*)'Making a secondary structureprofile (4):'
        call makessprof(profile,profss,proflen,loopgap,ssmatch,numaa,numss,win)
      end if
#endif
      
      if (gaplinear) then
        if (gapmax .gt. 0) then
          do i=1,proflen
            profile(i,penopen)=profile(i,penopen)*(1+gapmax/(i*gapdist))
            profile(i,penext)=profile(i,penext)*(1+gapmax/(i*gapdist))
          end do
        else
          do i=1,proflen
            profile(i,penopen)=profile(i,penopen)*(1-gapmax/((proflen-i)*gapdist))
            profile(i,penext)=profile(i,penext)*(1-gapmax/((proflen-i)*gapdist))
          end do
        end if
      end if

#ifdef PRFTEST
#ifdef PRFPRF
      do i=1,20
         write(*,*) i,profile(profcol,i),seqprofile(seqcol,i),profcol,seqcol
      end do
      write(*,'(A,e11.3)') 'SIMU> ',profprof(profcol,seqcol,profile
     $     ,seqprofile,profss,seqss,profssfreq,seqssfreq,numaa,shift
     $     ,lambda,method,ssmatch,proflen,seqlen,win,scaledpsiprf
     $     ,scaledpsiseq)

#else
      do i=1,20
         write(*,*) i,profile(profcol,i)
      end do
      write(*,'(A,e11.3)') 'SIMU> ',profile(profcol,seq(seqcol))
#endif
#else
#ifdef PRFPRF
C      write (*,*) "TEST TRACE1"
      call dyntrace_msa(profile,proflen,seqprofile,seqlen,alitype,score
     $,traceback,seqss,profss,seqssfreq,profssfreq,aa,shift,lambda
     $,method,ssmatch,win,scaledpsiprf,scaledpsiseq)
C      write (*,*) "TEST TRACE"
      if(scalepsiblast)then
         call copyprof(scaledpsiseq,scaledpsiseq2profile,seqlen)
      else
         call copyprof(seqprofile,seq2profile,seqlen)
      end if
#else
      call dyntrace(profile,proflen,seq,seqlen,alitype,score,traceback)
      call copyseq(seq,seq2,seqlen)
#endif

C Let us also calculate a Z-score
      do i=1,numzscore
#ifdef PRFPRF
      if(scalepsiblast)then
         call randomizeprofile(scaledpsiseq2profile,seqlen,aa)
         tempscore=dynscore_msa(profile,proflen,seq2profile,seqlen ,alitype
     $        ,seqss,profss,seqssfreq,profssfreq,aa,shift,lambda ,method
     $        ,ssmatch,win,scaledpsiprf,scaledpsiseq2profile)
      else
         call randomizeprofile(seq2profile,seqlen,aa)
C      write (*,*) "Test-xxx"
         tempscore=dynscore_msa(profile,proflen,seq2profile,seqlen,alitype
     $        ,seqss,profss,seqssfreq,profssfreq,aa,shift,lambda,method
     $        ,ssmatch,win,scaledpsiprf,scaledpsiseq)
C      write (*,*) "Test-Y",tempscore
      end if
#else
        call randomizeseq(seq2,seqlen)
        tempscore=dynscore(profile,proflen,seq2,seqlen,alitype)
#endif
        sumscore=sumscore+tempscore
        sum2score=sum2score+tempscore**2
      end do
      averscore=sumscore/numzscore
      sd=sqrt((sum2score-sumscore*sumscore/numzscore)/(numzscore-1))
      zscore=(score-averscore)/sd
      write(*,'(A,e11.3,1x,e11.3)') 'SCORE> ',score,zscore

#ifndef IFC
      if (resnumflag .eq. 1) then
        do i=1,maxseqlen
          write(resnum(i),*),i
          write(profresnum(i),*),i
        end do
      end if
#else
      if (resnumflag .eq. 1) then
        do i=1,maxseqlen
          resnum(i)="1"
          profresnum(i)="1"
        end do
      end if
#endif
      
C      write(*,*)'test:',pdbfile,':',alfile,':',x(1)
      if (alfile.ne.'xxx') call writealfile(alfile,seq,seqlen,profseq,proffile,traceback,resnum,profresnum)
      if (pdbfile.ne.'xxx' .and. x2(1).ne.-9999.0) 
     &     call writepdbfile(pdbfile,seq,seqlen,traceback,x2,y2,z2,resnum)

      call traceinvers(maxseqlen,traceback,traceback2)
      if (alfile2.ne.'xxx') call writealfile(alfile2,profseq,proflen,seq,seqfile,traceback2,profresnum,resnum)
      
      if (pdbfile2.ne.'xxx' .and. x(1).ne.-9999.0) 
     &     call writepdbfile(pdbfile2,profseq,proflen,traceback2,x,y,z,profresnum)

      if (printali) then
        do i=1,seqlen
          call numtochar(seqtoseq(seq(i),numaa),char1)
          if (seq(i) .eq. 0) char1='X'
          if (traceback(i).gt.0) then
            call numtochar(seqtoseq(profseq(traceback(i)),numaa),char2)
            if (profseq(traceback(i)) .eq. 0) char2='X'
          else
            char2='.'
          end if
#ifdef PRFPRF
          if (traceback(i)>0) then
             prfprf=profprof(traceback(i),i,profile
     $            ,seqprofile,profss,seqss,profssfreq ,seqssfreq,numaa
     $            ,shift,lambda,method ,ssmatch,proflen,seqlen,win
     $            ,scaledpsiprf ,scaledpsiseq)
          else
             prfprf=0
          end if
          write(*,('(3x,a,4x,a7,6x,a,1x,i9,1x,f8.3)'))char1,resnum(i)
     $         ,char2,traceback(i),prfprf

c add ,profssfreq ,seqssfreq,,proflen,seqlen
#else
            write(*,('(3x,a,4x,a7,6x,a,1x,i9,1x,f8.3)'))char1,resnum(i),char2,traceback(i),
     $         profile(traceback(i),seq(i))
#endif
C     else
C     write(*,*)'trace> ',i,' ',char1,'   -   '
C          end if
        end do
      end if
C           
C From ylva  
C
C      call writeprofile('foo.prf',profile,profseq,proflen,20)

      if (x2(1).ne.-9999) then
        call vdwEnergy(X2,Y2,Z2,seqlen,proflen,traceback,seq,E,R,Etot,Eaa,Eij,Eia)
        call CountSsMatch(seqss,profss,seqlen,traceback,
     &       fraction,gaps,numgaps,ssgaps,allen,numal)
        write(*,'(a,4i8,6e11.3,3i8)') 'SCORES> ',allen,numal,seqlen,proflen,
     &       score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps
      end if

      if (x(1).ne.-9999) then
        call vdwEnergy(X,Y,Z,proflen,seqlen,traceback2,profseq,E,R,Etot,Eaa,Eij,Eia)
        call CountSsMatch(profss,seqss,proflen,traceback2,
     &       fraction,gaps,numgaps,ssgaps,allen,numal)
        write(*,'(a,4i8,6e11.3,3i8)') 'SCORES2> ',allen,numal,seqlen,proflen,
     &       score,Etot,Eaa,Eij,Eia,fraction,gaps,ssgaps,numgaps
      end if

#endif

      stop
      end
***********************************************************************
      subroutine stopprocess(string)
      character*(*) string
      write(*,*) string
      write(*,*)'STOP> Job terminates'
      stop
      end

