C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C http://h18009.www1.hp.com/fortran/docs/vf-html/pg/pgwuscom.htm
C
      integer maxseqlen
      integer maxaa
      integer maxproflen
      integer penopen
      integer penext
      integer maxtype
      integer maxnum
      integer maxktup
      integer maxhash
      integer maxtop
      integer maxss
      integer maxnninp
      integer maxnnhidden
      integer maxnnout
      parameter (maxtop=50)
      parameter (maxproflen=1000)
      parameter (maxseqlen=1000)
      parameter (maxaa=60)
      parameter (maxtype=100)
      parameter (maxss=3)
      parameter (maxnum=2000)
      parameter (penopen=maxaa+1)
      parameter (penext=maxaa+2)
      parameter (maxktup=maxaa*maxaa)
      parameter (maxhash=201)
      parameter (maxnninp=100)
      parameter (maxnnhidden=40)
      parameter (maxnnout=3)
