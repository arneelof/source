C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
c*******************************************************************************

      real function dynscore(profile,proflen,seq,seqlen,type)

c type=0 => local
c type=1 => ends free
c type=1 => ends free
c type=10 => global 
      implicit none
#include <dynprog.h>

      real bestscore,maxscorey,maxscorex
      integer  x, y,type,yminone
      real profile(maxproflen,maxaa+2),wgtend
      real seqskip, profskip(maxseqlen),typefact,minscore
      real prescore(maxseqlen),  lastscore, bestjump
      real score, diagtest,  proftest
      real temp
      integer seqlen,proflen,seq(maxseqlen)
      integer xminone


c     init dimensions and bestscore
c     do the bottom row (y=1)

      wgtend=0
      if (type .eq. 10) then
        typefact=1.
        minscore=-9999.0
        bestscore = -9999.0
        profskip(1)=-9999.0
        prescore(1)=profile(1,seq(1))
        wgtend=profile(1,penopen)
C        write (*,*) 'test1:',1,1,prescore(1)
C        bestscore = max(bestscore,score)
        do x = 2, seqlen
          wgtend=wgtend+profile(1,penext)
          prescore(x)=profile(1,seq(x))+wgtend
          bestscore = max(bestscore,prescore(x))
          profskip(x) = -9999.0
C          write (5,'(a)') "test1:",x,1,prescore(x)
        end do
      elseif (type .eq. 0) then
        minscore=0.0
        bestscore = minscore
        typefact=0.
        do x = 1, seqlen
          score=profile(1,seq(x))
          prescore(x)= max(score,minscore)
          profskip(x) = -9999.0
          bestscore = max(bestscore,score)
C          write(*,*) "test1:",x,profile(1,seq(x))
        end do
      else
        call stopprocess('Unknown alignment methods')
      end if

      wgtend = (profile(1,penopen))*typefact
      seqskip=max(minscore,profile(1,penopen))
      maxscorey=minscore
      score=minscore
      do y = 2, proflen
        yminone=y-1
C        write(*,*)'test1',maxscorey,score
        maxscorey=max(maxscorey,score+profile(yminone,penopen))+profile(yminone,penext)
c     for x=1 
        x = 1

        score = max(minscore,(profile(y,seq(x))))
        lastscore = score
        bestscore = max(bestscore,score)
        seqskip = -9999.0
        wgtend = wgtend+profile(y,penext)*typefact
C        write (*,*) 'test2:',x,y,prescore(x)
        do x = 2, seqlen
          xminone=x-1
          
          temp = profile(y,seq(x))
          diagtest = prescore(xminone)
          proftest = profskip(xminone)
          score = max(diagtest,max(proftest,seqskip))+temp
          score = max(score,minscore)
          bestscore = max(bestscore,score)
          bestjump = prescore(xminone) + profile(y,penopen)

          seqskip = max(seqskip,bestjump)
          seqskip = seqskip + profile(y,penext)

          profskip(xminone) = max(bestjump,profskip(xminone))
          profskip(xminone) = profskip(xminone) + profile(y,penext)
          prescore(xminone) = lastscore
          lastscore = score
C          write (*,*) 'test3:',x,y,score
        end do
C     St�mmer detta verkligen skall det inte vara:
C        write(*,*)'test',y,score,maxscorey
        prescore(1)=profile(y,seq(1))+wgtend
C        prescore(1)=profile(y,seq(x))+wgtend
      end do


C      write(*,*)'dynscore',score,maxscorey,maxscorex,bestscore

      if (type .eq. 10) then
        maxscorex=max(minscore,score)
        temp=profile(proflen,penopen)
        do x = seqlen-1,1,-1
          temp=temp+profile(proflen,penext)
          maxscorex=max(maxscorex,prescore(x)+temp)
        end do
C        write(*,*)'testing',score,maxscorey,maxscorex
        dynscore=max(score,max(maxscorey,maxscorex))
      elseif (type .eq. 0) then 
        dynscore=bestscore
      end if
      return 

      end

