C The Palign protein alignment program 
C 
C Copyright (C) Arne Elofsson
C 
C 
C This program is free software; you can redistribute it and/or modify
C it under the terms of the GNU General Public License as published by
C the Free Software Foundation; version 2 of the License. With the
C exception that if you use this program in any scientific work you have
C to explicitly state that you have used PALIGN and cite the relevant
C publication (dependent on what you have used PALIGN for). My
C publicationlist can be found at http://www.sbc.su.se/~arne/papers/.
C This program is distributed in the hope that it will be useful, but
C WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
C General Public License for more details. You should have received a
C copy of the GNU General Public License along with this program (in 
C the file gpl.txt); if not, write to the Free Software Foundation, 
C Inc., 59 Temple Place - Suite 330, Boston, MA 02111-13
C
C For support please use email to arne@sbc.su.se and add PALIGN-SUPPORT 
C in the header
C 
C
*************************************************************
      subroutine readanyseq(filename,seq,seqlen,found,profile,seqss
     $     ,seqssfreq,x,y,z,type,resnum)
      implicit none
#include <dynprog.h>
      character*(*)filename,type
      character*1 chain
      character*4 resnum(*)
      Integer   SeqLen,seq(*),seqss(*),offset
      real profile(maxproflen,maxaa+2)
      real seqssfreq(maxproflen,maxss)
      real x(*),y(*),z(*)
      logical found


C     First we try to read a sequence file
C      write (*,*) "testing readseq"
C      write (*,*) "testing readpsiblastprofile"
      call readpsiblastprofile(filename,profile,seq,seqlen,found)
      if (found .and. seqlen .gt. 0)   then
        type="psiblast"
        return
      end if
        
c      write (*,*) "testing readslx"
      call readslx(filename, seq, seqlen, seqss, found)
      if (found .and. seqlen .gt. 0)   then
        type="slx"
        return
      end if

      chain='x'
c      write (*,*) "testing readpdb_ca"
      call readpdb_ca(filename,chain,seq,seqlen,seqss,found,x,y,z,offset,resnum)
      if (found .and. seqlen .gt. 0)   then
        type="pdb"
        return
      end if


C      write(*,*) "testing impala"
      call readimpala(filename,profile,seq,seqlen,found)
      if (found .and. seqlen .gt. 0)then
        type="impala"
        return
      end if


C      write (*,*) "testing readpsipred2"
      call readpsipred2(filename, seq, seqlen, seqss, seqssfreq, found)
      if (found .and. seqlen .gt. 0)   then
        type="psipred2"
c        write(*,*)'psssipred',seqssfreq(4,1:3)
        return
      end if

C      write (*,*) "testing readpsipred"
      call readpsipred(filename, seq, seqlen, seqss, found)
      if (found .and. seqlen .gt. 0)   then
        type="psipred"
        return
      end if


C      write (*,*) "testing readstride"
      call readstride(filename, seq, seqlen, seqss, found,resnum,chain)
      if (found .and. seqlen .gt. 0)   then
        type="stride"
        return
      end if

C      write (*,*) "testing readtmHMM"
      call readtmhmm(filename, seq, seqlen, seqss, found)
      if (found .and. seqlen .gt. 0)   then
        type="tmhmm"
        return
      end if


C      write(*,*) "testing sequence"
      call readseq(filename,seq,seqlen,found)
      if (found .and. seqlen .gt. 0)then
        type="seq"
        return
      end if


      write(*,*) filename
      call stopprocess('ERROR: sequence  not found')
      return
      END
*************************************************************
      subroutine readseq(filename,seq,seqlen,found)
      implicit none
#include <dynprog.h>      
      character*(*)filename
      character*6500 line
      character*1 char
      Integer   FilNum,SeqLen,seq(*)
      integer   length,j,chartonum
      logical found
      parameter (filnum=90)
      external chartonum

      found=.true.
      seqlen=0
      Open(unit=filnum,file=filename,Form='FORMATTED',status ='OLD',
     &     access='SEQUENTIAL',err=777)

      Read (FilNum,FMT='(A)',end=777) line
      do while (Index(line,'..' ) .eq. 0 .and. Index(line,'>') .ne. 1 )
        Read (FilNum,FMT='(A)',end=777) line
      end do

      do while (.TRUE. .and. seqlen .lt.maxseqlen)
        read (FilNum,FMT='(A)',END=999) line
        length=len(line)
        call trima(line,length)
        do j=1,length
          char=line(j:j)
          call muc(char)
          seqlen=seqlen+1
C          write(*,*)'test',seqlen,char
          seq(seqlen)=charToNum(char)
       end do
      end do

 999  continue
      close (unit=filnum)
      return

 777  found=.false.
      close (unit=filnum)
      return
      END

*****************************************************************
      subroutine readpsiblastprofile(infile,profile,seq,seqlen,found)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer seqlen
      integer length,i,j,k,filnum,namtonum,seq(*),wdlen
      character*(*) InFile
      character Line*200,word(40)*5,word2(40)*5,temp*3,temp2*3
      logical found
      external namtonum
      parameter (filnum=90)
      real profile(maxproflen,maxaa+2),foo(20)
c      write(*,*)'test readpsiblastprofile'
      found=.false.
      Open(unit=filnum,file=Infile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      read(filnum,'(A)',err=777,end=777) line
      do while (INDEX(line,'Last position-specific scoring matrix computed') .eq. 0)
        read(filnum,'(A)',err=777,end=777) line
C        write(*,*)'test ',line
      end do
      
      read(filnum,'(A)',err=777,end=777) line
      length=len(line)
      do i = 1,20
        call nextwd(line,length,word2(i),4,wdlen)
        if (word2(i)(3:3).eq.' ') then
          call onetothree(word2(i)(1:1),word(i))
        else
          word(i)=word2(i)(1:3)
        end if
      end do

      temp='   '
      seqlen = 1
      do while (.true.)
        read(filnum,'(A)',err=777,end=777) line
        length=len(line)
        call trima(line,length)
C        write (*,*) 'TEST ',line(1:1),length
        if (length.gt.0)  then
          read(line,*,end=300,err=300)  k,temp,(profile(seqlen,namtonum(word(i))),i=1,20)
          if (temp(3:3).eq.' ') then
            call onetothree(temp(1:1),temp2)
          else
            temp2=temp
          end if         
c          write(*,*)profile(seqlen,namtonum(word(41)))
          seq(seqlen)=namtonum(temp2)
          seqlen = seqlen + 1
          found=.true.
c          write (*,*) 'test1',line(1:10)
        end if
 300  end do

 777  close (unit=filnum)
      seqlen = seqlen - 1
      return
C call stopprocess('ERROR - not found')
      END
     
*****************************************************************
      subroutine readimpala(infile,profile,seq,seqlen,found)
C     
C     reads an impala profile (codes copies from psipred
C     
      implicit none
#include <dynprog.h>      
      integer seqlen,tempseqlen,j,chartonum
      integer length,i,k,filnum,namtonum,seq(*)
      character*(*) InFile
      character Line*6500,char
      logical found
      parameter (filnum=90)
      real profile(maxproflen,maxaa+2),temp,nextf
      external namtonum,nextf,chartonum

      found=.false.
      Open(unit=filnum,file=Infile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      read(filnum,*,err=777,end=777) tempseqlen
      read(filnum,'(A)',err=777,end=777) line
      length=len(line)
      call trima(line,length)
      do j=1,length
        char=line(j:j)
        call muc(char)
        seqlen=seqlen+1
C        write(*,*)'test',seqlen,char
        seq(seqlen)=charToNum(char)
      end do
      if (tempseqlen .ne. seqlen) then
C        write(*,*) 'WARNING: SEQLEN NE TEMPSEQLEN (1) ',tempseqlen,'  ',seqlen
        seqlen=0
        close (unit=filnum)
        return
      end if

      seqlen=0
      do while (line(1:7) .ne. '-32768 ')
        read(filnum,'(A)',err=777,end=777) line
      end do
      
      do while (line(1:7) .eq. '-32768 ')
        seqlen=seqlen+1
        length=len(line)
        call trima(line,length)
        temp=nextf(line,length)
        profile(seqlen,namtonum('ALA'))=nextf(line,length)/100.0 
        temp=nextf(line,length)
        profile(seqlen,namtonum('CYS'))=nextf(line,length)/100.0
        profile(seqlen,namtonum('ASP'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('GLU'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('PHE'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('GLY'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('HIS'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('ILE'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('LYS'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('LEU'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('MET'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('ASN'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('PRO'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('GLN'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('ARG'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('SER'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('THR'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('VAL'))=nextf(line,length)/100.0 
        profile(seqlen,namtonum('TRP'))=nextf(line,length)/100.0 
        temp=nextf(line,length)
        profile(seqlen,namtonum('TYR'))=nextf(line,length)/100.0 
        read(filnum,'(A)',err=777,end=300) line
      end do

 300  continue
      if (tempseqlen .ne. seqlen) then
C        write(*,*) 'SEQLEN NE TEMPSEQLEN (2) ',tempseqlen,'  ',seqlen
        seqlen=0
        close (unit=filnum)
        return
      else
        found=.true.
      end if

 777  close (unit=filnum)
      return
C call stopprocess('ERROR - not found')
      END
     
*****************************************************************
      subroutine readpsiblastfreq(infile,profile,seq,seqlen,found)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer seqlen,j
      integer length,i,k,filnum,namtonum,seq(*),wdlen
      character*(*) InFile
      character Line*200,word(40)*5,word2(40)*5,temp*3,temp2*3
      logical found
      external namtonum
      parameter (filnum=90)
      real profile(maxproflen,maxaa+2),foo(20)
c      write(*,*)'test readpsiblastfreq'
      found=.false.
      Open(unit=filnum,file=Infile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      read(filnum,'(A)',err=777,end=777) line
      do while (INDEX(line,'Last position-specific scoring matrix computed') .eq. 0)
        read(filnum,'(A)',err=777,end=777) line
      end do

      read(filnum,'(A)',err=777,end=777) line
      length=len(line)
      do i = 1,20
        call nextwd(line,length,word2(i),4,wdlen)
        if (word2(i)(3:3).eq.' ') then
          call onetothree(word2(i)(1:1),word(i))
        else
          word(i)=word2(i)(1:3)
        end if
      end do

      temp='   '
      seqlen = 1
      do while (.true.)
        read(filnum,'(A)',err=777,end=777) line
        length=len(line)
        call trima(line,length)
        if (length.gt.0)  then
          read(line,*,end=300,err=300)  k,temp,(foo(j),j=1,20)
     $         ,(profile(seqlen,namtonum(word(i))),i=1,20)
          do i=1,20
            profile(seqlen,namtonum(word(i)))=profile(seqlen,namtonum(word(i)))/100.0
          end do
          if (temp(3:3).eq.' ') then
            call onetothree(temp(1:1),temp2)
          else
            temp2=temp
          end if         
C          write(*,*)seqlen,temp2
          seq(seqlen)=namtonum(temp2)
          seqlen = seqlen + 1
          found=.true.
        end if
 300  end do

 777  close (unit=filnum)
      seqlen = seqlen - 1
      return
C call stopprocess('ERROR - not found')
      END
   

C*****************************************************************
C*         from Ylva
C*  This presently only reads a single sequence from an slx file
C*****************************************************************
      subroutine readslx (filename, seq, seqlen, ss, found)
      implicit none
      character*(*)filename
      character*100 line1
      character*100 line2
      character*1 char
      Integer   filnum, seqlen, seqlenss, seq(*), ss(*)
      integer   length, lengthss, j, chartonum, chartoss
      logical found
      parameter (filnum=90)
      external chartonum,chartoss

      found=.false.
      seqlen=0
      seqlenss = 0
      Open(unit=filnum,file = (filename),Form='FORMATTED',
     &     status ='OLD',access='SEQUENTIAL',err=999)
      
      Do While (.TRUE.)
        read (filnum, *,end=999) line1, line2

        if (line1(1:4).eq. '#=AU') found=.true.
        if (found .and. line1(1:4).eq. '#=SS') then
           lengthss = len(line2)
           call trima(line2, lengthss)
           do j = 1, lengthss
             char = line2(j:j)
             call muc(char)
             seqlenss = seqlenss+1
             ss(seqlenss) = chartoss(char)
           end do
            
         else if (found .and. line1(1:4) .ne. '#=RF' .and. line1(1:4) .ne. '#=AU') 
     &           then
            length = len(line2)
            call trima(line2, length)
            do j = 1, length
               char = line2(j:j)
               call muc(char)
                  seqlen = seqlen+1
                  seq(seqlen) = chartonum(char)
            end do
         end if
      End Do
      if (seqlenss .ne. seqlen .and. seqlenss .gt. 0) 
     $     call stopprocess('ERROR> Error Secondary structure length wrong !')
      
 999  continue
      close (unit=filnum)
      return
      END

C*****************************************************************
C*         from Ylva
C*  This presently only reads a single sequence from an slx file
C*****************************************************************
      subroutine readpsipred(filename, seq, seqlen, ss, found)
      implicit none
      character*(*)filename
      character*100 line1
      character*100 line2
      character*1 char
      Integer   filnum, seqlen, seqlenss, seq(*), ss(*)
      integer   length, lengthss, j, chartonum, chartoss
      logical found
      parameter (filnum=90)
      external chartonum,chartoss

      found=.false.
      seqlen=0
      seqlenss = 0
      Open(unit=filnum,file = (filename),Form='FORMATTED',
     &     status ='OLD',access='SEQUENTIAL',err=999)
      
      Do While (.TRUE.)
        read (filnum, *,end=999) line1, line2

C        write (*,*)'test',line1(1:3),":",line1(1:5)
        if (line1(1:5).eq. 'Pred:') then
          found=.TRUE.
          lengthss = len(line2)
          call trima(line2, lengthss)
          do j = 1, lengthss
            char = line2(j:j)
            call muc(char)
            seqlenss = seqlenss+1
C            write(*,*)'testing 'seqlenss,,char,chartoss(char)
            ss(seqlenss) = chartoss(char)
          end do
          
        else if (line1(1:3) .eq. 'AA:') then
          length = len(line2)
          call trima(line2, length)
          do j = 1, length
            char = line2(j:j)
            call muc(char)
            seqlen = seqlen+1
            seq(seqlen) = chartonum(char)
          end do
        end if
      End Do
      if (seqlenss .ne. seqlen .and. seqlenss .gt. 0) 
     $     call stopprocess('ERROR> Error Secondary structure length wrong !')
      
 999  continue
C      write(*,*)'TESTING:',seqlen,seqlenss,seq(1),ss(1)
      close (unit=filnum)
      return
      END
C************************************************************************
      subroutine readpsipred2(filename, seq, seqlen, ss, ssfreq, found)
      implicit none
#include <dynprog.h>
      character*(*)filename
      character*80 line
      character*1 char1,char2
      Integer   filnum, seqlen, seq(*), ss(*)
      integer    chartonum, chartoss
      integer   num,length
      real      coil,helix,sheet
      real ssfreq(maxproflen,maxss)
      logical found
      parameter (filnum=90)
      external chartonum,chartoss

      found=.false.
      seqlen=0
      num=0
      Open(unit=filnum,file = (filename),Form='FORMATTED',
     &     status ='OLD',access='SEQUENTIAL',err=999)
      
      Do While (.TRUE.)
        read (filnum,'(a)',end=999,err=998) line
        if (line(1:1) .ne. '#') then
          length=len(line)
          call trima(line,length)
C          write(*,*)'testa ',length,"  ",line(1:length)
          if (length .gt. 2) then
            read (line, *,end=998,err=998) num,char1,char2,coil,helix,sheet

c            write (*,*)'test',num,":",char1,":",char2,":",coil,":",helix,":",sheet
            call muc(char1)
            call muc(char2)
            ssfreq(num,1)=helix
            ssfreq(num,2)=sheet
            ssfreq(num,3)=coil
            seq(num) = chartonum(char1) 
            ss(num) = chartoss(char2)
C          else
C            write(*,*)'test2 ',line
          end if
C        else
C          write(*,*)'test3 ',line
        end if
      End Do

 999  continue
      seqlen=num
      if (seqlen .gt. 1)      found=.TRUE.
 998  close (unit=filnum)
C      write(*,*)'TESTING-pp2:',num,seqlen,found
      return
      END
C************************************************************************
C*****************************************************************
      subroutine readmsaslx (filename, seq, seqlen, ss, found)
      implicit none
      character*(*)filename
      character*100 line1
      character*100 line2
      character*1 char
      Integer   filnum, seqlen, seqlenss, seq(*), ss(*)
      integer   length, lengthss, j, chartonum
      logical found
      parameter (filnum=90)
      external chartonum

      found=.false.
      seqlen=0
      seqlenss = 0
      Open(unit=filnum,file = (filename),Form='FORMATTED',
     &     status ='OLD',access='SEQUENTIAL',err=999)
      
      Do While (.TRUE.)
        read (filnum, *,end=999) line1, line2

        if (line1(1:4).eq. '#=AU') found=.true.
        if (found .and. line1(1:4).eq. '#=SS') then
           lengthss = len(line2)
           call trima(line2, lengthss)
           do j = 1, lengthss
             char = line2(j:j)
               call muc(char)
                  seqlenss = seqlenss+1
                  ss(seqlenss) = chartonum(char)
            end do
            
         else if (found .and. line1(1:4) .ne. '#=RF' .and. line1(1:4) .ne. '#=AU') 
     &           then
            length = len(line2)
            call trima(line2, length)
            do j = 1, length
               char = line2(j:j)
               call muc(char)
                  seqlen = seqlen+1
                  seq(seqlen) = chartonum(char)
            end do
         end if
      End Do
      if (seqlenss .ne. seqlen .and. seqlenss .gt. 0) 
     $     call stopprocess('ERROR> Error Secondary structure length wrong !')
      
 999  continue
      close (unit=filnum)
      return
      END

**********************************************************************
*     READPDBFILE
*     
*     Reads a coordinate file in PDB format. 
      Subroutine readpdb_ca(filename,chain,seq,seqlen,ss,found,x,y,z,offset,resnum)
      implicit none

#include <dynprog.h>
      real  X(*),Y(*),Z(*)
      character*(*)filename
      character*6500 line
      character*4 resnum(*)
      character*1 tempres1,chain,testchain,tempres2
      character*4 atom,resnam,atnam
      Integer   FilNum,SeqLen,seq(*),atnum,namtonum,ss(*)
      integer   chartonum,offset,chartoss
      logical found
      parameter (filnum=90)
      external chartonum,namtonum,chartoss

      found = .FALSE.
      Open(Unit=FilNum, File = filename, Access='SEQUENTIAL',
     &     Status = 'OLD', Form='FORMATTED')
      seqlen=1
      atnam='    '
      do while (.true.)
        read (filnum,'(A)',end=26) line
        if (line(1:4).eq.'ATOM' .or. (line(1:6).eq.'HETATM' .and. line(18:20).eq.'MSE' )) then
 666      read(line,111,end=26,err=26) atom,atnum,atnam,tempres1,resnam,testchain,
     1         resnum(seqlen),tempres2,x(seqlen),y(seqlen),z(seqlen)
C          write(*,*)'test:',seqlen,":",atom,":",atnam,":",chain,":",testchain,":"
          
          if ( chain .eq. ' ' .or. chain .eq. 'X' .or. chain .eq. 'x')
     &         chain=testchain
          
          if(atnam .eq. 'CA  ' .and. chain .eq.testchain)  then
            if (tempres1 .eq. ' ' .or. seqlen .eq. 1) then
C      write(*,*)'found1:',seqlen,":",atom,":",atnam,":",chain,":",testchain,":",tempres1
C     $          ,":",resnum(seqlen)   
              found=.true.
              seq(seqlen)=namtonum(resnam)
              seqlen=seqlen+1
            elseif (resnum(seqlen) .ne. resnum(seqlen-1) ) then            
C      write(*,*)'found2:',seqlen,":",atom,":",atnam,":",chain,":",testchain,":",tempres1
C     $          ,":",resnum(seqlen)   
              found=.true.
              seq(seqlen)=namtonum(resnam)
              seqlen=seqlen+1
            end if
          end if
        end if
      end do
 110  format(a5,16x,i4,8x,i4)
 111  format(a4,3x,i4,2x,a3,a1,a3,1x,a1,a4,a1,3x,3f8.3)
 112  format(a5,1x,i5,1x,a4,3x,i4,2x,a3,a1,a3,1x,a1,a4,a1,3x,3f8.3)

 26   continue
      seqlen=seqlen-1
 27   close(unit=filnum)
      return
      END 
**********************************************************************
*     Reads a file in stride format. 
      Subroutine readstride(filename,seq,seqlen,ss,found,resnum,chain)
      implicit none

#include <dynprog.h>
      character*(*)filename
      character*6500 line
      character*3 resnam
      character*4 resnum(*)
      character*1 sstype
      character*1 chain,testchain
      Integer   FilNum,SeqLen,seq(*),namtonum,ss(*)
      integer   chartonum,chartoss
      logical found
      parameter (filnum=90)
      external chartonum,namtonum,chartoss

      found = .FALSE.
      Open(Unit=FilNum, File = filename, Access='SEQUENTIAL',
     &     Status = 'OLD', Form='FORMATTED')
      seqlen=1
      do while (.true.)
        read (filnum,'(a)',end=26) line
        if (line(1:4).eq.'ASG ') then
C          write(*,*)line
          read (line,110,end=26,err=26) resnam,testchain,resnum(seqlen),sstype
          if ( chain .eq. ' ' .or. chain .eq. 'X' .or. chain .eq. 'x')
     &         chain=testchain

C          write(*,*)'test1',resnam,':',sstype,testchain
          if (chain.eq.testchain) then
            found=.true.
            seq(seqlen)=namtonum(resnam)
            ss(seqlen)=chartoss(sstype)
C            write(*,*)'test2',seqlen,':',resnam,':',sstype,':',seq(seqlen),':',ss(seqlen)
            seqlen=seqlen+1
          end if
        end if
      end do
 110  format(3x,2x,a3,1x,a1,1x,a4,9x,a1)

 26   close(unit=filnum)
      seqlen=seqlen-1

C      if (.not. found .or. seqlen .le. 0)
C     $     call stopprocess('ERROR> Error reading PDB-file !')
      return
      END 
**********************************************************************
*     Reads a file in tmHMM format. 
      Subroutine readtmhmm(filename,seq,seqlen,ss,found)
      implicit none

#include <dynprog.h>
      character*(*)filename
      character*6500 line
      character*1 char
      Integer   FilNum,SeqLen,seq(*),ss(*)
      integer   chartonum,wdlen,nexti,length,i
      logical found
      parameter (filnum=90)
      real inside,outside,membrane,nextf
      external chartonum,nexti,nextf

      found = .FALSE.
      Open(Unit=FilNum, File = filename, Access='SEQUENTIAL',
     &     Status = 'OLD', Form='FORMATTED')
      seqlen=0
      do while (.true.)
        read (filnum,'(a)',end=26) line
        if (line(1:1).le.'9' .and. line(1:1).ge.'1') then
C          write(*,*) line
C          read (line,210,end=26) i,seq(seqlen),inside,membr,outside
          length=len(line)
          i=nexti(line,length)
          call trima(line,length)
          call nextwd(line,length,char,1,wdlen)
          if (chartonum(char) .ne. 21)  then
C          write(*,*) "test",seqlen,seq(seqlen),char
            inside=nextf(line,length)          
            membrane=nextf(line,length)          
            outside=nextf(line,length)          
            if ((inside+membrane+outside) .gt. 0.9) then
C              write(*,*) "test2",inside+membrane+outside
              seqlen=seqlen+1
              found=.true.
            end if
            seq(seqlen)=chartonum(char)
            ss(seqlen)=0
            if (inside .gt. membrane) then 
              if (inside .gt. outside) then 
                ss(seqlen)=1
              else
                ss(seqlen)=3
              end if
            else
              if (membrane .gt. outside) then 
                ss(seqlen)=2
              else
                ss(seqlen)=3
              end if
            end if
C            write(*,*) seqlen,seq(seqlen),ss(seqlen)
          end if
        end if
      end do
 26   close(unit=filnum)
C 210  format(i,x,a1,x,3(f,1x))
      return
      END 

*****************************************************************
      subroutine readseqtable(infile,table,tablesize)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer length,i,j,filnum,namtonum,tablesize,wdlen
      character*(*) InFile
      character Line*200,word(40)*5,word2(40)*5,char*1
      real table(tablesize,tablesize)
      external namtonum
      parameter (filnum=90)

      Open(unit=filnum,file=Infile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      read(filnum,'(A)',err=777,end=777) line
      do while (INDEX(line,'..') .eq. 0 .and. INDEX(line,'A  R  N') .eq. 0)
        read(filnum,'(A)',err=777,end=777) line
C        write(*,*)'test0',line
      end do
      
      length=len(line)
      do i = 1,22
        call nextwd(line,length,word2(i),4,wdlen)
        if (word2(i)(3:3).eq.' ') then
          call onetothree(word2(i)(1:1),word(i))
        else
          word(i)=word2(i)(1:3)
        end if
      end do
      
      j = 0
      do while (.true.)
        read(filnum,'(A)',err=777,end=600) line
        length=len(line)
C        call trima(line,length)
C        write (*,*) 'TEST ',line(1:1),length
        if (length.gt.0)  then
          j = j + 1
          if (line(1:1) .ne. ' ') then 
C            write (*,*)'test1:',line(1:1)
            read(line,*,err=777,end=300)  char,(table(namtonum(word(j)),namtonum(word(i))),i=1,22)
C            write (*,*) 'test1',j,line(1:10)
          else
C            write (*,*)'test2',line
            read(line,*,err=777,end=300)  (table(namtonum(word(j)),namtonum(word(i))),i=j,22)
C            write (*,*) 'test2',j,line(1:10)
            do  i = j,22
              table(namtonum(word(i)),namtonum(word(j)))=
     $             table(namtonum(word(j)),namtonum(word(i)))
              table(namtonum(word(j)),namtonum(word(i)))=
     $             table(namtonum(word(i)),namtonum(word(j)))
            end do 
          end if
        end if
 300  end do
 600  continue
      close (unit=filnum)
      return
 777  continue
C      call stopprocess('ERROR - Table not found')
      call defaulttable_pam250(table,tablesize)
      END

*****************************************************************
      subroutine readfastaseqtable(infile,table,tablesize)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer length,i,j,filnum,namtonum,tablesize,wdlen,chartonum
      character*(*) InFile
      character Line*200,word(40)*3,word2(40)*3
      
      real table(tablesize,tablesize)
      external namtonum,chartonum
      parameter (filnum=90)

      Open(unit=filnum,file=Infile,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      read(filnum,'(A)',err=777,end=777) line
      do while (INDEX(line,'@ *') .eq. 0.)
        read(filnum,'(A)',err=777,end=777) line
C        write(*,*)'test0',line
      end do
      
      read(filnum,'(A)',err=777,end=600) line
      length=len(line)
      do i = 1,22
        call nextwd(line,length,word2(i),4,wdlen)
        if (word2(i)(3:3).eq.' ') then
          call onetothree(word2(i)(1:1),word(i))
        else
          word(i)=word2(i)(1:3)
        end if
      end do
      
      read(filnum,'(A)',err=777,end=600) line
      j = 0
      do while (.true.)
        read(filnum,'(A)',err=777,end=600) line
        length=len(line)
        call trima(line,length)
C        write (*,*) 'TEST ',length
        if (length.gt.0)  then
          j = j + 1
C          write (*,*)'test1',j,line
          read(line,*,err=777,end=300) (table(namtonum(word(j)),namtonum(word(i))),i=1,j)
          do  i = 1,j
C           write (*,*) 'test2: ',j,i,word(j),word(i),namtonum(word(j)),namtonum(word(j)),
C     $           table(namtonum(word(j)),namtonum(word(i)))
            table(namtonum(word(i)),namtonum(word(j)))=
     $           table(namtonum(word(j)),namtonum(word(i)))
C            table(namtonum(word(j)),namtonum(word(i)))=
C     $           table(namtonum(word(i)),namtonum(word(j)))
          end do 
        end if
 300  end do
 600  continue
      close (unit=filnum)
      return
 777  continue
C      call stopprocess('ERROR - FASTA Table not found')
      call defaulttable_pam250(table,tablesize)

      END
************************************************************************
      subroutine defaulttable_pam250(table,tablesize)      
      implicit none
#include <dynprog.h>      
      integer tablesize
      real table(tablesize,tablesize)
      write(*,*) 'No sequence table was found using the default PAM250 MATRIX'

C ALA
      table(1,1 )=  2.00
      table(1,2 )= -2.00
      table(1,3 )=  0.00
      table(1,4 )=  0.00
      table(1,5 )= -4.00
      table(1,6 )=  1.00
      table(1,7 )= -1.00
      table(1,8 )= -1.00
      table(1,9 )= -1.00
      table(1,10)= -2.00
      table(1,11)= -1.00
      table(1,12)=  0.00
      table(1,13)=  1.00
      table(1,14)=  0.00
      table(1,15)= -2.00
      table(1,16)=  1.00
      table(1,17)=  1.00
      table(1,18)=  0.00
      table(1,19)= -6.00
      table(1,20)= -3.00
C CYS
      table(2,1 )= -2.00
      table(2,2 )= 12.00
      table(2,3 )= -5.00
      table(2,4 )= -5.00
      table(2,5 )= -4.00
      table(2,6 )= -3.00
      table(2,7 )= -3.00
      table(2,8 )= -2.00
      table(2,9 )= -5.00
      table(2,10)= -6.00
      table(2,11)= -5.00
      table(2,12)= -4.00
      table(2,13)= -3.00
      table(2,14)= -5.00
      table(2,15)= -4.00
      table(2,16)=  0.00
      table(2,17)= -2.00
      table(2,18)= -2.00
      table(2,19)= -8.00
      table(2,20)=  0.00
C ASP
      table(3,1 )=  0.00
      table(3,2 )= -5.00
      table(3,3 )=  4.00
      table(3,4 )=  3.00
      table(3,5 )= -6.00
      table(3,6 )=  1.00
      table(3,7 )=  1.00
      table(3,8 )= -2.00
      table(3,9 )=  0.00
      table(3,10)= -4.00
      table(3,11)= -3.00
      table(3,12)=  2.00
      table(3,13)= -1.00
      table(3,14)=  2.00
      table(3,15)= -1.00
      table(3,16)=  0.00
      table(3,17)=  0.00
      table(3,18)= -2.00
      table(3,19)= -7.00
      table(3,20)= -4.00
C GLU
      table(4,1 )=  0.00
      table(4,2 )= -5.00
      table(4,3 )=  3.00
      table(4,4 )=  4.00
      table(4,5 )= -5.00
      table(4,6 )=  0.00
      table(4,7 )=  1.00
      table(4,8 )= -2.00
      table(4,9 )=  0.00
      table(4,10)= -3.00
      table(4,11)= -2.00
      table(4,12)=  1.00
      table(4,13)= -1.00
      table(4,14)=  2.00
      table(4,15)= -1.00
      table(4,16)=  0.00
      table(4,17)=  0.00
      table(4,18)= -2.00
      table(4,19)= -7.00
      table(4,20)= -4.00
C PHE
      table(5,1 )= -4.00
      table(5,2 )= -4.00
      table(5,3 )= -6.00
      table(5,4 )= -5.00
      table(5,5 )=  9.00
      table(5,6 )= -5.00
      table(5,7 )= -2.00
      table(5,8 )=  1.00
      table(5,9 )= -5.00
      table(5,10)=  2.00
      table(5,11)=  0.00
      table(5,12)= -4.00
      table(5,13)= -5.00
      table(5,14)= -5.00
      table(5,15)= -4.00
      table(5,16)= -3.00
      table(5,17)= -3.00
      table(5,18)= -1.00
      table(5,19)=  0.00
      table(5,20)=  7.00
C GLY
      table(6,1 )=  1.00
      table(6,2 )= -3.00
      table(6,3 )=  1.00
      table(6,4 )=  0.00
      table(6,5 )= -5.00
      table(6,6 )=  5.00
      table(6,7 )= -2.00
      table(6,8 )= -3.00
      table(6,9 )= -2.00
      table(6,10)= -4.00
      table(6,11)= -3.00
      table(6,12)=  0.00
      table(6,13)= -1.00
      table(6,14)= -1.00
      table(6,15)= -3.00
      table(6,16)=  1.00
      table(6,17)=  0.00
      table(6,18)= -1.00
      table(6,19)= -7.00
      table(6,20)= -5.00
C HIS
      table(7,1 )= -1.00
      table(7,2 )= -3.00
      table(7,3 )=  1.00
      table(7,4 )=  1.00
      table(7,5 )= -2.00
      table(7,6 )= -2.00
      table(7,7 )=  6.00
      table(7,8 )= -2.00
      table(7,9 )=  0.00
      table(7,10)= -2.00
      table(7,11)= -2.00
      table(7,12)=  2.00
      table(7,13)=  0.00
      table(7,14)=  3.00
      table(7,15)=  2.00
      table(7,16)= -1.00
      table(7,17)= -1.00
      table(7,18)= -2.00
      table(7,19)= -3.00
      table(7,20)=  0.00
C ILE
      table(8,1 )= -1.00
      table(8,2 )= -2.00
      table(8,3 )= -2.00
      table(8,4 )= -2.00
      table(8,5 )=  1.00
      table(8,6 )= -3.00
      table(8,7 )= -2.00
      table(8,8 )=  5.00
      table(8,9 )= -2.00
      table(8,10)=  2.00
      table(8,11)=  2.00
      table(8,12)= -2.00
      table(8,13)= -2.00
      table(8,14)= -2.00
      table(8,15)= -2.00
      table(8,16)= -1.00
      table(8,17)=  0.00
      table(8,18)=  4.00
      table(8,19)= -5.00
      table(8,20)= -1.00
C LYS
      table(9,1 )= -1.00
      table(9,2 )= -5.00
      table(9,3 )=  0.00
      table(9,4 )=  0.00
      table(9,5 )= -5.00
      table(9,6 )= -2.00
      table(9,7 )=  0.00
      table(9,8 )= -2.00
      table(9,9 )=  5.00
      table(9,10)= -3.00
      table(9,11)=  0.00
      table(9,12)=  1.00
      table(9,13)= -1.00
      table(9,14)=  1.00
      table(9,15)=  3.00
      table(9,16)=  0.00
      table(9,17)=  0.00
      table(9,18)= -2.00
      table(9,19)= -3.00
      table(9,20)= -4.00
C LEU
      table(10,1 )= -2.00
      table(10,2 )= -6.00
      table(10,3 )= -4.00
      table(10,4 )= -3.00
      table(10,5 )=  2.00
      table(10,6 )= -4.00
      table(10,7 )= -2.00
      table(10,8 )=  2.00
      table(10,9 )= -3.00
      table(10,10)=  6.00
      table(10,11)=  4.00
      table(10,12)= -3.00
      table(10,13)= -3.00
      table(10,14)= -2.00
      table(10,15)= -3.00
      table(10,16)= -3.00
      table(10,17)= -2.00
      table(10,18)=  2.00
      table(10,19)= -2.00
      table(10,20)= -1.00
C MET
      table(11,1 )= -1.00
      table(11,2 )= -5.00
      table(11,3 )= -3.00
      table(11,4 )= -2.00
      table(11,5 )=  0.00
      table(11,6 )= -3.00
      table(11,7 )= -2.00
      table(11,8 )=  2.00
      table(11,9 )=  0.00
      table(11,10)=  4.00
      table(11,11)=  6.00
      table(11,12)= -2.00
      table(11,13)= -2.00
      table(11,14)= -1.00
      table(11,15)=  0.00
      table(11,16)= -2.00
      table(11,17)= -1.00
      table(11,18)=  2.00
      table(11,19)= -4.00
      table(11,20)= -2.00
C ASN
      table(12,1 )=  0.00
      table(12,2 )= -4.00
      table(12,3 )=  2.00
      table(12,4 )=  1.00
      table(12,5 )= -4.00
      table(12,6 )=  0.00
      table(12,7 )=  2.00
      table(12,8 )= -2.00
      table(12,9 )=  1.00
      table(12,10)= -3.00
      table(12,11)= -2.00
      table(12,12)=  2.00
      table(12,13)= -1.00
      table(12,14)=  1.00
      table(12,15)=  0.00
      table(12,16)=  1.00
      table(12,17)=  0.00
      table(12,18)= -2.00
      table(12,19)= -4.00
      table(12,20)= -2.00
C PRO
      table(13,1 )=  1.00
      table(13,2 )= -3.00
      table(13,3 )= -1.00
      table(13,4 )= -1.00
      table(13,5 )= -5.00
      table(13,6 )= -1.00
      table(13,7 )=  0.00
      table(13,8 )= -2.00
      table(13,9 )= -1.00
      table(13,10)= -3.00
      table(13,11)= -2.00
      table(13,12)= -1.00
      table(13,13)=  6.00
      table(13,14)=  0.00
      table(13,15)=  0.00
      table(13,16)=  1.00
      table(13,17)=  0.00
      table(13,18)= -1.00
      table(13,19)= -6.00
      table(13,20)= -5.00
C GLN
      table(14,1 )=  0.00
      table(14,2 )= -5.00
      table(14,3 )=  2.00
      table(14,4 )=  2.00
      table(14,5 )= -5.00
      table(14,6 )= -1.00
      table(14,7 )=  3.00
      table(14,8 )= -2.00
      table(14,9 )=  1.00
      table(14,10)= -2.00
      table(14,11)= -1.00
      table(14,12)=  1.00
      table(14,13)=  0.00
      table(14,14)=  4.00
      table(14,15)=  1.00
      table(14,16)= -1.00
      table(14,17)= -1.00
      table(14,18)= -2.00
      table(14,19)= -5.00
      table(14,20)= -4.00
C ARG
      table(15,1 )= -2.00
      table(15,2 )= -4.00
      table(15,3 )= -1.00
      table(15,4 )= -1.00
      table(15,5 )= -4.00
      table(15,6 )= -3.00
      table(15,7 )=  2.00
      table(15,8 )= -2.00
      table(15,9 )=  3.00
      table(15,10)= -3.00
      table(15,11)=  0.00
      table(15,12)=  0.00
      table(15,13)=  0.00
      table(15,14)=  1.00
      table(15,15)=  6.00
      table(15,16)=  0.00
      table(15,17)= -1.00
      table(15,18)= -2.00
      table(15,19)=  2.00
      table(15,20)= -4.00
C SER
      table(16,1 )=  1.00
      table(16,2 )=  0.00
      table(16,3 )=  0.00
      table(16,4 )=  0.00
      table(16,5 )= -3.00
      table(16,6 )=  1.00
      table(16,7 )= -1.00
      table(16,8 )= -1.00
      table(16,9 )=  0.00
      table(16,10)= -3.00
      table(16,11)= -2.00
      table(16,12)=  1.00
      table(16,13)=  1.00
      table(16,14)= -1.00
      table(16,15)=  0.00
      table(16,16)=  2.00
      table(16,17)=  1.00
      table(16,18)= -1.00
      table(16,19)= -2.00
      table(16,20)= -3.00
C THR
      table(17,1 )=  1.00
      table(17,2 )= -2.00
      table(17,3 )=  0.00
      table(17,4 )=  0.00
      table(17,5 )= -3.00
      table(17,6 )=  0.00
      table(17,7 )= -1.00
      table(17,8 )=  0.00
      table(17,9 )=  0.00
      table(17,10)= -2.00
      table(17,11)= -1.00
      table(17,12)=  0.00
      table(17,13)=  0.00
      table(17,14)= -1.00
      table(17,15)= -1.00
      table(17,16)=  1.00
      table(17,17)=  3.00
      table(17,18)=  0.00
      table(17,19)= -5.00
      table(17,20)= -3.00
C VAL
      table(18,1 )=  0.00
      table(18,2 )= -2.00
      table(18,3 )= -2.00
      table(18,4 )= -2.00
      table(18,5 )= -1.00
      table(18,6 )= -1.00
      table(18,7 )= -2.00
      table(18,8 )=  4.00
      table(18,9 )= -2.00
      table(18,10)=  2.00
      table(18,11)=  2.00
      table(18,12)= -2.00
      table(18,13)= -1.00
      table(18,14)= -2.00
      table(18,15)= -2.00
      table(18,16)= -1.00
      table(18,17)=  0.00
      table(18,18)=  4.00
      table(18,19)= -6.00
      table(18,20)= -2.00
C TRP
      table(19,1 )= -6.00
      table(19,2 )= -8.00
      table(19,3 )= -7.00
      table(19,4 )= -7.00
      table(19,5 )=  0.00
      table(19,6 )= -7.00
      table(19,7 )= -3.00
      table(19,8 )= -5.00
      table(19,9 )= -3.00
      table(19,10)= -2.00
      table(19,11)= -4.00
      table(19,12)= -4.00
      table(19,13)= -6.00
      table(19,14)= -5.00
      table(19,15)=  2.00
      table(19,16)= -2.00
      table(19,17)= -5.00
      table(19,18)= -6.00
      table(19,19)= 17.00
      table(19,20)=  0.00
C TYR
      table(20,1 )= -3.00
      table(20,2 )=  0.00
      table(20,3 )= -4.00
      table(20,4 )= -4.00
      table(20,5 )=  7.00
      table(20,6 )= -5.00
      table(20,7 )=  0.00
      table(20,8 )= -1.00
      table(20,9 )= -4.00
      table(20,10)= -1.00
      table(20,11)= -2.00
      table(20,12)= -2.00
      table(20,13)= -5.00
      table(20,14)= -4.00
      table(20,15)= -4.00
      table(20,16)= -3.00
      table(20,17)= -3.00
      table(20,18)= -2.00
      table(20,19)=  0.00
      table(20,20)= 10.00
      end
      

*****************************************************************
      subroutine defaulttable_blosum62(table,tablesize)      
      implicit none
#include <dynprog.h>      
      integer tablesize
      real table(tablesize,tablesize)
c     write(*,*) 'No sequence table was found using the default BLOSUM62 MATRIX'
      
C ALA
      table(1,1 )=  4.00
      table(1,2 )=  0.00
      table(1,3 )= -2.00
      table(1,4 )= -1.00
      table(1,5 )= -2.00
      table(1,6 )=  0.00
      table(1,7 )= -2.00
      table(1,8 )= -1.00
      table(1,9 )= -1.00
      table(1,10)= -1.00
      table(1,11)= -1.00
      table(1,12)= -1.00
      table(1,13)= -2.00
      table(1,14)= -1.00
      table(1,15)= -1.00
      table(1,16)=  1.00
      table(1,17)= -1.00
      table(1,18)=  0.00
      table(1,19)= -3.00
      table(1,20)= -2.00
C CYS
      table(2,1 )=  0.00
      table(2,2 )=  9.00
      table(2,3 )= -3.00
      table(2,4 )= -4.00
      table(2,5 )= -2.00
      table(2,6 )= -3.00
      table(2,7 )= -3.00
      table(2,8 )= -1.00
      table(2,9 )= -3.00
      table(2,10)= -1.00
      table(2,11)= -1.00
      table(2,12)= -3.00
      table(2,13)= -3.00
      table(2,14)= -3.00
      table(2,15)= -3.00
      table(2,16)= -1.00
      table(2,17)= -1.00
      table(2,18)= -1.00
      table(2,19)= -2.00
      table(2,20)= -2.00
C ASP
      table(3,1 )= -2.00
      table(3,2 )= -3.00
      table(3,3 )=  6.00
      table(3,4 )=  2.00
      table(3,5 )= -3.00
      table(3,6 )= -1.00
      table(3,7 )= -1.00
      table(3,8 )= -3.00
      table(3,9 )= -1.00
      table(3,10)= -4.00
      table(3,11)= -3.00
      table(3,12)=  1.00
      table(3,13)= -1.00
      table(3,14)=  0.00
      table(3,15)= -2.00
      table(3,16)=  0.00
      table(3,17)= -1.00
      table(3,18)= -3.00
      table(3,19)= -4.00
      table(3,20)= -3.00
C GLU
      table(4,1 )= -1.00
      table(4,2 )= -4.00
      table(4,3 )=  2.00
      table(4,4 )=  5.00
      table(4,5 )= -3.00
      table(4,6 )= -2.00
      table(4,7 )=  0.00
      table(4,8 )= -3.00
      table(4,9 )=  1.00
      table(4,10)= -3.00
      table(4,11)= -2.00
      table(4,12)=  0.00
      table(4,13)= -1.00
      table(4,14)=  2.00
      table(4,15)=  0.00
      table(4,16)=  0.00
      table(4,17)= -1.00
      table(4,18)= -2.00
      table(4,19)= -3.00
      table(4,20)= -2.00
C PHE
      table(5,1 )= -2.00
      table(5,2 )= -2.00
      table(5,3 )= -3.00
      table(5,4 )= -3.00
      table(5,5 )=  6.00
      table(5,6 )= -3.00
      table(5,7 )= -1.00
      table(5,8 )=  0.00
      table(5,9 )= -3.00
      table(5,10)=  0.00
      table(5,11)=  0.00
      table(5,12)= -3.00
      table(5,13)= -4.00
      table(5,14)= -3.00
      table(5,15)= -3.00
      table(5,16)= -2.00
      table(5,17)= -2.00
      table(5,18)= -1.00
      table(5,19)=  1.00
      table(5,20)=  3.00
C GLY
      table(6,1 )=  0.00
      table(6,2 )= -3.00
      table(6,3 )= -1.00
      table(6,4 )= -2.00
      table(6,5 )= -3.00
      table(6,6 )=  6.00
      table(6,7 )= -2.00
      table(6,8 )= -4.00
      table(6,9 )= -2.00
      table(6,10)= -4.00
      table(6,11)= -3.00
      table(6,12)=  0.00
      table(6,13)= -2.00
      table(6,14)= -2.00
      table(6,15)= -2.00
      table(6,16)=  0.00
      table(6,17)= -2.00
      table(6,18)= -3.00
      table(6,19)= -2.00
      table(6,20)= -3.00
C HIS
      table(7,1 )= -2.00
      table(7,2 )= -3.00
      table(7,3 )= -1.00
      table(7,4 )=  0.00
      table(7,5 )= -1.00
      table(7,6 )= -2.00
      table(7,7 )=  8.00
      table(7,8 )= -3.00
      table(7,9 )= -1.00
      table(7,10)= -3.00
      table(7,11)= -2.00
      table(7,12)= -1.00
      table(7,13)= -2.00
      table(7,14)=  0.00
      table(7,15)=  0.00
      table(7,16)= -1.00
      table(7,17)= -2.00
      table(7,18)= -3.00
      table(7,19)= -2.00
      table(7,20)=  2.00
C ILE
      table(8,1 )= -1.00
      table(8,2 )= -1.00
      table(8,3 )= -3.00
      table(8,4 )= -3.00
      table(8,5 )=  0.00
      table(8,6 )= -4.00
      table(8,7 )= -3.00
      table(8,8 )=  4.00
      table(8,9 )= -3.00
      table(8,10)=  2.00
      table(8,11)=  1.00
      table(8,12)= -3.00
      table(8,13)= -3.00
      table(8,14)= -3.00
      table(8,15)= -3.00
      table(8,16)= -2.00
      table(8,17)= -1.00
      table(8,18)=  3.00
      table(8,19)= -3.00
      table(8,20)= -1.00
C LYS
      table(9,1 )= -1.00
      table(9,2 )= -3.00
      table(9,3 )= -1.00
      table(9,4 )=  1.00
      table(9,5 )= -3.00
      table(9,6 )= -2.00
      table(9,7 )= -1.00
      table(9,8 )= -3.00
      table(9,9 )=  5.00
      table(9,10)= -2.00
      table(9,11)= -1.00
      table(9,12)=  0.00
      table(9,13)= -1.00
      table(9,14)=  1.00
      table(9,15)=  2.00
      table(9,16)=  0.00
      table(9,17)= -1.00
      table(9,18)= -2.00
      table(9,19)= -3.00
      table(9,20)= -2.00
C LEU
      table(10,1 )= -1.00
      table(10,2 )= -1.00
      table(10,3 )= -4.00
      table(10,4 )= -3.00
      table(10,5 )=  0.00
      table(10,6 )= -4.00
      table(10,7 )= -3.00
      table(10,8 )=  2.00
      table(10,9 )= -2.00
      table(10,10)=  4.00
      table(10,11)=  2.00
      table(10,12)= -3.00
      table(10,13)= -3.00
      table(10,14)= -2.00
      table(10,15)= -2.00
      table(10,16)= -2.00
      table(10,17)= -1.00
      table(10,18)=  1.00
      table(10,19)= -2.00
      table(10,20)= -1.00
C MET
      table(11,1 )= -1.00
      table(11,2 )= -1.00
      table(11,3 )= -3.00
      table(11,4 )= -2.00
      table(11,5 )=  0.00
      table(11,6 )= -3.00
      table(11,7 )= -2.00
      table(11,8 )=  1.00
      table(11,9 )= -1.00
      table(11,10)=  2.00
      table(11,11)=  5.00
      table(11,12)= -2.00
      table(11,13)= -2.00
      table(11,14)=  0.00
      table(11,15)= -1.00
      table(11,16)= -1.00
      table(11,17)= -1.00
      table(11,18)=  1.00
      table(11,19)= -1.00
      table(11,20)= -1.00
C ASN
      table(12,1 )= -2.00
      table(12,2 )= -3.00
      table(12,3 )=  1.00
      table(12,4 )=  0.00
      table(12,5 )= -3.00
      table(12,6 )=  0.00
      table(12,7 )=  1.00
      table(12,8 )= -3.00
      table(12,9 )=  0.00
      table(12,10)= -3.00
      table(12,11)= -2.00
      table(12,12)=  6.00
      table(12,13)= -2.00
      table(12,14)=  0.00
      table(12,15)=  0.00
      table(12,16)=  1.00
      table(12,17)=  0.00
      table(12,18)= -3.00
      table(12,19)= -4.00
      table(12,20)= -2.00
C PRO
      table(13,1 )= -1.00
      table(13,2 )= -3.00
      table(13,3 )= -1.00
      table(13,4 )= -1.00
      table(13,5 )= -4.00
      table(13,6 )= -2.00
      table(13,7 )= -2.00
      table(13,8 )= -3.00
      table(13,9 )= -1.00
      table(13,10)= -3.00
      table(13,11)= -2.00
      table(13,12)= -2.00
      table(13,13)=  7.00
      table(13,14)= -1.00
      table(13,15)= -2.00
      table(13,16)= -1.00
      table(13,17)= -1.00
      table(13,18)= -2.00
      table(13,19)= -4.00
      table(13,20)= -3.00
C GLN
      table(14,1 )= -1.00
      table(14,2 )= -3.00
      table(14,3 )=  0.00
      table(14,4 )=  2.00
      table(14,5 )= -3.00
      table(14,6 )= -2.00
      table(14,7 )=  0.00
      table(14,8 )= -3.00
      table(14,9 )=  1.00
      table(14,10)= -2.00
      table(14,11)=  0.00
      table(14,12)=  0.00
      table(14,13)= -1.00
      table(14,14)=  5.00
      table(14,15)=  1.00
      table(14,16)=  0.00
      table(14,17)= -1.00
      table(14,18)= -2.00
      table(14,19)= -2.00
      table(14,20)= -1.00
C ARG
      table(15,1 )= -1.00
      table(15,2 )= -3.00
      table(15,3 )= -2.00
      table(15,4 )=  0.00
      table(15,5 )= -3.00
      table(15,6 )= -2.00
      table(15,7 )=  0.00
      table(15,8 )= -3.00
      table(15,9 )=  2.00
      table(15,10)= -2.00
      table(15,11)= -1.00
      table(15,12)=  0.00
      table(15,13)=  2.00
      table(15,14)=  1.00
      table(15,15)=  5.00
      table(15,16)= -1.00
      table(15,17)= -1.00
      table(15,18)= -3.00
      table(15,19)= -3.00
      table(15,20)= -2.00
C SER
      table(16,1 )=  1.00
      table(16,2 )= -1.00
      table(16,3 )=  0.00
      table(16,4 )=  0.00
      table(16,5 )= -2.00
      table(16,6 )=  0.00
      table(16,7 )= -1.00
      table(16,8 )= -2.00
      table(16,9 )=  0.00
      table(16,10)= -2.00
      table(16,11)= -1.00
      table(16,12)=  1.00
      table(16,13)= -1.00
      table(16,14)=  0.00
      table(16,15)= -1.00
      table(16,16)=  4.00
      table(16,17)=  1.00
      table(16,18)= -2.00
      table(16,19)= -3.00
      table(16,20)= -2.00
C THR
      table(17,1 )=  0.00
      table(17,2 )= -1.00
      table(17,3 )=  1.00
      table(17,4 )= -1.00
      table(17,5 )= -2.00
      table(17,6 )= -2.00
      table(17,7 )= -2.00
      table(17,8 )= -1.00
      table(17,9 )= -1.00
      table(17,10)= -1.00
      table(17,11)= -1.00
      table(17,12)=  0.00
      table(17,13)= -1.00
      table(17,14)= -1.00
      table(17,15)= -1.00
      table(17,16)=  1.00
      table(17,17)=  5.00
      table(17,18)=  0.00
      table(17,19)= -2.00
      table(17,20)= -2.00
C VAL
      table(18,1 )=  0.00
      table(18,2 )= -1.00
      table(18,3 )= -3.00
      table(18,4 )= -2.00
      table(18,5 )= -1.00
      table(18,6 )= -3.00
      table(18,7 )= -3.00
      table(18,8 )=  3.00
      table(18,9 )= -2.00
      table(18,10)=  1.00
      table(18,11)=  1.00
      table(18,12)= -3.00
      table(18,13)= -2.00
      table(18,14)= -2.00
      table(18,15)= -3.00
      table(18,16)= -2.00
      table(18,17)=  0.00
      table(18,18)=  4.00
      table(18,19)= -3.00
      table(18,20)= -1.00
C TRP
      table(19,1 )= -3.00
      table(19,2 )= -2.00
      table(19,3 )= -4.00
      table(19,4 )= -3.00
      table(19,5 )=  1.00
      table(19,6 )= -2.00
      table(19,7 )= -2.00
      table(19,8 )= -3.00
      table(19,9 )= -3.00
      table(19,10)= -2.00
      table(19,11)= -1.00
      table(19,12)= -4.00
      table(19,13)= -4.00
      table(19,14)= -2.00
      table(19,15)= -3.00
      table(19,16)= -3.00
      table(19,17)= -2.00
      table(19,18)= -3.00
      table(19,19)= 11.00
      table(19,20)=  2.00
C TYR
      table(20,1 )= -2.00
      table(20,2 )= -2.00
      table(20,3 )= -3.00
      table(20,4 )= -2.00
      table(20,5 )=  3.00
      table(20,6 )= -3.00
      table(20,7 )=  2.00
      table(20,8 )= -1.00
      table(20,9 )= -2.00
      table(20,10)= -1.00
      table(20,11)= -1.00
      table(20,12)= -2.00
      table(20,13)= -3.00
      table(20,14)= -1.00
      table(20,15)= -2.00
      table(20,16)= -2.00
      table(20,17)= -2.00
      table(20,18)= -1.00
      table(20,19)=  2.00
      table(20,20)=  7.00
      end 
      
*****************************************************************
      subroutine writeseqtable(table,tablesize)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer i,j,tablesize
      real table(tablesize,tablesize)
      character*3 word
      
      do i=1,20
        call numtonam(i,word)
        write(*,'(A3,1x,20(f5.2,1x))')  word,(table(i,j),j=1,20)
      end do
      return
      end

*****************************************************************
      subroutine writeprofile(file,profile,seq,seqlen,aa)
C     
C     write scoring table
C     
      implicit none
#include <dynprog.h>      
      integer i,j,seqlen,aa,seq(*)
      real profile(maxproflen,maxaa+2)
      character*3 word
      character*(*) file
      integer filnum
      parameter (filnum=90)
      Open(unit=filnum,file=file,Form='FORMATTED',status ='UNKNOWN',
     &     access='SEQUENTIAL')
      do i=1,seqlen
        call numtonam(seq(i),word)
        write(filnum,'(A3,1x,20(f8.3,1x)),2x,20(f8.3,1x)),2x,20(f8.3,1x)),2x,2(f8.3,1x))')
     &       word,(profile(i,j),j=1,aa),profile(i,maxaa+1),profile(i,maxaa+2)
      end do
      close(unit=filnum)
      return
      end


*****************************************************************
      subroutine readprofile(file,profile,seq,seqlen,aa)
C     
C     write scoring table
C     
      implicit none
#include <dynprog.h>      
      integer i,j,seqlen,aa,seq(*)
      real profile(maxproflen,maxaa+2)
      character*3 word
      character*(*) file
      integer filnum
      parameter (filnum=90)    
      Open(unit=filnum,file=file,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      do i=1,seqlen
        call numtonam(seq(i),word)
        read(filnum,'(A3,1x,20(f8.3,1x),2x,20(f8.3,1x),2x,20(f8.3,1x))',err=777,end=777)
     &       word,(profile(i,j),j=1,aa),profile(i,maxaa+1),profile(i,maxaa+2)
      end do
      return
 777  continue
      call stopprocess('ERROR - Profile not found')
      end

*****************************************************************
C      subroutine readnn(file)
CC     
CC     read Artificial Neural Network
CC     
C      implicit none
C#include <dynprog.h>      
C#include <seqio.h>      
C      integer i,j
C      character*(*) file
C      character*80 temp
C      integer filnum
C      parameter (filnum=90)   
C
C      Open(unit=filnum,file=file,form='FORMATTED',status='OLD',
C     &     access='SEQUENTIAL',err=777)
C      
C      read(filnum,*,err=777,end=777) temp,nlayers
C      read(filnum,*,err=777,end=777) temp,nin
C      read(filnum,*,err=777,end=777) temp,nhidden
C      read(filnum,*,err=777,end=777) temp,nout
C      read(filnum,*,err=777,end=777) temp,nwts
C      read(filnum,*,err=777,end=777) temp
C      
C      do i=1,nin
C         read(filnum,*,err=777,end=777) (w1(i,j),j=1,nhidden)
C      end do
C      read(filnum,*,err=777,end=777) temp
C      do i=1,nout
C         read(filnum,*,err=777,end=777) (b1(j,nout),j=1,nhidden)
C      end do
C      read(filnum,*,err=777,end=777) temp
C      do i=1,nhidden
C         read(filnum,*,err=777,end=777) (w2(i,j),j=1,nout)
C         
C      end do
C      read(filnum,*,err=777,end=777) temp
C      read(filnum,*,err=777,end=777) (b2(j),j=1,nout)
C 300  continue
C      close(unit=filnum)
Cc      write(*,*)'io',w1(1,1:nhidden),'->',w1(1:nhidden,1),nin,nhidden,nout
C      
C      return
C 777  continue
C      call stopprocess('ERROR - No network found')
C      end
C
      subroutine readnn(file)
C     
C     read Artificial Neural Network
C     
      implicit none
#include <dynprog.h>      
#include <seqio.h>      
      integer i,j,n
      character*(*) file
      character*80 temp,foo
      integer filnum
      integer nets
      integer bar
      parameter (filnum=90)    
      Open(unit=filnum,file=file,form='FORMATTED',status='OLD',
     &     access='SEQUENTIAL',err=777)
      
      read(filnum,*,err=777,end=777) temp,nets
      if(temp .eq. "nlayers")then
         nlayers=nets
         nets=1
         read(filnum,*,err=777,end=777) temp,nin
         read(filnum,*,err=777,end=777) temp,nhidden
         read(filnum,*,err=777,end=777) temp,nout
         read(filnum,*,err=777,end=777) temp,nwts
         read(filnum,*,err=777,end=777) temp
      else
         read(filnum,*,err=777,end=777) temp,nlayers
         read(filnum,*,err=777,end=777) temp,nin
         read(filnum,*,err=777,end=777) temp,nhidden
         read(filnum,*,err=777,end=777) temp,nout
         read(filnum,*,err=777,end=777) temp,nwts
         read(filnum,*,err=777,end=777) temp
      endif

      do n=1,nets
         if(n .gt.1)then
            read(filnum,*,err=777,end=777) temp,foo,bar
            read(filnum,*,err=777,end=777) temp,bar
            read(filnum,*,err=777,end=777) temp,bar
            read(filnum,*,err=777,end=777) temp,bar
            read(filnum,*,err=777,end=777) temp,bar
            read(filnum,*,err=777,end=777) temp

         endif

         do i=1,nin
            read(filnum,*,err=777,end=777) (w1(i+(n-1)*nin,j),j=1,nhidden)
         end do
         read(filnum,*,err=777,end=777) temp
         do i=1,nout
            read(filnum,*,err=777,end=777) (b1(j,nout+(n-1)*nout),j=1,nhidden)
         end do
         read(filnum,*,err=777,end=777) temp
         do i=1,nhidden
            read(filnum,*,err=777,end=777) (w2(i+(n-1)*nhidden,j),j=1,nout)
            
         end do
         read(filnum,*,err=777,end=777) temp
         read(filnum,*,err=777,end=777) (b2(j+(n-1)*nout),j=1,nout)

      enddo
      write(*,*)'nets',nets
 300  continue
      close(unit=filnum)
      
      return
 777  continue
      call stopprocess('ERROR - No network found')
      end
      
*****************************************************************
      subroutine writealfile(file,seq,seqlen,profseq,profname,trace,resnum,profresnum)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer i,seqlen,seq(*),profseq(*),trace(*),filnum
      character*4 resnum(*),profresnum(*)
      integer seqtoseq,numaa,index2
      character*1 char1,char2
      character*(*) profname
      character*(*) file
      external seqtoseq
      parameter (filnum=90)
      numaa=20

      Open(unit=filnum,file=file,Form='FORMATTED',status ='UNKNOWN',
     &     access='SEQUENTIAL')

      write(filnum,'(A)') "PFRMAT AL"
      index2=index(profname,' ')
      write(filnum,'(A,A)') "PARENT ",profname(1:index2)
      do i=1,seqlen
        call numtochar(seqtoseq(seq(i),numaa),char1)
        if (seq(i) .eq. 0) char1='X'
        if (trace(i).gt.0) then
          call numtochar(seqtoseq(profseq(trace(i)),numaa),char2)
          if (profseq(trace(i)) .eq. 0) char2='X'
        else
          char2='.'
        end if
        if (trace(i).gt.0) then
C          write(*,*)'test',i,seq(i),trace(i),resnum(trace(i)),profseq(trace(i))
          write(filnum,('(3x,a,4x,a4,6x,a,1x,i7)'))char1,resnum(i),char2,trace(i)
C          else
C            write(*,*)'trace> ',i,' ',char1,'   -   '
        end if
      end do
      write(filnum,'(A)') "TER"
      write(filnum,'(A)') "END"
      close(unit=filnum)
      return
 777  continue
      call stopprocess('ERROR - writing AL file')
      end


*****************************************************************
      subroutine writepdbfile(file,seq,seqlen,trace,x,y,z,resnum)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer i,seqlen,seq(*),trace(*),filnum
      character*4 resnum(*)
      integer seqtoseq,numaa
      character*3 resnam
      character*(*) file
      external seqtoseq
      real x(*),y(*),z(*)
      parameter (filnum=90)
      numaa=20
      Open(unit=filnum,file=file,Form='FORMATTED',status ='UNKNOWN',
     &     access='SEQUENTIAL',err=777)


      do i=1,seqlen
        call numtonam(seqtoseq(seq(i),numaa),resnam)
        if (trace(i).gt.0) then
 666      write(filnum,111) i,resnam,resnum(i),x(trace(i)),y(trace(i)),z(trace(i))
        end if
      end do
      close(unit=filnum)
      return
 777  continue
      call stopprocess('ERROR - writing PDB file')
 111  format('ATOM',3x,i4,2x,'CA',2x,a3,2x,a4,4x,3f8.3)
      end


*****************************************************************
      subroutine copyseq(seq,tempseq,len)
C     
C     read scoring table
C     
      implicit none
      integer i,len,seq(*),tempseq(*)

      do i=1,len
        tempseq(i)=seq(i)
      end do
      return
      end
*****************************************************************
      subroutine copyprof(prof,tempprof,len)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>      
      integer i,j,len,prof(maxproflen,maxaa+2),tempprof(maxproflen,maxaa+2)

      do i=1,len
        do j=1,maxaa+2
          tempprof(i,j)=prof(i,j)
        end do
      end do
      return
      end
*****************************************************************
      subroutine randomizeseq(seq,len)
C     
C     read scoring table
C     
      implicit none
      integer i,len,seq(*),j
      real ran,rand,r

      do i=1,len
#ifdef IFC
        j=1+len*rand(1)
#else
         CALL RANDOM_NUMBER(r)
         j=1+len*r
#endif
        seq(i)=seq(j)
      end do
      return
      end
*****************************************************************
      subroutine randomizeprofile(profile,len,numaa)
C     
C     read scoring table
C     
      implicit none
#include <dynprog.h>
      integer i,len,j,k,numaa
      real ran,rand,r,profile(maxproflen,maxaa+2)
      integer*4 timeArray(3) 

      do i=1,len
         call itime(timeArray)  ! Get the current time
#ifdef IFC
c         write(*,*)'foo',rand(),5*rand()
c         j=1+len*rand()
         r = rand ( timeArray(1)+timeArray(2)+timeArray(3) )
         j=1+len*rand(r)
#else
         CALL RANDOM_NUMBER(r)
         j=1+len*r
#endif
        do k=1,numaa
          profile(i,k)=profile(j,k)
        end do
      end do
      return
      end
