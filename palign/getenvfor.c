#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#ifdef SGI
void getenvfor_(char *result, int rlen, char *arg, int arglen)
#elif SUN
void getenvfor_( result, arg, rlen, arglen)
#else
void getenvfor_(char *result, char *arg, int rlen, int arglen)
#endif
{
#ifdef SUN
  /* char *results;
     char *arg; 
     int rlen; 
     int arglen; */
#endif
char *cp;
char *s;
for (cp = result; cp < result+rlen; ++cp) *cp = '\0';
for (cp = arg; *cp != '\0' && !isspace(*cp); ++cp) ;
*cp = '\0';
s = getenv(arg);
#if 0
printf("arg = %s, arglen = %d\n",arg,arglen);
printf("result = %s\n",s==NULL?"NULL":s);
#endif
if (s == NULL) {
        /*      strcpy(result,arg);     */
		memset(result,0,rlen);
        rlen= arglen;
        }
else {
        strcpy(result,s);
        rlen = strlen(result);
        }
}
